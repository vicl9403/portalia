// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'client/bower_components/jquery/dist/jquery.js',
      'client/bower_components/angular/angular.js',
      'client/bower_components/angular-resource/angular-resource.js',
      'client/bower_components/angular-cookies/angular-cookies.js',
      'client/bower_components/angular-sanitize/angular-sanitize.js',
      'client/bower_components/angular-route/angular-route.js',
      'client/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'client/bower_components/lodash/dist/lodash.compat.js',
      'client/bower_components/ng-file-upload/ng-file-upload.js',
      'client/bower_components/highlightjs/highlight.pack.js',
      'client/bower_components/rltm/web/rltm.js',
      'client/bower_components/angular-chat/angular-chat.js',
      'client/bower_components/jquery.countdown/dist/jquery.countdown.js',
      'client/bower_components/angular-elastic/elastic.js',
      'client/bower_components/bootstrap-offcanvas/dist/js/bootstrap.offcanvas.js',
      'client/bower_components/moment/moment.js',
      'client/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
      'client/bower_components/angular-google-chart/ng-google-chart.js',
      'client/bower_components/webcomponentsjs/webcomponents.js',
      'client/bower_components/promise-polyfill/Promise.js',
      'client/bower_components/angular-recaptcha/release/angular-recaptcha.js',
      'client/bower_components/isInViewport/lib/isInViewport.min.js',
      'client/bower_components/ngFitText/dist/ng-FitText.min.js',
      'client/bower_components/angular-socket-io/socket.js',
      'client/bower_components/angular-update-meta/dist/update-meta.js',
      'client/bower_components/pdfmake/build/pdfmake.js',
      'client/bower_components/pdfmake/build/vfs_fonts.js',
      'client/bower_components/fullcalendar/dist/fullcalendar.js',
      'client/bower_components/sweetalert/dist/sweetalert.min.js',
      'client/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js',
      'client/bower_components/angular-mocks/angular-mocks.js',
      // endbower
      'client/app/app.js',
      'client/{app,components}/**/*.module.js',
      'client/{app,components}/**/*.js',
      'client/{app,components}/**/*.html'
    ],

    preprocessors: {
      '**/*.html': 'ng-html2js',
      'client/{app,components}/**/*.js': 'babel'
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'client/'
    },

    babelPreprocessor: {
      options: {
        sourceMap: 'inline'
      },
      filename: function(file) {
        return file.originalPath.replace(/\.js$/, '.es5.js');
      },
      sourceFileName: function(file) {
        return file.originalPath;
      }
    },

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // reporter types:
    // - dots
    // - progress (default)
    // - spec (karma-spec-reporter)
    // - junit
    // - growl
    // - coverage
    reporters: ['spec'],

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
