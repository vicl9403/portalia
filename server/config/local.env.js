'use strict';

// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  PORT: '9000',
  DOMAIN: 'http://localhost',
  SESSION_SECRET: 'portalia-secret',

  /**********************************************
   * 0 -> Global                                 *
   * 1 -> LMX                                    *
   * 2 -> PTP                                    *
   ***********************************************/

  PROJECT_TYPE: '0',
  //KEY y ID de la API de paypal
  PAYPAL_ID: '',
  PAYPAL_KEY: '',
  PAYPAL_URL: 'https://api.sandbox.paypal.com/',

  //KEY y ID de la API  de openpay
  OPENPAY_ID: 'mkx9erv2qm0efa14gvvz',
  OPENPAY_KEY: 'sk_29f6320a9c734292a044d861885c8385',
  OPENPAY_URL: 'https://dashboard.openpay.mx', //sandbox mode

  // OPENPAY_URL:'https://dashboard.openpay.mx', //producction mode

  FACEBOOK_ID: '542917605891843',
  FACEBOOK_SECRET: '40ed0b2242aa142edaf8d1c157758ef5',

  // Control debug level for modules using visionmedia/debug
  DEBUG: '',
  NODE_ENV: 'development' //cambiar en producction
};
