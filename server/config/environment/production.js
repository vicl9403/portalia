'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP ||
    process.env.IP ||
    undefined,

  // Server port
  port: 9000,

  plans: {
    Semestral: {
      Mensual: {
        ID: 'pdjgf081nnb9rx2i8wos',
        plan: 'select'
      },
      Trimestral: {
        ID: 'pjdhqft82m8hpebadlvt',
        plan: 'select'
      }
    },
    Anual: {
      Mensual: {
        ID: 'pdze3iycsx66lulrodns',
        plan: 'select'
      },
      Trimestral: {
        ID: 'pkcbn1z7ylvzfspl0o3a',
        plan: 'select'
      },
      Semestral: {
        ID: 'ppex0zhqakrk7yqhji7s',
        plan: 'select'
      }
    }
  },

  mandrillApiKey : '6DmLhODPtO7J9MkR3tZUpQ',

  algoliaconfig : {
    idApplication : 'Z0PK4O90D9',
    adminKey : 'd6d4a86131024e99dfda7df12d3e98e7',
    searchOnlyKey: 'cee882aa6ab6753d86530f537afe5e87'
  },

  openpay : {
    id : 'mkx9erv2qm0efa14gvvz',
    private : 'sk_29f6320a9c734292a044d861885c8385',
    public: 'pk_b8c80547e64b415c80287b5ff935d19f',
    productionMode: true,
    setSandboxMode: false
  },

  // MongoDB connection options
  mongo: {
    uri: process.env.MONGOLAB_URI ||
      process.env.MONGOHQ_URL ||
      process.env.OPENSHIFT_MONGODB_DB_URL +
      process.env.OPENSHIFT_APP_NAME ||
      'mongodb://portalia:Qwerty123PortaliaPlus@184.171.253.98:27017/portalia'
  },

  auth: {
    paypal: {
      ID: process.env.PAYPAL_ID || '',
      KEY: process.env.PAYPAL_KEY || '',
      URL: process.env.PAYPAL_URL || ''
    },
    openpay: {
      ID: process.env.OPENPAY_ID || 'mkx9erv2qm0efa14gvvz',
      KEY: process.env.OPENPAY_KEY || 'sk_29f6320a9c734292a044d861885c8385',
      URL: process.env.OPENPAY_URL || 'https://dashboard.openpay.mx',
      URL: process.env.PRODUCTION || true
    }
  },

  facebook: {
    clientID: process.env.FACEBOOK_ID || '542917605891843',
    clientSecret: process.env.FACEBOOK_SECRET || '40ed0b2242aa142edaf8d1c157758ef5',
    callbackURL: 'https://portaliaplus.com/auth/facebook/callback'
  }
};
