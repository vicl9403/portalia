'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/portalia-dev'
  },
  port: 9000,

  // Seed database on startup
  seedDB: false,

  mandrillApiKey : '3wwOd0vNNOQJrPNIZ7qrnQ',
  // mandrillApiKey : '6DmLhODPtO7J9MkR3tZUpQ',

  algoliaconfig : {
    idApplication : 'TLTIO1USTU',
    adminKey : '8948ce8d428c138f7e4a89f19dccd032',
    searchOnlyKey: '70491295560d1ccc04b7a106ed305c86'
  },

  openpay : {
    id : 'mkfcjhndcb0q5r4frogm',
    private : 'sk_fabe66e519904f3c9af97137d82b821f',
    public: 'pk_3e3b95ccc7c1457082695fc834dfb01d',
    productionMode: false,
    setSandboxMode: true
  },

  plans: {
    Semestral: {
      Mensual: {
        ID: 'p4s7ocbbwk8fkwagja3l',
        plan: 'select'
      },
      Trimestral: {
        ID: 'pngc7ee8ago5g3edin7k',
        plan: 'select'
      }
    },
    Anual: {
      Mensual: {
        ID: 'p6lqtmrqssnjjt4oniue',
        plan: 'select'
      },
      Trimestral: {
        ID: 'pa3lmabgztuqnk4cy4ga',
        plan: 'select'
      },
      Semestral: {
        ID: 'pfxu8n1x3ukspwjo8lm0',
        plan: 'select'
      }
    }
  },

  auth: {
    paypal: {
      ID: process.env.PAYPAL_ID || '',
      KEY: process.env.PAYPAL_KEY || '',
      URL: process.env.PAYPAL_URL || ''
    },
    openpay: {
      ID: process.env.OPENPAY_ID || 'my1qymxtirofw0vduzb4',
      KEY: process.env.OPENPAY_KEY || 'sk_167b1a05bea74b30972782c02adbe275',
      URL: process.env.OPENPAY_URL || 'https://sandbox-dashboard.openpay.mx',
      URL: process.env.PRODUCTION || false
    }
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || '542917605891843',
    clientSecret: process.env.FACEBOOK_SECRET || '40ed0b2242aa142edaf8d1c157758ef5',
    callbackURL: 'http://portalia.it4pymes.mx:9000/auth/facebook/callback'
  }
};
