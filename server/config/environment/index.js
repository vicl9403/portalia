'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT || 9000,

  // Server IP
  ip: process.env.IP || '0.0.0.0',

  // Server Domain
  domain: process.env.DOMAIN || 'http://localhost:' + process.env.PORT,

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'portalia-secret'
  },

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true
      }
    }
  },
  project: {
    Type: process.env.PROJECT_TYPE || '0'
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'id',
    clientSecret: process.env.FACEBOOK_SECRET || 'secret',
    callbackURL: 'http://localhost:9000/auth/facebook/callback'
  },
  auth: {
    paypal: {
      ID: process.env.PAYPAL_ID || '',
      KEY: process.env.PAYPAL_KEY || '',
      URL: process.env.PAYPAL_URL || ''
    },
    openpay: {
      ID: process.env.OPENPAY_ID || '',
      KEY: process.env.OPENPAY_KEY || '',
      URL: process.env.OPENPAY_URL || ''
    }
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./shared'),
  require('./' + process.env.NODE_ENV + '.js') || {});
