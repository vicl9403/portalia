/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Sector from '../api/sector/sector.model';
import Chat from '../api/chat/chat.model';
import Estado from '../api/estado/estado.model';
import Plan from '../api/plan/plan.model';
import UpdatesOnUser from '../api/updatesOnUser/updatesOnUser.model'
