/**
 * Main application file
 */

'use strict';

import express from 'express';
import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import config from './config/environment';
import http from 'http';

const fileUpload = require('express-fileupload');

// Connect to MongoDB
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function(err) {
  console.error('MongoDB connection error: ' + err);
  process.exit(-1);
});

// Populate databases with sample data
if (config.seedDB) {
  require('./config/seed');
}

var bodyParser = require('body-parser');

// Setup server
var app = express();

var server = http.createServer(app);

app.use( fileUpload() );

var jsonParser       = bodyParser.json({limit:1024*1024*20, type:'application/json'});
var urlencodedParser = bodyParser.urlencoded({ extended:true,limit:1024*1024*20,type:'application/x-www-form-urlencoding' })


app.use(jsonParser);
app.use(urlencodedParser);


var io = require('socket.io')(server);

// Establecer conecciones para el socket del chat
io.on('connection', function(socket){

  // Chats privados de cada par de usuarios
  socket.on('chat_message', function( obj ) {
    var id = obj.id;
    io.emit('chat_message_' + id , obj );
  });

  // Generación de un nuevo chat
  socket.on('chat_created', function( obj ) {
    // console.log( obj.id );
    io.emit('chat_created' + obj.id );
  });

});

require('./config/express').default(app);
require('./routes').default(app);

// Start server
function startServer() {
  app.angularFullstack = server.listen(config.port, config.ip, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
  });
}

setImmediate(startServer);

// Expose app
exports = module.exports = app;
