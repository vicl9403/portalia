/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/commonRegisters              ->  index
 * POST    /api/commonRegisters              ->  create
 * GET     /api/commonRegisters/:id          ->  show
 * PUT     /api/commonRegisters/:id          ->  update
 * DELETE  /api/commonRegisters/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import CommonRegister from './commonRegister.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function incrementCount() {
    return function(entity) {
        entity.count = entity.count + 1;
        return entity.save()
            .then(updated => {
                return updated;
            });
    };
}


function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of CommonRegisters
export function index(req, res) {
  return CommonRegister.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single CommonRegister from the DB
export function show(req, res) {
  return CommonRegister.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new CommonRegister in the DB
export function create(req, res) {
  return CommonRegister.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing CommonRegister in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return CommonRegister.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing CommonRegister in the DB
export function updateCount(req, res) {
    return CommonRegister.findById(req.params.id).exec()
        .then(handleEntityNotFound(res))
        .then(incrementCount())
        .then(respondWithResult(res))
        .catch(handleError(res));
}


// Deletes a CommonRegister from the DB
export function destroy(req, res) {
  return CommonRegister.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
