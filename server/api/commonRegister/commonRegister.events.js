/**
 * CommonRegister model events
 */

'use strict';

import {EventEmitter} from 'events';
import CommonRegister from './commonRegister.model';
var CommonRegisterEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CommonRegisterEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  CommonRegister.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CommonRegisterEvents.emit(event + ':' + doc._id, doc);
    CommonRegisterEvents.emit(event, doc);
  }
}

export default CommonRegisterEvents;
