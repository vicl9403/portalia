'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var commonRegisterCtrlStub = {
  index: 'commonRegisterCtrl.index',
  show: 'commonRegisterCtrl.show',
  create: 'commonRegisterCtrl.create',
  update: 'commonRegisterCtrl.update',
  destroy: 'commonRegisterCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var commonRegisterIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './commonRegister.controller': commonRegisterCtrlStub
});

describe('CommonRegister API Router:', function() {

  it('should return an express router instance', function() {
    commonRegisterIndex.should.equal(routerStub);
  });

  describe('GET /api/commonRegisters', function() {

    it('should route to commonRegister.controller.index', function() {
      routerStub.get
        .withArgs('/', 'commonRegisterCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/commonRegisters/:id', function() {

    it('should route to commonRegister.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'commonRegisterCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/commonRegisters', function() {

    it('should route to commonRegister.controller.create', function() {
      routerStub.post
        .withArgs('/', 'commonRegisterCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/commonRegisters/:id', function() {

    it('should route to commonRegister.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'commonRegisterCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/commonRegisters/:id', function() {

    it('should route to commonRegister.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'commonRegisterCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/commonRegisters/:id', function() {

    it('should route to commonRegister.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'commonRegisterCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
