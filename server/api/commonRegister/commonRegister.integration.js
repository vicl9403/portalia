'use strict';

var app = require('../..');
import request from 'supertest';

var newCommonRegister;

describe('CommonRegister API:', function() {

  describe('GET /api/commonRegisters', function() {
    var commonRegisters;

    beforeEach(function(done) {
      request(app)
        .get('/api/commonRegisters')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          commonRegisters = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      commonRegisters.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/commonRegisters', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/commonRegisters')
        .send({
          name: 'New CommonRegister',
          info: 'This is the brand new commonRegister!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newCommonRegister = res.body;
          done();
        });
    });

    it('should respond with the newly created commonRegister', function() {
      newCommonRegister.name.should.equal('New CommonRegister');
      newCommonRegister.info.should.equal('This is the brand new commonRegister!!!');
    });

  });

  describe('GET /api/commonRegisters/:id', function() {
    var commonRegister;

    beforeEach(function(done) {
      request(app)
        .get('/api/commonRegisters/' + newCommonRegister._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          commonRegister = res.body;
          done();
        });
    });

    afterEach(function() {
      commonRegister = {};
    });

    it('should respond with the requested commonRegister', function() {
      commonRegister.name.should.equal('New CommonRegister');
      commonRegister.info.should.equal('This is the brand new commonRegister!!!');
    });

  });

  describe('PUT /api/commonRegisters/:id', function() {
    var updatedCommonRegister;

    beforeEach(function(done) {
      request(app)
        .put('/api/commonRegisters/' + newCommonRegister._id)
        .send({
          name: 'Updated CommonRegister',
          info: 'This is the updated commonRegister!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCommonRegister = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCommonRegister = {};
    });

    it('should respond with the updated commonRegister', function() {
      updatedCommonRegister.name.should.equal('Updated CommonRegister');
      updatedCommonRegister.info.should.equal('This is the updated commonRegister!!!');
    });

  });

  describe('DELETE /api/commonRegisters/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/commonRegisters/' + newCommonRegister._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when commonRegister does not exist', function(done) {
      request(app)
        .delete('/api/commonRegisters/' + newCommonRegister._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
