'use strict';

import mongoose from 'mongoose';

var CommonRegisterSchema = new mongoose.Schema({
  name: { type: String, unique: true },
  count: Number
});

export default mongoose.model('CommonRegister', CommonRegisterSchema);
