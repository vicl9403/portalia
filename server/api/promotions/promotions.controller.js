/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/promotions              ->  index
 * POST    /api/promotions              ->  create
 * GET     /api/promotions/:id          ->  show
 * PUT     /api/promotions/:id          ->  update
 * DELETE  /api/promotions/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Promotions from './promotions.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      var updated = _.merge(entity, updates);
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Promotionss
export function index(req, res) {
  return Promotions.find().exec()
    .then(respondWithResult(res))
}

// Gets a single Promotions from the DB
export function show(req, res) {
  return Promotions.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
}

// Creates a new Promotions in the DB
export function create(req, res) {
  return Promotions.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing Promotions in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Promotions.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
}

// Deletes a Promotions from the DB
export function destroy(req, res) {
  return Promotions.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
}
