'use strict';

var app = require('../..');
import request from 'supertest';

var newPromotions;

describe('Promotions API:', function() {

  describe('GET /api/promotions', function() {
    var promotionss;

    beforeEach(function(done) {
      request(app)
        .get('/api/promotions')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          promotionss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      promotionss.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/promotions', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/promotions')
        .send({
          name: 'New Promotions',
          info: 'This is the brand new promotions!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newPromotions = res.body;
          done();
        });
    });

    it('should respond with the newly created promotions', function() {
      newPromotions.name.should.equal('New Promotions');
      newPromotions.info.should.equal('This is the brand new promotions!!!');
    });

  });

  describe('GET /api/promotions/:id', function() {
    var promotions;

    beforeEach(function(done) {
      request(app)
        .get('/api/promotions/' + newPromotions._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          promotions = res.body;
          done();
        });
    });

    afterEach(function() {
      promotions = {};
    });

    it('should respond with the requested promotions', function() {
      promotions.name.should.equal('New Promotions');
      promotions.info.should.equal('This is the brand new promotions!!!');
    });

  });

  describe('PUT /api/promotions/:id', function() {
    var updatedPromotions;

    beforeEach(function(done) {
      request(app)
        .put('/api/promotions/' + newPromotions._id)
        .send({
          name: 'Updated Promotions',
          info: 'This is the updated promotions!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedPromotions = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPromotions = {};
    });

    it('should respond with the updated promotions', function() {
      updatedPromotions.name.should.equal('Updated Promotions');
      updatedPromotions.info.should.equal('This is the updated promotions!!!');
    });

  });

  describe('DELETE /api/promotions/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/promotions/' + newPromotions._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when promotions does not exist', function(done) {
      request(app)
        .delete('/api/promotions/' + newPromotions._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
