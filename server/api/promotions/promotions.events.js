/**
 * Promotions model events
 */

'use strict';

import {EventEmitter} from 'events';
import Promotions from './promotions.model';
var PromotionsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PromotionsEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Promotions.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PromotionsEvents.emit(event + ':' + doc._id, doc);
    PromotionsEvents.emit(event, doc);
  }
}

export default PromotionsEvents;
