'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var promotionsCtrlStub = {
  index: 'promotionsCtrl.index',
  show: 'promotionsCtrl.show',
  create: 'promotionsCtrl.create',
  update: 'promotionsCtrl.update',
  destroy: 'promotionsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var promotionsIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './promotions.controller': promotionsCtrlStub
});

describe('Promotions API Router:', function() {

  it('should return an express router instance', function() {
    promotionsIndex.should.equal(routerStub);
  });

  describe('GET /api/promotions', function() {

    it('should route to promotions.controller.index', function() {
      routerStub.get
        .withArgs('/', 'promotionsCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/promotions/:id', function() {

    it('should route to promotions.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'promotionsCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/promotions', function() {

    it('should route to promotions.controller.create', function() {
      routerStub.post
        .withArgs('/', 'promotionsCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/promotions/:id', function() {

    it('should route to promotions.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'promotionsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/promotions/:id', function() {

    it('should route to promotions.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'promotionsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/promotions/:id', function() {

    it('should route to promotions.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'promotionsCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
