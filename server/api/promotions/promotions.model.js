'use strict';

import mongoose from 'mongoose';

var PromotionsSchema = new mongoose.Schema({
  name: String,
  active: Boolean
});

export default mongoose.model('Promotions', PromotionsSchema);
