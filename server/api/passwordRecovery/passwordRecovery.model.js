'use strict';

import mongoose from 'mongoose';

var PasswordRecoverySchema = new mongoose.Schema({
  urlKey: String,
  email: String
});

export default mongoose.model('PasswordRecovery', PasswordRecoverySchema);
