'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var passwordRecoveryCtrlStub = {
  index: 'passwordRecoveryCtrl.index',
  show: 'passwordRecoveryCtrl.show',
  create: 'passwordRecoveryCtrl.create',
  update: 'passwordRecoveryCtrl.update',
  destroy: 'passwordRecoveryCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var passwordRecoveryIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './passwordRecovery.controller': passwordRecoveryCtrlStub
});

describe('PasswordRecovery API Router:', function() {

  it('should return an express router instance', function() {
    passwordRecoveryIndex.should.equal(routerStub);
  });

  describe('GET /api/passwordRecoverys', function() {

    it('should route to passwordRecovery.controller.index', function() {
      routerStub.get
        .withArgs('/', 'passwordRecoveryCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/passwordRecoverys/:id', function() {

    it('should route to passwordRecovery.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'passwordRecoveryCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/passwordRecoverys', function() {

    it('should route to passwordRecovery.controller.create', function() {
      routerStub.post
        .withArgs('/', 'passwordRecoveryCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/passwordRecoverys/:id', function() {

    it('should route to passwordRecovery.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'passwordRecoveryCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/passwordRecoverys/:id', function() {

    it('should route to passwordRecovery.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'passwordRecoveryCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/passwordRecoverys/:id', function() {

    it('should route to passwordRecovery.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'passwordRecoveryCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
