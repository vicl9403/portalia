/**
 * PasswordRecovery model events
 */

'use strict';

import {EventEmitter} from 'events';
import PasswordRecovery from './passwordRecovery.model';
var PasswordRecoveryEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PasswordRecoveryEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  PasswordRecovery.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PasswordRecoveryEvents.emit(event + ':' + doc._id, doc);
    PasswordRecoveryEvents.emit(event, doc);
  }
}

export default PasswordRecoveryEvents;
