/**
 * CatalogoFormulario model events
 */

'use strict';

import {EventEmitter} from 'events';
import CatalogoFormulario from './catalogo_formulario.model';
var CatalogoFormularioEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CatalogoFormularioEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  CatalogoFormulario.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CatalogoFormularioEvents.emit(event + ':' + doc._id, doc);
    CatalogoFormularioEvents.emit(event, doc);
  }
}

export default CatalogoFormularioEvents;
