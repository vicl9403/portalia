'use strict';

import mongoose from 'mongoose';

var CatalogoFormularioSchema = new mongoose.Schema({
    formularioID: Number,
	data: {}
});

export default mongoose.model('CatalogoFormulario', CatalogoFormularioSchema);
