'use strict';

var app = require('../..');
import request from 'supertest';

var newCatalogoFormulario;

describe('CatalogoFormulario API:', function() {

  describe('GET /api/catalogo_formularios', function() {
    var catalogoFormularios;

    beforeEach(function(done) {
      request(app)
        .get('/api/catalogo_formularios')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          catalogoFormularios = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      catalogoFormularios.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/catalogo_formularios', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/catalogo_formularios')
        .send({
          name: 'New CatalogoFormulario',
          info: 'This is the brand new catalogoFormulario!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newCatalogoFormulario = res.body;
          done();
        });
    });

    it('should respond with the newly created catalogoFormulario', function() {
      newCatalogoFormulario.name.should.equal('New CatalogoFormulario');
      newCatalogoFormulario.info.should.equal('This is the brand new catalogoFormulario!!!');
    });

  });

  describe('GET /api/catalogo_formularios/:id', function() {
    var catalogoFormulario;

    beforeEach(function(done) {
      request(app)
        .get('/api/catalogo_formularios/' + newCatalogoFormulario._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          catalogoFormulario = res.body;
          done();
        });
    });

    afterEach(function() {
      catalogoFormulario = {};
    });

    it('should respond with the requested catalogoFormulario', function() {
      catalogoFormulario.name.should.equal('New CatalogoFormulario');
      catalogoFormulario.info.should.equal('This is the brand new catalogoFormulario!!!');
    });

  });

  describe('PUT /api/catalogo_formularios/:id', function() {
    var updatedCatalogoFormulario;

    beforeEach(function(done) {
      request(app)
        .put('/api/catalogo_formularios/' + newCatalogoFormulario._id)
        .send({
          name: 'Updated CatalogoFormulario',
          info: 'This is the updated catalogoFormulario!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCatalogoFormulario = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCatalogoFormulario = {};
    });

    it('should respond with the updated catalogoFormulario', function() {
      updatedCatalogoFormulario.name.should.equal('Updated CatalogoFormulario');
      updatedCatalogoFormulario.info.should.equal('This is the updated catalogoFormulario!!!');
    });

  });

  describe('DELETE /api/catalogo_formularios/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/catalogo_formularios/' + newCatalogoFormulario._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when catalogoFormulario does not exist', function(done) {
      request(app)
        .delete('/api/catalogo_formularios/' + newCatalogoFormulario._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
