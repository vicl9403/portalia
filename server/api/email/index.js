'use strict';

var express = require('express');
var controller = require('./email.controller');

var router = express.Router();

router.post('/', controller.create);
router.post('/invitacion', controller.createInvitation);
router.post('/invitacionAnonima', controller.createInvitationAnonima);
router.post('/confirmacion', controller.createRegistro);
router.post('/contacto', controller.createContactado);



module.exports = router;
