'use strict';

import _ from 'lodash';
import nodemailer from 'nodemailer';
var _jade = require('jade');
var fs = require('fs');
var P = require('bluebird')

var transporter = nodemailer.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  },
  tls: {
    rejectUnauthorized: false
  }
})



function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Creates a new Email in the DB
export function create(req, res) {
  console.log('Enviando el correo');
  console.log(req.body);
  // specify jade template to load
  var template = process.cwd() + '/views/invitacion.html';

  return transporter.sendMail({
    from: 'hola@portaliaplus.com', // sender address
    to: req.body.to, // list of receivers
    cco: 'mkt@portaliaplus.com',
    cc: 'membresias@portaliaplus.com.mx',
    subject: req.body.subject, // Subject line
    text: req.body.text, //Text line
    html: req.body.html // html body
  }, function(error, info) {
    if (error) {
      console.log(error);
      res.writeHead(500, {
        'Content-Type': 'text/plain'
      });
    } else {
      console.log('Message sent: ' + info.response);
      res.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      res.end('ok');
    }
  });
}

// Creates a new Email in the DB
export function createRegistro(req, res) {
  console.log('Enviando el correo');
  console.log(req.body);


  var template = './server/views/confirmacion.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {};
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);


      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: req.body.email, // list of receivers
        subject: req.body.subject, // Subject line
        html: html // html body
      }, function(error, info) {
        if (error) {
          console.log(error);
          res.writeHead(500, {
            'Content-Type': 'plain/text'
          });
          res.end("erro");
        } else {
          console.log('Message sent: ' + info.response);
          res.writeHead(200, {
            'Content-Type': 'plain/text'
          });
          res.end("ok");
        }
      });

    }
  });


}

// Creates a new Email in the DB
export function createContactado(req, res) {
  console.log('Enviando el correo');
  console.log(req.body);


  var template = './server/views/contacto.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });


      var context = {
        correo_usuario: req.body.email,
        mensaje: req.body.mensaje,
      };


      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

        transporter.sendMail({
          from: 'hola@portaliaplus.com', // sender address
          to: req.body.to, // list of receivers
          subject: req.body.subject, // Subject line
          html: html // html body
        }, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });


      res.writeHead(200, {
        'Content-Type': 'plain/text'
      });
      res.end("ok");


    }
  });


}

// Creates a new Email in the DB
export function createInvitationAnonima(req, res) {
  console.log('Enviando el correo');
  console.log(req.body);


  var template = './server/views/invitacion_anonima.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });

      var context = {};


      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

      var log = [{}];
      for (var i = 0; i < req.body.to.length; i++) {

        transporter.sendMail({
          from: 'hola@portaliaplus.com', // sender address
          to: req.body.to[i], // list of receivers
          subject: "Hola te invitamos a registrarte en Portalia Plus", // Subject line
          html: html // html body
        }, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });

      }

      res.writeHead(200, {
        'Content-Type': 'plain/text'
      });
      res.end("ok");


    }
  });


}

// Creates a new Email in the DB
export function createInvitation(req, res) {
  console.log('Enviando el correo');
  console.log(req.body);


  var template = './server/views/invitacion.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });


      var context = {
        nombre_usuario: req.body.name,
        codigo_usuario: req.body._id,
      };


      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

      var log = [{}];
      for (var i = 0; i < req.body.to.length; i++) {

        transporter.sendMail({
          from: 'hola@portaliaplus.com', // sender address
          to: req.body.to[i], // list of receivers
          subject: req.body.subject, // Subject line
          html: html // html body
        }, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });

      }

      res.writeHead(200, {
        'Content-Type': 'plain/text'
      });
      res.end("ok");


    }
  });


}

export function mailValidation(req, res) {
  console.log('Enviando el correo');
  console.log(req.body);


  var template = './server/views/invitacion.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {
        nombre_usuario: req.body.name,
        codigo_usuario: req.body._id,
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

      var log = [{}];
      for (var i = 0; i < req.body.to.length; i++) {

        transporter.sendMail({
          from: 'hola@portaliaplus.com', // sender address
          to: req.body.to[i], // list of receivers
          subject: req.body.subject, // Subject line
          html: html // html body
        }, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });

      }

      res.writeHead(200, {
        'Content-Type': 'plain/text'
      });
      res.end("ok");


    }
  });


}



