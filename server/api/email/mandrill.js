'use strict';

// Variable para consumir la API de Mandrill
import QuotationUser from "../quotation_user/quotation_user.model";

let mandrill = require('mandrill-api/mandrill');

// Esta  es una función de MailChimo ( Mandrill )
// para mandar las cotizaciones a un usuario, se puede
// usar cuantas veces sea necesaria
import config from "../../config/environment";


// Esta  es una función de MailChimo ( Mandrill )
// para mandar las cotizaciones a un usuario, se puede
// usar cuantas veces sea necesaria
export function sendQuoteEmail(user, quote) {

  let mandrill_client = new mandrill.Mandrill(config.mandrillApiKey);

  // Todas estas son las variables a mostrar en la vista (declaraciones)
  let username = user.nombre ;
  let applicant = quote.contact_name ;
  let product = quote.product ;
  let quantity =  quote.quantity + ' ' + quote.unity ;
  let frecuency = ( quote.frecuency != null && quote.frecuency != '' ) ? quote.frecuency : 'No se especificó' ;
  let state = quote.state ;
  let email = ( user.emailSec != null ) ? user.emailSec : user.email ;

  // Selección de la plantilla y asignación de variables al correo
  let template_name = "Cotizaciones";
  let template_content = [
    {
      "name": "username",
      "content": username
    },
    {
      "name" : "applicant",
      "content" : applicant
    },
    {
      "name": "product",
      "content": product
    },
    {
      "name": "quantity",
      "content": quantity
    },
    {
      "name": "frecuency",
      "content": frecuency
    },
    {
      "name": "state",
      "content": state
    }
  ];

  // Configuración básica del email
  let message = {
    'html' : 'Example content',
    "text" :  'Example test content',
    "subject": "Cotización de " + product,
    "from_email": "cotizaciones@sociosportaliaplus.mx",
    "from_name": "Socios portaliaplus",
    "to": [{
      "email": email,
      "name" : username,
      "type" : 'to'
    }],
    "headers": {
      "Reply-To": "message.reply@example.com"
    },
    "important": false,
    "inline_css": true,

    "merge": true,
    "merge_language": "mailchimp",
    "global_merge_vars": [{
      "name": "merge1",
      "content": "merge1 content"
    }],
    "merge_vars": [{
      "rcpt": "lopez.victor94@gmail.com",
      "vars": [{
        "name": "merge2",
        "content": "merge2 content"
      }]
    }],
  };

  var async = false;
  var ip_pool = "Main Pool";

  // Envio del correo
  mandrill_client.messages.sendTemplate({
      "template_name": template_name,
      "template_content": template_content,
      "message": message,
      "async": async,
      "ip_pool": ip_pool,
    },
    function(result) {

      // Setear el envio de la cotización
      if( result[0].status == 'sent') {
        QuotationUser.findOne({
          "user"    : user._id,
          "quote"   : quote._id
        }).exec()
          .then( function (entity) {
            if( entity != null ) {
              entity.wasSend = true;
              entity.sendDate = new Date();
              entity.emailId = result[0]._id;
              entity.save();
            }
          })
      }

      /*
      [{
              "email": "lopez.victor94@gmail.com",
              "status": "sent",
              "reject_reason": "hard-bounce",
              "_id": "abc123abc123abc123abc123abc123"
          }]
      */
    }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });

  return 'Sending';
}

