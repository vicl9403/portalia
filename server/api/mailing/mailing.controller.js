/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/mailings              ->  index
 * POST    /api/mailings              ->  create
 * GET     /api/mailings/:id          ->  show
 * PUT     /api/mailings/:id          ->  update
 * DELETE  /api/mailings/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Mailing from './mailing.model';
var mailin = require("mailin-api-node-js");
var client = new Mailin("https://api.sendinblue.com/v2.0", "wFKdNjXJx3ZYUL8M");

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Mailings
export function index(req, res) {
  return Mailing.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Mailing from the DB
export function show(req, res) {
  return Mailing.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Mailing in the DB
export function create(req, res) {
  return Mailing.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing Mailing in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Mailing.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Mailing from the DB
export function destroy(req, res) {
  return Mailing.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

function testMails() {
  var data = {
    "to": {
      "ramiroauditore@hotmail.com": "to whom!"
    },
    "from": ["rgarcilazo@it4pymes.mx", "from email!"],
    "subject": "My subject",
    "html": "This is the <h1>HTML</h1>"
  }


  client.send_email(data).on('complete', function(data) {
    console.log(data);
  });

  // var data = { "from_name" : "Portalia Plus",
  //   "template_name" : "Plantilla Genérica",
  //   "html_url" : "https://drive.google.com/file/d/0B3T0GKmkoOQCZzF2dEgtcExPUzQ/view?usp=sharing",
  //   "subject" : "%TITLE%",
  //   "from_email" : "no-reply@portaliaplus.com",
  //   "status" : 1 //again
  // }
  //
  // client.create_template(data).on('complete', function(data) {
  //   console.log(data);
  // });
}

// testMails();

export function sendMails(req, res) {
  var results = [];
  var x;
  Mailing.findById(req.params.id).exec()
    .then(bloque => {
      for (x = 0; x < bloque.email.length; x++) {
        console.log(bloque.email.length);
        console.log("equis", x);
        var data = {
          "id": 4,
          "to": bloque.email[x].mail,
          "attr": {
            "TITLE": bloque.email[x].title,
            "BODY": bloque.email[x].body
          },
          "headers": {
            "Content-Type": "text/html;charset=iso-8859-1",
            "X-param1": "value1",
            "X-param2": "value2",
            "X-Mailin-custom": "my custom value",
            "X-Mailin-tag": "my tag value"
          }
        }

        client.send_transactional_template(data).on('complete', function(data) {
          console.log(data);
          var dataJSON = JSON.parse(data);
          results.push(dataJSON.code);
        });
      }

      var status = "éxito"
      res.status(200).json(status);
    })
}

export function shareMail(req, res) {
  var data = {
    "id": 4,
    "to": req.body.suCorreo,
    "attr": {
      "TITLE": 'Te han compartido un enlace de Portalia Plus',
      "BODY": 'Hola ' + req.body.suNombre + ', ' + req.body.tuNombre + ' ha compartido el siguiente enlace contigo: ' + req.body.url
    },
    "headers": {
      "Content-Type": "text/html;charset=iso-8859-1",
      "X-param1": "value1",
      "X-param2": "value2",
      "X-Mailin-custom": "my custom value",
      "X-Mailin-tag": "my tag value"
    }
  }

  client.send_transactional_template(data).on('complete', function(data) {
    var dataJSON = JSON.parse(data);
    var status = "éxito"
    res.status(200).json(status);
  });
}
