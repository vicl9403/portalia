/**
 * Mailing model events
 */

'use strict';

import {EventEmitter} from 'events';
import Mailing from './mailing.model';
var MailingEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MailingEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Mailing.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    MailingEvents.emit(event + ':' + doc._id, doc);
    MailingEvents.emit(event, doc);
  }
}

export default MailingEvents;
