'use strict';

import mongoose from 'mongoose';

var MailingSchema = new mongoose.Schema({
  bloque: String,
  email:[{
    nombre: String,
    mail: String,
    categoria_name: String,
    sector_name: String,
    title: String,
    body: String
  }]
});

export default mongoose.model('Mailing', MailingSchema);
