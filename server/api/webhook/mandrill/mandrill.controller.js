'use strict';

import QuotationUser from "../../quotation_user/quotation_user.model";
import HistoricalQuotation from "../../historical_quotation/historical_quotation.model";

// Petición para el webhook de mandrill
export function webhookMandrill(req, res) {
  //se verifica que sea webhook de mandrill
  if(typeof req.body.mandrill_events == 'undefined')
    return res.status(200).send('Petición no realizada exitòsamente.');

  //obtenemos la información del evento
  let data = JSON.parse(req.body.mandrill_events);
  data = data[0];

  console.log("ID:" +data._id + ", EVENTO: " + data.event);
  //buscamos por el id del email
  QuotationUser.findOne({
    "emailId"    : data._id
  }).populate('quote')
    .exec()
    .then( function (entity) {
      //verificamos que exista
      if( entity != null ) {
        //obtenemos la categoría
        let sector = entity.quote.sector;
        let category = entity.quote.category;

        //actualizamos el estatos coforme al mensaje
        entity.emailStatus = data.event;
        entity.save();

        //buscamos si existe historico con esa categoría
        HistoricalQuotation.findOne({
          "category"    : category,
          "sector"      : sector
        }).exec()
          .then( function (historical) {
            //verificamos que exista
            if( historical == null )
            {
              //creamos un usuario con la categoría
              historical = new HistoricalQuotation();
            }
            //actualizamos el historico
            historical.category = category;
            historical.sector = sector;
            historical[data.event] = historical[data.event] + 1;
            historical.save();
            return res.status(200).send('Histórico actualizado.');
          });
      }else{
        return res.status(200).send('El id no pertence a ningúna cotización.');
      }
    });

}

// Petición para el webhook de mandrill de black and white list
export function webhookMandrillList(req, res) {
  //se verifica que sea webhook de mandrill
  if(typeof req.body.mandrill_events == 'undefined')
    return res.status(200).send('Petición no realizada exitòsamente.');

  //obtenemos la información del evento
  let data = JSON.parse(req.body.mandrill_events);
  data = data[0];
  //buscamos por el id del email
  QuotationUser.findOne({
    "emailId"    : data._id
  }).populate('quote')
    .exec()
    .then( function (entity) {
      //verificamos que exista
      if( entity != null ) {
        //obtenemos la categoría
        let sector = entity.quote.sector;
        let category = entity.quote.category;

        //actualizamos el estatos coforme al mensaje
        entity.emailStatus = data.type;
        entity.save();

        //buscamos si existe historico con esa categoría
        HistoricalQuotation.findOne({
          "category"    : category,
          "sector"      : sector
        }).exec()
          .then( function (historical) {
            //verificamos que exista
            if( historical == null )
            {
              //creamos un usuario con la categoría
              historical = new HistoricalQuotation();
            }
            //actualizamos el historico
            historical.category = category;
            historical.sector = sector;
            historical[data.type] = historical[data.type] + 1;
            historical.save();
            return res.status(200).send('Histórico actualizado.');
          });
      }else{
        return res.status(200).send('El id no pertence a ningúna cotización.');
      }
    });


}

