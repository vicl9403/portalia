'use strict';

var express = require('express');
var mandrillController = require('./mandrill/mandrill.controller');

var router = express.Router();

router.post('/mandrill', mandrillController.webhookMandrill);
router.post('/mandrill-lists', mandrillController.webhookMandrillList);

module.exports = router;
