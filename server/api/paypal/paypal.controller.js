'use strict';

import _ from 'lodash';
import config from '../../config/environment';
import http from 'http';
import paypal from 'paypal-rest-sdk';
import promise from 'promise';
import os from 'os';

paypal.configure({
  'host': config.auth.paypal.URL,
  'client_id': config.auth.paypal.ID,
  'client_secret': config.auth.paypal.KEY
});

var create_payment_json = {
  "intent": "sale",
  "payer": {
    "payment_method": "paypal"
  },
  "redirect_urls": {
    "return_url": "",
    "cancel_url": "http://localhost:/" + config.port || 9000
  },
  "transactions": [{
    "amount": {
      "currency": "MXN",
      "total": ""
    },
    "description": "This is the payment description."
  }]
};

// Generates a new paypal link payment
export function create(req, res) {
  console.log(req.params);
  create_payment_json.transactions[0].amount.total = req.params.adeudo;
  create_payment_json.redirect_urls.return_url = config.domain + "/confirmacion/" + req.params.adeudo;
  paypal.payment.create(create_payment_json, function(err, response) {
    if (err) {
      console.log(err);
    } else if (response) {
      console.log("..::Respuesta::..", response);
      console.log("..::Link::..", response.links[2].href);
      res.end(response.links[1].href);
    }
  });
}

export function execute(req, res) {

  var params = {
    "payer_id": req.body.PayerID
  };
  paypal.payment.execute(req.body.paymentId, params, function(err, payment) {
    if (err) {
      console.error(err);
    } else {
      console.log(payment);
      res.end(payment.state);
    }
  });
}
