
import algoliasearch from 'algoliasearch';

import config from '../../config/environment';

var client = algoliasearch( config.algoliaconfig.idApplication , config.algoliaconfig.adminKey );


var index = client.initIndex('users');

import User from './user.model';

export function createIndexFromAllUsers() {

  var usersToIndex = [];

  var users = User.find()
    .exec()
    .then( (res) => {
      let users = res;
      for( let i = 0 ; i < users.length ; i++ ) {

        let plan_number = 1;

        if( users[i].paquete == 'select' )
          plan_number = 2;

        if( users[i].paquete == 'plus' )
          plan_number = 3;

        if( usersToIndex.length >= 500 ) {
          // index.saveObjects( usersToIndex, function (err, content) {
          //   console.log( content );
          //   console.log( err );
          //   console.log('content ready');
          // });
          usersToIndex = [];
        }

        let user =  {
          objectID      : users[i]._id,
          name          : users[i].nombre,
          sector        : users[i].sector_name,
          category      : users[i].categoria_name,
          state         : users[i].estado_name,
          municipality  : users[i].municipio_name,
          cover         : users[i].cover,
          stars         : users[i].stars,
          plan          : plan_number,
          about         : users[i].about,
          tags          : users[i].tags,
          products      : users[i].products,
          subCategories : users[i].subcategories
        };

        if( users[i].subcategories != null ) {
          index.saveObject(user, function (err, content) {
            if (err) {
              console.log(err);
            }
            else {
              console.log('success');
            }
          });
        }

        usersToIndex.push({
          objectID      : users[i]._id,
          name          : users[i].nombre,
          sector        : users[i].sector_name,
          category      : users[i].categoria_name,
          state         : users[i].estado_name,
          municipality  : users[i].municipio_name,
          cover         : users[i].cover,
          stars         : users[i].stars,
          plan          : plan_number,
          about         : users[i].about,
          tags          : users[i].tags,
          products      : users[i].products,
          subCategories : users[i].subcategories
        });

      }

      if( usersToIndex.length > 0 ) {
        // Solo se debe de ejecutar una vez
        // index.saveObjects(usersToIndex, function (err, content) {
        //   console.log(content);
        //   console.log(err);
        //   console.log('content ready');
        // });
      }

    });

}


export function setUserData( id ) {

  // Asignar un tiempo de espera para actualizar el índice con el
  // usuario ya modificado
  setTimeout( function () {
    User.findById(id)
      .then( user => {
        let plan_number = 1;

        if( user.paquete == 'select' )
          plan_number = 2;

        if( user.paquete == 'plus' )
          plan_number = 3;


        let usr =  {
          objectID      : user._id,
          name          : user.nombre,
          sector        : user.sector_name,
          category      : user.categoria_name,
          state         : user.estado_name,
          municipality  : user.municipio_name,
          cover         : user.cover,
          stars         : user.stars,
          plan          : plan_number,
          about         : user.about,
          tags          : user.tags,
          products      : user.products,
          subCategories : user.subcategories
        };

        index.saveObject(usr, function(err, content) {
          console.log(content);
        });
      })
  },1000)

}
