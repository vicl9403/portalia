'use strict';

import crypto from 'crypto';
import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {
  Schema
} from 'mongoose';


const authTypes = ['github', 'twitter', 'facebook', 'google'];

var UserSchema = new Schema({
  active: {
    type: Boolean,
    default: false
  },
  stars: {
    type: Number,
    default: 5
  },

  profilePic : { type: String , default: null },
  alreadyRated: [],
  pushSuscriberId: [String],
  wantsNotifications: {
    type: Boolean,
    default: false
  },
  destacado: {
    type: Boolean,
    default: false
  },
  seenQuotes: [],
  contactos: [],
  numeroContactos: {
    type: Number,
    default: 0
  },
  visitaFotos: [],
  hasPic: {
    type: Boolean,
    default: false
  },
  busquedas: [],
  numeroVisitasGene: Number,
  email: {
    type: String,
    lowercase: true,
    required: function() {
      if (authTypes.indexOf(this.provider) === -1) {
        return true;
      } else {
        return false;
      }
    }
  },
  emailSec: {
    type: String,
    lowercase: true
  },
  videos: [],
  visitasTel: [],
  visitasGene: [],
  archivo: String,
  role: {
    type: String,
    default: 'user'
  },
  paquete: {
    type: String,
    default: 'gratis'
  },
  noPaquete: {
    type: Number,
    default: 1
  },
  password: {
    type: String,
    required: function() {
      if (authTypes.indexOf(this.provider) === -1) {
        return true;
      } else {
        return false;
      }
    }
  },
  facebook: {},
  tags: {
    type: Array
  },
  products: {
    type: Array
  },
  productsImg: [],
  productsCatalog: [],
  telefono: {
    type: String,
    default: ''
  },
  telefonos: [{
    tel: String,
    nombre: String
  }],
  provider: String,
  salt: String,
  nombre: String,
  nombre_contacto : { type : String , default : '' },
  razon: String,
  rfc: String,
  web: String,
  direccion: String,
  sector: String,
  categoria: String,
  sector_name: String,

  categoria_name: String,

  subCategoria: String,

  subcategories : {
    type: Array,
    default : [],
  },

  otraSub: String,
  estado: String,
  estado_name: String,
  municipio: String,
  municipio_name: String,
  //municipio: [ { type: Schema.Types.ObjectId, ref: 'Municipio'}],
  codigo_invitacion: String,
  fechaPago: Date,
  fechaCorte: Date,
  about: {
    type: String,
    default: ''
  },
  ventajas: {
    type: String,
    default: ''
  },
  redes: {
    facebook: {
      type: String,
      default: 'http://www.facebook.com/'
    },
    twitter: {
      type: String,
      default: 'http://www.twitter.com/'
    },
    youtube: {
      type: String,
      default: 'http://www.youtube.com/'
    },
    linkedin: {
      type: String,
      default: 'http://www.linkedin.com/'
    },
    googleplus: {
      type: String,
      default: 'http://www.plus.google.com/'
    }
  },

  sucursales: [{
    estadoSucursal: {
      type: String,
      default: 'Placeholder'
    },
    ciudad: {
      type: String,
      default: 'Placeholder'
    },
    colonia: {
      type: String,
      default: 'Placeholder'
    },
    calle: {
      type: String,
      default: 'Placeholder'
    },
    numero: {
      type: String,
      default: 'Placeholder'
    },
    cp: {
      type: String,
      default: 'Placeholder'
    },
    nombreSucu: {
      type: String,
      default: 'Nombre de la Sucursal'
    },
    loc: {
      type: [Number],
      index: '2d'
    },
    latitud: {
      type: String,
      default: 'Placeholder'
    },
    longitud: {
      type: String,
      default: 'Placeholder'
    }

  }],

  favoritos: [],
  articulosGuardados: [],
  suscripcion: {
    activa: {
      type: Boolean,
      default: false
    },
    selecciondas: []
  },
  planSolicitado: {
    plan: String,
    period: Number,
    openPayResponse: {}
  },
  planSolicitadoPricing: {
    plan: String,
    periodo: String,
    periodoInt: Number,
    frecuencia: String,
    numeroPagos: Number,
    frequentCost: Number,
    total: Number
  },
  datosDeFacturacion: {
    requested: {
      type: Boolean,
      default: false
    },
    razon: String,
    rfc: String,
    calle: String,
    numeroCalle: String,
    colonia: String,
    codigoPostal: String,
    ciudad: String,
    estado: String,
    correoFactu: String
  },

  //Horarios de apertura de cada usuario
  shedules: [
    {
      day:  { type:String, default:'Lunes'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    },
    {
      day:  { type:String, default:'Martes'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    },
    {
      day:  { type:String, default:'Miércoles'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    },
    {
      day:  { type:String, default:'Jueves'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    },
    {
      day:  { type:String, default:'Viernes'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    },
    {
      day:  { type:String, default:'Sábado'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    },
    {
      day:  { type:String, default:'Domingo'},
      oppened: { type: Boolean },
      opening: { type: String },
      closing: { type: String }
    }
  ],

  cover : String,
  openpayID: String,
  subscription:{},
  subscriptionCounter: Number,
  quotes  : { type:Schema.ObjectId, ref:"Parent", childPath:"Quotation" },

  contacts : { type:Schema.ObjectId, ref: "Parent", childPath:"Contact" },

  contactsPerDay : { type : Number , default: 0 } ,

  createdChats : { type:Schema.ObjectId, ref:"userOrigin", childPath:"Chat" },
  invitedChats : { type:Schema.ObjectId, ref:"userDest", childPath:"Chat" },

  // Los loggins del usuario
  user: [{ type:Schema.ObjectId, ref:"Login" }],

  // Campo utilizado para los resultados de búsqueda
  score: {
    type: Number,
    default: 5
  },

});

/**
 * Virtuals
 */

// Public profile information
UserSchema
  .virtual('profile')
  .get(function() {
    return {
      'name': this.name,
      'role': this.role
    };
  });

// Non-sensitive info we'll be putting in the token
UserSchema
  .virtual('token')
  .get(function() {
    return {
      '_id': this._id,
      'role': this.role
    };
  });

/**
 * Validations
 */

// Validate empty email
UserSchema
  .path('email')
  .validate(function(email) {
    if (authTypes.indexOf(this.provider) !== -1) {
      return true;
    }
    return email.length;
  }, 'Email cannot be blank');

// Validate empty password
UserSchema
  .path('password')
  .validate(function(password) {
    if (authTypes.indexOf(this.provider) !== -1) {
      return true;
    }
    return password.length;
  }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
  .path('email')
  .validate(function(value, respond) {
    var self = this;
    if (authTypes.indexOf(this.provider) !== -1) {
      return respond(true);
    }
    return this.constructor.findOne({
        email: value
      }).exec()
      .then(function(user) {
        if (user) {
          if (self.id === user.id) {
            return respond(true);
          }
          return respond(false);
        }
        return respond(true);
      })
      .catch(function(err) {
        throw err;
      });
  }, 'The specified email address is already in use.');

var validatePresenceOf = function(value) {
  return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', function(next) {
    // Handle new/update passwords
    if (!this.isModified('password')) {
      return next();
    }

    if (!validatePresenceOf(this.password)) {
      if (authTypes.indexOf(this.provider) === -1) {
        return next(new Error('Invalid password'));
      } else {
        return next();
      }
    }

    // Make salt with a callback
    this.makeSalt((saltErr, salt) => {
      if (saltErr) {
        return next(saltErr);
      }
      this.salt = salt;
      this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
        if (encryptErr) {
          return next(encryptErr);
        }
        this.password = hashedPassword;
        next();
      });
    });
  });

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} password
   * @param {Function} callback
   * @return {Boolean}
   * @api public
   */
  authenticate(password, callback) {
    if (!callback) {
      return this.password === this.encryptPassword(password);
    }

    this.encryptPassword(password, (err, pwdGen) => {
      if (err) {
        return callback(err);
      }

      if (this.password === pwdGen) {
        callback(null, true);
      } else {
        callback(null, false);
      }
    });
  },

  /**
   * Make salt
   *
   * @param {Number} byteSize Optional salt byte size, default to 16
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  makeSalt(byteSize, callback) {
    var defaultByteSize = 16;

    if (typeof arguments[0] === 'function') {
      callback = arguments[0];
      byteSize = defaultByteSize;
    } else if (typeof arguments[1] === 'function') {
      callback = arguments[1];
    }

    if (!byteSize) {
      byteSize = defaultByteSize;
    }

    if (!callback) {
      return crypto.randomBytes(byteSize).toString('base64');
    }

    return crypto.randomBytes(byteSize, (err, salt) => {
      if (err) {
        callback(err);
      } else {
        callback(null, salt.toString('base64'));
      }
    });
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  encryptPassword(password, callback) {
    if (!password || !this.salt) {
      if (!callback) {
        return null;
      } else {
        return callback('Missing password or salt');
      }
    }

    var defaultIterations = 10000;
    var defaultKeyLength = 64;
    var salt = new Buffer(this.salt, 'base64');

    if (!callback) {
      return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength)
        .toString('base64');
    }

    return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, (err, key) => {
      if (err) {
        callback(err);
      } else {
        callback(null, key.toString('base64'));
      }
    });
  }
};

export default mongoose.model('User', UserSchema);
