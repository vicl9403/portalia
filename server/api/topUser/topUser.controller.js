/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/topUsers              ->  index
 * POST    /api/topUsers              ->  create
 * GET     /api/topUsers/:id          ->  show
 * PUT     /api/topUsers/:id          ->  upsert
 * PATCH   /api/topUsers/:id          ->  patch
 * DELETE  /api/topUsers/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import TopUser from './topUser.model';
import User from '../user/user.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of TopUsers
export function index(err, res) {
  return TopUser.find({}, function(err, docs) {
    User.populate(docs, {
      path: "idUser"
    }, function(err, users) {
      res.status(200).json(users);
    })
  });
}

// Gets a single TopUser from the DB
export function show(req, res) {
  return TopUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new TopUser in the DB
export function create(req, res) {
  TopUser.count({}, function(err, count) {
    if(count == 12){
      return res.status(200).json('Limite alcanzado');
    } else {
      return TopUser.create(req.body)
        .then(respondWithResult(res, 201))
        // .catch(handleError(res));
    }
  })
}

// Upserts the given TopUser in the DB at the specified ID
export function upsert(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return TopUser.findOneAndUpdate(req.params.id, req.body, {
      upsert: true,
      setDefaultsOnInsert: true,
      runValidators: true
    }).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing TopUser in the DB
export function patch(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return TopUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a TopUser from the DB
export function destroy(req, res) {
  return TopUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

export function deleteDis(req, res) {
  console.log('kjfslkdjfslk');
  return TopUser.findOne({
    idString: req.body.idString
  }).exec().then(function(doc) {
    console.log(doc);
    doc.remove();
    res.status(200).end();
  })
}
