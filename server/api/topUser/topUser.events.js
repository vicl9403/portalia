/**
 * TopUser model events
 */

'use strict';

import {EventEmitter} from 'events';
import TopUser from './topUser.model';
var TopUserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TopUserEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  TopUser.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    TopUserEvents.emit(event + ':' + doc._id, doc);
    TopUserEvents.emit(event, doc);
  };
}

export default TopUserEvents;
