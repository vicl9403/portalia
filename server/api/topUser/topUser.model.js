'use strict';

import mongoose from 'mongoose';
var Schema = mongoose.Schema;
var User = mongoose.model('User');

var TopUserSchema = new mongoose.Schema({
  idUser: [{ type:Schema.ObjectId, ref:"User" }],
  idString: String
});

export default mongoose.model('TopUser', TopUserSchema);
