'use strict';

import mongoose from 'mongoose';
var Schema = mongoose.Schema;
var User = mongoose.model('User');

var ContactSchema = new mongoose.Schema({

  name: { type : String, required: true },
  email: { type : String, required: true },
  phone: { type : String, required: true },
  description: { type : String, required: true },

  from: [{ type:Schema.ObjectId, ref:"User" }],
  to: [{ type:Schema.ObjectId, ref:"User" }],

  created_at: { type: Date , default: new Date() },

  authorized: { type: Boolean, default: false },

  active: Boolean,

});

export default mongoose.model('Contact', ContactSchema);
