'use strict';

var express = require('express');
var controller = require('./contact.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/authorized', controller.authorized);
router.get('/not-authorized', controller.notAuthorized);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.post('/sendContactMail' , controller.sendContactMail);
router.get('/userContacts/:id', controller.userContacts);

module.exports = router;
