/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/chats              ->  index
 * POST    /api/chats              ->  create
 * GET     /api/chats/:id          ->  show
 * PUT     /api/chats/:id          ->  update
 * DELETE  /api/chats/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Chat from './chat.model';
import User from '../user/user.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Chats
export function index(req, res) {
  return Chat.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Obtener los chats de un usuario
export function getUserChats(req, res) {

  var user = req.params.id;

  Chat
  .find({
    $or:
      [
        {
          'userOrigin': user
        },
        {
          'userDest': user
        }
      ]
  })
  .populate({
    path:  'userOrigin'
  })
  .populate({
    path: 'userDest'
  })
  .exec( function (err, chats) {
    res.status(200).send(chats);
  });

}

export function chatExists( req, res) {
  var id1 = req.params.id1;
  var id2 = req.params.id2;

  return Chat.find({
    $or:
      [
        {
          $and :
            [
              {
                'userOrigin': id1
              },
              {
                'userDest': id2
              }
            ]
        },
        {
          $and :
            [
              {
                'userOrigin': id2
              },
              {
                'userDest': id1
              }
            ]
        }
      ]
  }).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));

}
// Gets a single Chat from the DB
export function show(req, res) {
  return Chat.find({
      personsId: {
        $elemMatch: {
          id: req.params.id
        }
      }
    }).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Chat in the DB
export function create(req, res) {
  return Chat.create(req.body)
    .then(respondWithResult(res, 201))
}

// // Updates an existing Chat in the DB
// export function update(req, res) {
//   if (req.body._id) {
//     delete req.body._id;
//   }
//   return Chat.findById(req.params.id).exec()
//     .then(handleEntityNotFound(res))
//     .then(saveUpdates(req.body))
//     .then(respondWithResult(res))
//     .catch(handleError(res));
// }

export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  console.log(req.body);
  return Chat.findByIdAndUpdate(req.params.id, req.body, {new: true}).exec()
    .then(entity=>{
      res.status(200).json(entity);
    })
    .catch(error =>{
      console.log(error);
      res.status(500).end();
    });
}

// Deletes a Chat from the DB
export function destroy(req, res) {
  return Chat.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
