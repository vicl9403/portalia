'use strict';

import mongoose from 'mongoose';

var Schema = mongoose.Schema;
var User = mongoose.model('User');

var ChatSchema = new mongoose.Schema({
  // personsId: [{
  //   id: String,
  //   name: String
  // }],
  // msj: [{
  //   idEnv: String,
  //   msj: String,
  //   timestamp: {
  //     type: Date,
  //     default: Date.now
  //   }
  // }]

  userOrigin :  [{ type:Schema.ObjectId, ref:"User" }],
  userDest   :  [{ type:Schema.ObjectId, ref:"User" }],
  messages : [{
    from:     String,
    to:       String,
    content:  String,
    hasReadOrigin: { type: Boolean, default: false },
    hasReadDest: { type: Boolean, default: false },
    created_at : { type: Date , default: Date.now },
  }],
  active: {
    type: Boolean,
    default: true
  }


});

export default mongoose.model('Chat', ChatSchema);
