'use strict';

import mongoose from 'mongoose';

var BusquedaSchema = new mongoose.Schema({
  termino: String,
  idUsuario: {
    type: String,
    default: 'Anónimo'
  },
  rolUsuario: {
    type: String,
    default: 'Anónimo'
  },
  timestamp: {
    type: Date,
    default: Date.now
  }
});

export default mongoose.model('Busqueda', BusquedaSchema);
