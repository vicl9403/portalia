/**
 * Busqueda model events
 */

'use strict';

import {EventEmitter} from 'events';
import Busqueda from './busqueda.model';
var BusquedaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BusquedaEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Busqueda.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    BusquedaEvents.emit(event + ':' + doc._id, doc);
    BusquedaEvents.emit(event, doc);
  }
}

export default BusquedaEvents;
