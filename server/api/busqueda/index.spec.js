'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var busquedaCtrlStub = {
  index: 'busquedaCtrl.index',
  show: 'busquedaCtrl.show',
  create: 'busquedaCtrl.create',
  update: 'busquedaCtrl.update',
  destroy: 'busquedaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var busquedaIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './busqueda.controller': busquedaCtrlStub
});

describe('Busqueda API Router:', function() {

  it('should return an express router instance', function() {
    busquedaIndex.should.equal(routerStub);
  });

  describe('GET /api/busquedas', function() {

    it('should route to busqueda.controller.index', function() {
      routerStub.get
        .withArgs('/', 'busquedaCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/busquedas/:id', function() {

    it('should route to busqueda.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'busquedaCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/busquedas', function() {

    it('should route to busqueda.controller.create', function() {
      routerStub.post
        .withArgs('/', 'busquedaCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/busquedas/:id', function() {

    it('should route to busqueda.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'busquedaCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/busquedas/:id', function() {

    it('should route to busqueda.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'busquedaCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/busquedas/:id', function() {

    it('should route to busqueda.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'busquedaCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
