'use strict';

var app = require('../..');
import request from 'supertest';

var newBusqueda;

describe('Busqueda API:', function() {

  describe('GET /api/busquedas', function() {
    var busquedas;

    beforeEach(function(done) {
      request(app)
        .get('/api/busquedas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          busquedas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      busquedas.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/busquedas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/busquedas')
        .send({
          name: 'New Busqueda',
          info: 'This is the brand new busqueda!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newBusqueda = res.body;
          done();
        });
    });

    it('should respond with the newly created busqueda', function() {
      newBusqueda.name.should.equal('New Busqueda');
      newBusqueda.info.should.equal('This is the brand new busqueda!!!');
    });

  });

  describe('GET /api/busquedas/:id', function() {
    var busqueda;

    beforeEach(function(done) {
      request(app)
        .get('/api/busquedas/' + newBusqueda._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          busqueda = res.body;
          done();
        });
    });

    afterEach(function() {
      busqueda = {};
    });

    it('should respond with the requested busqueda', function() {
      busqueda.name.should.equal('New Busqueda');
      busqueda.info.should.equal('This is the brand new busqueda!!!');
    });

  });

  describe('PUT /api/busquedas/:id', function() {
    var updatedBusqueda;

    beforeEach(function(done) {
      request(app)
        .put('/api/busquedas/' + newBusqueda._id)
        .send({
          name: 'Updated Busqueda',
          info: 'This is the updated busqueda!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedBusqueda = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBusqueda = {};
    });

    it('should respond with the updated busqueda', function() {
      updatedBusqueda.name.should.equal('Updated Busqueda');
      updatedBusqueda.info.should.equal('This is the updated busqueda!!!');
    });

  });

  describe('DELETE /api/busquedas/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/busquedas/' + newBusqueda._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when busqueda does not exist', function(done) {
      request(app)
        .delete('/api/busquedas/' + newBusqueda._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
