'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var candidateCtrlStub = {
  index: 'candidateCtrl.index',
  show: 'candidateCtrl.show',
  create: 'candidateCtrl.create',
  update: 'candidateCtrl.update',
  destroy: 'candidateCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var candidateIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './candidate.controller': candidateCtrlStub
});

describe('Candidate API Router:', function() {

  it('should return an express router instance', function() {
    candidateIndex.should.equal(routerStub);
  });

  describe('GET /api/candidates', function() {

    it('should route to candidate.controller.index', function() {
      routerStub.get
        .withArgs('/', 'candidateCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/candidates/:id', function() {

    it('should route to candidate.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'candidateCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/candidates', function() {

    it('should route to candidate.controller.create', function() {
      routerStub.post
        .withArgs('/', 'candidateCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/candidates/:id', function() {

    it('should route to candidate.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'candidateCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/candidates/:id', function() {

    it('should route to candidate.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'candidateCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/candidates/:id', function() {

    it('should route to candidate.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'candidateCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
