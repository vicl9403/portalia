'use strict';

import mongoose from 'mongoose';

var CandidateSchema = new mongoose.Schema({
  email: String,
  phone: Number
});

export default mongoose.model('Candidate', CandidateSchema);
