'use strict';

var app = require('../..');
import request from 'supertest';

var newCandidate;

describe('Candidate API:', function() {

  describe('GET /api/candidates', function() {
    var candidates;

    beforeEach(function(done) {
      request(app)
        .get('/api/candidates')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          candidates = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      candidates.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/candidates', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/candidates')
        .send({
          name: 'New Candidate',
          info: 'This is the brand new candidate!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newCandidate = res.body;
          done();
        });
    });

    it('should respond with the newly created candidate', function() {
      newCandidate.name.should.equal('New Candidate');
      newCandidate.info.should.equal('This is the brand new candidate!!!');
    });

  });

  describe('GET /api/candidates/:id', function() {
    var candidate;

    beforeEach(function(done) {
      request(app)
        .get('/api/candidates/' + newCandidate._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          candidate = res.body;
          done();
        });
    });

    afterEach(function() {
      candidate = {};
    });

    it('should respond with the requested candidate', function() {
      candidate.name.should.equal('New Candidate');
      candidate.info.should.equal('This is the brand new candidate!!!');
    });

  });

  describe('PUT /api/candidates/:id', function() {
    var updatedCandidate;

    beforeEach(function(done) {
      request(app)
        .put('/api/candidates/' + newCandidate._id)
        .send({
          name: 'Updated Candidate',
          info: 'This is the updated candidate!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCandidate = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCandidate = {};
    });

    it('should respond with the updated candidate', function() {
      updatedCandidate.name.should.equal('Updated Candidate');
      updatedCandidate.info.should.equal('This is the updated candidate!!!');
    });

  });

  describe('DELETE /api/candidates/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/candidates/' + newCandidate._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when candidate does not exist', function(done) {
      request(app)
        .delete('/api/candidates/' + newCandidate._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
