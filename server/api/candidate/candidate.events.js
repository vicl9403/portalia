/**
 * Candidate model events
 */

'use strict';

import {EventEmitter} from 'events';
import Candidate from './candidate.model';
var CandidateEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CandidateEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Candidate.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CandidateEvents.emit(event + ':' + doc._id, doc);
    CandidateEvents.emit(event, doc);
  }
}

export default CandidateEvents;
