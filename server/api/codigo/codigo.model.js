'use strict';

import mongoose from 'mongoose';

var CodigoSchema = new mongoose.Schema({
  codigo: String,
  fechaInicial: Date,
  fechaFinal: Date,
  numeroOfrecido: Number,
  unidadOfrecida: String,
  paquete: String,
  usos: Number,
  usosRestantes: Number,
  email: String,
  redeemers: []
});

export default mongoose.model('Codigo', CodigoSchema);
