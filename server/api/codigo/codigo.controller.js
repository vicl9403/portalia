/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/codigos              ->  index
 * POST    /api/codigos              ->  create
 * GET     /api/codigos/:id          ->  show
 * PUT     /api/codigos/:id          ->  update
 * DELETE  /api/codigos/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Codigo from './codigo.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Codigos
export function index(req, res) {
  return Codigo.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function busqueda(req, res) {
  console.log("hola");
  return Codigo.findOne({
      'codigo': req.body.codigo
    }).exec()
    .then(codigo => {
      res.status(200).json(codigo);
    })
}

// Gets a single Codigo from the DB
export function show(req, res) {
  return Codigo.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Codigo in the DB
export function create(req, res) {
  return Codigo.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing Codigo in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Codigo.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Codigo from the DB
export function destroy(req, res) {
  return Codigo.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
