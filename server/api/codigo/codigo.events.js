/**
 * Codigo model events
 */

'use strict';

import {EventEmitter} from 'events';
import Codigo from './codigo.model';
var CodigoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CodigoEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Codigo.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CodigoEvents.emit(event + ':' + doc._id, doc);
    CodigoEvents.emit(event, doc);
  }
}

export default CodigoEvents;
