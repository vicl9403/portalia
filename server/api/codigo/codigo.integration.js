'use strict';

var app = require('../..');
import request from 'supertest';

var newCodigo;

describe('Codigo API:', function() {

  describe('GET /api/codigos', function() {
    var codigos;

    beforeEach(function(done) {
      request(app)
        .get('/api/codigos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          codigos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      codigos.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/codigos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/codigos')
        .send({
          name: 'New Codigo',
          info: 'This is the brand new codigo!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newCodigo = res.body;
          done();
        });
    });

    it('should respond with the newly created codigo', function() {
      newCodigo.name.should.equal('New Codigo');
      newCodigo.info.should.equal('This is the brand new codigo!!!');
    });

  });

  describe('GET /api/codigos/:id', function() {
    var codigo;

    beforeEach(function(done) {
      request(app)
        .get('/api/codigos/' + newCodigo._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          codigo = res.body;
          done();
        });
    });

    afterEach(function() {
      codigo = {};
    });

    it('should respond with the requested codigo', function() {
      codigo.name.should.equal('New Codigo');
      codigo.info.should.equal('This is the brand new codigo!!!');
    });

  });

  describe('PUT /api/codigos/:id', function() {
    var updatedCodigo;

    beforeEach(function(done) {
      request(app)
        .put('/api/codigos/' + newCodigo._id)
        .send({
          name: 'Updated Codigo',
          info: 'This is the updated codigo!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCodigo = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCodigo = {};
    });

    it('should respond with the updated codigo', function() {
      updatedCodigo.name.should.equal('Updated Codigo');
      updatedCodigo.info.should.equal('This is the updated codigo!!!');
    });

  });

  describe('DELETE /api/codigos/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/codigos/' + newCodigo._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when codigo does not exist', function(done) {
      request(app)
        .delete('/api/codigos/' + newCodigo._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
