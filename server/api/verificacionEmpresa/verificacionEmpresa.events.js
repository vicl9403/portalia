/**
 * VerificacionEmpresa model events
 */

'use strict';

import {EventEmitter} from 'events';
import VerificacionEmpresa from './verificacionEmpresa.model';
var VerificacionEmpresaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
VerificacionEmpresaEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  VerificacionEmpresa.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    VerificacionEmpresaEvents.emit(event + ':' + doc._id, doc);
    VerificacionEmpresaEvents.emit(event, doc);
  }
}

export default VerificacionEmpresaEvents;
