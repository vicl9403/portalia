'use strict';

var app = require('../..');
import request from 'supertest';

var newVerificacionEmpresa;

describe('VerificacionEmpresa API:', function() {

  describe('GET /api/verificacionEmpresas', function() {
    var verificacionEmpresas;

    beforeEach(function(done) {
      request(app)
        .get('/api/verificacionEmpresas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          verificacionEmpresas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      verificacionEmpresas.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/verificacionEmpresas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/verificacionEmpresas')
        .send({
          name: 'New VerificacionEmpresa',
          info: 'This is the brand new verificacionEmpresa!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newVerificacionEmpresa = res.body;
          done();
        });
    });

    it('should respond with the newly created verificacionEmpresa', function() {
      newVerificacionEmpresa.name.should.equal('New VerificacionEmpresa');
      newVerificacionEmpresa.info.should.equal('This is the brand new verificacionEmpresa!!!');
    });

  });

  describe('GET /api/verificacionEmpresas/:id', function() {
    var verificacionEmpresa;

    beforeEach(function(done) {
      request(app)
        .get('/api/verificacionEmpresas/' + newVerificacionEmpresa._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          verificacionEmpresa = res.body;
          done();
        });
    });

    afterEach(function() {
      verificacionEmpresa = {};
    });

    it('should respond with the requested verificacionEmpresa', function() {
      verificacionEmpresa.name.should.equal('New VerificacionEmpresa');
      verificacionEmpresa.info.should.equal('This is the brand new verificacionEmpresa!!!');
    });

  });

  describe('PUT /api/verificacionEmpresas/:id', function() {
    var updatedVerificacionEmpresa;

    beforeEach(function(done) {
      request(app)
        .put('/api/verificacionEmpresas/' + newVerificacionEmpresa._id)
        .send({
          name: 'Updated VerificacionEmpresa',
          info: 'This is the updated verificacionEmpresa!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedVerificacionEmpresa = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedVerificacionEmpresa = {};
    });

    it('should respond with the updated verificacionEmpresa', function() {
      updatedVerificacionEmpresa.name.should.equal('Updated VerificacionEmpresa');
      updatedVerificacionEmpresa.info.should.equal('This is the updated verificacionEmpresa!!!');
    });

  });

  describe('DELETE /api/verificacionEmpresas/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/verificacionEmpresas/' + newVerificacionEmpresa._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when verificacionEmpresa does not exist', function(done) {
      request(app)
        .delete('/api/verificacionEmpresas/' + newVerificacionEmpresa._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
