'use strict';

import mongoose from 'mongoose';

var VerificacionEmpresaSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('VerificacionEmpresa', VerificacionEmpresaSchema);
