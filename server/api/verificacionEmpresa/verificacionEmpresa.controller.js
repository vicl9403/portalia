/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/verificacionEmpresas              ->  index
 * POST    /api/verificacionEmpresas              ->  create
 * GET     /api/verificacionEmpresas/:id          ->  show
 * PUT     /api/verificacionEmpresas/:id          ->  update
 * DELETE  /api/verificacionEmpresas/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import VerificacionEmpresa from './verificacionEmpresa.model';
import email from '../email/email.controller';
import nodemailer from 'nodemailer';
var _jade = require('jade');
var fs = require('fs');
var P = require('bluebird')

var transporter = nodemailer.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  }
})

function VerificacionEmpresaEmail(data) 
{
  var template = './server/views/verificacionEmpresa.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) 
    {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } 
    else 
    {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = 
      {
        empresa: data,
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);
      return transporter.sendMail(
      {
        from: 'hola@portaliaplus.com', // sender address
        to: 'mkt@portaliaplus.com', // list of receivers
        // to: 'lopez.victor94@gmail.com',
        subject: "Verificación de empresa", // Subject line
        html: html // html body
      }, 
      function(error, info) 
      {
        if (error)
          return false;
        else 
          return true;
      });

    }
  });

}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of VerificacionEmpresas
export function index(req, res) {
  return VerificacionEmpresa.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single VerificacionEmpresa from the DB
export function show(req, res) {
  return VerificacionEmpresa.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new VerificacionEmpresa in the DB
export function create(req, res) {

  var sendMail = VerificacionEmpresaEmail(req.body);

  if(sendMail != false)
  {
    res.writeHead(200, {
        'Content-Type': 'text/plain'
      });
      res.end('ok');
  }
  else
  {
    res.writeHead(500, {
        'Content-Type': 'text/plain'
      });
      res.end('error');
  }
  

  // return VerificacionEmpresa.create(req.body)
  //   .then(respondWithResult(res,201))
  //   .catch(handleError(res));
}

// Updates an existing VerificacionEmpresa in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return VerificacionEmpresa.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a VerificacionEmpresa from the DB
export function destroy(req, res) {
  return VerificacionEmpresa.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
