/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/quotes_configurations              ->  index
 * POST    /api/quotes_configurations              ->  create
 * GET     /api/quotes_configurations/:id          ->  show
 * PUT     /api/quotes_configurations/:id          ->  update
 * DELETE  /api/quotes_configurations/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import QuotesConfiguration from './quotes_configuration.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      var updated = _.merge(entity, updates);
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of QuotesConfigurations
export function index(req, res) {
  return QuotesConfiguration.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets last element of QuotesConfigurations
export function last(req, res) {
  return QuotesConfiguration.findOne().sort( '-_id' ).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single QuotesConfiguration from the DB
export function show(req, res) {
  return QuotesConfiguration.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new QuotesConfiguration in the DB
export function create(req, res) {
  return QuotesConfiguration.create(req.body)
    .then(respondWithResult(res, 201));
}

// Updates an existing QuotesConfiguration in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return QuotesConfiguration.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a QuotesConfiguration from the DB
export function destroy(req, res) {
  return QuotesConfiguration.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
