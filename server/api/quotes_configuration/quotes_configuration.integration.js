'use strict';

var app = require('../..');
import request from 'supertest';

var newQuotesConfiguration;

describe('QuotesConfiguration API:', function() {

  describe('GET /api/quotes_configurations', function() {
    var quotesConfigurations;

    beforeEach(function(done) {
      request(app)
        .get('/api/quotes_configurations')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          quotesConfigurations = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      quotesConfigurations.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/quotes_configurations', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/quotes_configurations')
        .send({
          name: 'New QuotesConfiguration',
          info: 'This is the brand new quotesConfiguration!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newQuotesConfiguration = res.body;
          done();
        });
    });

    it('should respond with the newly created quotesConfiguration', function() {
      newQuotesConfiguration.name.should.equal('New QuotesConfiguration');
      newQuotesConfiguration.info.should.equal('This is the brand new quotesConfiguration!!!');
    });

  });

  describe('GET /api/quotes_configurations/:id', function() {
    var quotesConfiguration;

    beforeEach(function(done) {
      request(app)
        .get('/api/quotes_configurations/' + newQuotesConfiguration._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          quotesConfiguration = res.body;
          done();
        });
    });

    afterEach(function() {
      quotesConfiguration = {};
    });

    it('should respond with the requested quotesConfiguration', function() {
      quotesConfiguration.name.should.equal('New QuotesConfiguration');
      quotesConfiguration.info.should.equal('This is the brand new quotesConfiguration!!!');
    });

  });

  describe('PUT /api/quotes_configurations/:id', function() {
    var updatedQuotesConfiguration;

    beforeEach(function(done) {
      request(app)
        .put('/api/quotes_configurations/' + newQuotesConfiguration._id)
        .send({
          name: 'Updated QuotesConfiguration',
          info: 'This is the updated quotesConfiguration!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedQuotesConfiguration = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedQuotesConfiguration = {};
    });

    it('should respond with the updated quotesConfiguration', function() {
      updatedQuotesConfiguration.name.should.equal('Updated QuotesConfiguration');
      updatedQuotesConfiguration.info.should.equal('This is the updated quotesConfiguration!!!');
    });

  });

  describe('DELETE /api/quotes_configurations/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/quotes_configurations/' + newQuotesConfiguration._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when quotesConfiguration does not exist', function(done) {
      request(app)
        .delete('/api/quotes_configurations/' + newQuotesConfiguration._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
