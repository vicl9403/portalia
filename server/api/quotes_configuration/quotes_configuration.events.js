/**
 * QuotesConfiguration model events
 */

'use strict';

import {EventEmitter} from 'events';
import QuotesConfiguration from './quotes_configuration.model';
var QuotesConfigurationEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
QuotesConfigurationEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  QuotesConfiguration.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    QuotesConfigurationEvents.emit(event + ':' + doc._id, doc);
    QuotesConfigurationEvents.emit(event, doc);
  }
}

export default QuotesConfigurationEvents;
