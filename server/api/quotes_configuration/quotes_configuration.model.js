'use strict';

import mongoose from 'mongoose';

var QuotesConfigurationSchema = new mongoose.Schema({

  // Nombre del bloque a sensar
  name: String,

  // Campos para sensar el score de búsqueda de prospectos
  name_score        : { type: Number , required: true },
  products_score    : { type: Number , required: true },
  tags_score        : { type: Number , required: true },
  description_score : { type: Number , required: true },

  // Fecha de creación
  created_at : { type: Date , default : new Date() },

  // Score a utilizar
  min_score : { type: Number , required: true }

});

export default mongoose.model('QuotesConfiguration', QuotesConfigurationSchema);
