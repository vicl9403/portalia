'use strict';

import mongoose from 'mongoose';

var Schema = mongoose.Schema;

// var User = mongoose.model('User');
// var Quotation = require('../quotation/quotation.model.js');
//
// mongoose.model('Quotation');


var QuotationUserSchema = new mongoose.Schema({

  user: { type:Schema.ObjectId, ref:"User" , required : true },

  quote: { type:Schema.ObjectId, ref:"Quotation" , required: true },

  status: { type: String },

  wasSend : { type: Boolean , default: false },

  sendDate : { type: Date },

  tracing : { type: String },

  quoted : { type: Date },

  emailId : { type: String },

  emailStatus: { type: String },


});

export default mongoose.model('QuotationUser', QuotationUserSchema);
