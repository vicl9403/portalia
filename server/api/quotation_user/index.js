'use strict';

var express = require('express');
var controller = require('./quotation_user.controller');

var router = express.Router();

router.get('/countModel' , controller.getCountModel);


router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.post('/sendQuotesMail', controller.sendQuotesMail);

router.get('/getProspects/:id', controller.getProspects);

router.get('/getUserQuotes/:id' , controller.getUserQuotes);

router.get('/:since/:until' , controller.getProspectsByDate);

router.get('/report/:since/:until' , controller.getProspectsReportByDate);

module.exports = router;
