/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/quotation_user              ->  index
 * POST    /api/quotation_user              ->  create
 * GET     /api/quotation_user/:id          ->  show
 * PUT     /api/quotation_user/:id          ->  update
 * DELETE  /api/quotation_user/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import QuotationUser from './quotation_user.model';
import User from '../user/user.model';
import Quotation from '../quotation/quotation.model.js';
import moment from 'moment';
import config from "../../config/environment";

var mailin = require("mailin-api-node-js");

var client = new Mailin("https://api.sendinblue.com/v2.0", "wFKdNjXJx3ZYUL8M");

// Variable para consumir la API de Mandrill
let mandrill = require('../email/mandrill');

var ObjectId = require('mongodb').ObjectID;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      var updated = _.merge(entity, updates);
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of QuotationUsers
export function index(req, res) {
  return QuotationUser.find().exec()
    .then(respondWithResult(res))
}

// Gets a single QuotationUser from the DB
export function show(req, res) {
  return QuotationUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
}

// Creates a new QuotationUser in the DB
export function create(req, res) {
  return QuotationUser.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing QuotationUser in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return QuotationUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
}

// Deletes a QuotationUser from the DB
export function destroy(req, res) {
  return QuotationUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
}

export function getProspects( req , res ) {

  return QuotationUser.find({
    quote : req.params.id
  }).populate('quote')
    .populate('user', '-password -salt')
    .exec()
    .then( handleEntityNotFound(res) )
    .then(respondWithResult(res));
}

export function sendQuotesMail( req , res ) {

  QuotationUser.find({
    $and: [
      { "wasSend" : false },
      { "status" : { $ne : 'rejected' } }
    ]
  })
    .populate('user' , '-password -salt')
    .populate('quote')
    .exec()
    .then( function (res) {

      // Setear el lenguaje de las fechas a español
      moment.locale('es');

      let listOfQuotes = res;

      let quotesPerUser = {};

      // Este ciclo va a obtener todas las cotizaciones en las cuales el usuario resultó
      // estar como activo, y las va a poner en un índice particular
      for( let i = 0 ; i < listOfQuotes.length ; i ++ ) {

        if( quotesPerUser[ listOfQuotes[i].user._id ] == undefined ) {
          quotesPerUser[ listOfQuotes[i].user._id ]  = [] ;
        }
        quotesPerUser[ listOfQuotes[i].user._id ].push ({
          quote : listOfQuotes[i].quote,
          user : listOfQuotes[i].user
        });
      }

      // En este punto quotesPerUser ya contiene todas las cotizaciones organizadas por usuario

      for( let idUser in quotesPerUser ) {

        let user = quotesPerUser[idUser][0].user;

        let emailUser = '';

        if ( user.emailSec != null )
          emailUser = user.emailSec;
        else
          emailUser = user.email;

        let oportunidades = '';

        for (let i = 0; i < quotesPerUser[idUser].length; i++) {
          oportunidades += `-${i + 1} `+ quotesPerUser[idUser][i].quote.product + ' ' +  moment( quotesPerUser[idUser][i].quote.date ).format('D MMMM YYYY') + '<br><br>'
        }

        for (let i = 0; i < quotesPerUser[idUser].length; i++) {

          // Si son más de 3 correos deja de mandar para no saturar al usuario
          if( i >= 3 ) {
            break;
          }

          // Esta función manda el correo usando la API de Mandrill
          mandrill.sendQuoteEmail( quotesPerUser[idUser][i].user , quotesPerUser[idUser][i].quote );

        }



        // let data = {
        //   "id": 70,
        //   "to": emailUser,
        //   // "to": 'lopez.victor94@gmail.com',
        //   "from" : "cotizaciones@portaliaplus.mx",
        //   "attr": {
        //   "MES": moment().format('MMMM'),
        //     "EMPRESA": user.nombre,
        //     "OPORTUNIDAD": oportunidades ,
        //     "NUMEROOPORTUNIDADES": quotesPerUser[idUser].length,
        //     "FECHA" : moment().format('D MMMM YYYY'),
        //   },
        //   "headers": {
        //     "Content-Type": "text/html;charset=iso-8859-1",
        //       "X-param1": "value1",
        //       "X-param2": "value2",
        //       "X-Mailin-custom": "Oportunidades de negocio",
        //       "X-Mailin-tag": "Oportunidades de negocio"
        //   }
        // };
        //
        // client.send_transactional_template(data).on('complete', function (data) {
        //   let dataJSON = JSON.parse(data);
        //   // console.log( dataJSON );
        //
        //   for (let i = 0; i < quotesPerUser[idUser].length; i++) {
        //     QuotationUser.findOne({
        //       "user"    : user._id,
        //       "quote"   : quotesPerUser[idUser][i].quote._id
        //     }).exec()
        //       .then( function (entity) {
        //         entity.wasSend = true;
        //         entity.sendDate = new Date();
        //         entity.save();
        //       })
        //
        //   }
        //
        //
        // });

      }
    });

  return res.status(200).send('ok');
}

export function getUserQuotes(req , res) {

  let elementsToTake = 10;

  let page = 0;
  if( req.query.page != null )
    page = req.query.page;

  QuotationUser.find({
    $and : [
      { user : req.params.id },
      { status : { $ne : 'rejected' } }
    ]
  })
    .limit( elementsToTake )
    .skip( page * elementsToTake )
    .populate('user' , '-salt -password')
    .populate('quote')
    .sort('-_id')
    .exec( function (err, quotes) {

      if( err ) {
        return res.status(500).send('Error inesperado')
      }

      QuotationUser.count({
        $and : [
          { user : req.params.id },
          { status : { $ne : 'rejected' } }
        ]
      }).exec( function (err, count) {

        if( err ) {
          return res.status(500).send('Error ineseperado');
        }

        let response = {
          totalResults  : count,
          numPages      : Math.ceil(count / elementsToTake),
          quotes        : quotes,
          actualPage    : parseInt(page)
        };
        res.status(200).send(response);

      })
    })



}

export function getCountModel(req , res) {

  return QuotationUser.count()
    .exec( function (err, count) {

      if( err ) {
        return res.status(500).send(err);
      }
      return res.status(200).send( { count : count } );

    })
}

export function getProspectsByDate( req , res ) {

  QuotationUser.find({
    "_id" : {
      $gte: ObjectId(Math.floor((new Date(req.params.since))/1000).toString(16) + "0000000000000000") ,
      $lt: ObjectId(Math.floor((new Date(req.params.until))/1000).toString(16) + "0000000000000000")
    }
  })
    .populate('user', '-salt -password')
    .populate('quote')
    .sort('-_id')
    .exec( function (err, items) {
      if ( err ) {
        console.log( err );
        return res.status(500).send(err);
      }

      return res.status(200).send(items);
    })
}

export function getProspectsReportByDate( req , res ) {

  QuotationUser.aggregate(
    [
      {
        $group:
          {
            _id: "$user",
            count: {$sum: 1}
          },
      },
      {
        $lookup:
          {
            from: 'users',
            localField: "user",
            foreignField: 'ObjectId(_id)',
            as: 'user_quote'
          }
      }
    ]
  )
    .exec( function (err, report) {
      if ( err ) {
        console.log( err );
        return res.status(500).send(err);
      }
      console.log( report );
      return res.status(200).send(report);
    })

}
