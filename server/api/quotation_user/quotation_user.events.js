/**
 * QuotationUser model events
 */

'use strict';

import {EventEmitter} from 'events';
import QuotationUser from './quotation_user.model';
var QuotationUserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
QuotationUserEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  QuotationUser.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    QuotationUserEvents.emit(event + ':' + doc._id, doc);
    QuotationUserEvents.emit(event, doc);
  }
}

export default QuotationUserEvents;
