'use strict';

import mongoose from 'mongoose';

var EventSchema = new mongoose.Schema({
  title: String,
  start: Date,
  description: String,
  uerl: String,
  lugar: String,
  sector: String,
  imagen: String,
  end: Date,
  destacado: {
    type: Boolean,
    default: false
  }
});

export default mongoose.model('Event', EventSchema);
