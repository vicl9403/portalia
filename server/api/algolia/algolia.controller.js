'use strict';

import _ from 'lodash';
import config from '../../config/environment';

import User from '../user/user.model';
import moment from 'moment';
import algoliasearch from "algoliasearch";

let https = require('https');

var request = require('request');

var algoliaClient = algoliasearch( config.algoliaconfig.idApplication , config.algoliaconfig.adminKey );

export function getSearchesCount(req,res) {

  var options = {
    url: 'https://analytics.algolia.com/1/searches/users/popular?startAt=' + moment().subtract(1,'month').format('X') + '&endAt=' + moment().format('X'),
    headers: {
      'X-Algolia-API-Key' : config.algoliaconfig.adminKey,
      'X-Algolia-Application-Id' : config.algoliaconfig.idApplication
    }
  };

  request(options, function (err, result, body) {

    if( err )
      return res.status(500).send(err);

    return res.status(200).send( body );
  });




}


export function getSearchKeys(req, res) {
  return res.status(200).send(
    {
      'id' : config.algoliaconfig.idApplication,
      'key' : config.algoliaconfig.searchOnlyKey
    }
  );
}
