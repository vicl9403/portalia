'use strict';

var express = require('express');

var controller = require('./algolia.controller');

import * as auth from '../../auth/auth.service';


var router = express.Router();

router.get('/getSearchKeys',  controller.getSearchKeys );

router.get('/getPopularWords' , controller.getSearchesCount );
module.exports = router;
