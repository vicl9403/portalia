/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/preUsers              ->  index
 * POST    /api/preUsers              ->  create
 * GET     /api/preUsers/:id          ->  show
 * PUT     /api/preUsers/:id          ->  update
 * DELETE  /api/preUsers/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import PreUser from './preUser.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of PreUsers
export function index(req, res) {
  return PreUser.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single PreUser from the DB
export function show(req, res) {
  return PreUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new PreUser in the DB
export function create(req, res) {
  return PreUser.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

export function busqueda(req, res) {
  return PreUser.findOne({
      'email': req.body.email
    }).exec()
    .then(user => {
      res.status(200).json(user);
    })
}

// Updates an existing PreUser in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return PreUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a PreUser from the DB
export function destroy(req, res) {
  return PreUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
