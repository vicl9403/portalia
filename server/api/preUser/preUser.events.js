/**
 * PreUser model events
 */

'use strict';

import {EventEmitter} from 'events';
import PreUser from './preUser.model';
var PreUserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PreUserEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  PreUser.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PreUserEvents.emit(event + ':' + doc._id, doc);
    PreUserEvents.emit(event, doc);
  }
}

export default PreUserEvents;
