/**
 * UpdatesOnUser model events
 */

'use strict';

import {EventEmitter} from 'events';
import UpdatesOnUser from './updatesOnUser.model';
var UpdatesOnUserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
UpdatesOnUserEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  UpdatesOnUser.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    UpdatesOnUserEvents.emit(event + ':' + doc._id, doc);
    UpdatesOnUserEvents.emit(event, doc);
  }
}

export default UpdatesOnUserEvents;
