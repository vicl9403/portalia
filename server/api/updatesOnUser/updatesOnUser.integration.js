'use strict';

var app = require('../..');
import request from 'supertest';

var newUpdatesOnUser;

describe('UpdatesOnUser API:', function() {

  describe('GET /api/updatesOnUsers', function() {
    var updatesOnUsers;

    beforeEach(function(done) {
      request(app)
        .get('/api/updatesOnUsers')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          updatesOnUsers = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      updatesOnUsers.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/updatesOnUsers', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/updatesOnUsers')
        .send({
          name: 'New UpdatesOnUser',
          info: 'This is the brand new updatesOnUser!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newUpdatesOnUser = res.body;
          done();
        });
    });

    it('should respond with the newly created updatesOnUser', function() {
      newUpdatesOnUser.name.should.equal('New UpdatesOnUser');
      newUpdatesOnUser.info.should.equal('This is the brand new updatesOnUser!!!');
    });

  });

  describe('GET /api/updatesOnUsers/:id', function() {
    var updatesOnUser;

    beforeEach(function(done) {
      request(app)
        .get('/api/updatesOnUsers/' + newUpdatesOnUser._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          updatesOnUser = res.body;
          done();
        });
    });

    afterEach(function() {
      updatesOnUser = {};
    });

    it('should respond with the requested updatesOnUser', function() {
      updatesOnUser.name.should.equal('New UpdatesOnUser');
      updatesOnUser.info.should.equal('This is the brand new updatesOnUser!!!');
    });

  });

  describe('PUT /api/updatesOnUsers/:id', function() {
    var updatedUpdatesOnUser;

    beforeEach(function(done) {
      request(app)
        .put('/api/updatesOnUsers/' + newUpdatesOnUser._id)
        .send({
          name: 'Updated UpdatesOnUser',
          info: 'This is the updated updatesOnUser!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedUpdatesOnUser = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedUpdatesOnUser = {};
    });

    it('should respond with the updated updatesOnUser', function() {
      updatedUpdatesOnUser.name.should.equal('Updated UpdatesOnUser');
      updatedUpdatesOnUser.info.should.equal('This is the updated updatesOnUser!!!');
    });

  });

  describe('DELETE /api/updatesOnUsers/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/updatesOnUsers/' + newUpdatesOnUser._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when updatesOnUser does not exist', function(done) {
      request(app)
        .delete('/api/updatesOnUsers/' + newUpdatesOnUser._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
