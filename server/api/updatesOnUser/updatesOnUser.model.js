'use strict';

import mongoose from 'mongoose';
// var mongoose = require("mongoose");
var Schema = mongoose.Schema;

import User from '../user/user.model';

var UpdatesOnUserSchema = new mongoose.Schema({
	user: { type: Array },
	userUpdated : { type: Array },
	// user: [ { type: Schema.Types.ObjectId, ref: 'User'}],
	// userUpdated: [ { type: Schema.Types.ObjectId, ref: 'User'}],
	type: String,
	date: { type: Date, default: Date.now  },
});

export default mongoose.model('UpdatesOnUser', UpdatesOnUserSchema);
