/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/updatesOnUsers              ->  index
 * POST    /api/updatesOnUsers              ->  create
 * GET     /api/updatesOnUsers/:id          ->  show
 * PUT     /api/updatesOnUsers/:id          ->  update
 * DELETE  /api/updatesOnUsers/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import UpdatesOnUser from './updatesOnUser.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of UpdatesOnUsers
export function index(req, res) {
  return UpdatesOnUser.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single UpdatesOnUser from the DB
export function show(req, res) {
  return UpdatesOnUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets
export function getByDate(req, res)
{

  return UpdatesOnUser.find({
    $and: [{
      'date': {
        $gte: req.body.start,
        $lt:  req.body.end,
      }
    }]
  }).exec()
  .then(logs => {
    res.status(200).json(logs);
  })

}


// Creates a new UpdatesOnUser in the DB
export function create(req, res) {
  return UpdatesOnUser.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing UpdatesOnUser in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return UpdatesOnUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a UpdatesOnUser from the DB
export function destroy(req, res) {
  return UpdatesOnUser.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
