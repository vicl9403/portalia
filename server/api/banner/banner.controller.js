/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/banners              ->  index
 * POST    /api/banners              ->  create
 * GET     /api/banners/:id          ->  show
 * PUT     /api/banners/:id          ->  update
 * DELETE  /api/banners/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Banner from './banner.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {

      let updated = _.merge(entity, updates);

      if( updated.tags != updates.tags )
        updated.tags =updates.tags;

      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Banners
export function index(req, res) {
  return Banner.find().exec()
    .then(respondWithResult(res))
}

// Gets a single Banner from the DB
export function show(req, res) {
  return Banner.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
}

// Creates a new Banner in the DB
export function create(req, res) {
  return Banner.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing Banner in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Banner.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
}

// Deletes a Banner from the DB
export function destroy(req, res) {
  return Banner.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
}

export function findKeyword( req, res ) {
  let keyword = req.params.keyword;

  Banner.find(
    {
      $and : [
        {
          $text :
          {
            $search : keyword,
            $caseSensitive: false,
            $diacriticSensitive : false,
            $language: 'es'
          }
        },
        { "status" : { $ne : 'deleted' } }
      ]
    },
    { score : { $meta: "textScore" } }
  )
    .sort({ score : { $meta : 'textScore' } })
    .exec(function(err, results) {
      if( err ) {
        res.status(500).send(err);
      }
      res.status(200).send(results)
    });
}

export function findKeywordActiveBanners( req, res ) {
  let keyword = req.params.keyword;

  console.log('Try Banners');
  console.log( keyword );

  Banner.find(
    {
      $and : [
        {
            $text :
              {
                $search : keyword,
                $caseSensitive: false,
                $diacriticSensitive : false,
                $language: 'es'
              }
        },
        { "status" : 'approved' },
        { "isActive" : true },
        { "until" : { $gte: new Date() } }
      ]
    },
    { score : { $meta: "textScore" } }
  )
    .sort({ score : { $meta : 'textScore' } })
    .exec(function(err, results) {
      if( err ) {
        res.status(500).send(err);
      }
      res.status(200).send(results)
    });
}

export function getUserBanners( req, res ) {
  Banner.find({
    $and: [
      { "status" : { $ne : 'deleted' } },
      { user : req.params.id }
    ]
  })
    .sort('-_id')
    .exec( function (err, banners) {
    if( err ) {
      res.status(500).send(err);
    }

    res.status(200).send( banners );
  })
}

export function getBannersByStatus( req, res ) {

  let page = req.query.page ;
  let limitPerpage = 10;

  Banner.find({
    "status" : req.params.status
  }).populate('user','-password -salt')
    .limit( limitPerpage )
    .skip(page * limitPerpage)
    .sort('-_id')
    .exec( function (err, banners) {

      if( err ) {
        res.status(402).send(err);
      }

      Banner.count({
        "status" : req.params.status
      }).exec( function(err, count) {

        if( err ) {
          res.status(402).send(err);
        }

        let data = {
          count : count,
          items : banners
        }

        res.status(200).send(data);

      });



  })
}

export function getAlgoliaPrice( req, res ) {



}
