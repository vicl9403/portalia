'use strict';

import mongoose from 'mongoose';

var Schema = mongoose.Schema;

var User = mongoose.model('User');

var BannerSchema = new mongoose.Schema({

  since: { type: Date , required: true },
  until: { type: Date , required: true },

  tags: { type: Array , required : true },

  url : { type: String , required : true },

  img1: { type: String },
  img2: { type: String },

  user: { type: Schema.ObjectId, ref:"User" },

  status: { type: String },

  price: { type: Number },

  isActive: { type: Boolean, default : false }

});

export default mongoose.model('Banner', BannerSchema);
