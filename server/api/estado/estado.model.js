'use strict';

import mongoose from 'mongoose';

var EstadoSchema = new mongoose.Schema({
  name: String,
  active: Boolean,
  municipios: [{
    name: String,
    active: Boolean
  }]
});

export default mongoose.model('Estado', EstadoSchema);
