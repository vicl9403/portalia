/**
 * Estado model events
 */

'use strict';

import {EventEmitter} from 'events';
import Estado from './estado.model';
var EstadoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
EstadoEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Estado.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    EstadoEvents.emit(event + ':' + doc._id, doc);
    EstadoEvents.emit(event, doc);
  }
}

export default EstadoEvents;
