'use strict';

var express = require('express');
var controller = require('./login.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.patch('/logout/:idUser', controller.closeUserLoggins);
router.delete('/:id', controller.destroy);

router.get('/:since/:until' , controller.getLogginsByDate);

module.exports = router;
