/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/logins              ->  index
 * POST    /api/logins              ->  create
 * GET     /api/logins/:id          ->  show
 * PUT     /api/logins/:id          ->  update
 * DELETE  /api/logins/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Login from './login.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      var updated = _.merge(entity, updates);
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

export function closeUserLoggins( req, res) {
  let idUser = req.params.idUser;

  Login.find({
    $and : [
      { user :  idUser  },
      { finishedAt : null }
    ]
  })
  .exec()
  .then (function (loggins) {
      if ( loggins.length > 0 ) {

        for( let i = 0 ; i < loggins.length ; i ++ ) {
          loggins[i].finishedAt = new Date();
          loggins[i].save();
        }
      }
      return res.send(200);

  });
}

// Gets a list of Logins
export function index(req, res) {
  return Login.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Login from the DB
export function show(req, res) {
  return Login.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Login in the DB
export function create(req, res) {
  return Login.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Login in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Login.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Login from the DB
export function destroy(req, res) {
  return Login.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


export function getLogginsByDate( req , res ) {

  Login.find({
    "startAt" : {
      $gte: new Date(req.params.since),
      $lt: new Date(req.params.until)
    }
  })
    .populate('user', '-salt -password')
    .sort('-_id')
    .exec( function (err, items) {
      if ( err ) {
        console.log( err );
        return res.status(500).send(err);
      }

      return res.status(200).send(items);
    })
}
