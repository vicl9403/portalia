'use strict';

import mongoose from 'mongoose';

var Schema = mongoose.Schema;

var User = mongoose.model('User');

var LoginSchema = new mongoose.Schema({

  user: { type: Schema.ObjectId, ref:"User" },

  startAt: { type: Date , default: new Date() },

  finishedAt : { type: Date },

  active: Boolean

});

export default mongoose.model('Login', LoginSchema);
