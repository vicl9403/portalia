/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/product_categories              ->  index
 * POST    /api/product_categories              ->  create
 * GET     /api/product_categories/:id          ->  show
 * PUT     /api/product_categories/:id          ->  update
 * DELETE  /api/product_categories/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import ProductCategory from './product_category.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    if(entity) {
      var updated = _.merge(entity, updates);
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of ProductCategorys
export function index(req, res) {
  return ProductCategory.find().exec()
    .then(respondWithResult(res))
}

// Gets a single ProductCategory from the DB
export function show(req, res) {
  return ProductCategory.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
}

// Creates a new ProductCategory in the DB
export function create(req, res) {
  return ProductCategory.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing ProductCategory in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  console.log( req.body );
  return ProductCategory.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
}

export function updateSubcategories(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  ProductCategory.findById(req.params.id).exec()
    .then(function ( entity ) {
      entity.subCategories = req.body;
      entity.save();
      return res.send(200, 'success');
    })
}

// Deletes a ProductCategory from the DB
export function destroy(req, res) {
  return ProductCategory.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
}
