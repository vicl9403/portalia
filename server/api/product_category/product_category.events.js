/**
 * ProductCategory model events
 */

'use strict';

import {EventEmitter} from 'events';
import ProductCategory from './product_category.model';
var ProductCategoryEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ProductCategoryEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  ProductCategory.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    ProductCategoryEvents.emit(event + ':' + doc._id, doc);
    ProductCategoryEvents.emit(event, doc);
  }
}

export default ProductCategoryEvents;
