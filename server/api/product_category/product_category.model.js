'use strict';

import mongoose from 'mongoose';

var ProductCategorySchema = new mongoose.Schema({

  name: String,

  subCategories : [
    {
      name: { type: String },
      active: { type: Boolean }
    }
  ],

  search: [
    {
      name: { type: String },
      active: { type: Boolean }
    }
  ],

  active: Boolean

});

export default mongoose.model('ProductCategory', ProductCategorySchema);
