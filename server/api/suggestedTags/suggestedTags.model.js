'use strict';

import mongoose from 'mongoose';

var SuggestedTagsSchema = new mongoose.Schema({
    category_name: String,
    tags: [],

});

export default mongoose.model('SuggestedTags', SuggestedTagsSchema);
