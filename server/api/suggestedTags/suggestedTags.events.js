/**
 * SuggestedTags model events
 */

'use strict';

import {EventEmitter} from 'events';
import SuggestedTags from './suggestedTags.model';
var SuggestedTagsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
SuggestedTagsEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  SuggestedTags.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    SuggestedTagsEvents.emit(event + ':' + doc._id, doc);
    SuggestedTagsEvents.emit(event, doc);
  }
}

export default SuggestedTagsEvents;
