/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/suggestedTags              ->  index
 * POST    /api/suggestedTags              ->  create
 * GET     /api/suggestedTags/:id          ->  show
 * PUT     /api/suggestedTags/:id          ->  update
 * DELETE  /api/suggestedTags/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import SuggestedTags from './suggestedTags.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of SuggestedTagss
export function index(req, res) {
  return SuggestedTags.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single SuggestedTags from the DB
export function show(req, res) {
  return SuggestedTags.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new SuggestedTags in the DB
export function create(req, res) {
  return SuggestedTags.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing SuggestedTags in the DB
export function update(req, res) {

    if (req.body._id) {
        delete req.body._id;
    }
    SuggestedTags.findById(req.params.id, function(err, entity) {
        if (!entity)
            return next(new Error('Could not load Document'));
        else {
            // do your updates here
            entity.tags = req.body.tags;

            entity.save(function(err) {
                if (err)
                    return respondWithResult(res, 200)
                else
                    return respondWithResult(res, 500)
            });
        }
    }).then(respondWithResult(res));


  // return SuggestedTags.findById(req.params.id).exec()
  //   .then(handleEntityNotFound(res))
  //   .then(saveUpdates({ 'tags' : ['test' ] }))
  //   .then(respondWithResult(res))
  //   .catch(handleError(res));
}


// Deletes a SuggestedTags from the DB
export function destroy(req, res) {
  return SuggestedTags.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

// findByField Function
export function findByField(req, res) {
    return SuggestedTags.findOne(req.body).exec()
        .then(handleEntityNotFound(res))
        .then(respondWithResult(res))
        .catch(handleError(res));
}
