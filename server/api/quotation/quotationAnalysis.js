'use strict';

import User from '../user/user.model';
import Quotation from './quotation.model';
import QuotationUser from '../quotation_user/quotation_user.model'

var plural = require('pluralize-es');

class QuoteAnalysis {

  constructor( minScore ) {

    // Score Mínimo para poder mandar la petición
    this.minScore = minScore;

    // Artículos a eliminar en el preprocesamiento de las palabras
    // this.articles = [
    //   'el',
    //   'la',
    //   'los',
    //   'las',
    //   'un',
    //   'una',
    //   'unos',
    //   'unas',
    //   'lo',
    //   'de'
    // ]

  }

  removeSpecials( str ) {
    // Definimos los caracteres que queremos eliminar
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

    // Los eliminamos todos
    for (var i = 0; i < specialChars.length; i++) {
      str= str.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
    }

    // Cambiar a lower case todo
    str = str.toLowerCase();

    // Quitar los acentos
    str = str.replace(/á/gi,"a");
    str = str.replace(/é/gi,"e");
    str = str.replace(/í/gi,"i");
    str = str.replace(/ó/gi,"o");
    str = str.replace(/ú/gi,"u");
    return str;
  }

  removeArticles( str ) {

    let arrayStr = str.split(' ');
    let withoutArticles = '';

    withoutArticles = arrayStr.map( word => {
      if( this.articles.indexOf(word) == - 1 ) {
        return  word;
      }
      return '';
    });

    withoutArticles.join(' ');

    return withoutArticles;

  }

  findUsersToApply( word ) {

    return User.find({
      $text: {
        $search: word,
        $diacriticSensitive: false,
        $language: 'es'
      }
    },
    {
      "score": { $meta : "textScore" }
    }).exec()
    .then( users => users );

  }

  findValueInArrayByKey(arr, key , id ) {

    for( let i=0 ; i < arr.length ; i++ ) {
      if( arr[i][key].toString() == id.toString() ) {
        return true;
      }
    }

    return false;
  }

  setUserProspects( idQuote , users , customScore = false ) {

      let minScore = this.minScore;

      QuotationUser.remove({
        quote : idQuote
      }).exec()
        .then( function (res) {
          for( let i = 0 ; i < users.length ; i++ ) {

            // Checar si cumple con el score mínimo
            if( users[i].score >= minScore ) {
              // Ver si la cotización ya se le había asignado al usuario anteriormente
              QuotationUser.findOne({
                $and: [
                  {user: users[i]._id},
                  {quote: idQuote}
                ]
              }).exec()
                .then(function (res) {
                  if (res == null) {
                    let usrQuote = new QuotationUser({
                      user: users[i]._id,
                      quote: idQuote,
                      status: 'approved',
                      wasSend: false
                    });

                    usrQuote.save();
                  }

                });
            }
          }

        })

  }

}

function getActiveQuotes() {
  return Quotation.find({
    "status" : "approved"
  })
    .exec()
    .then( (quotes) => quotes)
}

export function processAnalysis( minScore , customQuote = null ) {
  let analysis = new QuoteAnalysis( minScore );
  let quotes;

  if( customQuote == null ) {

    getActiveQuotes()
      .then(function( data ) {
          quotes = data;
          //  Iterar sobre todas las cotizaciones
          quotes.forEach(function( quote ) {

            // Obtener el producto con el texto original
            let product = quote.product;

            if( quote.score == null || quote.score == 0 ) {
              quote.score = minScore;
              quote.save();
            }
            // Analizar la cotización con el texto original
            analysis.findUsersToApply(product)
              .then( function(res) {
                let users = res;

                // Eliminar las relaciones anteriores
                QuotationUser.remove({
                  'quote_id' : quote._id
                }).exec()
                  .then( function (res) {
                    if (users.length > 0) {
                      analysis.setUserProspects(quote._id, users , true );
                    }
                  });

              });
          })
      });
  }
  else {

    let quote = customQuote;

    // Obtener el producto con el texto original
    let product = quote.product;

    // Analizar la cotización con el texto original
    analysis.findUsersToApply(product)
      .then(function (res) {

        let users = res;

        // Eliminar las relaciones anteriores
        QuotationUser.remove({
          'quote_id' : quote._id
        }).exec()
          .then( function (res) {
            if (users.length > 0) {
              analysis.setUserProspects(quote._id, users , true );
            }
          });

      });
  }


}


