/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/quotes              ->  index
 * POST    /api/quotes              ->  create
 * GET     /api/quotes/:id          ->  show
 * PUT     /api/quotes/:id          ->  update
 * DELETE  /api/quotes/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Quotation from './quotation.model';
import User from '../user/user.model';
import nodemailer from 'nodemailer';



import * as quotationAnalysis from './quotationAnalysis'
let quotationConfiguration = require('../quotes_configuration/quotes_configuration.controller');

import config from '../../config/environment';

import QuotesConfiguration from '../quotes_configuration/quotes_configuration.model';

import QuoteUser from '../quotation_user/quotation_user.model';

// Variable para consumir la API de Mandrill
let mandrill = require('../email/mandrill');

var pagination = require('mongoose-pagination');
var util = require('util');
var fs = require('fs');
var _jade = require('jade');
var mailin = require("mailin-api-node-js");
var client = new Mailin("https://api.sendinblue.com/v2.0", "wFKdNjXJx3ZYUL8M");
var moment = require('moment');
var pagination = require('mongoose-pagination');
var asyncLoop = require('node-async-loop');
var request = require("request");
var transporter = nodemailer.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portalia0plus.com',
    pass: 'Qwerty123$%'
  },
  tls: {
    rejectUnauthorized: false
  }
});

var ObjectId = require('mongodb').ObjectID;

var schedule = require('node-schedule');


// Este cron se ejecuta diario y genera el match entre
// las cotizaciones que estén activas y los usuarios que apliquen para
// las cotizaciones
var quotesCron = schedule.scheduleJob('0 0 * * *', function() {

  QuotesConfiguration.findOne().sort( '-_id' ).exec()
      .then( ( res ) => {
      quotationAnalysis.processAnalysis( res.min_score );
  })

});

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

export function sendQuotesAnalysis( req , res ) {

  // Si viene el parámetro utilizarlo
  if( req.params.minScore != undefined ) {
    console.log( 'with param'+ req.params.minScore);
    quotationAnalysis.processAnalysis( req.params.minScore );

  }
  else {
    QuotesConfiguration.findOne().sort( '-_id' ).exec()
        .then( ( res ) => {
          quotationAnalysis.processAnalysis( res.min_score );
        })
  }


  res.status(200).send('ok');

}

export function sendQuotesAnalysisForOneQuote( req , res ) {

  Quotation.findById( req.params.quote )
    .exec()
    .then( function(quote) {
      quotationAnalysis.processAnalysis( req.params.minScore , quote );
      quote.score = req.params.minScore;
      quote.save();
    });

  res.status(200).send('ok');

}

// Esta función manda un mail automáticamente al usuario que se contacto
function sendQuoteMailToSpecificUser( quote ) {


  if( quote.user[0] != undefined ) {
    console.log( quote.user[0] );
    User.findById( quote.user[0]._id )
      .exec( function (err , user) {
        // Enviar correo usando la API de Mandrill
        mandrill.sendQuoteEmail( user , quote );
      })

  }

}


function saveUpdates(updates) {

  // Si tiene un usuario y cambia a aprobada se manda automáticamente el mail
  if( updates.user != undefined && updates.status == 'approved' ) {
    // Este bloque se ejecuta porque fué un contacto directo dentro de bussinesslist
    sendQuoteMailToSpecificUser( updates );
  }

  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Quotations
export function index(err, res) {

  Quotation.find({}, function(err, quotes) {
    User.populate(quotes, {
      path: "user"
    }, function(err, quotes) {
      res.status(200).send(quotes);
    })
  });

}

// Gets a single Quotation from the DB
export function show(req, res) {
  return Quotation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Quotation from the DB
export function newQuotes(err, res) {


  return Quotation.find(
    { "status" : null }
  ).populate('user', 'nombre , _id'   )
    .exec()
    .then(respondWithResult(res));

}

export function approvedQuotes(err, res){

  Quotation.find({
    "status" : 'approved'
  }, function(err, quotes) {
    User.populate(quotes, {
      path: "user"
    }, function(err, quotes) {
      res.status(200).send(quotes);
    })
  });
}

export function quotesByCategory(req, res){
  Quotation.find(
    {
      $and : [
        { "status" : 'approved' },
        { "category" : req.params.category }
      ]
    }, function(err, quotes) {
      res.status(200).send(quotes);
    })
}


// Creates a new Quotation in the DB
export function create(req, res) {
  return Quotation.create(req.body)
    .then(respondWithResult(res, 201))
}

// Updates an existing Quotation in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Quotation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
}

export function setQuotationFinished( req, res ) {
  return Quotation.findById(req.params.id)
    .exec()
    .then((res) => {
      res.status = 'done';
      res.save()
        .then(respondWithResult("good"))
    })
}


export function updateProspects(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Quotation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then( (entity) => {
      entity.prospects = req.body;
      entity.save()
        .then( respondWithResult(res) );
    })
}

// Deletes a Quotation from the DB
export function destroy(req, res) {
  return Quotation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

//Trae los contactos activas
export function activeContacts(req, res) {
  return Quotation.find({
      $and: [{
        'active': req.body.active
      }, {
        'user': {
          $not: {
            $size: 0
          }
        }
      }]
    })
    .paginate(req.body.requestedPage, 10, function(err, docs, total) {
      var respuesta = {
        'totalResults': total,
        'currentPage': req.body.requestedPage,
        'totalPages': Math.ceil(total / 10),
        'documents': docs
      };
      res.status(200).json(respuesta);
    });
}

//Trae los quotes activas
export function activeQuotes(req, res) {
  return Quotation.find({
      $and: [{
        'active': req.body.active
      }, {
        'user': {
          $size: 0
        }
      }]
    })
    .paginate(req.body.requestedPage, 10, function(err, docs, total) {
      var respuesta = {
        'totalResults': total,
        'currentPage': req.body.requestedPage,
        'totalPages': Math.ceil(total / 10),
        'documents': docs
      };
      res.status(200).json(respuesta);
    });
}

//Trae las contactos en un estado específico
export function searchContacts(req, res) {
  return Quotation.find({
      $and: [{
        'status': req.body.status
      }, {
        'user': {
          $not: {
            $size: 0
          }
        }
      }]
    })
    .paginate(req.body.requestedPage, 10, function(err, docs, total) {
      var respuesta = {
        'totalResults': total,
        'currentPage': req.body.requestedPage,
        'totalPages': Math.ceil(total / 10),
        'documents': docs
      };
      res.status(200).json(respuesta);
    });
}

//Trae las cotizaciones en un estado específico
export function searchQuotes(req, res) {
  return Quotation.find({
      $and: [{
        'status': req.body.status
      }, {
        'user': {
          $size: 0
        }
      }]
    })
    .paginate(req.body.requestedPage, 10, function(err, docs, total) {
      var respuesta = {
        'totalResults': total,
        'currentPage': req.body.requestedPage,
        'totalPages': Math.ceil(total / 10),
        'documents': docs
      };
      res.status(200).json(respuesta);
    });
}

//Cambia el estado de la cotización
export function changeContactStatus(req, res) {
  return Quotation.findById(req.body.id).exec()
    .then(quote => {
      quote.status = req.body.status;
      quote.active = req.body.active;
      quote.save();
      if (req.body.status == 'approved') {
        sendMail(quote);
      }
      res.status(200).json(quote);
    })
    .catch(handleError(res));
}

//Cambia el estado de la cotización
export function changeQuoteStatus(req, res) {
  console.log(req.body);
  return Quotation.findById(req.body.id).exec()
    .then(quote => {
      quote.status = req.body.status;
      quote.active = req.body.active;
      quote.save();
      if (req.body.status == 'approved') {
        sendMasive(quote);
      }
      res.status(200).json(quote);
    })
    .catch(err => {
      console.log(err);
      res.status(500).end();
    });
}

//Trae las cotizaciones dirigidas a un usuario
export function getUserQuotes(req, res) {
  return Quotation.find({
      $and: [{
        'status': 'approved'
      }, {
        'user': req.params.id
      }]
    }).exec()
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => {
      console.log(err);
      res.status(500).end();
    });
}

//Trae las cotizaciones dirigidas a un usuario
export function getSectorQuotes(req, res) {
  return Quotation.find({
    $and: [{
      'status': 'approved'
    }, {
      'sector': req.params.sector
    }]
  }).exec()
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => {
      console.log(err);
      res.status(500).end();
    });
}

//Trae las destacadas
export function getFeaturedQuotes(req, res) {
  return Quotation.find(
      {
        $or : [
          { 'featured': true },
          { 'status' : 'done' }
        ]
      }
    )
    .sort('-_id')
    .limit(20)
    .exec()
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => {
      console.log(err);
      res.status(500).end();
    })
}

//Trae todas con paginado
export function getAllQuotes(req, res) {
  var and = [];
  var categorias;
  var estados;
  var sectores;

  if (req.body.state != undefined) {
    and.push({
      state: new RegExp(req.body.state, 'i')
    })
  }

  if (req.body.sector != undefined) {
    and.push({
      sector: new RegExp(req.body.sector, 'i')
    })
  }

  if (req.body.category != undefined) {
    and.push({
      category: new RegExp(req.body.category, 'i')
    })
  }

  if (req.body.sortDate != undefined) {
    if (req.body.sortDate == 'asc') {
      var sorting = 'createdAt'
    } else if (req.body.sortDate == 'desc') {
      var sorting = '-createdAt'
    }
  } else {
    var sorting = 'createdAt';
  }

  if(and.length == 0){
    var query = {
      $or : [
        { 'status': 'approved' },
        { 'status': 'done' },
      ]
    }

    Quotation.distinct('state').exec()
      .then(results => {
        estados = results;
      })

    Quotation.distinct('sector').exec()
      .then(results => {
        sectores = results;
      })

    Quotation.distinct('category').exec()
      .then(results => {
        categorias = results;
      })
  } else {
    Quotation.distinct('state', {
      $and: and
    }).exec()
      .then(results => {
        estados = results;
      })

    Quotation.distinct('sector', {
      $and: and
    }).exec()
      .then(results => {
        sectores = results;
      })

    Quotation.distinct('category', {
      $and: and
    }).exec()
      .then(results => {
        categorias = results;
      })
  }

  and.push({
    $or : [
      { status: 'approved' },
      { status: 'done' },
    ]

  })

  var query = {
    $and: and
  }

  Quotation
    .find(query)
    .sort(sorting)
    .paginate(req.body.requestedPage, req.body.itemsPerPage, function(err, docs, total){
      var respuesta = {
        'totalResults': total,
        'currentPage': req.body.requestedPage,
        'totalPages': Math.ceil(total / 10),
        'categories': categorias,
        'sectors': sectores,
        'states': estados,
        'documents': [docs]
      };

      res.status(200).json(respuesta);
    });
}

//Esta función se encarga de enviar los correos cuando se aprueba una cotización
function sendMail(quote) {
  console.log('Sendmail triggered');
  User.populate(quote, {
    path: "user"
  }, function(err, quote) {
    switch (quote.sector) {
      case 'Electricidad y agua':
        quote.url = 'elecagua.jpg'
        break;
      case 'Turismo':
        quote.url = 'turismo.jpg'
        break;
      case 'Agroindustria':
        quote.url = 'agroindustria.jpg';
        break;
      case 'Logistica y transportación':
        quote.url = 'logistica.jpg';
        break;
      case 'Industrias manufactureras':
        quote.url = 'manufactura.jpg';
        break;
      case 'Construcción':
        quote.url = 'construccion.jpg';
        break;
      case 'Servicios especializados':
        quote.url = 'especializados.jpg';
        break;
    }
    var template = './server/views/cotizacionAdmin.jade';
    var templateDos = './server/views/cotizacionCopy.jade';

    // get template from file system
    fs.readFile(template, 'utf8', function(err, file) {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {
        data: quote,
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: quote.user[0].email, // list of receivers
        subject: "Cotización de Portalia Plus", // Subject line
        cco: 'mkt@portaliaplus.com',
        html: html // html body
      }, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });
    });

    // get template from file system
    fs.readFile(templateDos, 'utf8', function(err, file) {
      //compile jade template into function
      var compiledTmplDos = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var contextDos = {
        data: quote,
      };
      // get html back as a string with the context applied;
      var htmlDos = compiledTmplDos(contextDos);

      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: quote.contact_email, // list of receivers
        subject: "Tu cotización de Portalia Plus ha sido enviada", // Subject line
        html: htmlDos // html body
      }, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });
    });
  });
}

//Función de difusión masiva con send in blue
function sendMasive(quote) {
  return User.find({
    'categoria_name': quote.category
  }).exec()
  .then(users => {
    asyncLoop(users, function(user, next) {
      var telefono = quote.contact_phone.lada + quote.contact_phone.number;
      var data = {
        "id": 5,
        "to": user.email,
        "attr": {
          "DATE": moment(quote.date).format('DD/MM/YYYY'),
          "PRODUCT": quote.product,
          "QUANTITY": quote.quantity,
          "FREQUENCY": quote.frecuency,
          "COMMENTS": quote.comments,
          "CONTACTNAME": quote.contact_name,
          "CONTACTPHONE":  telefono,
          "CONTACTEMAIL": quote.contact_email,
          "CONTACTESTADO": quote.state,
          "CATEGORIA": quote.category
        },
        "headers": {
          "Content-Type": "text/html;charset=iso-8859-1",
          "X-param1": "value1",
          "X-param2": "value2",
          "X-Mailin-custom": "my custom value",
          "X-Mailin-tag": "my tag value"
        }
      }//lol

      client.send_transactional_template(data).on('complete', function(data) {
        var dataJSON = JSON.parse(data);
        console.log(dataJSON);
        if(user.wantsNotifications == true){
          request({
            url : "https://api.pushengage.com/apiv1/notifications",
            method : "POST",
            headers: {
              "api_key" : "Mz2LkrECnf0uFi4Pxj17974Jl9O8sTym"
            },
            form : {
              "notification_title": "Hay nuevas oportunidades de negocio para ti en Portalia Plus",
              "notification_message": "¡Revisa tu correo electrónico para más información!",
              "notification_url": "https://portaliaplus.com",
              "profile_id[0]": user._id,
              "image_url": 'https://portaliaplus.com/assets/images/icons/chat-message.png'
            }
        },function(err,res,body){
            next();
            if(!err){
              console.log(res.body);
            } else {
              console.log(err);
            }
        });
        } else {
          next();
        }
      });
    }, function(err) {
      if (err) {
        console.error('Error: ' + err.message);
        return;
      }

      console.log('Finished!');
    });
  })
  .catch(err => {
    console.log(err);
  });
}

function getOldPrespectsForAllQuotes() {

  Quotation.find()
    .exec()
    .then( function (res) {
      let quotes = res;

      for( let i = 0 ; i < quotes.length ; i ++ ) {
        if( quotes[i].prospects != null ) {

          if( quotes[i].prospects.length > 0 ) {

            for( let c = 0 ; c < quotes[i].prospects.length ; c ++ ) {
              QuoteUser.findOne({
                quote : quotes[i]._id,
                user : quotes[i].prospects[c].user
              }).exec()
                .then( function (res) {

                  if( res == null ) {

                    // Esto significa que no existe esta relación, por lo tanto se va a asignar
                    User.findById(quotes[i].prospects[c].user)
                      .exec()
                      .then( function (res) {
                        if( res != null ) {
                          let newRelation = new QuoteUser({
                            quote : quotes[i]._id,
                            user: quotes[i].prospects[c].user,
                            wasSend : quotes[i].prospects[c].wasSend,
                            status : quotes[i].status
                          });
                          newRelation.save();
                        }
                      })
                  }
                })
            }
          }

        }
      }
    })

}

export function getQuotesByDate(req,res) {

  Quotation.find({
    $and : [
      {
        "_id" : {
          $gte: ObjectId(Math.floor((new Date(req.params.since))/1000).toString(16) + "0000000000000000") ,
          $lt: ObjectId(Math.floor((new Date(req.params.until))/1000).toString(16) + "0000000000000000")
        }
      },
      { status: { $ne: null } }

    ]

  }).sort('-_id')
    .exec(function (err, quotes) {
    if( err ) {
      return res.status(402).send(err);
    }
    return res.status(200).send(quotes);
  })
}

export function getWatchedQuotes(req, res) {
  Quotation.find({
    "watched" : true
  })
    .exec( function (err, quotes) {
      if ( err )
        return res.status(402).send(err);

      return res.status(200).send( quotes );
    })
}
// Esta función solamente se debe de llamar si se desea analizar de nuevo las cotizacions con la funcionalidad anterior
//getOldPrespectsForAllQuotes();

