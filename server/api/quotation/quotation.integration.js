'use strict';

var app = require('../..');
import request from 'supertest';

var newQuotation;

describe('Quotation API:', function() {

  describe('GET /api/quotes', function() {
    var quotations;

    beforeEach(function(done) {
      request(app)
        .get('/api/quotes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          quotations = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      quotations.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/quotes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/quotes')
        .send({
          name: 'New Quotation',
          info: 'This is the brand new quotation!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newQuotation = res.body;
          done();
        });
    });

    it('should respond with the newly created quotation', function() {
      newQuotation.name.should.equal('New Quotation');
      newQuotation.info.should.equal('This is the brand new quotation!!!');
    });

  });

  describe('GET /api/quotes/:id', function() {
    var quotation;

    beforeEach(function(done) {
      request(app)
        .get('/api/quotes/' + newQuotation._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          quotation = res.body;
          done();
        });
    });

    afterEach(function() {
      quotation = {};
    });

    it('should respond with the requested quotation', function() {
      quotation.name.should.equal('New Quotation');
      quotation.info.should.equal('This is the brand new quotation!!!');
    });

  });

  describe('PUT /api/quotes/:id', function() {
    var updatedQuotation;

    beforeEach(function(done) {
      request(app)
        .put('/api/quotes/' + newQuotation._id)
        .send({
          name: 'Updated Quotation',
          info: 'This is the updated quotation!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedQuotation = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedQuotation = {};
    });

    it('should respond with the updated quotation', function() {
      updatedQuotation.name.should.equal('Updated Quotation');
      updatedQuotation.info.should.equal('This is the updated quotation!!!');
    });

  });

  describe('DELETE /api/quotes/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/quotes/' + newQuotation._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when quotation does not exist', function(done) {
      request(app)
        .delete('/api/quotes/' + newQuotation._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
