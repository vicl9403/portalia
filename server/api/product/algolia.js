
import algoliasearch from 'algoliasearch';

import config from '../../config/environment';

var client = algoliasearch( config.algoliaconfig.idApplication , config.algoliaconfig.adminKey );

var index = client.initIndex('products');

import Product from './product.model';

export function storeIndex( product ) {

  product.objectID = product._id;

  index.saveObject(product, function(err, content) {
    console.log(content);
  });
}


export function setUserData( id ) {

  // Asignar un tiempo de espera para actualizar el índice con el
  // usuario ya modificado
  setTimeout( function () {
    User.findById(id)
      .then( user => {
        let plan_number = 1;

        if( user.paquete == 'select' )
          plan_number = 2;

        if( user.paquete == 'plus' )
          plan_number = 3;


        let usr =  {
          objectID      : user._id,
          name          : user.nombre,
          sector        : user.sector_name,
          category      : user.categoria_name,
          state         : user.estado_name,
          municipality  : user.municipio_name,
          cover         : user.cover,
          stars         : user.stars,
          plan          : plan_number,
          about         : user.about,
          tags          : user.tags,
          products      : user.products,
          subCategories : user.subcategories
        };

        index.saveObject(usr, function(err, content) {
          console.log(content);
        });
      })
  },1000)

}
