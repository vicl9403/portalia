'use strict';

import mongoose from 'mongoose';
var Schema = mongoose.Schema;
var User = mongoose.model('User');

var ProductSchema = new mongoose.Schema({

    // El nombre del producto
    name: { type: String },

    // El usuario que crea el producto
    user: { type:Schema.ObjectId, ref:"User" },

    // La imágen principal del producto
    mainPicture: { type: String },

    //Las imágenes secundarias del producto
    pictures: { type: Array },

    // Categoria principal
    category : String,

    // Categoría secundaria
    subCategory : String,

    // La descripción es de tipo summernote
    description: { type: String , required: true },

    // Ofertas parciales
    offer: {
      image : {},
      description: {},
      until: { type: Date }
    },

    // La ficha técnica es un archivo
    datasheet: { type: String },

    currency: { type: String , default: 'MXN'},
    price: { type: Number },

    addedValue: { type: String },

    createdAt: { type: Date , default: new Date() },

    active: { type: Boolean , default: true },

});

export default mongoose.model('Product', ProductSchema);
