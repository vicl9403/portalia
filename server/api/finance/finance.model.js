'use strict';

import mongoose from 'mongoose';

var FinanceSchema = new mongoose.Schema({
  moneda: {
    usdAsk: Number,
    usdBid: Number,
    eurAsk: Number,
    eurBid: Number,
    gbpAsk: Number,
    gbpBid: Number,
    cnyAsk: Number,
    cnyBid: Number
  },
  metal: {
    aceroAsk: Number,
    aceroBid: Number,
    cobreAsk: Number,
    cobreBid: Number,
    plataAsk: Number,
    plataBid: Number,
    oilAsk: Number,
    oilBid: Number
  },
  elote: {
    cornAsk: Number,
    cornBid: Number,
    weatAsk: Number,
    weatBid: Number,
    soyAsk: Number,
    soyBid: Number
  }
});



export default mongoose.model('Finance', FinanceSchema);
