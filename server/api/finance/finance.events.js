/**
 * Finance model events
 */

'use strict';

import {EventEmitter} from 'events';
import Finance from './finance.model';
var FinanceEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
FinanceEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Finance.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    FinanceEvents.emit(event + ':' + doc._id, doc);
    FinanceEvents.emit(event, doc);
  }
}

export default FinanceEvents;
