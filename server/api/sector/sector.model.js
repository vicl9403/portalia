'use strict';

import mongoose from 'mongoose';

var SectorSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean,
  subsectores: [
    {
    name: String,
    info: String,
    active: Boolean,
    registros: Number
    }
  ]
});

export default mongoose.model('Sector', SectorSchema);
