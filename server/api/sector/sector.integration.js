'use strict';

var app = require('../..');
import request from 'supertest';

var newSector;

describe('Sector API:', function() {

  describe('GET /api/sectors', function() {
    var sectors;

    beforeEach(function(done) {
      request(app)
        .get('/api/sectors')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          sectors = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      sectors.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/sectors', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/sectors')
        .send({
          name: 'New Sector',
          info: 'This is the brand new sector!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newSector = res.body;
          done();
        });
    });

    it('should respond with the newly created sector', function() {
      newSector.name.should.equal('New Sector');
      newSector.info.should.equal('This is the brand new sector!!!');
    });

  });

  describe('GET /api/sectors/:id', function() {
    var sector;

    beforeEach(function(done) {
      request(app)
        .get('/api/sectors/' + newSector._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          sector = res.body;
          done();
        });
    });

    afterEach(function() {
      sector = {};
    });

    it('should respond with the requested sector', function() {
      sector.name.should.equal('New Sector');
      sector.info.should.equal('This is the brand new sector!!!');
    });

  });

  describe('PUT /api/sectors/:id', function() {
    var updatedSector;

    beforeEach(function(done) {
      request(app)
        .put('/api/sectors/' + newSector._id)
        .send({
          name: 'Updated Sector',
          info: 'This is the updated sector!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedSector = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSector = {};
    });

    it('should respond with the updated sector', function() {
      updatedSector.name.should.equal('Updated Sector');
      updatedSector.info.should.equal('This is the updated sector!!!');
    });

  });

  describe('DELETE /api/sectors/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/sectors/' + newSector._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when sector does not exist', function(done) {
      request(app)
        .delete('/api/sectors/' + newSector._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
