'use strict';

import config from "../../config/environment";
import User from '../user/user.model';

var Openpay = require('openpay');

var openpay = new Openpay( config.openpay.id , config.openpay.private , config.openpay.productionMode );


export function getApiKeysForPricings( req, res) {
  let keys = {
    id: config.openpay.id,
    pbk : config.openpay.public,
    sandboxMode : config.openpay.setSandboxMode
  }

  return res.status(200).send( keys );
}

export function clientSubscription( req, res ) {

  openpay.customers.subscriptions.list( req.user.openpayID , req.user.subscription.id , (err, list) => {
    return res.status(200).send(list );
  })

}

export function clientCards( req, res ) {
  if( req.user.openpayID == null )
    res.status(200).send([]);

  let searchParams = {
    'limit' : 15
  };

  openpay.customers.cards.list( req.user.openpayID , searchParams , function (err, list) {
    if( err )
      return res.status(500).send( err );

    return res.status(200).send( list );
  })

}
