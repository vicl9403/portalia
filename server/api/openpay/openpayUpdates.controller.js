'use strict';

import config from "../../config/environment";
import User from '../user/user.model';

var Openpay = require('openpay');

var openpay = new Openpay( config.openpay.id , config.openpay.private , config.openpay.productionMode );


export function clientSubscription( req, res ) {


  openpay.customers.subscriptions.list( req.user.openpayID , req.user.subscription.id , (err, list) => {
    return res.status(200).send(list );
  })

}

export function updateClientSubscription( req , res ) {
  let idSubscription = req.params.id;

  openpay.customers.subscriptions.update( req.user.openpayID , idSubscription , req.body , function (err, subscription) {

    if( err )
      return res.status(500).send(err);

    return res.status(200).send( subscription );

  });
}

export function deleteSubscription( req, res ) {

  let idSubscription = req.params.id;

  // Obtener las subscripciones del usuario para verificar que tenga al menos dos, en caso contrario no se puede eliminar
  openpay.customers.subscriptions.list( req.user.openpayID , req.user.subscription.id , (err, list) => {
    if( list.length > 1 ) {

      // Si se cancela la subscripción incial se instancia cualquier otra que tenga el usuario
      if( req.user.subscription.id == idSubscription ){
        for( var i=0 ; i < list.length ; i++ ) {
          if( list[i].id != req.user.subscription.id ) {
            console.log( req.user._id );
            User.findById( req.user._id )
              .exec()
              .then( user => {
                user.subscription = list[i];
                user.save();
              })
            break;
          }
        }
      }

      openpay.customers.subscriptions.delete( req.user.openpayID , idSubscription , function (err) {
        if( err )
          return res.status(500).send(err);

        return res.status(200).send('ok');
      })

    }
    else {
      return res.status(500).send('No se pueden calcelar todas las subscripciones');
    }
  })



}
