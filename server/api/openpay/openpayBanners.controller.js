'use strict';

import config from "../../config/environment";
import Banner from '../banner/banner.model';

var Openpay = require('openpay');

var openpay = new Openpay( config.openpay.id , config.openpay.private , config.openpay.productionMode );


export function buyBanner( req, res ) {

  openpay.customers.charges.create( req.user.openpayID , req.body , function (err, charge) {
    if( err )
      return res.status(500).send(err);

    if( charge.status == 'completed' ) {

      Banner.findById(req.params.id)
        .exec()
        .then( banner => {
          banner.status = 'approved';
          banner.isActive = true;
          banner.save();
        })

    }
    return res.status(200).send( charge );
  })

}

