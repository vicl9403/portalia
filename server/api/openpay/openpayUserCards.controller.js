'use strict';

import config from "../../config/environment";
import User from '../user/user.model';

var Openpay = require('openpay');

var openpay = new Openpay( config.openpay.id , config.openpay.private , config.openpay.productionMode );


export function createCard( req, res ) {

  if( req.user.openpayID == null ) {

  }
  openpay.customers.cards.create( req.user.openpayID , req.body , function(err, card)  {
    if( err )
      return res.status(500).send(err);

    return res.status( 200 ).send(card);
  });

}
export function clientSubscription( req, res ) {


  openpay.customers.subscriptions.list( req.user.openpayID , req.user.subscription.id , (err, list) => {
    console.log( list );
    return res.status(200).send(list );
  })

}
