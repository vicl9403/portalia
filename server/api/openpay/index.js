'use strict';

var express = require('express');

import * as auth from '../../auth/auth.service';

var router = express.Router();

// Controlador generar para manipular openpay
var controller = require('./openpay.controller');

// Controlador para manipular la información de los clientes de Openpay
var getterController = require('./openpayGetters.controller');

// Controlador para tener todos los updates sobre las subscripciones de los usuarios o membresías en general
var updatesController = require('./openpayUpdates.controller');

// Controlador para el uso de las tarjetas del usuario
var userCards = require('./openpayUserCards.controller');

// Controlador encargado de manipular las transacciones para los banners
var bannersTransactions = require('./openpayBanners.controller');


// Totas estas rutas pertenecen al controlador principal de pagos
router.post('/subscription', auth.isAuthenticated() , controller.subscription  );
router.post('/card', auth.isAuthenticated() , controller.card );
router.post('/webhook', controller.webhook);
router.post('/customers' , auth.isAuthenticated() , controller.createCustomer);

// Todas estas rutas pertenecen al controlador de getters
router.get('/card' , auth.isAuthenticated() , getterController.clientSubscription );
router.get('/user/cards' , auth.isAuthenticated() , getterController.clientCards );
router.get('/keys', auth.isAuthenticated() , getterController.getApiKeysForPricings);

// Todas estas rutas pertenecen al controlador de modificaciones
router.patch('/subscription/:id' , auth.isAuthenticated() , updatesController.updateClientSubscription );
router.delete('/subscription/:id' , auth.isAuthenticated() , updatesController.deleteSubscription );

// Rutas para la manipulación de las tarjetas
router.post('/newCard', auth.isAuthenticated() , userCards.createCard );

// Rutas para las transacciones de los banners
router.post('/banner/:id' , auth.isAuthenticated() , bannersTransactions.buyBanner );

module.exports = router;
