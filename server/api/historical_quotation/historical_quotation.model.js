'use strict';

import mongoose from 'mongoose';

var HistoricalQuotationSchema = new mongoose.Schema({
  category: { type: String , required : true },

  sector: { type: String , required : true },

  scoreMin: { type: Number, default:0 },

  scoreMax: { type: Number, default:0 },

  scoreAvg: { type: Number, default:0 },

  hard_bounce: { type: Number, default:0 },

  send: { type: Number, default:0 },

  deferral: { type: Number, default:0 },

  soft_bounce: { type: Number, default:0 },

  open: { type: Number, default:0 },

  click: { type: Number, default:0 },

  spam: { type: Number, default:0 },

  unsub: { type: Number, default:0 },

  reject: { type: Number, default:0 },

  blacklist: { type: Number, default:0 },

  whitelist: { type: Number, default:0 },

});

export default mongoose.model('HistoricalQuotation', HistoricalQuotationSchema);
