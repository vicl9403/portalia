'use strict';

import Quotation from '../quotation/quotation.model';
import Historical from './historical_quotation.model';
var schedule = require('node-schedule');



// Actualizar el histórico
var j = schedule.scheduleJob('* * * * *', function() {
   Quotation.find({}, function(err, quotations) {
    //obtenemos los scores de categoría y sector
    var scoresInformation = [];

    //iteramos sobre cada cotización
    quotations.forEach(function(quotation) {
      //verificamos que el score no sea vacío
      if (quotation.score != "")
      {
        //insertamos el score en el lugar correspondiente
        scoresInformation[quotation.sector] = scoresInformation[quotation.sector] || [];
        scoresInformation[quotation.sector][quotation.category] = scoresInformation[quotation.sector][quotation.category] || [];
        scoresInformation[quotation.sector][quotation.category].push(quotation.score);
      }

    });

    //iteramos por todos los sectores y  categorias
    for(let sector in scoresInformation) {
      for(let category in scoresInformation[sector]) {
        //buscamos el historico que pertenezca a categoría y sector
        Historical.findOne({
          "category"    : category,
          "sector"    : sector
        }).exec()
          .then( function (historical) {
            //verificamos que exista, s no existe creamos uno
            if (historical == null) {
              historical = new Historical();
              historical.category = category;
              historical.sector = sector;
            }
            //obtenemos los valores del array
            let values = scoresInformation[historical.sector][historical.category];
            //calculamos máximo, mínimo y promedio
            historical.scoreMax = Math.max(...values);
            historical.scoreMin = Math.min(...values);
            let sum = values.reduce(function(a, b) { return a + b; });
            historical.scoreAvg = sum / values.length;
            //guardamos el histórico
            historical.save();
          })
      }
    }

  });

});

