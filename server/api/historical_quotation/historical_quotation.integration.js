'use strict';

var app = require('../..');
import request from 'supertest';

var newHistoricalQuotation;

describe('HistoricalQuotation API:', function() {

  describe('GET /api/historical_quotations', function() {
    var historicalQuotations;

    beforeEach(function(done) {
      request(app)
        .get('/api/historical_quotations')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          historicalQuotations = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      historicalQuotations.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/historical_quotations', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/historical_quotations')
        .send({
          name: 'New HistoricalQuotation',
          info: 'This is the brand new historicalQuotation!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newHistoricalQuotation = res.body;
          done();
        });
    });

    it('should respond with the newly created historicalQuotation', function() {
      newHistoricalQuotation.name.should.equal('New HistoricalQuotation');
      newHistoricalQuotation.info.should.equal('This is the brand new historicalQuotation!!!');
    });

  });

  describe('GET /api/historical_quotations/:id', function() {
    var historicalQuotation;

    beforeEach(function(done) {
      request(app)
        .get('/api/historical_quotations/' + newHistoricalQuotation._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          historicalQuotation = res.body;
          done();
        });
    });

    afterEach(function() {
      historicalQuotation = {};
    });

    it('should respond with the requested historicalQuotation', function() {
      historicalQuotation.name.should.equal('New HistoricalQuotation');
      historicalQuotation.info.should.equal('This is the brand new historicalQuotation!!!');
    });

  });

  describe('PUT /api/historical_quotations/:id', function() {
    var updatedHistoricalQuotation;

    beforeEach(function(done) {
      request(app)
        .put('/api/historical_quotations/' + newHistoricalQuotation._id)
        .send({
          name: 'Updated HistoricalQuotation',
          info: 'This is the updated historicalQuotation!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedHistoricalQuotation = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedHistoricalQuotation = {};
    });

    it('should respond with the updated historicalQuotation', function() {
      updatedHistoricalQuotation.name.should.equal('Updated HistoricalQuotation');
      updatedHistoricalQuotation.info.should.equal('This is the updated historicalQuotation!!!');
    });

  });

  describe('DELETE /api/historical_quotations/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/historical_quotations/' + newHistoricalQuotation._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when historicalQuotation does not exist', function(done) {
      request(app)
        .delete('/api/historical_quotations/' + newHistoricalQuotation._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
