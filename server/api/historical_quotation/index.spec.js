'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var historicalQuotationCtrlStub = {
  index: 'historicalQuotationCtrl.index',
  show: 'historicalQuotationCtrl.show',
  create: 'historicalQuotationCtrl.create',
  update: 'historicalQuotationCtrl.update',
  destroy: 'historicalQuotationCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var historicalQuotationIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './historical_quotation.controller': historicalQuotationCtrlStub
});

describe('HistoricalQuotation API Router:', function() {

  it('should return an express router instance', function() {
    historicalQuotationIndex.should.equal(routerStub);
  });

  describe('GET /api/historical_quotations', function() {

    it('should route to historicalQuotation.controller.index', function() {
      routerStub.get
        .withArgs('/', 'historicalQuotationCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/historical_quotations/:id', function() {

    it('should route to historicalQuotation.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'historicalQuotationCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/historical_quotations', function() {

    it('should route to historicalQuotation.controller.create', function() {
      routerStub.post
        .withArgs('/', 'historicalQuotationCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/historical_quotations/:id', function() {

    it('should route to historicalQuotation.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'historicalQuotationCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/historical_quotations/:id', function() {

    it('should route to historicalQuotation.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'historicalQuotationCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/historical_quotations/:id', function() {

    it('should route to historicalQuotation.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'historicalQuotationCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
