/**
 * HistoricalQuotation model events
 */

'use strict';

import {EventEmitter} from 'events';
import HistoricalQuotation from './historical_quotation.model';
var HistoricalQuotationEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
HistoricalQuotationEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  HistoricalQuotation.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    HistoricalQuotationEvents.emit(event + ':' + doc._id, doc);
    HistoricalQuotationEvents.emit(event, doc);
  }
}

export default HistoricalQuotationEvents;
