/**
 * PaymentLog model events
 */

'use strict';

import {EventEmitter} from 'events';
import PaymentLog from './paymentLog.model';
var PaymentLogEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PaymentLogEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  PaymentLog.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PaymentLogEvents.emit(event + ':' + doc._id, doc);
    PaymentLogEvents.emit(event, doc);
  };
}

export default PaymentLogEvents;
