'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var paymentLogCtrlStub = {
  index: 'paymentLogCtrl.index',
  show: 'paymentLogCtrl.show',
  create: 'paymentLogCtrl.create',
  upsert: 'paymentLogCtrl.upsert',
  patch: 'paymentLogCtrl.patch',
  destroy: 'paymentLogCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var paymentLogIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './paymentLog.controller': paymentLogCtrlStub
});

describe('PaymentLog API Router:', function() {
  it('should return an express router instance', function() {
    paymentLogIndex.should.equal(routerStub);
  });

  describe('GET /api/paymentLogs', function() {
    it('should route to paymentLog.controller.index', function() {
      routerStub.get
        .withArgs('/', 'paymentLogCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/paymentLogs/:id', function() {
    it('should route to paymentLog.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'paymentLogCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/paymentLogs', function() {
    it('should route to paymentLog.controller.create', function() {
      routerStub.post
        .withArgs('/', 'paymentLogCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/paymentLogs/:id', function() {
    it('should route to paymentLog.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'paymentLogCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/paymentLogs/:id', function() {
    it('should route to paymentLog.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'paymentLogCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/paymentLogs/:id', function() {
    it('should route to paymentLog.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'paymentLogCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
