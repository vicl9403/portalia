'use strict';

var app = require('../..');
import request from 'supertest';

var newPaymentLog;

describe('PaymentLog API:', function() {
  describe('GET /api/paymentLogs', function() {
    var paymentLogs;

    beforeEach(function(done) {
      request(app)
        .get('/api/paymentLogs')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          paymentLogs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      paymentLogs.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/paymentLogs', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/paymentLogs')
        .send({
          name: 'New PaymentLog',
          info: 'This is the brand new paymentLog!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newPaymentLog = res.body;
          done();
        });
    });

    it('should respond with the newly created paymentLog', function() {
      newPaymentLog.name.should.equal('New PaymentLog');
      newPaymentLog.info.should.equal('This is the brand new paymentLog!!!');
    });
  });

  describe('GET /api/paymentLogs/:id', function() {
    var paymentLog;

    beforeEach(function(done) {
      request(app)
        .get(`/api/paymentLogs/${newPaymentLog._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          paymentLog = res.body;
          done();
        });
    });

    afterEach(function() {
      paymentLog = {};
    });

    it('should respond with the requested paymentLog', function() {
      paymentLog.name.should.equal('New PaymentLog');
      paymentLog.info.should.equal('This is the brand new paymentLog!!!');
    });
  });

  describe('PUT /api/paymentLogs/:id', function() {
    var updatedPaymentLog;

    beforeEach(function(done) {
      request(app)
        .put(`/api/paymentLogs/${newPaymentLog._id}`)
        .send({
          name: 'Updated PaymentLog',
          info: 'This is the updated paymentLog!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedPaymentLog = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPaymentLog = {};
    });

    it('should respond with the original paymentLog', function() {
      updatedPaymentLog.name.should.equal('New PaymentLog');
      updatedPaymentLog.info.should.equal('This is the brand new paymentLog!!!');
    });

    it('should respond with the updated paymentLog on a subsequent GET', function(done) {
      request(app)
        .get(`/api/paymentLogs/${newPaymentLog._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let paymentLog = res.body;

          paymentLog.name.should.equal('Updated PaymentLog');
          paymentLog.info.should.equal('This is the updated paymentLog!!!');

          done();
        });
    });
  });

  describe('PATCH /api/paymentLogs/:id', function() {
    var patchedPaymentLog;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/paymentLogs/${newPaymentLog._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched PaymentLog' },
          { op: 'replace', path: '/info', value: 'This is the patched paymentLog!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedPaymentLog = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedPaymentLog = {};
    });

    it('should respond with the patched paymentLog', function() {
      patchedPaymentLog.name.should.equal('Patched PaymentLog');
      patchedPaymentLog.info.should.equal('This is the patched paymentLog!!!');
    });
  });

  describe('DELETE /api/paymentLogs/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/paymentLogs/${newPaymentLog._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when paymentLog does not exist', function(done) {
      request(app)
        .delete(`/api/paymentLogs/${newPaymentLog._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
