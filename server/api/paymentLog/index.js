'use strict';

var express = require('express');
var controller = require('./paymentLog.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);
router.post('/getAll', controller.getAllLogs);
router.post('/filter', controller.filter);
router.post('/requestCSV', controller.generateCSV);

module.exports = router;
