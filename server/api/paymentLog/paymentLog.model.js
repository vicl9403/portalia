'use strict';

import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var PaymentLogSchema = new mongoose.Schema({
  paquete: String,
  periodo: String,
  numeroPagos: Number,
  precioPorPago: Number,
  total: Number,
  userId: String,
  userName: String,
  userEmail: String,
  userSector: String,
  userCategoria: String,
  userState: String,
  paymentDate: {
    type: Date,
    default: Date.now
  },
  endDate: Date
});

export default mongoose.model('PaymentLog', PaymentLogSchema);
