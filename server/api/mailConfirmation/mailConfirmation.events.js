/**
 * MailConfirmation model events
 */

'use strict';

import {EventEmitter} from 'events';
import MailConfirmation from './mailConfirmation.model';
var MailConfirmationEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MailConfirmationEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  MailConfirmation.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    MailConfirmationEvents.emit(event + ':' + doc._id, doc);
    MailConfirmationEvents.emit(event, doc);
  }
}

export default MailConfirmationEvents;
