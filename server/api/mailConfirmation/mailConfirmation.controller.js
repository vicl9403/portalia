/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/mailConfirmations              ->  index
 * POST    /api/mailConfirmations              ->  create
 * GET     /api/mailConfirmations/:id          ->  show
 * PUT     /api/mailConfirmations/:id          ->  update
 * DELETE  /api/mailConfirmations/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import MailConfirmation from './mailConfirmation.model';
import email from '../email/email.controller';
import nodemailer from 'nodemailer';
var _jade = require('jade');
var fs = require('fs');
var P = require('bluebird')

var transporter = nodemailer.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  }
})

function MailConfirmationEmail(to, randomStr) {

  var template = './server/views/validation.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function(err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {
        codigo_random: randomStr
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);


      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: to, // list of receivers
        subject: "Validar correo electronico en PortaliaPlus", // Subject line
        html: html // html body
      }, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });

    }
  });
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of MailConfirmations
export function index(req, res) {
  return MailConfirmation.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single MailConfirmation from the DB
export function show(req, res) {
  return MailConfirmation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new MailConfirmation in the DB
export function create(req, res) {
  require('crypto').randomBytes(128, function(err, buffer) {
    var randomStr = buffer.toString('hex');
    console.log(randomStr);
    MailConfirmation.findOne({
      'urlKey': randomStr
    }, function(err, result) {
      if (err) {
        console.log(err);
      }
      if (!result) {
        req.body.urlKey = randomStr;

        new MailConfirmationEmail(req.body.email, randomStr);

        return MailConfirmation.create(req.body)
          .then(respondWithResult(res, 201))
      }
    });
  });
}

// Updates an existing MailConfirmation in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return MailConfirmation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a MailConfirmation from the DB
export function destroy(req, res) {
  return MailConfirmation.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
