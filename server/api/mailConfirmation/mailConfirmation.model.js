'use strict';

import mongoose from 'mongoose';

var MailConfirmationSchema = new mongoose.Schema({
  urlKey: String,
  email: String
});

export default mongoose.model('MailConfirmation', MailConfirmationSchema);
