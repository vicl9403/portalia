'use strict';

var app = require('../..');
import request from 'supertest';

var newCatalogoCamposFormulario;

describe('CatalogoCamposFormulario API:', function() {

  describe('GET /api/catalogo_campos_formularios', function() {
    var catalogoCamposFormularios;

    beforeEach(function(done) {
      request(app)
        .get('/api/catalogo_campos_formularios')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          catalogoCamposFormularios = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      catalogoCamposFormularios.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/catalogo_campos_formularios', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/catalogo_campos_formularios')
        .send({
          name: 'New CatalogoCamposFormulario',
          info: 'This is the brand new catalogoCamposFormulario!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newCatalogoCamposFormulario = res.body;
          done();
        });
    });

    it('should respond with the newly created catalogoCamposFormulario', function() {
      newCatalogoCamposFormulario.name.should.equal('New CatalogoCamposFormulario');
      newCatalogoCamposFormulario.info.should.equal('This is the brand new catalogoCamposFormulario!!!');
    });

  });

  describe('GET /api/catalogo_campos_formularios/:id', function() {
    var catalogoCamposFormulario;

    beforeEach(function(done) {
      request(app)
        .get('/api/catalogo_campos_formularios/' + newCatalogoCamposFormulario._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          catalogoCamposFormulario = res.body;
          done();
        });
    });

    afterEach(function() {
      catalogoCamposFormulario = {};
    });

    it('should respond with the requested catalogoCamposFormulario', function() {
      catalogoCamposFormulario.name.should.equal('New CatalogoCamposFormulario');
      catalogoCamposFormulario.info.should.equal('This is the brand new catalogoCamposFormulario!!!');
    });

  });

  describe('PUT /api/catalogo_campos_formularios/:id', function() {
    var updatedCatalogoCamposFormulario;

    beforeEach(function(done) {
      request(app)
        .put('/api/catalogo_campos_formularios/' + newCatalogoCamposFormulario._id)
        .send({
          name: 'Updated CatalogoCamposFormulario',
          info: 'This is the updated catalogoCamposFormulario!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCatalogoCamposFormulario = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCatalogoCamposFormulario = {};
    });

    it('should respond with the updated catalogoCamposFormulario', function() {
      updatedCatalogoCamposFormulario.name.should.equal('Updated CatalogoCamposFormulario');
      updatedCatalogoCamposFormulario.info.should.equal('This is the updated catalogoCamposFormulario!!!');
    });

  });

  describe('DELETE /api/catalogo_campos_formularios/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/catalogo_campos_formularios/' + newCatalogoCamposFormulario._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when catalogoCamposFormulario does not exist', function(done) {
      request(app)
        .delete('/api/catalogo_campos_formularios/' + newCatalogoCamposFormulario._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
