/**
 * CatalogoCamposFormulario model events
 */

'use strict';

import {EventEmitter} from 'events';
import CatalogoCamposFormulario from './catalogo_campos_formulario.model';
var CatalogoCamposFormularioEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CatalogoCamposFormularioEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  CatalogoCamposFormulario.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CatalogoCamposFormularioEvents.emit(event + ':' + doc._id, doc);
    CatalogoCamposFormularioEvents.emit(event, doc);
  }
}

export default CatalogoCamposFormularioEvents;
