'use strict';

import _ from 'lodash';
import CatalogoCamposFormulario from './catalogo_campos_formulario.model';
import config from '../../config/environment';

function respondWithResult(res, statusCode) {
	statusCode = statusCode || 200;
	return function(entity) {
		if (entity) {
			res.status(statusCode).json(entity);
		}
	};
}

function saveUpdates(updates) {
	return function(entity) {
		var updated = _.merge(entity, updates);
		return updated.save()
			.then(updated => {
				return updated;
			});
	};
}

function removeEntity(res) {
	return function(entity) {
		if (entity) {
			return entity.remove()
				.then(() => {
					res.status(204).end();
				});
		}
	};
}

function handleEntityNotFound(res) {
	return function(entity) {
		if (!entity) {
			res.status(404).end();
			return null;
		}
		return entity;
	};
}

function handleError(res, statusCode) {
	statusCode = statusCode || 500;
	return function(err) {
		res.status(statusCode).send(err);
	};
}

// Gets a list of CatalogoCamposFormularios
export function index(req, res) {
	return CatalogoCamposFormulario.find().exec()
		.then(respondWithResult(res))
		.catch(handleError(res));
}

// Gets a single CatalogoCamposFormulario from the DB
export function show(req, res) {
	return CatalogoCamposFormulario.find({
			"formularioID": req.params.form,
			$or: [{
				"proyecto": 0
			}, {
				"proyecto": config.project.Type
			}]
		})
        .exec()
		.then(handleEntityNotFound(res))
		.then(respondWithResult(res))
		.catch(handleError(res));
}

// Creates a new CatalogoCamposFormulario in the DB
export function create(req, res) {
	return CatalogoCamposFormulario.create(req.body)
		.then(respondWithResult(res, 201))
}

// Updates an existing CatalogoCamposFormulario in the DB
export function update(req, res) {
	if (req.body._id) {
		delete req.body._id;
	}
	return CatalogoCamposFormulario.findById(req.params.id).exec()
		.then(handleEntityNotFound(res))
		.then(saveUpdates(req.body))
		.then(respondWithResult(res))
		.catch(handleError(res));
}

// Deletes a CatalogoCamposFormulario from the DB
export function destroy(req, res) {
	return CatalogoCamposFormulario.findById(req.params.id).exec()
		.then(handleEntityNotFound(res))
		.then(removeEntity(res))
		.catch(handleError(res));
}
