'use strict';

import mongoose from 'mongoose';

var CatalogoCamposFormularioSchema = new mongoose.Schema({
  nombre: String,
  tipo: String,
  formularioID: Number,
  tooltip: String,
  proyecto: Number,
  orden: Number,
  extra: {}
});

export default mongoose.model('CatalogoCamposFormulario', CatalogoCamposFormularioSchema);
