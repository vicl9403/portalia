'use strict';

var express = require('express');
var controller = require('./upload.controller');

var router = express.Router();

router.post('/profilePic/:id/:sector/:categoria/:name', controller.create);
router.post('/productsPics/:id/:sector/:categoria/:name', controller.products);
router.post('/productsFile/:id/:sector/:categoria/:name', controller.info);
router.post('/productsVideo/:id/:sector/:categoria/:name', controller.videos);
router.post('/imagenEvento/', controller.imagenEvento);

router.post('/catalogs/:id/:filename', controller.storeCatalog);
router.post('/product/:id', controller.storeProductDatasheet);

// Esta es una ruta genérica para subir imágenes, permite subir una imágen
// y regresa como resultado la ruta de donde se guardo, se puede sobre cargar
// mandándole el nombre del archivo y/o la ruta de donde lo queremos
router.post('/image' , controller.uploadImage );

module.exports = router;
