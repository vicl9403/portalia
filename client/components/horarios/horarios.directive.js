'use strict';

angular.module('portaliaApp')
  .directive('horarios', () => ({
    templateUrl: 'components/horarios/horarios.html',
    restrict: 'E',
    controller: 'HorariosController',
    controllerAs: 'horarios'
  }));