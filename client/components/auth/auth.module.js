'use strict';

angular.module('portaliaApp.auth', [
  'portaliaApp.constants',
  'portaliaApp.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
