'use strict';

angular.module('portaliaApp')
    .directive('bussinesslist', () => ({
        templateUrl: 'components/bussinessList/bussinessList.html',
        restrict: 'E',
        controller: 'BussinessListController',
        controllerAs: 'bussinesslist'
    }));
