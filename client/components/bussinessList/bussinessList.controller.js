'use strict';

// import algoliasearch from 'algoliasearch';


angular.module('portaliaApp')
    .controller('BussinessListController', function($scope, $location, $route,  $window, $http, Auth, $timeout, $routeParams) {

        let index = '';
        let client = {};





        $scope.showSectors = true;

        if( $route.current.$$route.template == '<anuncios></anuncios>')
          $scope.showSectors = false;

        $scope.logged = Auth.isLoggedIn;

        Auth.getCurrentUser( function (user) {
          $scope.userData = user;
        });

        // Variable para saber a quien se quiere contactar en el formulario de contacto
        $scope.contactBussiness = '';

        $scope.currentPage = 0;

        //Variable para detectar los filtros en la vista
        $scope.params = [];

        $scope.urlFilters = {};

        $scope.validType = function (  type ) {

          var validParams = [
            'sector',
            'categoria',
            'ubicacion',
            'tipo',
            'buscar',
            'pagina',
          ];

          return ( validParams.indexOf( type ) >= 0 ) ? true : false;
        };

        $scope.initParams = function ( paramsLength ) {
          for( var i=0; i < paramsLength ; i++ ){

            if ( $routeParams['param' + (i+1)] != null)
            {
              // Obtener el parámetro general de la url
              var param = $routeParams['param' + (i+1)];
              // Obtener el tipo de parámetro
              var paramType = param.split('-')[0];
              // Obtener el valor del parámetro
              var paramValue = param.split('-')[1];

              if ( $scope.validType( paramType ) )
                $scope.urlFilters[ paramType ] = paramValue;

            }
            if ( $scope.urlFilters['pagina'] == undefined )
              $scope.urlFilters['pagina'] = '1';
          }
        };

        $scope.getUserObjects = function ( hits ) {

          let ids = [];

          for( let i = 0 ; i < hits.length ; i++ ) {
            ids.push( hits[i].objectID );
          }

          $http.post('/api/users/getListOfUsers' , { data: ids } )
            .then(  (res) => {
                $scope.documents = res.data;
            });

        }

        $scope.getResults = function(filters = null) {

            $scope.loading = true;

            let urlFilters = [];

            let page = ($scope.urlFilters.pagina != undefined) ? $scope.urlFilters.pagina : 0 ;

            if( $scope.urlFilters.sector != undefined )
              urlFilters.push(`sector:${$scope.urlFilters.sector}`);


            if( $scope.urlFilters.categoria != undefined )
              urlFilters.push(`category:${$scope.urlFilters.categoria} `);

            if( $scope.urlFilters.ubicacion != undefined )
              urlFilters.push(`state:${$scope.urlFilters.ubicacion}`);

            // Convertir los filtros en un array cada elemento
            urlFilters = urlFilters.map( el => {
              let arr = [];
              arr.push( el );
              return arr;
            });

            if( $scope.urlFilters.buscar != undefined )
              $scope.getBanners( $scope.urlFilters.buscar );
            else
              $scope.getBanners( '' );

            // Llamar la api de búsqueda
            index.search({
              query: ( $scope.urlFilters.buscar != undefined ) ? $scope.urlFilters.buscar : '' ,
              facetFilters: urlFilters ,
              facets: [ 'sector', 'category', 'state' , 'municipality' ],
              page: page - 1
            })
              .then( res => {

                // Obtener los documentos de la búsqueda
                $scope.getUserObjects( res.hits );

                // Obtener el listado de los sectores de búsqueda
                $scope.searchSectors = res.facets.sector;

                // Obtener el listado de los estados de búsqueda
                $scope.searchStates = res.facets.state;

                // Obtener el listado de los municipios de búsqueda
                $scope.searchMunicipality = res.facets.municipality;

                // Obtener el listado de las categorias de búsqueda
                $scope.searchCategories = res.facets.category;

                // Asignar variables de paginación
                $scope.totalPages = Math.round(res.nbHits / 16);
                $scope.currentPage = res.page + 1;

                $scope.totalResults = res.nbHits;

                $scope.updatePagination();

                $scope.loading = false;
              });

            // $http.post('/api/users/newSearchSystem', apiFilters )
            //     .then(function(res) {
            //         $scope.categorias = res.data.categorias;
            //         $scope.sectores = res.data.sectores;
            //         $scope.estados = res.data.estados;
            //         $scope.documents = res.data.documents;
            //         $scope.totalPages = res.data.totalPages;
            //         $scope.totalResults = res.data.totalResults;
            //         $scope.currentPage = res.data.currentPage;
            //         $scope.updatePagination();
            //         $scope.loading = false;
            //         // console.log( $scope.categorias);
            //     });



            // console.log( $scope.params );

        }

        $scope.updatePagination = function() {
            $scope.next = parseInt($scope.currentPage) + 1;
            $scope.before = parseInt($scope.currentPage) - 1;

            if ($scope.totalPages >= 10) {
                if ($scope.totalPages - $scope.currentPage >= 5) {
                    if ($scope.currentPage - 10 < 0) {
                        $scope.lowerLimit = 0;
                        $scope.upperLimit = 9;
                    } else {
                        $scope.upperLimit = parseInt($scope.currentPage) + 4;
                        $scope.lowerLimit = parseInt($scope.upperLimit) - 9;
                    }
                } else {
                    $scope.upperLimit = parseInt($scope.totalPages) - 1;
                    $scope.lowerLimit = parseInt($scope.totalPages) - 10;
                }
            } else {
                $scope.lowerLimit = 0;
                $scope.upperLimit = $scope.totalPages - 1;
            }

        }

        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };

        $scope.setFilter = function(data, key) {

            //Variable auxiliar para los parámetros
            var param = {};

            if (data != 'requestedPage')
              $scope.urlFilters['pagina'] = 1;


            var url = $route.current.$$route.originalPath.split('/')[1] ;
            // console.log( url );

            //Setear el parametro que se mando
            $scope.urlFilters[key] = data;

            // Obteniendo todas las claves del JSON
            for (var key in $scope.urlFilters){
              // Controlando que $scope.urlFilters realmente tenga esa propiedad
              if ($scope.urlFilters.hasOwnProperty(key))
              {
                if ( $scope.urlFilters[key] != '')
                  url += '/' + key + '-' + $scope.urlFilters[key];
              }
            }

            //Actualizar los parámetros de la URL
            $location.path(url);

            //Obtener los nuevos resultados
            $scope.getResults();
            $("html, body").animate({
                scrollTop: 300
            }, "slow");

        }

        $scope.setOrderAlphabetical = function() {
            //Toglear el órden
            if ($scope.alphabetical == 'true' || $scope.alphabetical == undefined)
                $scope.alphabetical = 'false';
            else
                $scope.alphabetical = 'true';

            //Obtener los nuevos resultados
            $scope.getResults();

            $("html, body").animate({
                scrollTop: 300
            }, "slow");

        }

        $scope.sendToProfile = function(id) {
            $window.open('/perfil/' + id, '_blank');
        }

        angular.element(document).ready(function() {

          $http.get('/api/algolia/getSearchKeys')
            .then( res => {

              // Inicializar algolia con los parámetros que se necesitan
              // de igual manera con el índice
              client = algoliasearch( res.data.id , res.data.key );
              index = client.initIndex('users');

              $scope.initParams( 6 );

              $scope.getResults();

            })


          $scope.banners = [];

        });

        if ( !Auth.isLoggedIn() )
            $scope.logged = false;
        else
            $scope.logged = true;

        $scope.llamar = function(id) {

            $http.get('/api/users/' + id )
                .then( res => {
                    let user = res.data;
                    if ($scope.userData.nombre == undefined) {
                      $('#myModal').modal('show');
                    }
                    else {
                      if( user.telefono == undefined )
                        swal( "Oooops!" , 'El usuario no tiene ningún teléfono registrado, puedes intentar ponerte en contacto con el iniciando un chat dentro de su perfil.' , 'warning');
                      else
                        window.open("tel:" + user.telefono);
                    }
                })

        }

        $scope.isOpen = function(shedules) {
            $scope.date = new Date();
            $scope.day = $scope.date.getDay() - 1;

            if ( shedules[$scope.day] == undefined )
              return 'Cerrado';

            if (shedules[$scope.day].oppened == true)
            {
                $scope.opening = moment(shedules[$scope.day].opening, 'hh:mm a');
                $scope.closing = moment(shedules[$scope.day].closing, 'hh:mm a');
                $scope.currentTime = moment(moment().format('hh:mm a'), 'hh:mm a');
                $scope.resultOne = moment($scope.currentTime).isAfter($scope.closing);
                $scope.resultTwo = moment($scope.currentTime).isBefore($scope.opening);
                if ($scope.resultOne == false && $scope.resultTwo == false) {
                    return 'Abierto';
                } else {
                    return 'Cerrado';
                }
            }
            else
                return 'Cerrado';
        }


        $scope.getBanners = function ( searchItems ) {

          if( searchItems == '' )
          {
            $scope.banners.push({
              img1 : '/assets/images/gifs/categorias1.gif',
              img2 : '/assets/images/gifs/categorias1.gif',
              url : 'https://tecnotanques.com/venta-calentadores-solares-mayoreo/'
            })
            $scope.banners.push({
              img1 : '/assets/images/gifs/categorias2.gif' ,
              img2 : '/assets/images/gifs/categorias2.gif' ,
              url : 'https://tecnotanques.com/tanques-industriales/'
            })
            $scope.banners.push({
              img1 : '/assets/images/gifs/categorias3.gif',
              img2 : '/assets/images/gifs/categorias3.gif',
              url : 'https://proproyectos.com/Tinacos-para-proyectos'
            })
            return;

          }
          $http.get('/api/banners/findKeywordActiveBanners/' + searchItems )
            .then( (res) =>{

              let banners = res.data;

              console.log( moment().format('X') );
              for( let i = 0 ; i < banners.length ; i ++ ) {
                if( moment().format('X') >= moment(banners[i].since).format('X') ) {
                  $scope.banners.push({
                    img1: banners[i].img1,
                    img2: banners[i].img2,
                    url: banners[i].url
                  })
                }
              }

              if( banners.length == 0 ) {
                $scope.banners.push({
                  img1 : '/assets/images/gifs/categorias1.gif',
                  img1 : '/assets/images/gifs/categorias1.gif',
                  url : 'https://tecnotanques.com/venta-calentadores-solares-mayoreo/'
                })
                $scope.banners.push({
                  img1 : '/assets/images/gifs/categorias2.gif',
                  img2 : '/assets/images/gifs/categorias2.gif',
                  url : 'https://tecnotanques.com/tanques-industriales/'
                })
                $scope.banners.push({
                  img1 : '/assets/images/gifs/categorias3.gif',
                  img2 : '/assets/images/gifs/categorias3.gif',
                  url : 'https://proproyectos.com/Tinacos-para-proyectos'
                })
              }

            });

        }
    })

    .directive( 'onErrorSrc', function() {
      return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
      }
    });
