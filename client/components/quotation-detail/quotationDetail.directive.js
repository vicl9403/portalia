'use strict';

angular.module('portaliaApp')
    .directive('quotationdetail', () => ({
        templateUrl: 'components/quotation-detail/quotationDetail.html',
        restrict: 'E',
        controller: 'QuotationDetailController',
        controllerAs: 'quotationdetail'
    }));
