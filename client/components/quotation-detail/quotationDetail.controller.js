'use strict';

angular.module('portaliaApp')
  .controller('QuotationDetailController', function($scope, $location, $window, $http, Auth, $timeout, $route, $routeParams) {

    if ( $routeParams.id != undefined ) {
      $http.get('api/quotes/' + $routeParams.id)
        .then(function (response) {
          console.log(response);
          $scope.quotation = response.data;
        });
    }

    $scope.getDate = function (date) {
      var date = moment(date);
      date.locale('es');
      return ( date.format('LL') );
    }
    $scope.$on('getQuoteData', function(event, id) {
      if( id != '' ) {
        $http.get('api/quotes/' + id )
          .then(function (response) {
            $scope.quotation = response.data;
          });
      }
    });



  });
