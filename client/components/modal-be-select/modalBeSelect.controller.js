'use strict';

angular.module('portaliaApp')
  .controller('ModalBeSelectController', function($scope, $location, $window, $http, Auth, $timeout, $route, $routeParams) {

    $scope.goToPricing = function ( id ) {
      $("#"+id).modal('hide');
      $window.location.href = '/pricing';
    }

  });
