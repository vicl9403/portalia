'use strict';

angular.module('portaliaApp')
    .directive('modalbeselect', () => ({
        templateUrl: 'components/modal-be-select/modalBeSelect.html',
        restrict: 'E',
        controller: 'ModalBeSelectController',
        controllerAs: 'modalbeselect'
    }));
