'use strict';

angular.module('portaliaApp')
  .controller('FooterController', function($scope, $location, Auth, $window, $http, appConfig, $rootScope) {
    $scope.logged = Auth.isLoggedIn;
  });
