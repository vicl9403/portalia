'use strict';

angular.module('portaliaApp')
  .directive('footer', function() {
    return {
      templateUrl: 'components/footer/footer.html',
      restrict: 'E',
      controller: 'FooterController',
      link: function(scope, element) {
        element.addClass('footer');
      }
    };
  });
