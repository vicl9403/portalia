'use strict';

angular.module('portaliaApp')
  .directive('adminMenuLateral', function () {
    return {
      templateUrl: 'components/admin-menu-lateral/admin-menu-lateral.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
