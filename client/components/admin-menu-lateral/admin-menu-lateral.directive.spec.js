'use strict';

describe('Directive: adminMenuLateral', function () {

  // load the directive's module and view
  beforeEach(module('portaliaApp'));
  beforeEach(module('components/admin-menu-lateral/admin-menu-lateral.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<admin-menu-lateral></admin-menu-lateral>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the adminMenuLateral directive');
  }));
});
