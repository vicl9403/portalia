'use strict';

angular.module('portaliaApp')
  .controller('NavbarController', function($scope, $location, Auth, $window, $http, appConfig, $rootScope, $routeParams, $route, $timeout) {

    $scope.setCookie = function ( cname, cvalue, exdays ) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    $scope.getCookie = function(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    };

    $scope.sendToContact = function () {
      $window.location = '/contacto';
    }

    Auth.getCurrentUser(function (user) {

      //if( $scope.getCookie('buenfin') == '' && $location.$$path != '/contacto' ){
        //$scope.setCookie('buenfin' , 'promocion' , 1 );
        //$("#modalPromocion").modal('show');
      //}

      $http.get('/api/quotes/category/' + user.categoria_name )
        .then( (res) => {
          $scope.quotesByCategory = res.data;
        });

    });

    $scope.getRandom = function(){
      return Math.round( Math.random() +1);
    }

    $scope.redirect = function ( url ) {
      $window.location.href = url;
    };

    $scope.$on('$routeChangeSuccess', function() {
      // alert($location.path());
      if ($location.path() == '/') {
        $('#searchBar').css('display', 'none');
        // alert("Ocultar")
      } else {
        // alert("Mostrar")
        $('#searchBar').css('display', 'block');
      }
    });
    $scope.data = {};
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    }];
    $scope.userData = Auth.getCurrentUser();
    $scope.logged = Auth.isLoggedIn;
    $scope.isCollapsed = true;
    var mode = appConfig.mode || "portalito";
    $scope.mode = mode;


    $scope.getHtmlResource2 = function (text) {
      var aux = text.split(' ');
      return aux[0] + '<br> <span class="margin-left-20">' + aux[1] + '</span>';
    };

    $scope.getHtmlResource = function (text) {
      var aux = text.split(' ');
      return aux[0] + ' ' + aux[1] + '<br> <span class="margin-left-20">' + aux[2] + '</span>';
    };


    $scope.openLoginModal = function(modo) {
      $scope.type = modo;
      $('#myModal').modal('show');
    }

    $scope.sendToProfile = function () {
      $window.location.href = '/perfil-usuario';
    }
    $(document).keypress(function(e) {
      if (e.which == 13) {
        if ($(".autocompleteNavbar").is(":focus")) {
          if ($(".autocompleteNavbar").val().length > 3) {
            console.log("Holi");
            $scope.busquedaLog = {};
            $scope.busquedaLog.idUsuario = $scope.userData._id
            $scope.busquedaLog.rolUsuario = $scope.userData.role
            $scope.busquedaLog.termino = $(".autocompleteNavbar").val()
            $http.post('/api/busquedas/', $scope.busquedaLog)
              .then(function(res) {
                if ($scope.userData != undefined) {
                  $scope.userData.busquedas.push($scope.busquedaLog.termino)
                  Auth.updateProfile($scope.userData)
                }
                console.log(res);
              })
              .catch(function(err) {
                console.log(err);
              })
            $window.location = "/resultadosObtenidos/buscar-" + $(".autocompleteNavbar").val() + "/tipo-producto";
          }
        }
      }
    });

    $("#inicioBoton").hover(
      function() {
        $("#imagenInicio").attr("src", "assets/images/icono_inicio_activo.png");
        $('#textoInicio').css('color', 'black');
      },
      function() {
        $("#imagenInicio").attr("src", "assets/images/icono_inicio_inactivo.png");
        $('#textoInicio').css('color', 'white');
      }
    );

    $("#promoBoton").hover(
      function() {
        $("#imagenPromo").attr("src", "assets/images/icono_promociones_activo.png");
        $('#textoPromo').css('color', 'black');
      },
      function() {
        $("#imagenPromo").attr("src", "assets/images/icono_promociones_inactivo.png");
        $('#textoPromo').css('color', 'white');
      }
    );


    $("#pricingBoton").hover(
      function() {
        $("#imagenPricing").attr("src", "assets/images/icono_pricing_activo.png");
        $('#textoPricing').css('color', 'black');
      },
      function() {
        $("#imagenPricing").attr("src", "assets/images/icono_pricing_inactivo.png");
        $('#textoPricing').css('color', 'white');
      }
    );

    $("#contactoBoton").hover(
      function() {
        $("#imagenContacto").attr("src", "assets/images/icono_contacto_activo.png");
        $('#textoContacto').css('color', 'black');
      },
      function() {
        $("#imagenContacto").attr("src", "assets/images/icono_contacto_inactivo.png");
        $('#textoContacto').css('color', 'white');
      }
    );

    $("#cotizacionesButton").hover(function() {
      $("#cotizacionesButton a img").attr("src", "assets/images/icons/cotiza-black.png");
    }, function() {
      $("#cotizacionesButton a img").attr("src", "assets/images/icons/cotiza.png");
    });

    $("#blogButton").hover(function() {
      $("#blogButton a img").attr("src", "assets/images/icono_blog_activo.png");
    }, function() {
      $("#blogButton a img").attr("src", "assets/images/icono_blog_inactivo.png");
    });


    $scope.setActive = function(menuItem, index) {
      $scope.activeMenu = menuItem
      $scope.indiceDrop = index
      $scope.idSector = $scope.sectors[index]._id
      $('#menuDrop').show();
    }

    $(document).mouseup(function(e) {
      var container = $("#menuDrop");

      if (!container.is(e.target) // if the target of the click isn't the container...
        &&
        container.has(e.target).length === 0) // ... nor a descendant of the container
      {
        container.hide();
      }
    });

    $http.get("api/sectors")
      .then(function(response) {
        $scope.sectors = response.data;
        $scope.autocomplete_options = {
          suggest: suggest_state,
          on_select: selectedItem
        };

        document.querySelector('.autocompleteNavbar').addEventListener('keypress', function(e) {
          var key = e.which || e.keyCode;
          if (key === 13) { // 13 is enter
          }
        });
      });

    var itsLogin = $location.search().login;
    if (itsLogin)
      $('#myModal').modal('show')

    function selectedItem(selected) {
      if (selected.type == "empresa") {
        $scope.busquedaLog = {};
        $scope.busquedaLog.idUsuario = $scope.userData._id
        $scope.busquedaLog.rolUsuario = $scope.userData.role
        $scope.busquedaLog.termino = $(".autocompleteNavbar").val()
        $http.post('/api/busquedas/', $scope.busquedaLog)
          .then(function(res) {
            console.log(res);
          })
          .catch(function(err) {
            console.log(err);
          })
        var empresa = selected.obj;
        $window.location = "/resultadosObtenidos/buscar-" + selected.value + "/tipo-producto";
      } else {
        var sector = selected.obj;
        var subsector = sector.subsectores[selected.index]
        $window.location.href = "/anuncios/" + sector.name + "/" + subsector.name;
      }
    }


    function suggest_state(term) {
      if (term.length < 2) {
        // alert("TE TROLLIÉ PAPUBUELO")
      } else {
        var q = term.toLowerCase().trim();
        var results = [];

        for (var i = 0; i < $scope.sectors.length && results.length < 10; i++) {
          if (i == ($scope.sectors.length - 1)) {
            var params = {
              "keyword": q
            }
            return $http.post('/api/users/busquedaTags', params)
              .then(function(res) {
                var users = res.data;
                var results = [];
                for (var d = 0; d < users.length; d++) {
                  var temp = users[d].tags.MatchInArray(term);
                  var tempproducts = users[d].products.MatchInArray(term);
                  var flag = true;
                  if (temp > -1) {
                    var label = users[d].tags[users[d].tags.MatchInArray(term)];
                    var value = users[d].tags[users[d].tags.MatchInArray(term)];
                  }
                  if (tempproducts > -1) {
                    var label = users[d].products[users[d].products.MatchInArray(term)];
                    var value = users[d].products[users[d].products.MatchInArray(term)];
                  }
                  if (String(label).indexOf(",") > -1) {
                    flag = false;
                  }
                  for (var pp = 0; pp < results.length; pp++) {
                    if (results[pp].label == label) {
                      flag = false;
                    }
                  }
                  if (flag) {
                    results.push({
                      label: label,
                      value: value,
                      type: "empresa",
                      obj: users[d]
                    });
                  }
                }
                var flagp = true;
                for (var i = 0; i < results.length; i++) {
                  console.log("i " + i);
                  var palabras = results[i].label.split(" ");
                  for (var j = 0; j < palabras.length; j++) {
                    if (palabras[j] == term.toUpperCase() && palabras.length < 2) {
                      console.log("+9++9++9+9+9+9-----------  " + i);
                      flagp = false;
                      break;
                    }
                  }
                }
                if (flagp) {
                  results.splice(0, 0, {
                    label: term.toUpperCase(),
                    value: term.toUpperCase(),
                    type: "empresa",
                    obj: term
                  });
                }

                return results;
              })
              .catch(function(err) {
                console.log(err);
              })
          }
        }

        return results;
      }
    }

    Array.prototype.MatchInArray = function(value) {

      var i;

      for (i = 0; i < this.length; i++) {
        value = value.toUpperCase();
        this[i] = this[i].toUpperCase();
        if (this[i].match(value)) {

          return i;

        }

      }

      return -1;

    };


    $scope.isActive = function(route) {
      return route === $location.path();
    }

    $scope.goToSector = function(indice) {
      $location.$$search = {};
      $('#menuDrop').hide();
      $scope.url = '/anuncios/pagina-1/sector-' + encodeURIComponent($scope.sectors[$scope.indiceDrop].name) + '/categoria-' + encodeURIComponent($scope.sectors[$scope.indiceDrop].subsectores[indice].name) + '/tipo-producto';
      // $window.location.href = $scope.url;
    }

    $scope.goToSectorMob = function(indice) {
      $('#bs-example-navbar-collapse-2').collapse('toggle');
      $('#sectorsText').toggle();
      $location.path('/subsectores').search({
        id: $scope.sectors[indice]._id
      });
    }

    $scope.logearse = function() {
      if (document.getElementById('recuerdame').checked) {
        $scope.loginRemember();
      } else {
        $scope.login();
      }
    }
    function callbackForAddToSegment(response) {
    if(response === -1) {
        console.log('User is not a subscriber or has blocked notifications');
    }

    if(response === false) {
        console.log('Segment name provided is not valid. Maximum length of segment name can be 30 chars and it can only contain alphanumeric characters, underscore and dash.');
    }

    if(response === true) {
        console.log('User got added to the segment successfully. Now you may run any code you wish to execute after user gets added to segment successfully');
    }
}
function callbackFunctionOnSuccessfulSubscription(subscriberId, values) {
  console.log(values);
  if(values.status == 'ALREADYSUBSCRIBED'){
    if (location.pathname == "/") {
      $window.location.href = '/perfil-usuario';
    } else if ($routeParams.cupon) {
      $window.location.href = '/pricing?cupon=' + $routeParams.cupon;
    } else if (location.pathname == '/resultadosObtenidos' || location.pathname == '/anuncios/') {
      $scope.userData = Auth.getCurrentUser();
      $scope.$broadcast("myEvent");
      $('#myModal').modal('toggle');
      if ($routeParams.contacto == 'true') {
        $timeout(function() {
          $('#contact-modal').modal('toggle');
        }, 1000);
      }
      // location.reload();
      console.log('lol');
    } else {
      $window.location.href = location.pathname;
    }
  } else {
    $scope.userData.pushSuscriberId.push(subscriberId);
    Auth.updateProfile($scope.userData)
    if (location.pathname == "/") {
      $window.location.href = '/perfil-usuario/mis-oportunidades/';
    } else if ($routeParams.cupon) {
      $window.location.href = '/pricing?cupon=' + $routeParams.cupon;
    } else if (location.pathname == '/resultadosObtenidos' || location.pathname == '/anuncios/') {
      $scope.userData = Auth.getCurrentUser();
      $scope.$broadcast("myEvent");
      $('#myModal').modal('toggle');
      if ($routeParams.contacto == 'true') {
        $timeout(function() {
          $('#contact-modal').modal('toggle');
        }, 1000);
      }
      // location.reload();
      console.log('lol');
    } else {
      $window.location.href = location.pathname;
    }
  }
}

    // TODO cambiar por / cuando sea el bueno
    $scope.login = function() {
      Auth.login($scope.data)
        .then(() => {

            // Bloque para obtener el usuario y generar el registro de loggin
            Auth.getCurrentUser( function (user) {
              $scope.userData = user;

              let loginData = {
                user : user._id,
              }
              $http.post('/api/logins' , loginData )
                .then( function (res) {
                  $scope.loggedAt = res.data;
                })

            });


            if (location.pathname == "/") {

              $window.location.href = '/perfil-usuario/mis-oportunidades';
            } else if ($routeParams.cupon) {
              $window.location.href = '/pricing?cupon=' + $routeParams.cupon;
            } else if (location.pathname == '/resultadosObtenidos' || location.pathname == '/anuncios/') {
              $scope.userData = Auth.getCurrentUser();
              $scope.$broadcast("myEvent");
              $('#myModal').modal('toggle');
              if ($routeParams.contacto == 'true') {
                $timeout(function() {
                  $('#contact-modal').modal('toggle');
                }, 1000);
              }
            } else {
              $window.location.href = location.pathname;
            }
        })
        .catch(err => {
          console.log(err);
          // alert(err.message);
          if (err.message == "Missing credentials") {
            sweetAlert("Error", "Faltan credenciales, favor de completar los datos pedidos.", "error");
          } else {
            sweetAlert(err.message);
          }

        });
    }

    $scope.loginRemember = function() {
      Auth.loginRemember($scope.data)
        .then(() => {
          $window.location.href = '/';
        })
        .catch(err => {
          // alert(err.message);
          if (err.message == "Missing credentials") {
            sweetAlert("Error", "Faltan credenciales, favor de completar los datos pedidos.", "error");
          } else {
            sweetAlert(err.message);
          }
        });
    }



    $scope.data.onFolderNumberKeyPress = function(event) {
      if (event.charCode == 13) //if enter then hit the search button
        $scope.login();
    }


    $scope.logout = function() {


      Auth.logout();

      // Setear variable de finalización de loggin
      $http.patch('/api/logins/logout/'  +  $scope.userData._id )
        .then( function (res) {
        })

      $window.location.href = "/";

    }
  });
