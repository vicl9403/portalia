'use strict';

angular.module('portaliaApp')
    .directive('contactform', () => ({
        templateUrl: 'components/contactForm/contactForm.html',
        restrict: 'E',
        controller: 'ContactFormController',
        controllerAs: 'contactform'
    }));
