'use strict';

angular.module('portaliaApp')
  .controller('ContactFormController', function($scope, $location, $window, $http, Auth, $timeout, $route, $routeParams) {

    // La variable $scope.userData está definida en el padre del componente
    // Contiene la información del usuario

    // Variable para saber si hay un usuario autenticado
    // $scope.logged = Auth.isLoggedIn;

    // Método para obtener los sectores
    $scope.getSectors = function() {
      // Obtener los sectores
      $http.get('/api/sectors/')
        .then(function(res) {
          $scope.sectors = res.data;

          $scope.getSubSectors($scope.userData.sector_name);
        });
    }

    $scope.getSubSectors = function(sector_name) {
      // Iteramos en todos los sectores
      for (var i = 0; i < $scope.sectors.length; i++) {
        // Detectamos cuando el sector seleccionado corresponda al que se va iterando
        if (sector_name == $scope.sectors[i].name) {
          // Actualizamos los subsectores del sector seleccionado
          var subsectores = $scope.sectors[i].subsectores;

          $scope.subsectores = [];
          //Obtener el array simple de subsectores
          for (var c = 0; c < subsectores.length; c++)
            $scope.subsectores.push(subsectores[c].name)

          // Actualizamos el scope porque no se está utilizando función de angular
          // $scope.$apply();
          break;
        }
      }
    }

    // Método para obtener los estados
    $scope.allStates = function() {
      // Obtener los estados
      $http.get('/api/estados/')
        .then(function(res) {
          var states = res.data;

          $scope.states = [];
          //Obtener el array simple de subsectores
          for (var c = 0; c < states.length; c++)
            $scope.states.push(states[c].name)
        });
    }

    // Función para actualizar los subsectores
    $("#sector").change(function() {
      $scope.getSubSectors($("#sector option:selected").text());
    });

    //Arreglo con las unidades a mostrar en el combobox de unidad
    $scope.unities = [
      'Piezas', 'Kilogramos', 'Toneladas Métricas', 'Litros', 'Equipos', 'Envío', 'Pies cúbicos', 'Gramos', 'Libras', 'Frascos', 'Cajas', 'Capsulas / Tabletas', 'Metros',
      'Metros cuadrados', 'Metros cúbicos', 'Onzas', 'Rollos', 'Servicios', 'MMlb', 'fl oz.', 'BTU', 'Kit o paquete', 'Galón', 'Bolsas', 'Millar', 'BOU (Billones de Unidades)', 'Lote'
    ];

    //Arreglo con las frecuencias a mostrar en el combobox
    $scope.frecuencies = [
      'Diario', 'Semanal', 'Quincenal', 'Mensual', 'Bimestral', 'Trimestral', 'Semestral', 'Anual', 'Única vez', 'Para pruebas'
    ]

    //Arreglo con los tipos de usos a mostrar en el combobox
    $scope.productUses = [
      'Consumo propio', 'Reventa (soy distribuidor)', 'Reventa (soy trader / broker)', 'Reventa (yo también lo fabrico)'
    ]

    //Arreglo con los tipos de teléfono
    $scope.phoneTypes = [
      'Celular', 'Fax', 'Fijo', 'Nextel', 'Nextel ID', 'Sin costo (01 800)'
    ]

    $scope.$on("myEvent", function(event, args) {
      // alert('lol')
      $scope.logged = true;
      $scope.initialize();
    });
    // $scope.contactBussiness que viene desde el padre contiene el id de a quien se quiere contactar
    // En caso de que sea un contacto directo con la empresa

    $scope.initialize = function() {
      //Obtener el usuario loggeado
      var user = Auth.getCurrentUser();
      $scope.userData = user;
      if (user.nombre != undefined) {
        $scope.logged = true
      } else {
        $scope.logged = false;
      }

      // Arreglo con los datos del contacto/cotización
      $scope.contactData = {
        product: '',
        sector: (user.hasOwnProperty('sector_name')) ? user.sector_name : '',
        category: (user.hasOwnProperty('categoria_name')) ? user.categoria_name : '',
        state: (user.hasOwnProperty('estado_name')) ? user.estado_name : '',
        quantity: '',
        unity: '',
        frecuency: '',
        date: '',
        product_use: '',
        comments: '',
        segmentId: '',
        bussiness_name: (user.hasOwnProperty('nombre')) ? user.nombre : '',
        contact_name: '',
        contact_lastname: '',
        contact_email: (user.hasOwnProperty('email')) ? user.email : '',
        contact_phone: {
          type: '',
          lada: '',
          number: (user.hasOwnProperty('telefono')) ? user.telefono : '',
        },
        user: $routeParams.idContact,
      };
      if($routeParams.idContact == undefined){
        delete $scope.contactData.user;
      }
    }



    // Establecer el document ready del documento
    angular.element(document).ready(function() {
      jQuery('#date').datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD',
      });
      // Obtener los sectores
      $scope.getSectors();

      // Obtener los estados
      $scope.allStates();

      $timeout(function() {
        $scope.initialize();
      }, 100);


    });

    $scope.saveParent = function(id) {
      var param = {};
      param['idContact'] = id;
      $route.updateParams(param);
      $scope.Aidi = id;
    }

    $scope.$watch('Aidi', function(newValue, oldValue) {
      // scope.counter = scope.counter + 1;
    });

    $scope.openLoginModal = function() {
      var param = {};
      param['contacto'] = 'true';
      $route.updateParams(param);
      $("#contact-modal").modal('hide');
      $("#myModal").modal('show');
    }

    $scope.getMomentTime = function(time) {
      return moment(time).format('YYYY-MM-DDTHH:mm:SS');
    }

    $scope.saveContact = function(isValidForm) {
      if (isValidForm) {        //Parsear la fecha que seleccionó el usuario
        $scope.contactData.date = new Date($scope.getMomentTime($("#date").val()));
        $scope.contactData.user = $routeParams.idContact;
        switch ($scope.contactData.sector) {
          case 'Servicios especializados':
              $scope.contactData.segmentId = '146535'
            break;
          case 'Turismo':
              $scope.contactData.segmentId = '146536'
            break;
          case 'Logistica y transporación':
              $scope.contactData.segmentId = '146537'
            break;
          case 'Industrias manufactureras':
              $scope.contactData.segmentId = '146538'
            break;
          case 'Electricidad y agua':
              $scope.contactData.segmentId = '146539'
            break;
          case 'Construcción':
              $scope.contactData.segmentId = '146540'
            break;
          case 'Agroindustria':
              $scope.contactData.segmentId = '146541'
            break;
          default:
            console.log('visitante');

        }

        //Poner el id de la empresa a contactar

        $http.post('/api/quotes', $scope.contactData)
          .then(function(res) {
            $scope.initialize();
            if($routeParams.idContact == undefined){
              window.location.href = "/cotizaciones/exito";
            } else {
              $window.open("/contactoExitoso", '_self');
            }
            $scope.contactForm.$setPristine();
            $scope.contactForm.$setUntouched();
            $("#contact-modal").modal('hide');
          });

      } else
        swal("Error", "Hay errores en el formulario, por favor corrige los campos marcados en rojo", "error");

    }

    $scope.log = function() {
    }

  });
