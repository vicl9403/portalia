'use strict';

angular.module('portaliaApp')
  .directive('adminMenuSuperior', function () {
    return {
      templateUrl: 'components/admin-menu-superior/admin-menu-superior.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
