'use strict';

describe('Directive: adminMenuSuperior', function () {

  // load the directive's module and view
  beforeEach(module('portaliaApp'));
  beforeEach(module('components/admin-menu-superior/admin-menu-superior.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<admin-menu-superior></admin-menu-superior>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the adminMenuSuperior directive');
  }));
});
