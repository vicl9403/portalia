// $("#document").ready(function()
// {
//
//
//   google.charts.load('current', {packages: ['corechart']});
//
//   // Chart de registros generales
//   google.charts.setOnLoadCallback(drawChartRegistrosGenerales);
//   // Chart de registros sector
//   google.charts.setOnLoadCallback(drawChartRegistrosSector);
//
//   $('.grid-categoria').masonry();
//   $('.grid-tab').masonry();
//
//   $("#menu1").addClass('tab-pane');
//   $("#menu2").addClass('tab-pane');
//   $("#menu3").addClass('tab-pane');
//   $("#menu4").addClass('tab-pane');
//   $("#menu5").addClass('tab-pane');
//   $("#menu6").addClass('tab-pane');
//
//
//   $("#tab1").click();
//
//
//   $('#tabSubcategorias a').click(function (e) {
//     e.preventDefault()
//     $(this).tab('show')
//   })
//
// });
//
// function drawChartRegistrosGenerales()
// {
//     var data = google.visualization.arrayToDataTable([
//       ['Year', 'GRATIUTO', 'SELECT','PLUS'],
//       ['2004',  1000,      400,   100],
//       ['2005',  1170,      460,   366],
//       ['2006',  660,       1800,  100],
//       ['2007',  1030,      540,   800]
//     ]);
//
//     var options = {
//       legend: { position: 'top' },
//       colors: ['#FFD700', '#FF9000', '#ff0000']
//     };
//
//     var chart = new google.visualization.LineChart(document.getElementById('chartRegistrosGenerales'));
//
//     chart.draw(data, options);
//   }
//
//   function drawChartRegistrosSector()
//   {
//       var data = google.visualization.arrayToDataTable([
//         ['Year', 'INDUSTRIAS MANUFACTURERAS', 'ELECTRICIDAD Y AGUA','SERVICIOS ESPECIALIZADOS', 'CONSTRUCCIÓN' ,'AGROINDUSTRIA','TURISMO'],
//         ['2004',  1000,      400,   100 , 200, 300, 500],
//         ['2005',  1170,      460,   366 , 200, 300, 500],
//         ['2006',  660,       1800,  100 , 200, 300, 500],
//         ['2007',  1030,      540,   800 , 200, 300, 500]
//       ]);
//
//       var options = {
//         legend: { position: 'top', maxLines: 4 },
//
//         aggregationTarget: 'category',
//         colors: ['#ffd700', '#10b2ff', '#ff0000','9fd700', '6f00ff', 'ff9000']
//       };
//
//       var chart = new google.visualization.LineChart(document.getElementById('chartRegistrosSector'));
//
//       chart.draw(data, options);
//     }
