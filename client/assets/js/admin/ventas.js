$("#document").ready(function()
{

  $('#dateDesde2').datetimepicker();
  $('#dateHasta2').datetimepicker();

  google.charts.load('current', {packages: ['corechart']});
  google.charts.setOnLoadCallback(drawChartVentas);


});

function drawChartVentas()
{
    var data = google.visualization.arrayToDataTable([
      ['Year',  'SELECT','PLUS'],
      ['2004',    400,   100],
      ['2005',    460,   366],
      ['2006',    1800,  100],
      ['2007',    540,   800]
    ]);

    var options = {
      legend: { position: 'top' },
      colors: ['#FFD700', '#FF9000', '#ff0000']
    };

    var chart = new google.visualization.LineChart(document.getElementById('chartVentas'));

    chart.draw(data, options);
}
