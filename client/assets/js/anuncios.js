$(document).ready(function() {
  $(".favorite").mouseenter(function() {
    if ($(this).hasClass('marked') == false)
      $(this).attr('src', 'assets/images/icons/favorito-after.svg');
  });
  $(".favorite").mouseout(function() {
    if ($(this).hasClass('marked') == false)
      $(this).attr('src', 'assets/images/icons/favorito-before.svg');
  });
  $(".favorite").click(function() {
    $(this).toggleClass('marked');
  });
  $(".favorite-free").mouseenter(function() {
    if ($(this).hasClass('marked') == false)
      $(this).attr('src', 'assets/images/icons/favorito-after.svg');
  });
  $(".favorite-free").mouseout(function() {
    if ($(this).hasClass('marked') == false)
      $(this).attr('src', 'assets/images/icons/favorito-before.svg');
  });
  $(".favorite-free").click(function() {
    $(this).toggleClass('marked');
  });
  //Termina código que setea favoritos
});
