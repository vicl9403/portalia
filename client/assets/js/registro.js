$(document).ready(function() {
  // $(".navbar").removeClass('navbar-fixed-top');
  $('select').on("change", function() {
    $(this).removeClass('invalid');
  });

  // $("#formEmail").on("blur", function() {
  //   if (!isValidEmailAddress($(this).val())) {
  //     $(this).addClass('invalid');
  //     $("#error-mail").removeClass('hidden');
  //   } else {
  //     $(this).removeClass('invalid');
  //     $("#error-mail").addClass('hidden');
  //   }
  // });

  $("#password1").on("blur", function() {
    var pass1 = $("#password1").val();
    var pass2 = $("#password2").val();

    if (pass1 != pass2) {
      $("#password2").addClass('invalid');
      $("#error-password").removeClass('hidden');
    } else {
      $("#password2").removeClass('invalid');
      $("#password1").removeClass('invalid');
      $("#error-password").addClass('hidden');
    }
  });

  // $("#formEmail").on("blur", function() {
  //   var correo1 = $("#formEmail").val();
  //   var correo2 = $("#formEmailDos").val();

  //   if (correo1 != correo2) {
  //     $("#formEmailDos").addClass('invalid');
  //     $("#error-correo").removeClass('hidden');
  //   } else {
  //     $("#formEmailDos").removeClass('invalid');
  //     $("#formEmail").removeClass('invalid');
  //     $("#error-correo").addClass('hidden');
  //   }
  // });

  $("#nombreEmpresa").on("blur", function() {
    if ($(this).val() != '')
      $(this).removeClass('invalid');
  })
  $("#razon").on("blur", function() {
    if ($(this).val() != '')
      $(this).removeClass('invalid');
  })
  $("#password2").on("blur", function() {
    $("#password1").blur();
  });

  // $("#formEmailDos").on("blur", function() {
  //   $("#formEmail").blur();
  // });


  $("#rfc").on("blur", function() {
    if (!isValidRFC($(this).val()) && $(this).val() != '') {
      $(this).addClass('invalid');
      $("#error-rfc").removeClass('hidden');
    } else {
      $(this).removeClass('invalid');
      $("#error-rfc").addClass('hidden');
    }
  });


  $(".check-yellow").click(function() {
    if ($(this).attr('src') == 'assets/images/checkbox/yellow-checked.png') {
      $(this).prop('src', 'assets/images/checkbox/yellow-normal.png');
      $(this).prev('input').attr('checked', false);
    } else {
      $(this).prop('src', 'assets/images/checkbox/yellow-checked.png');
      $(this).prev('input').prop('checked', true);

    }
  });
  $(".check-white").click(function() {
    if ($(this).attr('src') == 'assets/images/checkbox/white-checked.png') {
      $(this).prop('src', 'assets/images/checkbox/white-normal.png');
      $(this).prev('input').attr('checked', false);
    } else {
      $(this).prop('src', 'assets/images/checkbox/white-checked.png');
      $(this).prev('input').prop('checked', true);

    }
  });

  $("[name=registro]").submit(function(e) {

    var valid = true;
    $('[name=registro] input').each(function() {

      if ($(this).val() == '' &&
        $(this).attr('id') != "cupon" &&
        $(this).attr('id') != "codigo" &&
        $(this).attr('id') != "razon" &&
        $(this).attr('id') != "rfc" &&
        $(this).attr('id') != "blog" &&
        $(this).attr('id') != "uno" &&
        $(this).attr('id') != "dos" &&
        $(this).attr('id') != "tres" &&
        $(this).attr('id') != "cuatro" &&
        $(this).attr('id') != "cinco" &&
        $(this).attr('id') != "seis"
        // hasclass
      ) {
        valid = false;
        $(this).addClass('invalid');
        $("#error-general").removeClass('hidden');
      }

    })
    $('[name=registro] select').each(function() {

      if ($(this).val() == '') {
        valid = false;
        $(this).addClass('invalid');
        $("#error-general").removeClass('hidden');
      }

    })
    if (valid == true) {
      $("#error-general").addClass('hidden');
    }

    return false;
  });
});


function isValidEmailAddress(emailAddress) {
  var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  return pattern.test(emailAddress);
};

function isValidRFC(rfcStr) {
  var strCorrecta;
  strCorrecta = rfcStr;
  if (rfcStr.length == 12) {
    var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
  } else {
    var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
  }
  var validRfc = new RegExp(valid);
  var matchArray = strCorrecta.match(validRfc);
  if (matchArray == null) {
    // alert('Cadena incorrectas');

    return false;
  } else {
    // alert('Cadena correcta:' + strCorrecta);
    return true;
  }

}
