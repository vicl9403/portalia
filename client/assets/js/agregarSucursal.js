var map;
var LatLang = {
  lat: 22.918200,
  lng: -102.089014
}
var marker;

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: LatLang,
    zoom: 5,
    streetViewControl: false
  });

  marker = new google.maps.Marker({
    position: LatLang,
    map: map,
    draggable: true
  });
  marker.addListener('dragend', function() {
    var lt = marker.getPosition().lat();
    var lg = marker.getPosition().lng();
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lt + ',' + lg + '&sensor=true', //NOTE: AÑADIR SERVER KEY CUANDO SE HAGA DEPLOY
        type: 'GET',
        dataType: 'json'
      })
      .success(function(res) {
        console.log("Datos del mapa: ");
        console.log(res);
        var datosMapa = res;
        document.getElementById("estado").value = datosMapa.results[0].address_components[5].long_name;
        document.getElementById("ciudad").value = datosMapa.results[0].address_components[3].long_name;
        document.getElementById("numero").value = datosMapa.results[0].address_components[0].long_name;
        document.getElementById("colonia").value = datosMapa.results[0].address_components[2].long_name;
        document.getElementById("calle").value = datosMapa.results[0].address_components[1].long_name;
        if (datosMapa.results[0].address_components[7] != undefined) {
          document.getElementById("cp").value = datosMapa.results[0].address_components[7].long_name;
        }
        document.getElementById("lat").value = lt;
        document.getElementById("long").value = lg;
      })
      .error(function(err) {
        console.log(err);
      });
  });
}

function placeMarker(location) {
  marker = new google.maps.Marker({
    position: location,
    map: map,
    draggable: true
  });
  map.setCenter(location);
  var markerPosition = marker.getPosition();
  populateInputs(markerPosition);
  google.maps.event.addListener(marker, "drag", function(mEvent) {
    populateInputs(mEvent.latLng);
  });
}

function setMarker(latitud, longitud) {
  marker.setMap(null);
  console.log(latitud);
  LatLang.lat = parseFloat(latitud);
  LatLang.lng = parseFloat(longitud);
  marker = new google.maps.Marker({
    position: LatLang,
    map: map,
    draggable: true
  });
  marker.addListener('dragend', function() {
    var lt = marker.getPosition().lat();
    var lg = marker.getPosition().lng();
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lt + ',' + lg + '&sensor=true', //NOTE: AÑADIR SERVER KEY CUANDO SE HAGA DEPLOY
        type: 'GET',
        dataType: 'json'
      })
      .success(function(res) {
        console.log("Datos del mapa: ");
        console.log(res);
        var datosMapa = res;
        document.getElementById("estado").value = datosMapa.results[0].address_components[5].long_name;
        document.getElementById("ciudad").value = datosMapa.results[0].address_components[3].long_name;
        document.getElementById("numero").value = datosMapa.results[0].address_components[0].long_name;
        document.getElementById("colonia").value = datosMapa.results[0].address_components[2].long_name;
        document.getElementById("calle").value = datosMapa.results[0].address_components[1].long_name;
        if (datosMapa.results[0].address_components[7] != undefined) {
          document.getElementById("cp").value = datosMapa.results[0].address_components[7].long_name;
        }
        document.getElementById("lat").value = lt;
        document.getElementById("long").value = lg;
      })
      .error(function(err) {
        console.log(err);
      });
  });
  var latLng = marker.getPosition(); // returns LatLng object
  map.setCenter(latLng); // setCenter takes a LatLng object
  map.setZoom(15);
}

function populateInputs(pos) {
  document.getElementById("t1").value = pos.lat()
  document.getElementById("t2").value = pos.lng();
}

function pan() {
  if (navigator.geolocation) {
    // Call getCurrentPosition with success and failure callbacks
    navigator.geolocation.getCurrentPosition(success, fail);
  } else {
    alert("Sorry, your browser does not support geolocation services.");
  }
}

function success(position) {

  var panPoint = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
  map.panTo(panPoint);
  map.setZoom(15);
  marker.setPosition(panPoint);
}

function fail() {
  alert("Ha habido un problema definiendo tu localización actual, por favor usa el marcador para buscarla manualmente")
}
