var newLatLang;
var map;
var resultados;
var LatLang = {
  lat: 22.918200,
  lng: -102.089014
}
var marker;
var markerUser;
var autocomplete;
var infowindow;
var markers = [];

function initMap() {
  initAutocomplete()
  map = new google.maps.Map(document.getElementById('map'), {
    center: LatLang,
    zoom: 5,
    scaleControl: true,
    streetViewControl: false,
    draggable: true,
    scrollwheel: false
  });

  infowindow = new google.maps.InfoWindow({
    content: "<div id='infowindow'>" + "Hola" + "</div>"
  });

  var image = {
    url: 'https://cdn0.iconfinder.com/data/icons/user-icons-4/100/user-17-512.png', // image is 512 x 512
    scaledSize: new google.maps.Size(50, 50),
  };

  markerUser = new google.maps.Marker({
    position: LatLang,
    map: map,
    draggable: true,
    icon: image
  });

  markerUser.addListener('dragend', function() {
    var lt = markerUser.getPosition().lat();
    var lg = markerUser.getPosition().lng();
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lt + ',' + lg + '&sensor=true', //NOTE: AÑADIR SERVER KEY CUANDO SE HAGA DEPLOY
        type: 'GET',
        dataType: 'json'
      })
      .success(function(res) {
        console.log("Datos del mapa: ");
        console.log(res);
        var datosMapa = res;
        document.getElementById('pac-input').value = datosMapa.results[0].formatted_address
        $("#latitude").val(datosMapa.results[0].geometry.location.lat);
        $("#longitude").val(datosMapa.results[0].geometry.location.lng);
        $('[ng-controller="zonaGeograficaClr"]').scope().buscar();
      })
      .error(function(err) {
        console.log(err);
      });
  });

  pan();
}

function pan() {
  if (navigator.geolocation) {
    // Call getCurrentPosition with success and failure callbacks
    navigator.geolocation.getCurrentPosition(success, fail);
  } else {
    alert("Sorry, your browser does not support geolocation services.");
  }
}

function success(position) {

  var panPoint = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
  map.panTo(panPoint);
  map.setZoom(13)
  markerUser.setPosition(panPoint);
  var lt = markerUser.getPosition().lat();
  var lg = markerUser.getPosition().lng();
  $.ajax({
      url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lt + ',' + lg + '&sensor=true', //NOTE: AÑADIR SERVER KEY CUANDO SE HAGA DEPLOY
      type: 'GET',
      dataType: 'json'
    })
    .success(function(res) {
      console.log("Datos del mapa Geoloc: ");
      console.log(res);
      var datosMapaGeo = res;
      document.getElementById('pac-input').value = datosMapaGeo.results[0].formatted_address
      console.log(datosMapaGeo.results[0].geometry.location.lat);
      console.log(datosMapaGeo.results[0].geometry.location.lng);
      $("#latitude").val(datosMapaGeo.results[0].geometry.location.lat);
      $("#longitude").val(datosMapaGeo.results[0].geometry.location.lng);
      // $('[ng-controller="zonaGeograficaClr"]').scope().buscar();
    })
    .error(function(err) {
      console.log(err);
    });
}

function fail() {
  //alert("Ha habido un problema definiendo tu localización actual, por favor usa el marcador para buscarla manualmente")
}

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    (document.getElementById('pac-input')), {
      types: ['geocode']
    });

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  var place = autocomplete.getPlace();
  newLatLang = {
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng()
  }

  $('#estado').val("");
  $('#municipio').val("");
  $('#radio').val("5");
  $("#latitude").val(place.geometry.location.lat());
  $("#longitude").val(place.geometry.location.lng());
  map.setCenter(newLatLang);
  map.setZoom(15);
  $('[ng-controller="zonaGeograficaClr"]').scope().buscar();
}

function setMapRadio(sel) {
  var zoomValue = sel.value;

  if (zoomValue == 2) {
    map.setZoom(15)
  } else if (zoomValue == 5) {
    map.setZoom(13)
  } else if (zoomValue == 10) {
    map.setZoom(12)
  }

}

function sendResults() {
  resultados = $('[ng-controller="zonaGeograficaClr"]').scope().items;
  paintResults();
}

function paintResults() {
  for (i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }

  for (var i = 0; i < resultados.length; i++) {
    for (var x = 0; x < resultados[i].sucursales.length; x++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(resultados[i].sucursales[x].latitud, resultados[i].sucursales[x].longitud),
        map: map
      });
      marker.set("id", x);
      var contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<a href="/perfil/' + resultados[i]._id + '" target="_blank"><h1 id="firstHeading" class="firstHeading">' + resultados[i].nombre + '</h1></a>' +
        '<div id="bodyContent">' +
        '<p>' + resultados[i].sucursales[x].nombreSucu + '<br>' + resultados[i].sucursales[x].calle + ' ' + resultados[i].sucursales[x].numero + ', ' +
        resultados[i].sucursales[x].colonia +
        '.</p>' +
        '<p><a target="_blank" href="https://maps.google.com?saddr=Current+Location&daddr=' +
        resultados[i].sucursales[x].latitud +
        ',' +
        resultados[i].sucursales[x].longitud + '">Obtener Direcciones</a></p> ' +
        '</div>' +
        '</div>';
      markers.push(marker);
      bindInfoWindow(marker, map, infowindow, i, x, contentString);
    }
  }
}

function bindInfoWindow(marker, map, infowindow, i, x, contentString) {
  marker.addListener('click', function() {
    infowindow.setContent(contentString);
    infowindow.open(map, this);
  });
}
