'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/categorias', {
        template: '<categorias></categorias>'
      });
  });
