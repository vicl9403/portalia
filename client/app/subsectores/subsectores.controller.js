'use strict';

(function () {

    angular.module('portaliaApp').component('subsectores', {
        templateUrl: 'app/subsectores/subsectores.html'
    }).controller('subsectoresCrl', function ($scope, $http, $routeParams, $window, $location, Auth) {
        $http.get('api/sectors/' + $routeParams.id).then(function (response) {
            console.log(response.data);
            $scope.sector = response.data;
        });


        $scope.goToAnuncios = function (indice) {

            $scope.url = '/anuncios?requestedPage=1&sector=' +
                encodeURIComponent($scope.sector.name) + '&categoria=' +
                encodeURIComponent($scope.sector.subsectores[indice].name) + '&tipoBusqueda=productos';

            $window.location.href = $scope.url;
        };
    });
})();
//# sourceMappingURL=subsectores.controller.js.map
