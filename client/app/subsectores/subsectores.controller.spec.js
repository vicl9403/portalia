'use strict';

describe('Component: SubsectoresComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SubsectoresComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    SubsectoresComponent = $componentController('subsectores', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
