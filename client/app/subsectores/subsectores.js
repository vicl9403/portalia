'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/subsectores', {
        template: '<subsectores></subsectores>'
      });
  });
