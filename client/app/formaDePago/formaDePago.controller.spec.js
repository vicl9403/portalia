'use strict';

describe('Component: FormaDePagoComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var FormaDePagoComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    FormaDePagoComponent = $componentController('FormaDePagoComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
