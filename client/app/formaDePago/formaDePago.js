'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/formaDePago', {
        template: '<forma-de-pago></forma-de-pago>'
      });
  });
