'use strict';
(function() {

  angular.module('portaliaApp')
    .component('formaDePago', {
      templateUrl: 'app/formaDePago/formaDePago.html'
    }).controller('pagoCrl', function($http, $scope, Auth, $routeParams, $window, $filter, appConfig, $timeout) {




      angular.element(document).ready(function () {

        var card = new Card({
          form: 'form#cardForm',
          container: '.card-wrapper',

          width: 450,

          placeholders: {
            number: '**** **** **** ****',
            name: 'Nombre',
            expiry: '**/****',
            cvc: '***'
          },

          messages: {
            validDate: 'expire\ndate',
            monthYear: 'mm/yy'
          }

        });

        // Variable para verificar el cuadro de términos en facturación
        $scope.terminos = false;
        // Variable para bloquear la interfaz y no permitir al usuario hacer nada cuando se le dio en pagar
        $scope.pendingTransaction = false;

        $scope.card = {};
        $scope.card.number = '';
        $scope.card.name = '';
        $scope.card.expiration = '';
        $scope.card.cvc = 222;

        $http.get('/api/openpay/keys')
          .then( res => {
            OpenPay.setId( res.data.id );
            OpenPay.setApiKey( res.data.pbk );
            OpenPay.setSandboxMode( res.data.sandboxMode );
          });

      });


      Auth.getCurrentUser(function(currentUser) {
        $scope.userData = currentUser // Your code
        if (!$scope.userData.planSolicitadoPricing) {
          //$window.location = '/pricing';
        } else {

          $http.post('api/users/calcularCodigos')
            .then(function(response) {

              $scope.information = response.data;

              if (moment().isBefore('2016-11-15') && moment().isAfter('2016-10-01')) {
                $scope.information.total = 0
              }

              $scope.fechaFront = moment($scope.userData.fechaCorte).format("DD/MM/YY");
              $scope.price = parseInt($scope.userData.planSolicitadoPricing.price);
              $scope.plan = $scope.userData.planSolicitadoPricing.plan;
              $scope.period = parseInt($scope.userData.planSolicitadoPricing.period);
              $scope.subtotal = $scope.price * $scope.period;

              switch ($scope.period) {
                case 1:
                  $scope.discount = parseFloat("0");
                  break;
                case 3:
                  $scope.discount = parseFloat("0.05");
                  break;
                case 6:
                  $scope.discount = parseFloat("0.10");
                  break;
                case 12:
                  $scope.discount = parseFloat("0.20");
                  break;
                default:
                  $scope.discount = parseFloat("0");
              }

              $scope.discountMoney = (($scope.price * $scope.period) * $scope.discount);
              /************************************************************************************/
              $scope.total = ($scope.price * $scope.period) - (($scope.price * $scope.period) * $scope.discount);
              if ($scope.information.credito > 0) {
                $scope.total -= parseInt($scope.information.credito);
              }
              $scope.openPayData = {};
              $scope.userData = currentUser;
              $scope.deviceSessionId = OpenPay.deviceData.setup('payment-form');
              $scope.openpayBankCalled = false;
            });


        }
      });

      $('#modal-success').on('hidden.bs.modal', function () {
        $scope.goToMain();
      })

      $scope.goToMain = function() {
        //Esta función te lleva al main después de pagar con éxito, pero antes de hacer la
        // redirección hace un post a PaymentLogs para el módulo administrativo
        //además de general el usuario de Wordpress para el blog de web marketing
        if ($scope.userData.planSolicitadoPricing.periodo == 'year') {
          $scope.type = 12;
        } else {
          $scope.type = 6;
        }
        $scope.paymentData = {
          paquete: 'Select',
          periodo: $scope.type + ' meses',
          numeroPagos: $scope.userData.planSolicitadoPricing.numeroPagos,
          precioPorPago: $scope.userData.planSolicitadoPricing.frequentCost,
          total: $scope.userData.planSolicitadoPricing.total,
          endDate: moment().add($scope.type, 'months'),
          userId: $scope.userData._id,
          userName: $scope.userData.nombre,
          userEmail: $scope.userData.email,
          userSector: $scope.userData.sector_name,
          userCategoria: $scope.userData.categoria_name,
          userState: $scope.userData.estado_name
        };
        $http.post('/api/PaymentLogs/', $scope.paymentData)
          .then(function(res){
            $scope.dataWP = {
              nombre: $scope.userData.email,
              email: $scope.userData.email
            }
            $http.post('/api/users/generarWP/', $scope.dataWP)
              .then(function(res){
                $window.location = '/perfil-usuario/membresia';
              });
          });
      };

      //Actualiza tus datos de facturación
      $scope.updateDatosFacturacion = function() {
        document.getElementById("pagar").style.pointerEvents = "all";

        var validacion = true;
        if(document.getElementById('acepto').checked){
          if ($scope.modalFacturacion.$valid) {
            if (isValidRFC($("#rfc").val())) {
              Auth.updateProfile($scope.userData)
              $('#modal-facturacion').modal('hide');
            } else {
              swal("Error", "Introduce un RFC válido.", "error");
            }
          } else {
            swal("Error", "Todos los campos requieren datos válidos", "error")
          }
        } else {
          swal("Error", "Acepta que tus datos son correctos", "error")
        }
      };

      // Actualizar el perfil del usuario para que se guarden sus preferencias
      $scope.updateUserProfile = function () {
        $http.patch('/api/users/' + $scope.userData._id , $scope.userData )
          .then( res => {
            $scope.userData = res.data;
          })
      }

      $scope.isValidCard = function () {

        if( $scope.card== undefined ) {
          swal('Advertencia', 'Los datos proporcionados son inválidos, por favor verifica tu información', 'warning');
          return false;
        }

        if( $scope.card.number == undefined || $scope.card.expiration == undefined || $scope.card.cvv == undefined ) {
          swal('Advertencia', 'Los datos proporcionados son inválidos, por favor verifica tu información', 'warning');
          return false;
        }

        if( ! $.payment.validateCardNumber( $scope.card.number )) {
          swal('Advertencia','El número de tarjeta ingresado es incorrecto', 'warning');
          return false;
        }


        if( ! $.payment.validateCardExpiry( $scope.card.expiration.split('/')[0] , $scope.card.expiration.split('/')[1] )) {
          swal('Advertencia', 'La fecha de expiración de tu tarjeta es incorrecta, o tiene un formato inválido' , 'warning');
          return false;
        }

        if( ! $.payment.validateCardCVC($scope.card.cvv) ) {
          swal('Advertencia', 'El código de seguridad de tu tarjeta es inválido', 'warning');
          return false;
        }

        return true ;

      }

      // Esta función toma los datos de la vista para asignarle a openpayData los datos para
      // que se puedan generar tokens
      $scope.setInitialOpenpayTokenData = function () {

        // Asignar los valores a Openpay
        $scope.openPayData.holder_name = $scope.card.name;
        $scope.openPayData.card_number = $scope.card.number.replace(/\s/g, "") ;
        $scope.openPayData.cvv2 = $scope.card.cvv;
        $scope.openPayData.expiration_month = $scope.card.expiration.split('/')[0].replace(/\s/g, "") ;
        $scope.openPayData.expiration_year = $scope.card.expiration.split('/')[1].replace(/\s/g, "") ;

      }

      //PAGO CON LA TARJETA DE CREDITO UNA SOLA EXHIBICIÓN
      $scope.openpayCard = function() {

        //Define el periodo de plan que se va a pagar, doce o seis meses
        if ($scope.userData.planSolicitadoPricing.periodo == 'year') {
          $scope.type = 12;
        } else {
          $scope.type = 6;
        }

        // Des habilitar el botón para prevenir dobles suscripciones
        $scope.pendingTransaction = true;

        // Validar los datos de la tarjeta desde el front
        if( ! $scope.isValidCard() ) {
          $scope.pendingTransaction = false;
          return;
        }

        if( $scope.userData.datosDeFacturacion.requested ) {
          if( ! $scope.invoiceForm.$valid ) {
            $scope.pendingTransaction = false;
            $scope.invoiceForm.$submitted = true;
            swal('Advertencia', 'Favor de verificar tus datos de facturación', 'warning');
            return;
          }
        }

        // Actualizar el perfil del usuario
        $scope.updateUserProfile();

        // Función que prepara openpayData con los datos necesarios para tokenizar tarjetas
        $scope.setInitialOpenpayTokenData();

        //Crea el token de OpenPay para el pago
        OpenPay.token.create($scope.openPayData, function(res) {


            if( res.status != 200 ) {
              swal('Error','No pudimos registrar tu tarjeta, intenta con otra por favor','error');
              return;
            }

            $scope.data = {
              'paymentData': {
                //Se utiliza el id del token para source_id, una variable que requiere OpenPay
                'source_id': res.data.id,
                'method': 'card', //Defines el método que se utilizará
                'amount': $scope.userData.planSolicitadoPricing.total, //El total a pagar (se obtiene de lo que generamos en pricing)
                'description': 'Pago de subscripción a Portalia Plus', //La descripción que llevará el cargo
                'device_session_id': $scope.deviceSessionId, //deviceSessionId es una variable generada por OpenPay cuando entras a la página, para su control de antifraudes

                //Los datos del cliente
                'customer': {
                  'name': $scope.userData.nombre ,
                  'last_name' : $scope.userData.nombre,
                  'phone_number': $scope.userData.telefono,
                  'email': $scope.userData.email
                }
              },
              'plan': 'select', //El plan que se pagará
              'period': $scope.type, //El periodo (lo definimos arriba)
              'discount': 0 //Descuento (ahorita está hardcodeado a 0 porque no hay promociones, sería cuestión de ponerle una variable y listo)
            }

            $http.post('/api/openpay/card', $scope.data)
              .then(function(response) {

                $("#modal-success").modal('show');

              });
          },
          //Error al crear el token de OpenPay
          function(err) {
            console.log(err);
            $scope.pendingTransaction = false;
            $scope.$apply();
            swal('Error', 'No pudimos dar de alta esta tarjeta, por favor intenta con otra123', 'error');
          });
      };

      //Esta es la función para pago diferido (OpenPay lo trata como una subscripción recurrente que se cancela en cierto punto)
      $scope.newSubscription = function() {

        // Des habilitar el botón para prevenir dobles suscripciones
        $scope.pendingTransaction = true;

        // Validar los datos de la tarjeta desde el front
        if( ! $scope.isValidCard() ) {
          $scope.pendingTransaction = false;
          return;
        }

        // Si pide factura verificar que el formulario sea válido
        if( $scope.userData.datosDeFacturacion.requested ) {
          if( ! $scope.invoiceForm.$valid ) {
            $scope.pendingTransaction = false;
            $scope.invoiceForm.$submitted = true;
            swal('Advertencia', 'Favor de verificar tus datos de facturación', 'warning');
            return;
          }
          $scope.userData.datosDeFacturacion.rfc = $scope.userData.datosDeFacturacion.rfc.toUpperCase();
        }

        // Función que prepara openpayData con los datos necesarios para tokenizar tarjetas
        $scope.setInitialOpenpayTokenData();

        $scope.updateUserProfile();

        //Generar token
        OpenPay.token.create($scope.openPayData, function(res) {

          //Tipo de subscripción, 2 = anual, 1 = seis meses
          if ($scope.userData.planSolicitadoPricing.periodo == 'year') {
            $scope.type = 2;
          } else {
            $scope.type = 1;
          }
          $http.post('/api/openpay/subscription', {

            //Número de pagos que se hará
            payments: $scope.userData.planSolicitadoPricing.numeroPagos,

            //Tipo de subscripción que se definió arriba
            type: $scope.type,

            //La tarjeta que se utilizará (el resultado de lo que regresó OpenPay)
            card: res.data.id

          })
            .then(function(response) {
              $("#modal-success").modal('show');
              // $scope.pendingTransaction = false;

            })
            // Manejo de errores
            .catch(function(err) {

              if( err.data.error_code == 3001 ) {
                swal('Error', 'La tarjeta fué rechazada, por favor intenta con otra.', 'error');
              }
              if( err.data.error_code == 3002 ) {
                swal('Error', 'La tarjeta ingresada ha expirado, por favor intenta con otra.', 'error');
              }
              if( err.data.error_code == 3003 ) {
                swal('Error', 'La tarjeta no tiene fondos suficientes, por favor intenta con otra.', 'error');
              }
              if( err.data.error_code == 3004 ) {
                swal('Error', 'La tarjeta ha sido identificada como una tarjeta robada, por favor intenta con otra.', 'error');
              }
              if( err.data.error_code == 3005 ) {
                swal('Error', 'La tarjeta ha sido rechazada por el sistema anti fraudes, por favor intenta con otra.', 'error');
              }

              if( err.data.error_code == 1005 ){
                swal('Error', 'La tarjeta ha sido rechazada por el sistema, por favor intenta con otra.', 'error');
              }

              console.log(err);
              $scope.pendingTransaction = false;
            });
        },
        function (err) {
          $scope.pendingTransaction = false;
          swal('Error', err.data.description , 'error' );
        });
      }

      //Código de pago con transferencia
      //PAGO CON TRANSFERENCIA
      // $scope.openpayBank = function() {
      //   var validationSameRequest = false;
      //   if ($scope.openpayBankCalled) {
      //     $('#modal-informacion').modal('show');
      //     return;
      //   }
      //
      //   if ($scope.userData.planSolicitado) {
      //     if (
      //       ($scope.userData.planSolicitado.period == $scope.period) &&
      //       ($scope.userData.planSolicitado.plan == $scope.plan) &&
      //       ($scope.userData.planSolicitado.openPayResponse.amount == $scope.total) &&
      //       moment($scope.userData.planSolicitado.openPayResponse.due_date).isBefore()) {
      //       //alert("Same information");
      //       validationSameRequest = true;
      //     }
      //   }
      //
      //
      //
      //   if (validationSameRequest) {
      //     //alert("validationSameRequest");
      //     $scope.responseDataInformationModal = $scope.userData.planSolicitado.openPayResponse.payment_method;
      //     $scope.responseDataInformationModal.pdf = $scope.userData.planSolicitado.openPayResponse.pdf;
      //     $scope.responseDataInformationModal.amount = $filter('currency')($scope.userData.planSolicitado.openPayResponse.amount);
      //     $scope.responseDataInformationModal.due_date = moment($scope.userData.planSolicitado.openPayResponse.due_date).format("DD/MM/YY");
      //     $('#modal-informacion').modal('show');
      //     return;
      //   }
      //
      //   //NOTE: Todo el código hasta antes de este comentario se dedica a revisar si ya habías presionado el botón de generar PDF para no generar nuevos y volverte a servir el mismo en caso que lo hayas perdido
      //   //y lo necesites de nuevo o hallas cerrado el modal sin abrirlo
      //
      //   $scope.data = { //Los datos necesarios para generar el SPEI
      //     amount: $scope.total,
      //     plan: $scope.plan,
      //     nombre: $scope.userData.nombre,
      //     email: $scope.userData.email,
      //     userID: $scope.userData._id,
      //     period: $scope.period,
      //     discount: $scope.discountMoney
      //   };
      //
      //   $http.post('/api/openpay/bank', $scope.data)
      //     .then(function(res) {//En el then de la función de SPEI se setea todo los datos que te muestra el modal, incluyendo el PDF, un link a la página de openpay
      //       console.log(res);
      //       $scope.openpayBankCalled = true;
      //       $scope.bankResponse = res.data;
      //       $scope.responseDataInformationModal = res.data.payment_method;
      //       $scope.responseDataInformationModal.pdf = res.data.pdf;
      //       $scope.responseDataInformationModal.amount = $filter('currency')(res.data.amount);
      //       $scope.responseDataInformationModal.due_date = moment(res.data.due_date).format("DD/MM/YY");
      //       $('#modal-informacion').modal('show');
      //     })
      //     .catch(function(err) {
      //       console.log(err);
      //     });
      // };


      $http.get("api/estados")
        .then(function(response) {
          $scope.estados = response.data;
        });


    });

})();
