'use strict';

(function () {

  angular.module('portaliaApp').component('pricing', {
    templateUrl: 'app/pricing/pricing.html'
  }).controller("pricingCrl", function ($scope, $http, Auth, $window, $routeParams, $timeout, $rootScope) {
    $scope.data = {};



    $rootScope.canonical = 'https://portaliaplus.com/pricing';

    // Variables para enviar

    $scope.plan = "";
    $scope.periodo = "sixmonth";
    $scope.periodoInt = 0;
    $scope.frecuencia = "mensual";
    $scope.numeroPagos = 0;
    $scope.frequentCost = 0;
    $scope.totalSelect = 0;
    $scope.total = 0;

    // Función que actualiza el scope dependiendo del periodo y frecuencia
    $scope.updateConditions = function () {
      var periodo = $scope.periodo;
      var frecuencia = $scope.frecuencia;
      var selectPrice = 0;
      var period = 0;
      var plusPrice = 0;

      if( periodo == 'year')
      {
        $scope.periodoInt = 2;
        selectPrice = 18000;
        plusPrice = 72000; //wtf
        period = 12;
        console.log(plusPrice);
      }
      else if ( periodo == 'sixmonth' )
      {
        $scope.periodoInt = 1;
        selectPrice = 12000;
        period = 6;
        if ( $scope.frecuencia == 'anual')
          $scope.frecuencia = 'mensual';
      }

      $scope.total = selectPrice;
      switch ( frecuencia ){
        case 'mensual':
          $scope.numeroPagos = period;
          $scope.selectPrice = selectPrice / period;
          if(periodo == 'year'){
            $scope.plusPrice = 6000;
          } else if (periodo == 'sixmonth'){
            $scope.plusPrice = 8000;
          }
          break;
        case 'trimestral' :
          $scope.numeroPagos = period / 3;
          $scope.selectPrice = selectPrice / (period / 3);
          if(periodo == 'year'){
            $scope.plusPrice = 18000;
          } else if (periodo == 'sixmonth'){
            $scope.plusPrice = 24000;
          }
          break;
        case 'semestral' :
          $scope.numeroPagos = period / 6;
          $scope.selectPrice = selectPrice / (period / 6);
          if(periodo == 'year'){
            $scope.plusPrice = 36000;
          } else if (periodo == 'sixmonth'){
            $scope.plusPrice = 48000;
          }
          break;
        case 'anual' :
          $scope.numeroPagos = period / 12;
          $scope.selectPrice = selectPrice / (period / 12);
          $scope.plusPrice = 72000;
          break;
        default:
          $scope.selectPrice = 0;
          $scope.plusPrice = 0;
          break;

      }

    }

    // Watcher de periodo para actualizar los demás valores
    $scope.$watch('periodo', function () {
      $scope.updateConditions()
    });

    // Watcher de frecuencia para actualizar los demás valores
    $scope.$watch('frecuencia', function () {
      $scope.updateConditions()
    });

    Auth.getCurrentUser(function (res) {
      $scope.userData = res;


    });

    $scope.adjustIframe = function (frame, div) {
      // Obtener el texto del Iframe
      var videoHeigh = $("#" + frame).height();
      var textHeigh = $("#" + div).height();

      if (videoHeigh < textHeigh) {
        // Sacar la diferencia del teto con el video
        var dif = textHeigh - videoHeigh;

        // Ajustar el div
        if (dif > 0) $("#" + frame).css('margin-top', dif / 2);
      } else {
        // Sacar la diferencia del teto con el video
        var dif = videoHeigh - textHeigh;

        // Ajustar el div
        if (dif > 0) $("#" + div).css('margin-top', dif / 2);
      }
    };

    angular.element(document).ready(function () {

      // $scope.adjustIframe("video", "preguntas");
    });

    $scope.sendToPay = function (plan) {
      $scope.plan = plan;

      // Verificar que el usuario este loggeado
      if ( $scope.userData == undefined  || jQuery.isEmptyObject($scope.userData) ) {
        $('#myModal').modal('show');
        return;
      }
      // Solo se puede comprar select por el momento
      if( plan == 'select' )
      {
        $scope.frequentCost = $scope.selectPrice ;
        // Armar el objeto necesario
        var user =
        {
          planSolicitadoPricing: //Este objeto se guarda en el usuario para usarlo en formaDePago
          {
            plan : $scope.plan, //El plan seleccionado (plus/select)
            periodo : $scope.periodo, //El periodo seleccionado en letras, los posibles valores hoy en día son sixmonth o year
            periodoInt : $scope.periodoInt, //El periodo en número (de meses), posibles valores hoy en día son 6 o 12
            frecuencia : $scope.frecuencia, //La frecuencia con que se va a cobrar (mensual, trimestral, etc)
            numeroPagos : $scope.numeroPagos, //El número de pagos que se harán en total
            frequentCost : $scope.frequentCost, //El costo de cada pago
            total : $scope.total, //El costo total (sumando todos los pagos)
          }
        };
        $http.patch('/api/users/' + $scope.userData._id, user).then(function (response) {
          $window.location.href = '/formaDePago';
        });

      }
    }

    //Prevenir el scroll en paneles
    $('#primera').click(function($e) {
      $e.preventDefault();
    });
    $('#segunda').click(function($e) {
      $e.preventDefault();
    });
    $('#tercera').click(function($e) {
      $e.preventDefault();
    });
    $('#cuarta').click(function($e) {
      $e.preventDefault();
    });


  });
})();
//# sourceMappingURL=pricing.controller.js.map
