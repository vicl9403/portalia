'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/pricing', {
        template: '<pricing></pricing>'
      });
  });
