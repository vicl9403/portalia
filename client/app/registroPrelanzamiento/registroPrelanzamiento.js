'use strict';

angular.module('portaliaApp')
  .config(function($routeProvider) {
    $routeProvider
      .when('/registroPrelanzamiento', {
        template: '<registro-prelanzamiento></registro-prelanzamiento>'
      });
  });
