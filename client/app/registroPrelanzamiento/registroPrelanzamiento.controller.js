'use strict';
(function() {

  angular.module('portaliaApp')
    .component('registroPrelanzamiento', {
      templateUrl: 'app/registroPrelanzamiento/registroPrelanzamiento.html',
    })
    .controller('registroPreCrl', function($scope, $http, $window, Auth, $location) {
      //$("#portalito").removeClass("navbar-fixed-top");
      //$("#contenedorMadre").css("height", "101px");
      $location.path("/registro");

      $scope.data = {};
      $scope.temp = {
        sector: ""
      };
      $scope.data.role = "empresa";

      var codigo = $location.search().codigo;
      if (codigo)
        $scope.data.codigo_invitacion = codigo;

      $http.get("api/sectors")
        .then(function(response) {
          console.log(response.data);
          $scope.sectors = response.data;
        });


      $http.get("api/estados")
        .then(function(response) {
          console.log(response.data);
          $scope.estados = response.data;
        });

      $scope.changeSector = function(sector) {
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un sub sector</option>')
          .val('');

        for (var x in $scope.sectors[sector].subsectores) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, x));
        }
        $scope.temp.sector_position = sector;
        $scope.temp.sector = $scope.sectors[sector]._id;
        $scope.temp.sector_name = $scope.sectors[sector].name;
      };


      $scope.changeCategoria = function(categoria) {
        $scope.temp.categoria = $scope.sectors[$scope.temp.sector_position].subsectores[categoria]._id;
        $scope.temp.categoria_name = $scope.sectors[$scope.temp.sector_position].subsectores[categoria].name;
      };


      $scope.changeEstado = function(estado) {
        $('#municipio')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un municipio</option>')
          .val('');

        $scope.estados[estado].municipios.sort(function(a, b) {
          var textA = a.name.toUpperCase();
          var textB = b.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        for (var x in $scope.estados[estado].municipios) {
          $("#municipio").append(new Option($scope.estados[estado].municipios[x].name, $scope.estados[estado].municipios[x]._id));
        }
        $scope.temp.estado = $scope.estados[estado]._id;
      };

      $scope.checkEmail = function() {
        $http.post('/api/users/check', {
            email: $scope.data.email
          })
          .then(function(res) {
            console.log(res);
            $scope.checkedEmail = true;
            if (res.status == 201) {
              $scope.isValidEmail = true;
              document.getElementById("boton").disabled = false;
              $("#boton").css("border", "#f08f04");
              $("#boton").css("background-color", "#f08f04");
            } else if (res.status == 200) {
              $scope.isValidEmail = false;
              document.getElementById("boton").disabled = true;
              $("#boton").css("border", "grey");
              $("#boton").css("background-color", "grey");
            }
          })
          .catch(function(err) {
            console.log(err);
          })
      }

      $(document).ready(function() {
        $("#formEmail").focusout(function() {
          $scope.checkEmail();
        });
      });

      $scope.registrar = function() {
        if (document.getElementById('terms').checked) {
          if (!$scope.registro.$invalid) {
            if ($scope.data.password == $scope.data.password2) {
              if ($scope.myRecaptchaResponse.length > 0)
                $scope.captcha = true;
              else
                $scope.captcha = false;

              if ($scope.captcha == true) {
                $scope.data.sector = $scope.temp.sector;
                $scope.data.sector_name = $scope.temp.sector_name;
                $scope.data.categoria_name = $scope.temp.categoria_name;

                Auth.createUser($scope.data)
                  .then(function(response) {
                    $http.post('/api/mailConfirmations', $scope.data)
                      .then(function(response) {
                        console.log(response);
                      })
                      .catch(function(err) {
                        console.log(err);
                      });
                    console.log(response);

                    var data = {};
                    data.subject = 'Correo de confirmacion Portalia Plus';
                    data.email = $scope.data.email;

                    $http.post('/api/email/confirmacion', data)
                      .then(function(response) {
                        console.log(response);
                        if (response.status === 200) {

                        } else {
                          //$window.alert('hubo un problema enviando el correo, intentelo de nuev');
                        }
                      })
                      .catch(function(err) {
                        console.log(err);
                      });
                    $window.location.href = '/';
                  })
                  .catch(function(err) {
                    console.log(err);
                  });

              } else {
                console.log($scope.myRecaptchaResponse);
                alert("Debes verificar el Captcha");
              }
            } else {
              //grecaptcha.reset();
              alert("No son iguales");
            }
          } else {
            //grecaptcha.reset();
            alert("Debes verificar el Captcha");
          }
        } else {
          //grecaptcha.reset();
          alert("Debes aceptar los Términos y Condiciones")
        }
      };

    });


})();
