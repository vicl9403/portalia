'use strict';
(function(){

class PromocionesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('promociones', {
    templateUrl: 'app/promociones/promociones.html',
    controller: PromocionesComponent
  }).controller('PromocionesController', function($scope, $http, $window ) {

    $("#modalPromocion").modal('hide');

    $scope.formulario = {};
  	$scope.submit= function(){
  		console.log($scope.formulario);

  	}


  	$scope.enviar = function () {
  	  $scope.data = {};
  	  $scope.data.subject = 'Solicitud de Promoción';
  	  $scope.data.to = 'asistencia@portaliaplus.com.mx';
  	  $scope.data.text = 'Nombre: ' + $scope.formulario.nombre + '\n\nCorreo: ' + $scope.formulario.correo + '\n\nTelefono: ' + $scope.formulario.telefono;
  	  console.log($scope.data);
  	  console.log($scope.formulario);
  	  if ($scope.formulario.nombre == ""|| $scope.formulario.nombre == undefined || $scope.formulario.correo == "" || $scope.formulario.correo == undefined || $scope.formulario.telefono == "" || $scope.formulario.telefono == undefined) {
  	  	$window.alert('Asegurate de llenar todos los campos');
  	  } else {
  	    $http.post('/api/email', $scope.data).then(function (response) {
  	      if (response.status === 200) {
  	        $scope.data = {};
  	        // swal("Tu correo se envió exiosamente!", "Nosotros te contactaremos", "success")
            $window.location.href = '/graciaspromocion';
  	      } else {
  	      	swal("Intentela de nuevo", "Hubo un problema enviando el correo :')", "error")
  	      }
  	    }).catch(function (err) {
  	      console.log(err);
  	    });
  	  }
  	};

  	$scope.goToTop = function () {
      $("html, body").animate({
        scrollTop: $("#top").offset().top
      }, "slow");
    }
  });

})();
