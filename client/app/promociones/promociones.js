'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/3mesesgratis', {
        template: '<promociones></promociones>'
      });
  });
