'use strict';
(function(){

  angular.module('portaliaApp')
    .controller('DashboardController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope) {

      $scope.pendingChats = 0;
      $scope.socket = io();


      Auth.isLoggedIn( function (logged) {
        if ( ! logged )
          $window.location.href = '/';

        Auth.getCurrentUser( function (user) {

          $scope.user = user;

          // Evento que escucha la generación de un nuevo chat
          $scope.socket.on('chat_created' + user._id , function(){
            // Setear los chats pendientes
            if ( $location.$$path != '/perfil-usuario/chat' )
              $scope.pendingChats ++ ;
            $scope.$apply();
          });

          // $http.get('/api/chats/user/' + user._id )
          //   .then( function ( res ) {
          //
          //     $scope.userChats = res.data;
          //
          //     for ( var i=0; i < $scope.userChats.length ; i++ ) {
          //       $scope.socket.on('chat_message_' + $scope.userChats[i]._id, function ( msg ) {
          //
          //         // Setear los chats pendientes
          //         if ( msg.message.from != user._id && $location.$$path != '/perfil-usuario/chat' )
          //           $scope.pendingChats++;
          //
          //
          //         $scope.$apply();
          //       });
          //     }
          //
          //   });





        })
      })


      $scope.$on('$viewContentLoaded', function(){
        $("#loading-page").addClass('hide');
        $('body').removeClass('page-on-load'); // remove page loading indicator
      });

      $scope.sendToPortalia = function () {
        $window.location.href  = '/';
      }

      $scope.sendToProfile = function () {
        $window.location.href  = `/perfil/${ $scope.user._id }/${ $scope.user.nombre }`;
      }

      $scope.openSurvey = function() {
        var myWindow = window.open("https://es.surveymonkey.com/r/KNZ7LYD", "_blank");
      }


    });

})();
