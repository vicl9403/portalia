'use strict';

describe('Component: LandingComponent', function() {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var LandingComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController, $rootScope) {
    scope = $rootScope.$new();
    LandingComponent = $componentController('LandingComponent', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
