'use strict';
(function() {

  angular.module('portaliaApp')
    .component('landing', {
      templateUrl: 'app/landing/landing.html'
    }).controller('landingCrl', function($scope, $http, $routeParams, $location, Auth, $window, socket) {
      $scope.$http = $http;
      $scope.socket = socket;
      $scope.awesomeThings = [];
      $scope.$on('$destroy', function() {
        socket.unsyncUpdates('thing');
      });
      $scope.data = {};
      $scope.data.active = true;
      $scope.userData = Auth.getCurrentUser();
      var itsInvitar = $location.search().invitar;
      if (itsInvitar)
        $('#modal-amigos').modal('show')


      // $scope.onInit = function() {
      //   alert("lol")
      //   this.$http.get('/api/things')
      //     .then(response => {
      //       this.awesomeThings = response.data;
      //       this.socket.syncUpdates('thing', this.awesomeThings);
      //     });
      // }

      $scope.addThing = function() {
        if (this.newThing) {
          this.$http.post('/api/things', {
            name: this.newThing
          });
          this.newThing = '';
        }
      }

      $scope.deleteThing = function(thing) {
        this.$http.delete('/api/things/' + thing._id);
      }

      if ($routeParams.key) {
        $scope.key = $routeParams.key
        $http.post('/api/users/activar/' + $scope.key, $scope.data)
          .then(function(res) {
            $('#modal-success').modal('show');
          })
          .catch(function(err) {
            console.log(err);
          });
      }

      function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

      $scope.enviar = function() {
        $scope.userData.to = [];
        var emails = document.getElementById('emails').getElementsByTagName('input');
        for (var i = 0; i < emails.length; i++) {
          if (validateEmail(emails[i].value))
            $scope.userData.to.push(emails[i].value);
        }

        $http.post('/api/email/invitacionAnonima', $scope.userData)
          .then(function(response) {
            console.log(response);
            if (response.status === 200) {
              $window.alert('Correos enviados');
            } else {
              //$window.alert('hubo un problema enviando el correo, intentelo de nuev');
            }
          })
          .catch(function(err) {
            console.log(err);
          });

      };

      $scope.goToAnchor = function(anchor) {
        var loc = document.location.toString().split('#')[0];
        document.location = loc + '#' + anchor;
        return false;
      }

    });
})();
