'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/landing', {
        template: '<landing></landing>'
      });
  });
