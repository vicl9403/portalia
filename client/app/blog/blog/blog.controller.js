'use strict';

(function() {

  angular.module('portaliaApp')
    .component('blog', {
      templateUrl: 'app/blog/blog/blog.html',
    }).controller('blogCtrl', function($scope, $routeParams, $http, $window, Auth) {

      if($routeParams.mailing){
        $('#likes').modal('toggle')
      }

      $scope.postsPerPage = 6;
      $scope.currentPage = 1;

      $scope.currentPageCat = 0;
      $scope.pageSizeCat = 5;
      $scope.numberOfPagesCat = function() {
        return Math.ceil($scope.categories.length / $scope.pageSizeCat);
      }

      $scope.currentPageTag = 0;
      $scope.pageSizeTag = 15;
      $scope.numberOfPagesTag = function() {
        return Math.ceil($scope.tags.length / $scope.pageSizeTag);
      }

      if ($routeParams.category) {
        //alert("category")
        $scope.title = "Categoria: " + $routeParams.category_name;
        $http.post('https://blog.portaliaplus.com/api/get_category_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage + '&category_id=' + $routeParams.category)
          .then(function(response) {
            $scope.posts = response.data.posts;
            $scope.totalPages = response.data.pages
            if ($scope.totalPages === $scope.currentPage) {
              $("#masPostsBtn").hide();
            }
          });
      } else if ($routeParams.tag) {
        $scope.title = "Tag: " + $routeParams.tag_name;
        $http.post('https://blog.portaliaplus.com/api/get_tag_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage + '&tag_id=' + $routeParams.tag)
          .then(function(response) {
            $scope.posts = response.data.posts;
            $scope.totalPages = response.data.pages
            if ($scope.totalPages === $scope.currentPage) {
              $("#masPostsBtn").hide();
            }
          });
      } else {
        // Count se usa para paginar
        $http.post('https://blog.portaliaplus.com/api/get_recent_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage)
          .then(function(response) {
            $scope.posts = response.data.posts;
            $scope.totalPages = response.data.pages
            if ($scope.totalPages === $scope.currentPage) {
              $("#masPostsBtn").hide();
            }
          });
      }


      $http.post('https://blog.portaliaplus.com/?json=get_category_index')
        .then(function(response) {
          $scope.categories = response.data.categories;
        });

      $http.post('https://blog.portaliaplus.com/?json=get_tag_index')
        .then(function(response) {
          $scope.tags = response.data.tags;
        });

      $scope.masPosts = function() {
        $scope.currentPage += 1;
        if ($routeParams.category) {
          //alert("category")
          $scope.title = "Categoria: " + $routeParams.category_name;
          $http.post('https://blog.portaliaplus.com/api/get_category_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage + '&category_id=' + $routeParams.category)
            .then(function(response) {
              $scope.posts = response.data.posts;
              $scope.totalPages = response.data.pages
              if ($scope.totalPages === $scope.currentPage) {
                $("#masPostsBtn").hide();
              }
            });
        } else if ($routeParams.tag) {
          $scope.title = "Tag: " + $routeParams.tag_name;
          $http.post('https://blog.portaliaplus.com/api/get_tag_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage + '&tag_id=' + $routeParams.tag)
            .then(function(response) {
              $scope.posts = response.data.posts;
              $scope.totalPages = response.data.pages
              if ($scope.totalPages === $scope.currentPage) {
                $("#masPostsBtn").hide();
              }
            });
        } else {
          // Count se usa para paginar
          $http.post('https://blog.portaliaplus.com/api/get_recent_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage)
            .then(function(response) {
              $scope.posts = response.data.posts;
              $scope.totalPages = response.data.pages
              if ($scope.totalPages === $scope.currentPage) {
                $("#masPostsBtn").hide();
              }
            });
        }
      }

      $scope.menosPosts = function() {
        $scope.currentPage -= 1;
        if ($routeParams.category) {
          //alert("category")
          $scope.title = "Categoria: " + $routeParams.category_name;
          $http.post('https://blog.portaliaplus.com/api/get_category_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage + '&category_id=' + $routeParams.category)
            .then(function(response) {
              $scope.posts = response.data.posts;
              if ($scope.posts.length === 0) {
                $("#masPostsBtn").hide();
              } else {
                $("#masPostsBtn").show();
              }
            });
        } else if ($routeParams.tag) {
          $scope.title = "Tag: " + $routeParams.tag_name;
          $http.post('https://blog.portaliaplus.com/api/get_tag_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage + '&tag_id=' + $routeParams.tag)
            .then(function(response) {
              $scope.posts = response.data.posts;
              if ($scope.posts.length === 0) {
                $("#masPostsBtn").hide();
              } else {
                $("#masPostsBtn").show();
              }
            });
        } else {
          // Count se usa para paginar
          $http.post('https://blog.portaliaplus.com/api/get_recent_posts?count=' + $scope.postsPerPage + '&page=' + $scope.currentPage)
            .then(function(response) {
              $scope.posts = response.data.posts;
              if ($scope.posts.length === 0) {
                $("#masPostsBtn").hide();
              } else {
                $("#masPostsBtn").show();
              }
            });
        }
      }


      $http.post('https://blog.portaliaplus.com/?json=get_category_index')
        .then(function(response) {
          $scope.categories = response.data.categories;
        });

      $http.post('https://blog.portaliaplus.com/?json=get_tag_index')
        .then(function(response) {
          $scope.tags = response.data.tags;
        });


    });

})();
