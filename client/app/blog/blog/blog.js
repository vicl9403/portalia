'use strict';

angular.module('portaliaApp')
  .config(function($routeProvider) {
    $routeProvider
      .when('/blog', {
        template: '<blog></blog>'
      });
  });
