'use strict';

(function() {


  angular.module('portaliaApp')
    .component('post', {
      templateUrl: 'app/blog/post/post.html',
    }).controller('postCtrl', function($scope, $routeParams, $http, $window, Auth) {
      angular.element(document).ready(function() {
        $(".fb-comments").attr("data-href", location.protocol + '//' + location.host + location.pathname);
        $(".fb-like").attr("data-href", location.protocol + '//' + location.host + location.pathname);
      });

      // Shorthand for $( document ).ready()
      $(function() {
        console.log("ready!");
        // $(".fb-comments").attr("data-href", location.protocol + '//' + location.host + location.pathname);
        // $(".fb-like").attr("data-href", location.protocol + '//' + location.host + location.pathname);
      });
      $scope.userData = Auth.getCurrentUser();
      $scope.params = $routeParams;

      $scope.currentPageCat = 0;
      $scope.pageSizeCat = 5;
      $scope.numberOfPagesCat = function() {
        return Math.ceil($scope.categories.length / $scope.pageSizeCat);
      }

      $scope.currentPageTag = 0;
      $scope.pageSizeTag = 15;
      $scope.numberOfPagesTag = function() {
        return Math.ceil($scope.tags.length / $scope.pageSizeTag);
      }

      $http.post('https://blog.portaliaplus.com/?json=get_category_index')
        .then(function(response) {
          $scope.categories = response.data.categories;
        });

      $http.post('https://blog.portaliaplus.com/?json=get_tag_index')
        .then(function(response) {
          $scope.tags = response.data.tags;
        });

      $http.get('https://blog.portaliaplus.com/api/get_post/?post_id=' + $scope.params.id)
        .then(function(response) {
          $scope.post = response.data.post;
        });

      $scope.guardarArticulo = function() {
        // $scope.exists = false; //Variable para ver si existe la empresa ya en favoritos, falsa por defecto
        $scope.linkBlog = $scope.params.id; //UsuarioFav es lo que se va a parchear al usuario, se le da el valor del id de la empresa a agregar a favoritos

        if (!$scope.userData.favoritos) {
          $('#myModal').modal('show');
        }
        if ($scope.userData.articulosGuardados.length === 0) { //Si aun no hay ningun elemento en favoritos lo pone directamente en la posición 0
          $scope.userData.articulosGuardados[0] = $scope.linkBlog;
        } else { //Si no pasa aquí
          if ($scope.userData.articulosGuardados.indexOf($scope.linkBlog) > -1) { //Si la variable es verdadera te arroja una alerta de que ya es empresa favorita
            alert("Ya has guardado este artículo previamente")
          } else { //Si no hace push al arreglo y te arroja mensaje de exito
            alert("Agregado a artículos guardados con éxito")
            $scope.userData.articulosGuardados.push($scope.linkBlog);
          }

        }
        Auth.updateProfile($scope.userData); //Hace el update, y se acaba el infierno de if's
      }

    })


})();
