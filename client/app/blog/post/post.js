'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/blog/post/:slug/:id', {
        template: '<post></post>'
      });
  });
