'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/anuncios/:param1?/:param2?/:param3?/:param4?/:param5?/:param6?/:param7?', {
        template: '<anuncios></anuncios>'
      });
  });
