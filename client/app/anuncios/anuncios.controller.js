'use strict';

(function() {

    angular.module('portaliaApp')
        .component('anuncios', {
            templateUrl: 'app/anuncios/anuncios.html'
        }).controller('anunciosCtrl', function($scope, $routeParams, $route, $http, $window, Auth, $location, $timeout, $rootScope) {


        $scope.routeParams = $routeParams;

        $rootScope.canonical = 'https://portaliaplus.com/anuncios';

        $scope.customTitle = null;
        $scope.customDescription = null;

        $scope.categoria = $routeParams.categoria;
        $scope.sector = $routeParams.sector;

        $scope.initParams = function ( paramsLength ) {
          console.log($routeParams);
          for( var i=0; i < paramsLength ; i++ ){
            console.log($routeParams['param' + (i+1)])
            if ( $routeParams['param' + (i+1)] != null)
            {
              // Obtener el parámetro general de la url
              var param = $routeParams['param' + (i+1)];
              // Obtener el tipo de parámetro
              var paramType = param.split('-')[0];
              // Obtener el valor del parámetro
              var paramValue = param.split('-')[1];
              console.log(paramType);

              if ( paramType == 'categoria' )
                $scope.categoria = paramValue;
              if ( paramType == 'sector' )
                $scope.sector = paramValue;

              console.log($scope.sector);
            }

          }
        $scope.mejorarSEO( $scope.categoria )
        };

        $scope.mejorarSEO =  function (subSectorName)
        {
          console.log($scope.sector);
          if ($scope.routeParams.categoria == "" || ($scope.categoria == "Suministros" && $scope.sector == 'Electricidad y agua') || ($scope.categoria == "Suministros" && $scope.sector == 'Turismo'))
            $('#imagenSector').css('background-image', 'url(assets/images/gear_narrow_background.png)');
          else
          {
            $scope.url = "assets/images/iconos_anuncios/" + encodeURIComponent($scope.categoria) + ".jpg";
            $('#imagenSector').css('background-image', 'url(' + $scope.url + ')');
          }
          switch (subSectorName) {
            case "Agrícola":
              $scope.title = "Directorio Agroindustrial - Proveedores, Distribuidores y Comercializadores";
              $scope.sidebartag = 'agricola'
              $scope.description = "Encuentra comercializadoras frutícolas, proveedores para la venta de agroquímicos, lombricomposta y más empresas agrícolas ¡Contacta Ahora!";
              $scope.customTitle = "Directorio de Empresas Agrícolas";
              $scope.customDescription = "Esta sección funge como directorio agroindustrial, en donde encontrarás a todas las empresas del sector Agricola, así como proveedores, productores y distribuidores de composta, agroquímicos, lumbricomposta, grasas vegetales, comercializadoras de semillas, hortalizas y más. Si no encuentras lo que buscas puedes solicitarlo directamente <a href='/cotizaciones' style='ponter:mouse'>aquí.</a>"
              break;
            case "Pesca":
              $scope.title = "Directorio Empresas Pesqueras en México - Sector de Acuicultura";
              $scope.sidebartag = 'pesca';
              $scope.description = "El directorio de pesca con las mejores opciones de empresas comercializadoras del sector acuicultura para ti. ¡Contacta o regístrate gratis! ingresa ahora mismo y compruébalo.";
              $scope.customTitle = "Directorio Pesquero";
              $scope.customDescription = "Directorio digital dirigido al rubro pesquero, podrás contactar a empresas que se dediquen a la pesca, producción y comercialización de camarón, filete, ostión, etc. Contacta ahora o <a href='/registro' style='ponter:mouse'>regístrate gratis.</a>.";
              break;
            case "Ganadería":
              $scope.title = "Directorio de Empresas Ganaderas en México - Sector Agro";
              $scope.description = "Encuentra las mejores opciones de comercializadores, productores y proveedores en el sector ganadero así como empresas de ganado ¡Entra ya!";
              $scope.customTitle = "Directorio Ganadero";
              $scope.sidebartag = 'ganaderia';
              $scope.customDescription = "En este sector encontrarás empresas ganaderas, proveedores, comercializadores y distribuidores de productos cárnicos, venta de carnes bovinas, aceites vegetales, sales minerales,   y más. Podrás contactarlas para suplir tus necesidades de la industria o registrarte para formar parte de nuestro Directorio Ganadero. En caso de que no encuentres lo que buscas solicitalo <a href='/cotizaciones' style='ponter:mouse'>aquí.</a>.";
              break;
            case "Suministros Agroindustriales":
                $scope.title = "Directorio de Suministros - Empresas de la Industria Agraria";
                $scope.description = "Proveedores y distribuidores de suministros agrícolas, encuentra agronutrientes, maquinaria, semillas, herbicidas y más ¡Ingresa al Directorio!";
                $scope.customTitle = "Suministros Agrícolas";
                $scope.sidebartag = 'suministros-agroindustriales';
                $scope.customDescription = "Todo lo que necesites para el sector agroindustrial, en cuanto a suministros se refiere lo encontrarás aquí, encuentra agroquímicos, herbicidas, fertilizantes, venta de composta, proveedores de semillas de maíz, hortalizas, comercializadores de maquinaria agrícola, distribuidores de agrobiológicos, equipo apícola, encuentra en portalia el mejor lugar de búsqueda de suministros ¡Contacta a los mejores proveedores! ";
              break;
            case "Suministros Eléctricos":
            $scope.title = "Directorio de Suministros - Electricidad y agua";
            $scope.description = "Proveedores y distribuidores de suministros agrícolas, encuentra agronutrientes, maquinaria, semillas, herbicidas y más ¡Ingresa al Directorio!";
            $scope.customTitle = "Suministros de Electricidad y agua";
            $scope.sidebartag = 'suministros-electricos';
            $scope.customDescription = "¿Necesitas fabricantes de compuertas deslizables o rejillas para agua? Contamos con las mejores empresas de suministros  ¡Conocelas!";
              break;
              case "Suministros Turísticos":
              $scope.title = "Directorio Empresarial de Suministros de la Industria | Portalia Plus";
              $scope.description = "Los suministros son básicos para el desarrollo de las empresas, ingresa a Portalia Plus el directorio más completo y conoce las empresas.";
              $scope.customTitle = "Directorio Empresarial de Suministros de la Industria";
              $scope.sidebartag = 'hoteles';
              $scope.customDescription = "Los suministros son básicos para el desarrollo de las empresas, ingresa a Portalia Plus el directorio más completo y conoce las empresas.";
                break;
                case "Suministros Industrial":
                $scope.title = "Directorio Empresarial de Suministros Industriales - Portalia Plus";
                $scope.description = "Proveedores y distribuidores de suministros agrícolas, encuentra agronutrientes, maquinaria, semillas, herbicidas y más ¡Ingresa al Directorio!";
                $scope.customTitle = "Directorio Empresarial de Suministros Industriales";
                $scope.sidebartag = 'suministros-industrial';
                $scope.customDescription = "Tenemos las mejores empresas de suministros del sector turismo,te invitamos a registrarte y entrar en contacto con ellas o con nuevos clientes.";
                  break;
            case "Edificación":
              $scope.title = "Directorio Industrial de la Construcción - Categoría Edificación";
              $scope.description = "Encuentra la más amplia variedad de empresas constructoras en el ramo edificación, así como materiales de construcción comienza a contactar aquí.";
              $scope.customTitle = "Empresas Constructoras de Edificación";
              $scope.sidebartag = 'edificacion';
              $scope.customDescription = "En esta sección de nuestro directorio industrial de la construcción encontrarás un extenso directorio de empresas constructoras del país dedicadas a la edificación, ideal para encontrar suministros de construcción, materiales como concretos, aceros, maquinaria pesada, excavadoras, bombas de concreto, ingenieros, arquitectos, etc. En caso de que no encuentres lo que estás buscando, puedes solicitar información directamente <a href='/cotizaciones'style='ponter:mouse'>aquí.</a>. ";
              break;
            case "Trabajos especiales de construcción":
              $scope.title = "Directorio de Construcción - Trabajos Especiales de Construcción";
              $scope.sidebartag = 'trabajos-especiales-de-construccion'
              $scope.description = "Encuentra gran cantidad de empresas especializadas en el ramo de la construcción, con las mejores opciones de abastecimiento para trabajos especiales.";
              $scope.customTitle = "Trabajos especiales de construcción";
              $scope.customDescription = "El sector constructor cuenta es uno de los más complejos y multidisciplinario, es por eso que contamos con una sección exclusiva para los trabajos especiales que se requieren en la construcción. Aquí encontrarás proveedores de materiales para construcción, proveedores de aluminios, procesadoras de vidrio, pisos industriales, renta de grúas para la construcción, pisos industriales y más. ¡Encuentra todo lo que buscas para tu proyecto!";
              break;
            case "Urbanización":
              $scope.title = "Directorio de Empresas Constructoras en México | Urbanización";
              $scope.description = "Las mejores empresas para tus proyectos de urbanización se encuentran en Portalia Plus. Somos un directorio industrial a nivel nacional ¡Entra y contacta!";
              $scope.customTitle = "Empresas de Urbanización";
              $scope.sidebartag = 'urbanizacion';
              $scope.customDescription = "En este sector encontrarás todo lo necesario para acabados de construcción, urbanización y los mejores proveedores para darle a tu obra el toque final. Contacta con constructoras especializadas en planeación de proyectos como carreteras, autopistas, drenajes,  desarrollo de fraccionamientos, instalaciones de alumbrado público, y más. ";
              break;
            case "Instalaciones":
              $scope.title = "Directorio de Empresas Constructoras e Instalaciones | Portalia Plus";
              $scope.description = "Tenemos las mejores opciones en instalaciones para tus proyectos, entra a nuestro directorio y solicita tu cotización sin costo. !Haz click ahora!";
              $scope.customTitle = "Instalaciones Especiales de Construccion";
              $scope.sidebartag = 'instalaciones';
              $scope.customDescription = "¿Buscas instalaciones especiales para el sector de la construcción?  Encuentra constructoras especializadas en instalaciones de pisos, concretos estampados, instalacion de laminas, aires acondicionados industriales, etc. Además contacta con proveedores, comercializadores y fabricantes de recubrimientos industriales, metálicos, anticorrosivos y cerámicos. Utiliza el buscador para encontrar lo que buscas o nuestro sistema de <a href='/cotizaciones' style='ponter:mouse'>cotizaciones</a> múltiples para recibir cotizaciones de manera más rápida y eficaz de las empresas ya registradas.";
              break;
            case "Electricidad":
              $scope.title = "Directorio Industrial de Empresas del Sector Electricidad ";
              $scope.description = "Los mejores proveedores del sector electricidad los encuentras en Portalia Plus. Ingresa al directorio y solicita cotizaciones gratuitas.";
              $scope.customTitle = "Directorio Industrial de Electricidad";
              $scope.sidebartag = 'electricidad';
              $scope.customDescription = "Encuentra proveedores líderes a nivel nacional e internacional en fabricación de componentes de la Industria eléctrica, así como suministros, instalaciones eléctricas, generadores de alta y baja tension, equipo electrico industrial. Diseño y Construcción de Redes, Líneas de Media y Baja tensión, Aéreo y subterráneo, Proyectos y construcción de Subestaciones Eléctricas. Todo lo que necesitas para tus proyectos, contacta, <a href='/cotizaciones' style='ponter:mouse'>cotiza</a> y compara precios. ";
              break;
            case "Tratamiento, conducción y distribución de agua":
              $scope.title = "Directorio de Empresas - Tratamiento Conducción y Distribución de Agua";
              $scope.description = "Portalia Plus cuenta con la mejor opción en empresas encargadas de tratamiento, conducción y distribución de agua. Ingresa al directorio y compruébalo.";
              $scope.customTitle = "Empresas de Tratamiento Conducción y Distribución de Agua";
              $scope.sidebartag = 'tratamiento-conduccion-y-distribucion-de-agua';
              $scope.customDescription = "Encuentra un listado amplio de empresas de tratamiento de agua, proveedores y fabricantes de equipos para  purificacion de agua industriales, venta de filtros de arena y suavisadores, equipos de osmosis inversa y ozonificación, lámparas purificadoras uv,  filtros de carbón, químicos para agua y equipos de análisis de agua. También podrás encontrar plantas purificadoras de agua que distribuyen productos para que comiences tu negocio de purificación de agua industrial o local. ¿Quieres solicitar presupuestos de uno o varios productos en específico? Solicitalo <a href='/cotizaciones' style='ponter:mouse'>aquí.</a>";
              break;
            case "Mantenimiento":
              $scope.title = "Mantenimiento para Empresas del Sector Electricidad y Agua";
              $scope.sidebartag = 'tratamiento-conduccion-y-distribucion-de-agua';
              $scope.description = "El mantenimiento es básico para un buen funcionamiento. Aquí encontrarás las mejores empresas para hacerlo en el sector electricidad y agua. Contáctalas Ya";
              break;
            case "Empresas ecológicas y nuevas energías":
              $scope.title = "Directorio Industrial | Empresas Ecológicas y Nuevas Energías";
              $scope.sidebartag = 'empresas-ecologicas-y-nuevas-energias';
              $scope.description = "Las empresas ecológicas tienen alternativas para mejorar el ambiente. En Portalia Plus las encuentras para tus proyectos. ¡Entra y Cotiza Ya!";
              $scope.customTitle = "Empresas Ecológicas y Comercializadores de Nuevas Tecnologías";
              $scope.customDescription = "El mundo evoluciona a paso rápido, y con esta evolución nacen nuevas tecnologías amigables al medio ambiente, que además ayudan a ahorrar dinero con equipos ecologicos industriales. A continuación te mostramos un listado de empresas dedicadas a la fabricación de lámparas led, instalación y reparación de paneles solares, comercializadoras de paneles fotovoltaicos, instalaciones fotovoltaicas, todo en soluciones integrales en energías autosustentables. La energía sustentable es el futuro del mundo, además de que ahorras mientras proteges al medio ambiente. ¡Solicita <a href='/cotizaciones' style='ponter:mouse'>cotización</a> gratis a estas empresas! ";
              break;
            case "Servicios":
              $scope.title = "Directorio Empresarial | Servicios para el Sector Electricidad y Agua";
              $scope.sidebartag = 'suministros-electricos';
              $scope.description = "Entra al directorio y encontrarás las mejores empresas en brindar servicio al ramo de la electricidad y agua..Contacta directo y cotiza ¡Ingresa Ya!";
              break;
            case "Alimentos y bebidas":
              $scope.title = "Directorio de la Industria Alimentaria | El más completo de méxico";
              $scope.sidebartag = 'alimentosybebidas';
              $scope.description = "En la industria de alimentos y bebidas es fundamental que se cuente con... En el directorio puedes encontrar la mejor opción para tu empresa ¡Compruébalo!";
              $scope.customTitle = "Directorio de la Industria Alimentaría  y bebidas";
              $scope.customDescription = "Directorio de la  industria alimenticia, encuentra aquí fabricantes y proveedores de aditivos alimenticios como almidones, maltodextrina, texturizantes, almidones de maíz, etc. También encuentra fabricantes de maquinaria y equipo especializado en el sector alimenticio como cocinas industriales, mezcladores, refacciones para maquinas de envasado, y además encuentra empresas de etiquetado de alimentos, y/o producción de alimentos. <a href='/cotizaciones' style='ponter:mouse'> Solicita más información</a> sobre: materia prima, maquinaria y equipo, envases y servicios, a las empresas registradas.";
              break;
            case "Minería y metalúrgica":
              $scope.title = "Directorio Industrial de Minería y metalúrgica | Manufactureras";
              $scope.sidebartag ='mineriaymetalugica';
              $scope.description = "La minería y metalúrgica son industrias en las que puedes encontrar: fundidoras, estructuras metálicas, herramientas y más para tu empresa...!Haz click!";
              $scope.customTitle = "Directorio de Metalurgia y Minería";
              $scope.customDescription = "Encuentra empresas del sector metalmecánico y metalúrgico, distribuidores de herramientas neumáticas, manufactura de refacciones industriales, fabricantes de soleras, bujes de cobre, poleas, resortes de tensión, compresión y torsión. También contacta con proveedores de acero estructural y láminas. ";
              break;
            case "Materiales componentes y accesorios":
              $scope.sidebartag = 'materialescomponentesyaccesorios';
              break;
            case "Química":
              $scope.title = "Directorio de la Industria Química | Industria Manufacturera";
              $scope.sidebartag = 'quimica'
              $scope.description = "La química es una rama muy importante en el sector industrial, nosotros tenemos la empresa que estás buscando en el sector químico. ¡Da click y descúbrelo!";
              $scope.customTitle = "Directorio de la Industria Química";
              $scope.customDescription = "Encuentra proveedores de productos químicos industriales, empresas del sector químico, o registra tu empresa en nuestro directorio de industrias químicas";
              break;
            case "Materiales, componentes y accesorios":
              $scope.title = "Directorio Industrial | Materiales, Componentes y Accesorios";
              $scope.sidebartag = 'materialescomponentesyaccesorios';
              $scope.description = "En Portalia Plus contamos con las mejores empresas que ofrecen materiales, componentes y accesorios para toda clase de proyectos. ¡Ven y Conócelos!";
              break;
            case "Mantenimiento industrial":
              $scope.title = "Directorio de Empresas para Mantenimiento Industrial | Portalia Plus";
              $scope.sidebartag = 'mantenimientoindustrial';
              $scope.description = "Entra al directorio más completo y encuentra las empresas de mantenimiento industrial de mayor calidad. Solicita tu cotización gratis. ¡Haz click aquí!";
              break;
            case "Servicios especializados":
              $scope.title = "Directorio Industrial | Empresas de Servicios Especializados";
              $scope.sidebartag = 'serviciosespecializados';
              $scope.description = "Portalia Plus cuenta con servicios especializados para todo tipo de empresas, ¡haz click ahora! Contacta a las empresas o Registra tu empresa o negocio.";
              break;
            case "Automotriz":
              $scope.title = "Directorio Empresarial de la Industria Automotriz | Portalia Plus";
              $scope.description = "Portalia Plus ofrece el mejor de los servicios para el sector automotriz, contacta a la empresas en el directorio y obtén cotización sin costo.¡Ingresa Ya!";
              $scope.customTitle = "Directorio Industrial Automotriz";
              $scope.sidebartag = 'automotriz';
              $scope.customDescription = "Uno de los sectores con más crecimiento en México, es el de la Industria Automotriz, mismo que engloba todo sobre producción y manufactura automotriz. En este apartado podrás encontrar miles de empresas y proveedores dedicados a esta gran industria";
              break;
            case "Petróleo y derivados":
              $scope.sidebartag = 'petroleo-y-derivados';
              $scope.title = "Directorio Industrial Empresarial | Sector Petróleo y Derivados";
              $scope.description = "Directorio Empresarial del petróleo y sus derivados, encuentra proveedores a nivel nacional y solicita una cotización gratuita.¡Haz click ahora¡";
              break;
            case "Textil":
              $scope.sidebartag = 'textil'
              $scope.title = "Directorio Industrial de Empresas del Sector Textil - Portalia Plus";
              $scope.description = "Para cualquier ocasión y evento encuentra las mejores empresas del sector textil a nivel nacional, en Portalia Plus el directorio más completo.¡Entra Ya!";
              break;
            case "Materiales de Empaque":
              $scope.sidebartag = 'materiales-de-empaque';
              $scope.title = "Directorio Empresarial de la Industria - Materiales de Empaque";
              $scope.description = "Tenemos lo que necesitas en materiales de empaque, los mejores proveedores que puedes encontrar... Cajas, bolsas, frascos, botellas, etc...¡Haz click aquí!";
              break;
            case "Almacenamiento":
              $scope.title = "Directorio Industrial de Empresas de Almacenamiento - Portalia Plus";
              $scope.sidebartag = 'almacenamiento';
              $scope.description = "¿Necesitas almacenar mercancía? Ingresa al directorio y busca la mejor opción para tu empresa. O Registra tu empresa de manera gratuita. ¡Haz click ahora¡";
              break;
            case "Logística":
              $scope.title = "Directorio Industrial de Empresas de Logística- Portalia Plus";
              $scope.sidebartag = 'logistica';
              $scope.description = "Encuentra a los expertos en logística y deja que se encarguen de tu proyecto, ingresa al mejor directorio de México y regístrate gratis... ¡Ingresa ahora!";
              break;
            case "Transporte":
              $scope.title = "Directorio Empresarial de Industrias del Transporte | Portalia Plus";
              $scope.sidebartag = 'transporte';
              $scope.description = "Los mejores servicios para enviar y recibir tu mercancía, busca en el directorio la empresa que necesitas y cotiza. La mensajería más completa. ¡Haz click!";
              break;
            case "Importación y exportación":
              $scope.title = "Directorio de Empresas Industriales | Importación y Exportación";
              $scope.sidebartag = 'importacion-y-exportacion';
              $scope.description = "Ingresa al directorio Portalia Plus y encuentra las mejores empresas de importación y exportación. Ponte en contacto con ellas. ¡Haz click aquí!";
              break;
            case "Administración":
              $scope.title = "Directorio Industrial de Empresas del Sector Administración";
              $scope.sidebartag = 'administracion';
              $scope.description = "Consulta a los expertos, tenemos empresas del sector administración que te apoyarán en tus procesos de nómina, reclutamiento, capacitación... ¡Entra Ya!";
              break;
            case "Computación y sistemas":
              $scope.title = "Directorio de Empresas en Computación y Sistemas | Portalia Plus";
              $scope.sidebartag = 'computacion-y-sitemas'
              $scope.description = "Tenemos el mejor directorio en computación y sistemas para ti, encuentra lo que necesitas en soluciones de software, aplicaciones moviles, alarmas, portones... ¡Haz click aquí!";
              break;
            case "Formación y Capacitación":
              $scope.title = "Directorio Empresarial de formación y Capacitación | Portalia Plus";
              $scope.sidebartag = 'formacion-y-capacitacion';
              $scope.description = "Encuentra las mejores opciones de formación y capacitación para tu empresa, a través de especialistas en cursos, coaching, escuelas... ¡Ingresa Aquí!";
              break;
            case "Profesionales":
              $scope.title = "Directorio Empresarial de Servicios Profesionales | Portalia Plus";
              $scope.sidebartag = 'profesionales';
              $scope.description = "Somos el directorio con más servicios profesionales a tu alcance, contamos con especialistas, asesores, ejecutivos, consultores... ¡Ponte en contacto aquí!";
              break;
            case "Telecomunicación":
              $scope.title = "Directorio Empresarial de la Industria | Sector de Telecomunicación";
              $scope.sidebartag = 'telecomunicacion';
              $scope.description = "Los mejores servicios de telecomunicación en nuestro directorio, conoce a los expertos en Portalia Plus. Solicita tu cotización sin costo. !Haz click aquí!";
              break;
            case "Comercializadores":
              $scope.title = "Directorio Industrial de Empresas Comercializadoras | Portalia Plus";
              $scope.sidebartag = 'comercializadores';
              $scope.description = "Las empresas comercializadoras son aquellas...Encuentra las mejores en el directorio, como abastecedoras, fabricantes de todo tipo de productos.¡Cotiza Ya!";
              break;
            case "Financiero":
              $scope.title = "Directorio Empresarial Financiero de la Industria | Portalia Plus";
              $scope.sidebartag = 'financiero';
              $scope.description = "Las empresas financieras son indispensables en... Por eso contamos con las mejores empresas del ramo crediticio, financiero...¡Ingresa al directorio aquí!";
              break;
            case "Inmobiliarios":
              $scope.title = "Directorio Empresarial de la Industria Inmobiliaria | Portalia Plus";
              $scope.sidebartag = 'inmobiliarios';
              $scope.description = "Encuentra las mejores opciones del sector Inmobiliario, elige tu próxima oficina ,cambia tus muebles. El directorio empresarial que necesitas. ¡Entra Ya!";
              break;
            case "Públicos y Sociales":
              $scope.title = "Directorio Industrial de Empresas | Sector Público y Social";
              $scope.sidebartag = 'publicos-y-sociales';
              $scope.description = "Portalia Plus es un directorio de calidad, donde podrás encontrar las mejores opciones de empresas públicas y sociales. ¡Haz click e ingresa!";
              break;
            case "Agencias Especializadas":
              $scope.title = "Directorio Empresarial de Agencias Especializadas | Portalia Plus";
              $scope.sidebartag = 'agencias-especializadas';
              $scope.description = "Las agencias especializadas cuentan con variedad de paquetes para viajes, eventos especiales... Encuentra los mejores precios en el directorio. ¡Haz click!";
              break;
            case "Restaurantes":
              $scope.title = "Directorio Empresarial de Restaurantes | Portalia Plus";
              $scope.sidebartag = 'restaurantes';
              $scope.description = "¿Comida de negocios? Conoce los mejores restaurantes en la ciudad donde te encuentres. Portalia Plus el directorio empresarial de restaurantes. ¡Entra Ya!";
              break;
            case "Hoteles":
              $scope.title = "Directorio Empresarial de Hoteles | Sector Turismo";
              $scope.sidebartag = 'hoteles';
              $scope.description = "¿Sales de viaje? Tenemos la mejor opción en hoteles, ingresa a Portalia Plus el directorio empresarial más completo y compruébalo. ¡Haz click ahora!";
              break;
            case "Transporte Turístico":
              $scope.title = "Directorio Empresarial de Transporte Turístico | Sector Turismo";
              $scope.sidebartag = 'transporteturistico';
              $scope.description = "¿Necesitas transporte para tu viaje?Encuentra la mejor opción y elige la que más te agrade. El directorio más completo de transporte turístico. ¡Cotiza Ya!";
              break;
            default:
              $scope.title = "Directorio de Empresas Industriales Portalia Plus";
              $scope.sidebartag = 'sidebar';
              $scope.description = "Anúnciate gratis en nuestro directorio para empresas. Soluciones especializadas para tu negocio. Además disfruta de beneficios para hacer crecer tus ventas.";
              break;
          }
          $scope.getBlog();
        }

        Auth.getCurrentUser(function (user) {

          $scope.user = user;

          // Leer los parametros recibidos
          $scope.initParams( 6 );

          // Aplicar función para seo
          // $scope.mejorarSEO( $scope.categoria )

        });

        $scope.getBlog = function(){
          $http.post('https://blog.portaliaplus.com/api/get_tag_posts?count=50&page=1&tag_id=194')
            .then(function(response) {
              $scope.posts = [];
              console.log(response);
              for(var i = 0; i < response.data.posts.length; i++) {
                for(var x = 0; x < response.data.posts[i].tags.length; x++) {
                  if (response.data.posts[i].tags[x].slug === $scope.sidebartag) {
                      $scope.posts.push(response.data.posts[i])
                    }
                }
              }
            })
            .catch(function(err){
              console.log(err);
            });
        }

        // mejorarSEO($routeParams.categoria);


    });



})();
