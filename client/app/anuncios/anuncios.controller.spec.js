'use strict';

describe('Component: AnunciosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var AnunciosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    AnunciosComponent = $componentController('anuncios', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
