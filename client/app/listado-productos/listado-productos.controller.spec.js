'use strict';

describe('Component: ListadoProductosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ListadoProductosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    ListadoProductosComponent = $componentController('listado-productos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
