'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/listado-productos/:param1/:page', {
        template: '<listado-productos></listado-productos>'
      });
  });
