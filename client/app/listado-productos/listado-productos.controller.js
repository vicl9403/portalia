'use strict';

(function(){

class ListadoProductosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('listadoProductos', {
    templateUrl: 'app/listado-productos/listado-productos.html',
    controller: ListadoProductosComponent,
    controllerAs: 'listadoProductosCtrl'
    }).controller('ListadoProductosController', function($scope, $http, $window, Auth, $routeParams) {

    // Instanciar el cliente de algolia
    // Production key
    var client = algoliasearch("Z0PK4O90D9", "cee882aa6ab6753d86530f537afe5e87");
    //Development key
    // let client = algoliasearch("TLTIO1USTU", "ba114bfd085d193af38c6017937cfb37");

    let index = client.initIndex('products');

    $scope.getProducts = function () {

      // Llamar la api de búsqueda
      index.search({
        query: $routeParams.param1 ,

        page: 0
      })
        .then( function (res) {
          // Obtener los documentos de la búsqueda
          $scope.products = res.hits;
          $scope.loading = false;
          $scope.$apply();
        });


    }


    $scope.getProductImage = function ( product ) {

      if( product.pictures.length == 0)
        return '/assets/images/foto-perfil-default.png';

      for( let i = 0 ; i < product.pictures.length ; i++ ) {
        if( product.pictures[i].mainPicture )
          return product.pictures[i].url.replace('./client','');
      }
      return product.pictures[0].url.replace('./client','')
    }
    angular.element(document).ready(function () {

      $scope.getProducts();

    });




  });

})();
