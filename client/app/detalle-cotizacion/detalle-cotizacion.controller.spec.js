'use strict';

describe('Component: DetalleCotizacionComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var DetalleCotizacionComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    DetalleCotizacionComponent = $componentController('DetalleCotizacionComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
