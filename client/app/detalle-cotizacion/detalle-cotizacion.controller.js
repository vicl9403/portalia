'use strict';
(function(){

class DetalleCotizacionComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('detalleCotizacion', {
    templateUrl: 'app/detalle-cotizacion/detalle-cotizacion.html',
    controller: DetalleCotizacionComponent
  }).controller('detalleCotizacionController', function($scope, $http, $window, Auth, $timeout) {



    Auth.getCurrentUser(function(currentUser) {

      $scope.userData = currentUser;

      if( currentUser.nombre == undefined )
        $scope.userData = null;

      if ($scope.userData.paquete == 'gratis') {
        //$window.location = '/oportunidades-de-negocio';
        swal( "¡Ojo!", "Recuerda que este módulo va a estar disponible solo por tiempo limitado, es un módulo exclusivo de las empresas con membresía select o plus",'warning');
      }
    });



  });

})();
