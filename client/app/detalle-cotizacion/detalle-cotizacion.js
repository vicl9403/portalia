'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/detalle-cotizacion/:id', {
        template: '<detalle-cotizacion></detalle-cotizacion>'
      });
  });
