'use strict';
(function(){

class Error404Component {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('error404', {
    templateUrl: 'app/error-404/error-404.html',
    controller: Error404Component
  });

})();
