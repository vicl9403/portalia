'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/error-404', {
        template: '<error-404></error-404>'
      });
  });
