'use strict';

describe('Component: Error404Component', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var Error404Component, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    Error404Component = $componentController('Error404Component', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
