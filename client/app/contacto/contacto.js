'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/contacto', {
        template: '<contacto></contacto>'
      });
  });
