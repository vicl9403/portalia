'use strict';
(function() {


  angular.module('portaliaApp')
    .component('contacto', {
      templateUrl: 'app/contacto/contacto.html'
    }).controller('contactoCrl', function($scope, $http, $window, Auth) {
      $scope.data = {};
      $scope.data.subject = 'Correo de contacto';

      $scope.logged = Auth.isLoggedIn;

      $scope.isLoading = false;


      $scope.getPromotions = function () {
        $http.get('/api/promotions')
          .then( res => {
            $scope.promotions = res.data;
          })
      }

      $scope.enviar = function() {
        $scope.data.text = 'El usuario ' + $scope.data.nombre + ', con el correo ' + $scope.data.from + ', y telefono ' +
          $scope.data.telefono + ' envio un correo de contacto y la promoción de : ' + $scope.data.promocion   + ' con el mensaje:\n\n' + $scope.data.msj ;
        if (!$scope.formulario.$invalid) {

          $scope.data.to = 'membresias@portaliaplus.com.mx;';

          $scope.isLoading = true;

          $http.post('/api/email', $scope.data)
            .then(function(response) {
              if (response.status === 200) {
                $scope.data = {};
                swal('Éxito','Tu correo se envió sin problemas, nosotros te contactaremos','success');
                $scope.isLoading = false;
              } else {
                swal('Advertencia','Hubo un problema enviando el correo, intentelo de nuevo', 'warning');
              }
            })
            .catch(function(err) {
              console.log(err);
            });
        } else {
          swal('Advertencia','Asegurate de llenar correctamente todos los campos','warning');
        }
      };


      angular.element(document).ready(function () {
        $scope.getPromotions();
      });


    });

})();
