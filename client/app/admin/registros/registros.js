'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/registros', {
        template: '<registros></registros>'
      });
  });
