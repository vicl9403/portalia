'use strict';
(function() {

  angular.module('portaliaApp')
    .component('registros', {
      templateUrl: 'app/admin/registros/registros.html'
    }).controller('registrosCrl', function($scope, $http, $window, Auth, $location) {
      Auth.getCurrentUser(function(currentUser) {
        $scope.userData = currentUser // Your code
        if ($scope.userData.role != 'admin') {
          $window.location = '/';
        }
      });

      $("#document").ready(function() {
        $http.get("api/sectors")
          .then(function(response) {
            $scope.sectors = response.data;
            console.log($scope.sectors);
          });

        $http.get('/api/users/adminMes')
          .then(function(response) {
            $scope.datosMensuales = response.data;
            google.charts.setOnLoadCallback(drawChartRegistrosGenerales);
            console.log($scope.datosMensuales);
          })
          .catch(function(error) {
            console.log(error);
          });

        google.charts.load('current', {
          packages: ['corechart']
        });

        // Chart de registros sector
        // google.charts.setOnLoadCallback(drawChartRegistrosSector);

        $('.grid-categoria').masonry();
        $('.grid-tab').masonry();

        $("#menu1").addClass('tab-pane');
        $("#menu2").addClass('tab-pane');
        $("#menu3").addClass('tab-pane');
        $("#menu4").addClass('tab-pane');
        $("#menu5").addClass('tab-pane');
        $("#menu6").addClass('tab-pane');


        $("#tab1").click();


        $('#tabSubcategorias a').click(function(e) {
          e.preventDefault()
          $(this).tab('show')
        })

      });

      $scope.chartSectores = function() {

        $('#chartRegistrosGenerales').show();
        $('#lol').css('display', 'none');
        $('#visitantes').hide();
        $http.post('/api/users/adminSectores')
          .then(function(response) {
            console.log(response);
            $scope.datosSectores = response.data;
            google.charts.setOnLoadCallback(drawChartRegistrosSector);
          })
          .catch(function(error) {
            console.log(error);
          });
      }

      $scope.chartGeneral = function() {
        $('#chartRegistrosGenerales').show();
        $('#lol').css('display', 'none');
        $('#visitantes').hide();
        $http.get('/api/users/adminMes')
          .then(function(response) {
            $scope.datosMensuales = response.data;
            google.charts.setOnLoadCallback(drawChartRegistrosGenerales);
            console.log($scope.datosMensuales);
          })
          .catch(function(error) {
            console.log(error);
          });
      }

      var i = 0;
      $scope.subSectors = [];
      $scope.chartSubsectores = function() {
        $scope.seeRegistros = false;
        $http.get('/api/users/aggregateCategories')
          .then(function(response) {
            console.log(response);
            $scope.categories = response.data;
          })
          .catch(function(err){
            console.log(err);
          });
        document.getElementById('titulo').innerHTML = "ESTADÍSTICAS DE REGISTRO POR SUBSECTOR";
        $('#chartRegistrosGenerales').hide();
        $('#visitantes').hide();
        $('#lol').css('display', 'block');
        $('.sectorgrid').css('position', 'static');
      }

      $scope.preregistros = function() {
        document.getElementById("registros").disabled = true;
        document.getElementById("preregistros").disabled = true;
        $scope.seeRegistros = true;
        $http.get('/api/users/aggregatePreCategories')
          .then(function(response) {
            console.log(response);
            document.getElementById("registros").disabled = false;
            document.getElementById("preregistros").disabled = false;
            $scope.categories = response.data;
          })
          .catch(function(err){
            console.log(err);
          });
      }

      $scope.normalRegistros = function() {
        document.getElementById("registros").disabled = true;
        document.getElementById("preregistros").disabled = true;
        $scope.seeRegistros = false;
        $http.get('/api/users/aggregateCategories')
          .then(function(response) {
            console.log(response);
            document.getElementById("registros").disabled = false;
            document.getElementById("preregistros").disabled = false;
            $scope.categories = response.data;
          })
          .catch(function(err){
            console.log(err);
          });
      }

      $scope.chartVisitantes = function() {
        document.getElementById('titulo').innerHTML = "ESTADÍSTICAS DE REGISTRO DE VISITANTES";
        $http.get('/api/users/getVisitors')
          .then(function(response) {
            $scope.visitantes = response.data;
            console.log($scope.visitantes);
          })
          .catch(function(error) {
            console.log(error);
          });
        $('#chartRegistrosGenerales').hide();
        $('#lol').css('display', 'none');
        $('#visitantes').show();
      }

      $scope.search = function(row) {
        return (angular.lowercase(row.nombre).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
          angular.lowercase(row.email).indexOf(angular.lowercase($scope.query) || '') !== -1);
      };

      function drawChartRegistrosGenerales() {
        document.getElementById('titulo').innerHTML = "ESTADÍSTICAS DE REGISTRO POR PAQUETE";
        var data = google.visualization.arrayToDataTable([
          ['Mes.', 'GRATUITO', 'SELECT', 'PLUS'],
          ['Ene.', $scope.datosMensuales.Gratuito.Enero, $scope.datosMensuales.SelectP.Enero, $scope.datosMensuales.Plus.Enero],
          ['Feb.', $scope.datosMensuales.Gratuito.Febrero, $scope.datosMensuales.SelectP.Febrero, $scope.datosMensuales.Plus.Febrero],
          ['Mar.', $scope.datosMensuales.Gratuito.Marzo, $scope.datosMensuales.SelectP.Marzo, $scope.datosMensuales.Plus.Marzo],
          ['Abr.', $scope.datosMensuales.Gratuito.Abril, $scope.datosMensuales.SelectP.Abril, $scope.datosMensuales.Plus.Abril],
          ['May.', $scope.datosMensuales.Gratuito.Mayo, $scope.datosMensuales.SelectP.Mayo, $scope.datosMensuales.Plus.Mayo],
          ['Jun.', $scope.datosMensuales.Gratuito.Junio, $scope.datosMensuales.SelectP.Junio, $scope.datosMensuales.Plus.Junio],
          ['Jul.', $scope.datosMensuales.Gratuito.Julio, $scope.datosMensuales.SelectP.Julio, $scope.datosMensuales.Plus.Julio],
          ['Ago.', $scope.datosMensuales.Gratuito.Agosto, $scope.datosMensuales.SelectP.Agosto, $scope.datosMensuales.Plus.Agosto],
          ['Sep.', $scope.datosMensuales.Gratuito.Septiembre, $scope.datosMensuales.SelectP.Septiembre, $scope.datosMensuales.Plus.Septiembre],
          ['Oct.', $scope.datosMensuales.Gratuito.Octubre, $scope.datosMensuales.SelectP.Octubre, $scope.datosMensuales.Plus.Octubre],
          ['Nov.', $scope.datosMensuales.Gratuito.Noviembre, $scope.datosMensuales.SelectP.Noviembre, $scope.datosMensuales.Plus.Noviembre],
          ['Dic.', $scope.datosMensuales.Gratuito.Diciembre, $scope.datosMensuales.SelectP.Diciembre, $scope.datosMensuales.Plus.Diciembre]
        ]);

        var options = {
          legend: {
            position: 'top'
          },
          colors: ['#FFD700', '#FF9000', '#ff0000']
        };

        var chart = new google.visualization.LineChart(document.getElementById('chartRegistrosGenerales'));

        chart.draw(data, options);
      }

      function drawChartRegistrosSector() {
        document.getElementById('titulo').innerHTML = "ESTADÍSTICAS DE REGISTRO POR SECTOR";
        var data = google.visualization.arrayToDataTable([
          ['Mes', $scope.sectors[0].name, $scope.sectors[1].name, $scope.sectors[2].name, $scope.sectors[3].name, $scope.sectors[4].name, $scope.sectors[5].name],
          ['Ene.', $scope.datosSectores.agroindustria.ene, $scope.datosSectores.construccion.ene, $scope.datosSectores.electricidadAgua.ene, $scope.datosSectores.industriasManufactureras.ene, $scope.datosSectores.serviciosEspecializados.ene, $scope.datosSectores.turismo.ene],
          ['Feb.', $scope.datosSectores.agroindustria.feb, $scope.datosSectores.construccion.feb, $scope.datosSectores.electricidadAgua.feb, $scope.datosSectores.industriasManufactureras.feb, $scope.datosSectores.serviciosEspecializados.feb, $scope.datosSectores.turismo.feb],
          ['Mar.', $scope.datosSectores.agroindustria.mar, $scope.datosSectores.construccion.mar, $scope.datosSectores.electricidadAgua.mar, $scope.datosSectores.industriasManufactureras.mar, $scope.datosSectores.serviciosEspecializados.mar, $scope.datosSectores.turismo.mar],
          ['Abr.', $scope.datosSectores.agroindustria.abr, $scope.datosSectores.construccion.abr, $scope.datosSectores.electricidadAgua.abr, $scope.datosSectores.industriasManufactureras.abr, $scope.datosSectores.serviciosEspecializados.abr, $scope.datosSectores.turismo.abr],
          ['May.', $scope.datosSectores.agroindustria.may, $scope.datosSectores.construccion.may, $scope.datosSectores.electricidadAgua.may, $scope.datosSectores.industriasManufactureras.may, $scope.datosSectores.serviciosEspecializados.may, $scope.datosSectores.turismo.may],
          ['Jun.', $scope.datosSectores.agroindustria.jun, $scope.datosSectores.construccion.jun, $scope.datosSectores.electricidadAgua.jun, $scope.datosSectores.industriasManufactureras.jun, $scope.datosSectores.serviciosEspecializados.jun, $scope.datosSectores.turismo.jun],
          ['Jul.', $scope.datosSectores.agroindustria.jul, $scope.datosSectores.construccion.jul, $scope.datosSectores.electricidadAgua.jul, $scope.datosSectores.industriasManufactureras.jul, $scope.datosSectores.serviciosEspecializados.jul, $scope.datosSectores.turismo.jul],
          ['Ago.', $scope.datosSectores.agroindustria.ago, $scope.datosSectores.construccion.ago, $scope.datosSectores.electricidadAgua.ago, $scope.datosSectores.industriasManufactureras.ago, $scope.datosSectores.serviciosEspecializados.ago, $scope.datosSectores.turismo.ago],
          ['Sep.', $scope.datosSectores.agroindustria.sep, $scope.datosSectores.construccion.sep, $scope.datosSectores.electricidadAgua.sep, $scope.datosSectores.industriasManufactureras.sep, $scope.datosSectores.serviciosEspecializados.sep, $scope.datosSectores.turismo.sep],
          ['Oct.', $scope.datosSectores.agroindustria.oct, $scope.datosSectores.construccion.oct, $scope.datosSectores.electricidadAgua.oct, $scope.datosSectores.industriasManufactureras.oct, $scope.datosSectores.serviciosEspecializados.oct, $scope.datosSectores.turismo.oct],
          ['Nov.', $scope.datosSectores.agroindustria.nov, $scope.datosSectores.construccion.nov, $scope.datosSectores.electricidadAgua.nov, $scope.datosSectores.industriasManufactureras.nov, $scope.datosSectores.serviciosEspecializados.nov, $scope.datosSectores.turismo.nov],
          ['Dic.', $scope.datosSectores.agroindustria.dic, $scope.datosSectores.construccion.dic, $scope.datosSectores.electricidadAgua.dic, $scope.datosSectores.industriasManufactureras.dic, $scope.datosSectores.serviciosEspecializados.dic, $scope.datosSectores.turismo.dic]
        ]);

        var options = {
          legend: {
            position: 'top',
            maxLines: 4
          },

          aggregationTarget: 'category',
          colors: ['#ffd700', '#10b2ff', '#ff0000', '9fd700', '6f00ff', 'ff9000']
        };

        var chart = new google.visualization.LineChart(document.getElementById('chartRegistrosGenerales'));

        chart.draw(data, options);
      }


    });


})();
