'use strict';

describe('Component: RegistrosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RegistrosComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    RegistrosComponent = $componentController('RegistrosComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
