'use strict';
(function(){

class SeguimientoCotizacionesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('seguimientoCotizaciones', {
    templateUrl: 'app/admin/seguimiento-cotizaciones/seguimiento-cotizaciones.html',
    controller: SeguimientoCotizacionesComponent
  }).controller('SeguimientoCotizacionesController', function($scope, $http, $window, Auth, $location , $timeout) {

    //Las Apis de este controlador hacen llamadas a
    //las rutas de la carpeta quotation del servidor
    //pueden consultarse las rutas en el archivo index.js

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {


      if( user.role != 'admin' || user == null )
        $window.location.url = '/';

      $scope.user = user;
      $scope.activeQuote = null;
      $scope.activeProspect = null;
      $scope.prospects = null;

      $http.get('/api/quotes/approvedQuotes').then(function (res) {
        $scope.quotes = res.data;
      });

      // Variables de configuración para los índices de búsqueda de coincidencias
      $http.get('/api/quotes_configurations/last')
        .then( (res) => {
          console.log( res.data );
          delete res.data._id;
          $scope.configuration = res.data;
        })


    });

    $scope.sendTestEmail = function () {
      $http.get('/api/quotes/mandrill')
        .then( function (res) {
          console.log( res );
        })
    }

    $scope.getDateForHummans = function ( date ) {
      moment.locale('es');
      return moment(date).format('D MMMM YYYY');
    }

    $scope.viewFollow = function ( quote ) {

      $http.get('/api/quotation_user/getProspects/' + quote._id )
        .then( res => {
          $scope.prospects = res.data;
        });

      $scope.activeQuote = quote;
    }

    $scope.viewProspectDetail = function ( prospect ) {
      $scope.activeProspect = prospect;
      console.log( prospect );
      $("#modal-prospect-details").modal('show');
    }

    $scope.setConfigFile = function () {

      $http.post( '/api/quotes_configurations' , $scope.configuration )
        .then( (res) =>{
          swal( "Éxito" , 'Archivo de configuraciones actualizado correctamente' , 'success');
          delete res.data._id;
          $scope.configuration = res.data;
        })

    }

    $scope.updateProspect = function (entity , status) {

      entity.status = status;

      $http.patch('/api/quotation_user/' + entity._id , entity )
        .then( res => {
          swal('Éxito',`Se cambió la acción para la cotización`, 'success' );
        });

    }

    $scope.sendQuotesMail = function () {

      swal({
          title: "Estas seguro?",
          text: "Si continuas se va a enviar un correo a todos los usuarios del seguimiento",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, enviar",
          closeOnConfirm: false
        },
        function(){
          $http.post('/api/quotation_user/sendQuotesMail')
            .then( function (res) {
              $http.get('/api/quotes/approvedQuotes').then(function (res) {
                $scope.quotes = res.data;
                swal("Listo!", "Se envió el correo correctamente", "success");
              });

            })

        });


    }

    $scope.sendQuotesAnalysis = function () {

      swal({
          title: "Estas seguro?",
          text: "Se van a analizar todos los usuarios, es probable que la página tarde en responder si ejecutas esta operación",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, continuar",
          closeOnConfirm: false
        },
        function(){
          $http.post('/api/quotes/sendQuotesAnalysis')
            .then( (res) => {
              $http.get('/api/quotes/approvedQuotes').then(function (res) {
                $scope.quotes = res.data;
                swal("Éxito",'El análisis se mandó a ejecutar correctamente' , 'success');
              });
            })

        });


    }

  $scope.sendCustomQuoteAnalysis = function ( quote ) {

    swal({
        title: "Estas seguro?",
        text: "Se van a analizar todos los usuarios, es probable que la página tarde en responder si ejecutas esta operación",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, continuar",
        closeOnConfirm: false
      },
      function(){
        $http.post('/api/quotes/sendQuotesAnalysisForOneQuote/' + quote._id + '/' + quote.score )
          .then( (res) => {
          $http.get('/api/quotes/approvedQuotes').then(function (res) {
          $scope.quotes = res.data;
          window.location.href = '/admin/seguimiento-cotizaciones'
        });
      })

      });


  }

    $scope.defuseQuote = function ( quote ) {

      swal({
          title: "Estas seguro?",
          text: "Si almacenas esta cotización ya no podrás mandarla por correo después, solo se verá en el perfil de los usuarios",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, almacenar",
          closeOnConfirm: false
        },
        function(){
          quote.status = 'done';
          $http.patch('/api/quotes/' + quote._id , quote )
            .then( (res) => {
              $http.get('/api/quotes/approvedQuotes').then(function (res) {
                $scope.quotes = res.data;
                swal("Éxito",'La cotización fué desactivada' , 'success');
              });

            })
        });

    }

  });

})();
