'use strict';

describe('Component: SeguimientoCotizacionesComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SeguimientoCotizacionesComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    SeguimientoCotizacionesComponent = $componentController('SeguimientoCotizacionesComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
