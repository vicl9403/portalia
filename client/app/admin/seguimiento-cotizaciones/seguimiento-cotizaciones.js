'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/seguimiento-cotizaciones', {
        template: '<seguimiento-cotizaciones></seguimiento-cotizaciones>'
      });
  });
