'use strict';
(function() {

  class UpdateUserComponent {
    constructor() {
      this.message = 'Hello';
    }
  }

  angular.module('portaliaApp')
    .component('updateUser', {

      templateUrl: 'app/admin/updateUser/updateUser.html',
      controller: UpdateUserComponent

    }).controller('UpdateUserCtr', function($scope, $http, Auth, $routeParams, $window, Upload) {
      Auth.getCurrentUser(function(currentUser) {
        $scope.userData = currentUser // Your code
        if ($scope.userData.role == 'admin' && $scope.userData.role == 'tmk')
          $window.location = '/';

      });

      $scope.user = {};
      $scope.user.sucursales = [];
      $scope.sectors = {};
      $scope.subSectors = {};
      $scope.estados = {};

      $scope.originalemail = '';

      $scope.getUser = function() {
        $http.get('/api/users/' + $routeParams.id)
          .then(function(response) {
            $scope.user = response.data;
            console.log($scope.user);
            $scope.originalemail = $scope.user.email;
          })
      }


      $scope.horModal = function() {
          console.log($routeParams);
          $("#shedules-modal").modal("toggle");
      }

      $scope.getSectors = function() {
        $scope.subsectores = {};

        // Obtener los sectores
        $http.get('/api/sectors/')
          .then(function(res) {
            $scope.sectors = res.data;

            for (var i = 0; i < $scope.sectors.length; i++) {
              if ($scope.user.sector == $scope.sectors[i]._id) {
                $scope.subsectores = $scope.sectors[i].subsectores;
                break;
              }
            }

          });
      }

      $scope.getEstados = function() {
        // Obtener los estados
        $http.get('/api/estados/')
          .then(function(res) {
            $scope.estados = res.data;
            for (var i = 0; i < $scope.estados.length; i++) {
              if ($scope.user.estado == i) {
                $scope.municipios = $scope.estados[i].municipios;
                break;
              }
            }
          });
      }

      // Esperar a que cargue la página
      angular.element(document).ready(function() {

        // Cachamos el id del usuario a modificar
        var idUser = $routeParams.id;

        // Redireccionamos si no tiene un id
        if (idUser == undefined)
          window.llocation.href = '/';

        // Obtenemos los datos del usuario en cuestión
        $scope.getUser();
        $scope.getSectors();
        $scope.getEstados();
      });

      // Función para actualizar los subsectores
      $("#sector").change(function() {
        // Iteramos en todos los sectores
        for (var i = 0; i < $scope.sectors.length; i++) {
          // Detectamos cuando el sector seleccionado corresponda al que se va iterando
          if ($("#sector").val() == $scope.sectors[i]._id) {
            // Actualizamos los subsectores del sector seleccionado
            $scope.subsectores = $scope.sectors[i].subsectores;
            // Actualizamos el scope porque no se está utilizando función de angular
            $scope.$apply();
            break;
          }
        }
      });

      // Detectar el cambio de selección en un estado
      $("#estado").change(function() {
        // Recorremos los estados hasta encontrar el que pertenezca al que se selecciono en el input
        for (var i = 0; i < $scope.estados.length; i++) {
          if ($("#estado").val() == i) {
            // Modificamos los municipios correspondientes
            $scope.municipios = $scope.estados[i].municipios;
            // Refrescamos el scope, porque no esta bindeado con angular
            $scope.$apply();
            break;
          }
        }
      });

      $scope.sendRecoveryPassword = function() {

        $http.post('/api/passwordRecoverys/promotion', $scope.user).then(function(response) {
          swal("Exito", "Correo de recuperación enviado correctamente", "success");
        }).catch(function(err) {
          swal("Error", "Ups detectamos un error intenta de nuevo o ponte en contacto con soporte@portaliaplus.com", "error");
          console.log(err);
        });
      }

      // Función de modificación  general
      $scope.update = function(user, title, message, type = 'success') {
        $http.patch('/api/users/' + $routeParams.id, user)
          .then(function(response) {
            swal(title, message, type);
            $scope.getUser();
          });

        Auth.getCurrentUser(function(currentUser) {
          var date = new Date();

          var data = {
            user: currentUser,
            userUpdated: $scope.user,
            type: title,
            date: date,
          };
          $http.post('/api/updatesOnUsers/', data)
            .then(function(res) {
              console.log('Se registro una modificacion');
            })
        });

      }

      // Funciones para modificar por bloques
      $scope.setBasicData = function() {
        $http.post('/api/users/check', {
            email: $scope.user.email
          })
          .then(function(res) {
            $scope.checkedEmail = true;
            if (res.status == 201) {

              // Generamos el JSON con los datos para actualizar
              var data = {
                'telefono': $scope.user.telefono,
                'email': $scope.user.email,
                'role': $scope.user.role
              };
              // Mandamos llamar la función que actualiza los datos
              $scope.update(data, "Datos básicos", "modificados correctamente");

            } else if (res.status == 200) {
              // Generamos el JSON con los datos para actualizar
              var data = {
                'telefono': $scope.user.telefono,
                'role': $scope.user.role
              };

              if ($scope.user.email == $scope.originalemail)
                $scope.update(data, "Datos básicos", "modificados correctamente");
              else
                $scope.update(data, "Advertencia", "El teléfono se modifico correctamente, el email seleccionado ya pertenece a otro usuario", 'warning');
            }
          })
          .catch(function(err) {
            console.log(err);
          });
      }
      $scope.setFacturacion = function() {
        // Generamos el JSON con los datos para actualizar
        var facturacion = {
          'razon': $scope.user.razon,
          'rfc': $scope.user.rfc,
          'direccion': $scope.user.direccion,
        };
        // Mandamos llamar la función que actualiza los datos
        $scope.update(facturacion, "Datos de factuación", "modificados correctamente");
      }
      $scope.setEmpresa = function() {
        for (var x = 0; x < $scope.estados[$('#estado').val()].municipios.length; x++) {
          if ($scope.estados[$('#estado').val()].municipios[x]._id == $("#municipio").val()) {
            $scope.municipio_name = $scope.estados[$('#estado').val()].municipios[x].name;
          }
        }
        var datosEmpresa = {
          'nombre': $scope.user.nombre,
          'sector': $("#sector").val(),
          'sector_name': $("#sector option:selected").text(),
          'categoria': $("#subsector option:selected").val(),
          'categoria_name': $("#subsector option:selected").text(),
          'estado': $("#estado").val(),
          'estado_name': $scope.estados[$('#estado').val()].name,
          'municipio': $("#municipio").val(),
          'municipio_name': $scope.municipio_name,
          'emailSec': $scope.user.emailSec,
        };
        // Mandamos llamar la función que actualiza los datos
        $scope.update(datosEmpresa, "Datos de empresa", "modificados correctamente");
      }
      $scope.setPerfilInfo = function() {
        var perfilInfo = {
          'about': $scope.user.about,
          'ventajas': $scope.user.ventajas,
          'tags': $scope.user.tags.toString().split(","),
          'products': $scope.user.products.toString().split(","),
        };
        // Mandamos llamar la función que actualiza los datos
        $scope.update(perfilInfo, "Datos de perfil", "modificados correctamente");
      }
      $scope.setRedes = function() {
        var redes = {
          'redes': {
            'facebook': $scope.user.redes.facebook,
            'linkedin': $scope.user.redes.linkedin,
            'twitter': $scope.user.redes.twitter,
            'youtube': $scope.user.redes.youtube,
            'googleplus': $scope.user.redes.googleplus,
          },
          'web': $scope.user.web,
        };
        // Mandamos llamar la función que actualiza los datos
        $scope.update(redes, "Redes sociales", "modificadas correctamente");
      }

      $scope.uploadProfilePic = function(file) {
        $scope.uploading = true;

        if (file == null || file.size > 10000 * 1000 * 1000) {
          swal("Error al subir el logo", "Revisa el peso de tu imagen, debe ser menor a 10mb", "error");
          // $scope.uploading = false;
          return;
        } else {
          Upload.upload({
            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            url: '/api/upload/profilePic/' + $scope.user._id + '/' +
              encodeURIComponent($scope.user.sector_name) + '/' +
              encodeURIComponent($scope.user.categoria_name) + '/' +
              encodeURIComponent($scope.user.nombre),
            file: file
          }).then(function(resp) {
            //upload function returns a promise
            console.log(resp);

            if (resp.data.errorCode === 0)
              swal("Exito", "Logo de la empresa agregado exitosamente");
            else
              swal("Error", "Ocurrió un error resconocido, por favor intenta de nuevo más tarde", "error");

            $scope.uploading = false;

          }, function(resp) {
            //catch error
            console.log('Error status: ' + resp.status);
            swal("Error", "Revisa el peso de tu imagen, debe ser menor a 10mb", "error");
          }, function(evt) {
            // console.log(evt);
            // console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total));
            // var progress = parseInt(100.0 * evt.loaded / evt.total);
            // waitingDialog.update(progress);
            // $scope.uploading = false;
          });
          //
        };
      };

      $scope.updateAdress = function() {
        console.log($scope.place);
        if ($scope.place.address_components.length == 7) {
          $scope.sucursal = {
            'estadoSucursal': $scope.place.address_components[3].long_name,
            'ciudad': $scope.place.address_components[2].long_name,
            'numero': 'Sin número',
            'colonia': $scope.place.address_components[1].long_name,
            'calle': $scope.place.address_components[0].long_name,
            'cp': 'Sin Código Postal',
            'loc': [$scope.place.geometry.location.lng(), $scope.place.geometry.location.lat()],
            'nombreSucu': 'Sucursal Principal',
            'longitud': $scope.place.geometry.location.lng(),
            'latitud': $scope.place.geometry.location.lat()
          }
        } else if ($scope.place.address_components.length == 8) {
          $scope.sucursal = {
            'estadoSucursal': $scope.place.address_components[5].long_name,
            'ciudad': $scope.place.address_components[3].long_name,
            'numero': $scope.place.address_components[0].long_name,
            'colonia': $scope.place.address_components[2].long_name,
            'calle': $scope.place.address_components[1].long_name,
            'cp': $scope.place.address_components[7].long_name,
            'loc': [$scope.place.geometry.location.lng(), $scope.place.geometry.location.lat()],
            'nombreSucu': 'Sucursal Principal',
            'longitud': $scope.place.geometry.location.lng(),
            'latitud': $scope.place.geometry.location.lat()
          }
        }

        $scope.user.sucursales.push($scope.sucursal);
        console.log($scope.user);
        Auth.updateProfile($scope.user);
      };







    });

})();
