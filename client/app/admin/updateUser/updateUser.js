'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/updateUser/:id', {
        template: '<update-user></update-user>'
      });
  });
