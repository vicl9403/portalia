'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/searchUser', {
        template: '<search-user></search-user>'
      });
  });
