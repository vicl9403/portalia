'use strict';

describe('Component: SearchUserComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SearchUserComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    SearchUserComponent = $componentController('SearchUserComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
