'use strict';
(function() {

  class SearchUserComponent {
    constructor() {
      this.message = 'Hello';
    }
  }

  angular.module('portaliaApp')
    .component('searchUser', {

      templateUrl: 'app/admin/searchUser/searchUser.html',
      controller: SearchUserComponent

    }).controller('SearchCtr', function($scope, $http, $window, Auth) {

      // Verificar que tenga permiso de estar aquí
      Auth.getCurrentUser(function(currentUser) {
        $scope.soloDestacados = false;
        $scope.userData = currentUser // Your code
        $scope.exportData = {}; //data para exportación
        if ($scope.userData.role != 'admin' && $scope.userData.role != 'tmk')
          $window.location = '/';

      });


      $scope.users = {};
      $http.get('/api/users/usuariosDestacados')
        .then(function(res) {
          $scope.users = res.data;
        })

      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
        });

      $http.get("api/estados")
        .then(function(response) {
          $scope.estados = response.data;
        });

      $scope.telefono = '';
      $scope.email = '';
      $scope.nombre = '';
      $scope.id = '';
      $scope.user = '';

      $scope.searchUser = function() {
        var data = {
          nombre: $scope.nombre,
          telefono: $scope.telefono,
          email: $scope.email,
        };
        $http.post('/api/users/searchToUpdate', data)
          .then(function(res) {
            console.log(res.data);
            $scope.users = res.data;
          })
      }

      $scope.searchById = function() {
        $http.get('/api/users/' + $scope.id)
          .then(function(res) {
            console.log(res.data);
            $scope.users = {
              0: res.data
            };
          })
      }

      $scope.changeCategoria = function(categoria) {
        if (categoria == '') {
          delete $scope.exportData.categoria
        }
      }

      $scope.changeEstado = function(estado) {
        if (estado == '') {
          delete $scope.exportData.estado
        }
      }

      $scope.changeSector = function(sector) {
        console.log(sector);
        if (sector != '') {
          $scope.temporal = $scope.sectors[parseInt($scope.exportData.sector)].name;
          $scope.exportData.sector_name = $scope.temporal;
          $('#categoria')
            .find('option')
            .remove()
            .end()
            .append('<option value="">Selecciona un sub sector</option>')
            .val('');

          for (var x = 0; x < $scope.sectors[sector].subsectores.length; x++) {
            $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, $scope.sectors[sector].subsectores[x].name));
          }
          $scope.temp.sector_position = sector;
          $scope.temp.sector = $scope.sectors[sector]._id;
          $scope.temp.sector_name = $scope.sectors[sector].name;
          if ($routeParams.email || $routeParams.id) {
            $("#categoria option[value='" + $scope.data.categoria + "']").prop('selected', true);
            // $scope.changeCategoria($scope.data.categoria);
          }
        } else {
          console.log('kkkkk');
          delete $scope.exportData.sector_name;
        }
      };

      $scope.export = function() {
        console.log($scope.exportData);
        if ($scope.exportData.sector_name == undefined && $scope.exportData.categoria == undefined && $scope.exportData.estado == undefined) {
          swal('Error', 'Selecciona al menos un filtro para la exportación', 'error')
        } else {
          $('#modal-loading').modal('toggle');
          $http.post('/api/users/generarCSV', $scope.exportData)
            .then(function(res) {
              $('#modal-loading').modal('toggle');
              if (res.data == 'No hay resultados') {
                swal('Error', 'Estas condiciones no arrojaron usuarios', 'error');
              } else {
                swal('Éxito', 'El CSV se descargará en un momento', 'success');
                window.location = '/assets/uploads/file.csv';
              }
            })
        }
      }

      $scope.destacarUser = function(user) {
        $scope.new = {
          idUser: user._id,
          idString: user._id
        };

        $http.post('/api/TopUsers/', $scope.new)
          .then(function(res) {
            if (res.data == 'Limite alcanzado') {
              swal("Error", " Haz alcanzado el límite de usuarios destacados", "error")
            } else {
              console.log(res);
              user.destacado = true;
              Auth.updateProfile(user);
            }
          })
          .catch(function(err) {
            console.log(err);
          });
      }

      $scope.undestacar = function(user) {
        console.log('kk');
        $scope.remove = {
          idString: user._id
        };

        $http.post('/api/topUsers/delete', $scope.remove)
          .then(function(res) {
            console.log(res);
            user.destacado = false;
            Auth.updateProfile(user);
          })
      }

      $scope.getFeaturedOnly = function(){
        $http.get('/api/topUsers/')
          .then(function(res){
            $scope.users = [];
            console.log(res);
            for(var x = 0; x < res.data.length; x++){
              $scope.users.push(res.data[x].idUser[0]);
            }
            console.log($scope.users);
            $scope.soloDestacados = true;
          })
          .catch(function(err){
            console.log(err);
          })
      }

      $scope.clear = function(){
        $http.get('/api/users/usuariosDestacados')
          .then(function(res) {
            $scope.soloDestacados = false;
            $scope.users = res.data;
          })
      }

    });

})();
