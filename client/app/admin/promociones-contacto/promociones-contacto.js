'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/promociones-contacto', {
        template: '<promociones-contacto></promociones-contacto>'
      });
  });
