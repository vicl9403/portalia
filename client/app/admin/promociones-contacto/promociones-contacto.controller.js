'use strict';

(function(){

class PromocionesContactoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('promocionesContacto', {
    templateUrl: 'app/admin/promociones-contacto/promociones-contacto.html',
    controller: PromocionesContactoComponent,
    controllerAs: 'promocionesContactoCtrl'
  }).controller('PromocionesContactoController', function ($scope, $http, Auth, $timeout, $window, $location, $rootScope) {

    $scope.getPromotions = function () {
      $http.get('/api/promotions')
        .then( res => {
          console.log( res.data );
          $scope.promotions = res.data;
        })
    }

    $scope.newpromotion = function () {
      $http.post('/api/promotions' , $scope.newPromotion )
        .then( res => {
          $scope.newPromotion = {
            name : ''
          };
          $scope.getPromotions();
        })
    }

    $scope.deletePromotion = function (id) {
      $http.delete('/api/promotions/' + id )
        .then( res => {
          $scope.getPromotions();
        })
    }

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;

      if( user.role != 'admin')
        $window.location.href = '/';

      $scope.getPromotions();

      $scope.newPromotion = {
        name : ''
      };

    });

})

})();
