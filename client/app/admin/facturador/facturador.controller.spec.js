'use strict';

describe('Component: FacturadorComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var FacturadorComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    FacturadorComponent = $componentController('facturador', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
