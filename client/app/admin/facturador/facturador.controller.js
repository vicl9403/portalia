'use strict';

(function(){

angular.module('portaliaApp')
  .component('facturador', {
    templateUrl: 'app/admin/facturador/facturador.html'
  }).controller('facturadorCtrl', function($scope, $http, $window, Auth){
    $scope.data = {};
    Auth.getCurrentUser(function(currentUser) {
      $scope.userData = currentUser // Your code
      if ($scope.userData.role != 'admin') {
        $window.location = '/';
      }
    });

    $scope.generar = function(){
      console.log($scope.data);
      $http.post('/api/busquedas/soap/', $scope.data)
        .then(function(res){
          console.log(res);
          if(res.status == 200){
            swal('Éxito', 'Factura timbrada', 'success');
            $scope.data = {};
          }
        })
        .catch(function(err){
          console.log(err);
        });
    }
  });

})();
