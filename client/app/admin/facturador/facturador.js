'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/facturador', {
        template: '<facturador></facturador>'
      });
  });
