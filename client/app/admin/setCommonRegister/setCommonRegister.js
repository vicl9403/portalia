'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/setCommonRegister', {
        template: '<set-common-register></set-common-register>'
      });
  });
