'use strict';
(function(){

class SetCommonRegisterComponent {
    constructor() {
        this.message = 'Hello';
    }
}

angular.module('portaliaApp')
    .component('setCommonRegister', {
        templateUrl: 'app/admin/setCommonRegister/setCommonRegister.html',
        controller: SetCommonRegisterComponent
    }).controller('SetCommonRegisterCtr', function ($scope, $http, $window, Auth) {

        $scope.commonRegisters = [];
        $scope.text = '';

        $scope.getCommonRegisters = function () {
            $http.get('/api/commonRegisters/').then(function (res) {
                $scope.commonRegisters = res.data;
            }, function (resp) {
                //
            });
        };
        $scope.addCommonRegister = function () {
          console.log($scope.text);
          if ($scope.text != "")
          {            var data = {
                          name: $scope.text,
                          count: 0,
                      };
                      $http.post('/api/commonRegisters/', data).then(function (res) {
                          $scope.getCommonRegisters();
                          $scope.text = '';
                          console.log(resp);
                      }, function (resp) {
                          if(resp.data.code == 11000)
                              swal("Cuidado!","Ya hay un registro ingresado con ese nombre, prueba con otro", "warning")
                      });
          }
          else
          {
              swal("Cuidado", "No debes dejar el espacio en blanco", "warning")
          }
        }
        $scope.editOpen = function (item) {
          $('#editModal').modal('toggle');
          $scope.aEditar = item;
        }

        $scope.saveName = function () {
          $http.patch('/api/commonRegisters/' + $scope.aEditar._id, $scope.aEditar)
            .then(function(res){
              console.log(res);
            })
            .catch(function(err){
              console.log(err);
            })
        }

        $scope.deleteCommonRegister = function ( id ) {
            if( $scope.commonRegisters.length <= 1 )
                swal("Error","Lo sentimos, pero debes tener al menos un elemento disponible, no podemos eliminarlo", "error")
            else
            {
                swal({
                        title: "Estas seguro?",
                        text: "Si borras este registro se perdera el conteo que tiene",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar",
                        closeOnConfirm: false
                    },
                    function () {
                        $http.delete('/api/commonRegisters/' + id).then(function (res) {
                            $scope.getCommonRegisters();
                            swal("Listo!", "El registro se eliminó exitosamente", "success");
                        }, function (resp) {
                            swal("Error!", "Ocurrió un problema al intentar eliminar el registro, por favor intenta más tarde", "error");
                        });

                    });
            }
        }

        angular.element(document).ready(function () {
            $scope.getCommonRegisters();
        });


    });

})();
