'use strict';

describe('Component: SetCommonRegisterComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SetCommonRegisterComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    SetCommonRegisterComponent = $componentController('SetCommonRegisterComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
