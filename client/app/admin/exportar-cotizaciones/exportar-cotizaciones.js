'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/exportar-cotizaciones', {
        template: '<exportar-cotizaciones></exportar-cotizaciones>'
      });
  });
