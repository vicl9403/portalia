'use strict';

(function(){

class ExportarCotizacionesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('exportarCotizaciones', {
    templateUrl: 'app/admin/exportar-cotizaciones/exportar-cotizaciones.html',
    controller: ExportarCotizacionesComponent,
    controllerAs: 'exportarCotizacionesCtrl'
  }).controller('ExportarCotizacionesController', function($scope, $http, $window, Auth, $location, $routeParams) {


    $scope.filter = function () {
      let since =  moment( $("#since").val() ).format('YYYY-MM-DD');
      let until = moment( $("#until").val() ).format('YYYY-MM-DD');


      $scope.getQuotesByDate( since, until );


      //$http.get('/api/quotation_user/' + since + "/" + until )
        //.then( res => {
          //$scope.registers = res.data;
          //$scope.isLoading = false;
        //})
    };


    $scope.jsonToCsv = function (objArray) {
      let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
      let str = '';

      for (let i = 0; i < array.length; i++) {
        let line = '';
        for (let index in array[i]) {
          if (line != '') line += ','

          line += array[i][index];
        }

        str += line + '\r\n';
      }

      return str;
    }

    $scope.downloadCsv = function ( csvString , name = null ) {
      var a         = document.createElement('a');
      a.href        = 'data:attachment/csv,' +  encodeURIComponent(csvString);
      a.target      = '_blank';
      if( name == null )
        a.download    = 'reporte-cotizaciones.csv';
      else
        a.download    = name + '.csv';

      document.body.appendChild(a);
      a.click();
    }

    $scope.exportToCsv = function ( data = null ) {

      $scope.isLoading = true;

      let csvResult = {};

      if ( data == null ) {
        let prospectsParsed = [];

        for (let i = 0; i < $scope.prospects.length; i++) {

          if ($scope.prospects[i].user != null) {

            prospectsParsed.push({
              nombre: $scope.prospects[i].user.nombre,
              email: $scope.prospects[i].user.email,
              telefono: $scope.prospects[i].user.telefono,
              fecha: $scope.getDateForHummans($scope.prospects[i].startAt),
              id: $scope.prospects[i].user._id
            })

          }
        }

        csvResult = $scope.jsonToCsv( prospectsParsed );

      }
      else
        csvResult = $scope.jsonToCsv( data );

      $scope.downloadCsv( csvResult , $scope.activeQuote.product + '-' + $scope.getDateForHummans($scope.activeQuote.createdAt) );

      $scope.isLoading = false;
    }

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY HH:MM');
    };

    $scope.getQuotesByDate = function (since, until) {
      $scope.isLoading = true;
      $http.get(`/api/quotes/date/${ since }/${ until }`)
        .then( res =>{
          $scope.isLoading = false;
          $scope.quotes = res.data;
        })
    }

    // Verificar que tenga permiso de estar aquí
    Auth.getCurrentUser(function(user) {

      if (user.role != 'admin')
        $window.location = '/';

      $scope.since = null;

      $scope.user = user;

      $scope.isLoading = false;

      $scope.activeQuote = null;

      $("#since").datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD',
      });
      $("#until").datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD',
      });

    });

    $scope.setWatcherToQuote = function (quote) {
      quote.watched = true;
      $http.patch('/api/quotes/' + quote._id , quote )
        .then( res =>{
          swal('Éxito','Se agregó esta cotización a la lista de seguimiento.','success');
          $scope.filter();
        })
    }

    $scope.setUnwatchedQuote = function (quote) {
      quote.watched = false;

      $http.patch('/api/quotes/' + quote._id , quote )
        .then( res =>{
          swal('Éxito','Se agregó esta cotización a la lista de seguimiento.','success');
          $scope.filter();
        })
    }

    $scope.viewQuoteProspects = function (quote) {

      // Emite el detalle de la cotización directamente en el componente hijo
      $scope.$broadcast ('getQuoteData', quote._id );

      $http.get('/api/quotation_user/getProspects/' + quote._id )
        .then( res => {
          $scope.prospects = res.data;
          $scope.activeQuote = quote;
        })
    }

    $scope.getProspectsReport = function ( quotes ) {

      let since =  moment( $("#since").val() ).format('YYYY-MM-DD');
      let until = moment( $("#until").val() ).format('YYYY-MM-DD');

      $http.get(`/api/quotation_user/report/${ since }/${ until }`)
        .then( res =>{
          console.log( res.data );
        })

    }

  });

})();
