'use strict';

describe('Component: ExportarCotizacionesComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ExportarCotizacionesComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    ExportarCotizacionesComponent = $componentController('exportar-cotizaciones', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
