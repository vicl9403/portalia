'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/redaccionMail', {
        template: '<redaccion-mail></redaccion-mail>'
      });
  });
