'use strict';

describe('Component: RedaccionMailComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RedaccionMailComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    RedaccionMailComponent = $componentController('RedaccionMailComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
