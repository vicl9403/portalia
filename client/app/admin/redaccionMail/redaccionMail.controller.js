'use strict';
(function(){

angular.module('portaliaApp')
  .component('redaccionMail', {
    templateUrl: 'app/admin/redaccionMail/redaccionMail.html'
  }).controller('redaccionMailCrl', function($scope, $http, $window, Auth, $location, $routeParams) {
    $http.get('api/mailings/'+ $routeParams.param).then(function(response){
      $scope.dataMail = response.data;
      console.log($scope.dataMail);
    })
    .catch(function (err) {
      console.log(err);
    })
    console.log($routeParams);

    $scope.message = {}
    $scope.test = {}
    var i = 0;
    $scope.enviarMail = function(){
      var x = 0;
      //Comprobar que el Title y/o el Body no esten vacios
      if ($scope.test.title == undefined || $scope.test.body == undefined) {
        swal("Error", "Debes ingresar el titulo y el cuerpo del correo","error")
        $scope.test.title = undefined;
        $scope.test.body = undefined;
      }
      else {
      $scope.x = $scope.dataMail.email[i].nombre;
      $scope.y = $scope.dataMail.email[i].sector_name;
      $scope.z = $scope.dataMail.email[i].categoria_name;
      var objectReplace = {
        '{ { nombre } }':$scope.x,
        '{ { sector_name } }':$scope.y,
        '{ { categoria_name } }':$scope.z,
      };
      //Remplazo de los nombres en los titulos
      $scope.dataMail.email[i].title = $scope.test.title.replace(/{ { nombre } }|{ { sector_name } }|{ { categoria_name } }/gi, function(matched){
        return objectReplace[matched];
      });
      //Remplazo de los nombres en el cuerpo del correo
      $scope.dataMail.email[i].body = $scope.test.body.replace(/{ { nombre } }|{ { sector_name } }|{ { categoria_name } }/gi, function(matched){
        return objectReplace[matched];
      });
      //Recursividad
      if (i<$scope.dataMail.email.length-1)
      {
        i++;
        $scope.enviarMail();
      }
      else {
        $http.patch('api/mailings/'+ $routeParams.param, $scope.dataMail).then(function(response){
          console.log(response);
              $http.post('api/mailings/sendMails/'+ $routeParams.param).then(function(response){
                console.log(response);
                swal("Exito", "Los correos están en proceso de envío, para más detalles sobre la difusión revisa el panel de SendInBlue","success")
              }).catch(function (err){
                console.log(err);
              })

        })
        .catch(function (err) {
          console.log(err);
        })
      }
    }
  }
  });
})();
