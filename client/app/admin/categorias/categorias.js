'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('admin/categorias', {
        template: '<categorias></categorias>'
      });
  });
