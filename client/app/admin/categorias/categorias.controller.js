'use strict';

(function(){

class CategoriasComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('categorias', {
    templateUrl: 'app/admin/categorias/categorias.html',
    controller: CategoriasComponent,
    controllerAs: 'categoriasCtrl'
  });

})();
