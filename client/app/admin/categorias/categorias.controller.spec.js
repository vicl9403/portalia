'use strict';

describe('Component: CategoriasComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CategoriasComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    CategoriasComponent = $componentController('categorias', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
