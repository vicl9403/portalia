'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/bd', {
        template: '<bd></bd>'
      });
  });
