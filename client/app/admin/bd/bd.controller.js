'use strict';
(function(){

class BdComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('bd', {
    templateUrl: 'app/admin/bd/bd.html',
    controller: BdComponent
  });

})();
