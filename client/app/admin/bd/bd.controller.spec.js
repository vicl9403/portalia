'use strict';

describe('Component: BdComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var BdComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    BdComponent = $componentController('BdComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
