'use strict';
(function(){

class CreateUserComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
    .component('createUser', {
        templateUrl: 'app/admin/createUser/createUser.html',
        controller: CreateUserComponent
    }).controller('CreateUserController', function ($scope, $http, Auth, $routeParams,$window,Upload)
    {
        Auth.getCurrentUser(function(currentUser)
        {
            $scope.userData = currentUser // Your code
            if ($scope.userData.role == 'admin' && $scope.userData.role == 'tmk')
                $window.location = '/';

        });

        $scope.user = {};
        $scope.sectors = {};
        $scope.subSectors = {};
        $scope.estados = {};
        $scope.user.role = "empresa";

        $scope.originalemail = '';
        $scope.user.role = 'empresa'
        $scope.user.shedules =
            [
                {
                    day:  'Lunes',
                    oppened:  false ,
                    opening:  '' ,
                    closing:  ''
                },
                {
                    day:  'Martes' ,
                    oppened:  false ,
                    opening: '',
                    closing: ''
                },
                {
                    day:  'Miércoles',
                    oppened: false ,
                    opening: '',
                    closing: ''
                },
                {
                    day:  'Jueves' ,
                    oppened:  false ,
                    opening: '',
                    closing: ''
                },
                {
                    day:  'Viernes',
                    oppened: false ,
                    opening: '',
                    closing: ''
                },
                {
                    day:  'Sábado',
                    oppened: false ,
                    opening: '',
                    closing: ''
                },
                {
                    day:  'Domingo',
                    oppened: false,
                    opening: '',
                    closing: ''
                }
            ];

        $scope.getUser = function()
        {
            $http.get('/api/users/' + $routeParams.id )
                .then(function(response)
                {
                    $scope.user = response.data;
                    $scope.originalemail = $scope.user.email;
                })
        }

        $scope.getSectors = function()
        {
            $scope.subsectores = {};

            // Obtener los sectores
            $http.get('/api/sectors/')
                .then(function(res)
                {
                    $scope.sectors = res.data;

                    for(var i=0; i<$scope.sectors.length; i++)
                    {
                        if($scope.user.sector == $scope.sectors[i]._id)
                        {
                            $scope.subsectores = $scope.sectors[i].subsectores;
                            break;
                        }
                    }

                });
        }

        $scope.getEstados = function()
        {
            // Obtener los estados
            $http.get('/api/estados/')
                .then(function(res)
                {
                    $scope.estados = res.data;
                    for (var i = 0; i < $scope.estados.length ; i++)
                    {
                        if($scope.user.estado == i)
                        {
                            $scope.municipios = $scope.estados[i].municipios;
                            break;
                        }
                    }
                });
        }

        $scope.isValidEmailAddress = function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }


        $scope.isValidRFC = function(rfcStr) {
            var strCorrecta;
            strCorrecta = rfcStr;
            if (rfcStr.length == 12){
                var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
            }else{
                var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
            }
            var validRfc=new RegExp(valid);
            var matchArray=strCorrecta.match(validRfc);
            if (matchArray==null) {

                return false;
            }
            else
            {
                return true;
            }

        }

        $scope.isValidUrl = function ( url ) {
            var exp = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
            var re = new RegExp( exp , 'i' );
            return re.test(url);
        }

        $scope.validateUser = function ( user ) {
            var errors = [];

            if ( user.telefono == null || user.telefono == '')
                errors.push("El teléfono es un campo requerido ")
            if (user.email == null || user.email == '')
                errors.push("El campo email es requerido ")
            if (user.role == null || user.role == '')
                errors.push("El campo role es requerido")
            else {
                if ( ! $scope.isValidEmailAddress( user.email ) )
                    errors.push("El formato del email está equivocado ");
            }
            if ( user.password == null || user.password == '')
                errors.push("Es necesario que ingreses una contraseña genérica")

            if ( user.nombre == null || user.nombre == '')
                errors.push("El campo nombre es requerido ")
            if ( $("#sector").val() == '' )
                errors.push("Es necesario que selecciones un sector ")
            if ( $("#subsector").val() == '' )
                errors.push("Es necesario que selecciones un subsector ")
            if ( $("#estado").val() == '' )
                errors.push("Es necesario que ingreses un estado")
            if ( $("#municipio").val() == '' )
                errors.push("Es necesario que ingreses un municipio")

            if ( user.rfc != null ) {
                if (!$scope.isValidRFC(user.rfc))
                    errors.push("El RFC que ingresaste es inválido");
            }
            if ( user.direccion != null ) {
                if( user.direccion.length > 255)
                    errors.push("El campo dirección solo puede tener 255 caracteres")
            }
            if( user.redes != undefined ) {
                if( user.redes.facebook != null && user.redes.facebook != '' ) {
                    if (!$scope.isValidUrl(user.redes.facebook))
                        errors.push("El campo facebook no contiene una url válida")
                }
                if( user.redes.twitter != null && user.redes.twitter != '' ) {
                    if (!$scope.isValidUrl(user.redes.twitter))
                        errors.push("El campo twitter no contiene una url válida")
                }
                if( user.redes.youtube != null && user.redes.youtube != '' ) {
                    if (!$scope.isValidUrl(user.redes.youtube))
                        errors.push("El campo youtube no contiene una url válida")
                }
                if( user.redes.linkedin != null && user.redes.linkedin != '' ) {
                    if (!$scope.isValidUrl(user.redes.linkedin))
                        errors.push("El campo linkedin no contiene una url válida")
                }
                if( user.redes.googleplus != null && user.redes.googleplus != '' ) {
                    if (!$scope.isValidUrl(user.redes.googleplus))
                        errors.push("El campo googleplus no contiene una url válida")
                }
            }
            if( user.web != null && user.web != '' ) {
                if (!$scope.isValidUrl(user.web))
                    errors.push("El campo web no contiene una url válida")
            }

            return errors;
        };
        // Esperar a que cargue la página
        angular.element(document).ready(function ()
        {


            // Obtenemos los datos del usuario en cuestión
            $scope.getSectors();
            $scope.getEstados();
        });

        // Función para actualizar los subsectores
        $("#sector").change(function()
        {
            if ($("#sector").val() == "")
                $scope.subsectores = {};
            else {
                // Iteramos en todos los sectores
                for (var i = 0; i < $scope.sectors.length; i++) {
                    // Detectamos cuando el sector seleccionado corresponda al que se va iterando
                    if ($("#sector").val() == $scope.sectors[i]._id) {
                        // Actualizamos los subsectores del sector seleccionado
                        $scope.subsectores = $scope.sectors[i].subsectores;
                        // Actualizamos el scope porque no se está utilizando función de angular
                        $scope.$apply();
                        break;
                    }
                }
            }
        });

        // Detectar el cambio de selección en un estado
        $("#estado").change(function()
        {
            // Recorremos los estados hasta encontrar el que pertenezca al que se selecciono en el input
            for (var i = 0; i < $scope.estados.length ; i++)
            {
                if($("#estado").val() == i)
                {
                    // Modificamos los municipios correspondientes
                    $scope.municipios = $scope.estados[i].municipios;
                    // Refrescamos el scope, porque no esta bindeado con angular
                    $scope.$apply();
                    break;
                }
            }
        });


        // Función de modificación  general
        $scope.create = function()
        {
            var errors = $scope.validateUser($scope.user);
            console.log($scope.user.shedules);
            $http.post('/api/users/check', {email: $scope.user.email})
                .then(function (res) {
                    if (res.status == 200)
                        errors.push("El email seleccionado ya le pertenece a otro usuario")

                    if( errors.length == 0 ) {

                        //Inyectar en el usuario los datos faltantes
                        $scope.user.sector          = $("#sector").val();
                        $scope.user.sector_name     = $("#sector option:selected").text();
                        $scope.user.categoria       = $("#subsector option:selected").val();
                        $scope.user.categoria_name  = $("#subsector option:selected").text();
                        $scope.user.estado          = $("#estado").val();
                        $scope.user.estado_name     = $scope.estados[$('#estado').val()].name;
                        $scope.user.municipio       = $("#municipio").val();
                        $scope.user.municipio_name  = $scope.municipio_name;

                        if( $scope.user.tags != undefined)
                            $scope.user.tags        = $scope.user.tags.toString().split(",");
                        if ( $scope.user.products != undefined)
                            $scope.user.products   = $scope.user.products.toString().split(",");

                        $http.post('/api/users' ,$scope.user)
                            .then(function(response) {
                                console.log($scope.user);
                                swal("Exito", "Usuario agregado exitosamente",'success');
                                $scope.user = {};
                                $scope.sectors = {};
                                $scope.subSectors = {};
                                $scope.estados = {};

                                $scope.getSectors();
                                $scope.getEstados();
                            });


                    }
                    else
                        swal("Error",errors.join("\n\n"),"error");

                })
                .catch(function (err) {
                    console.log(err);
                });


        }

        $scope.uploadProfilePic = function (file)
        {
            $scope.uploading = true;

            if (file == null || file.size > 10000 * 1000 * 1000)
            {
                swal("Error al subir el logo" ,"Revisa el peso de tu imagen, debe ser menor a 10mb" , "error");
                // $scope.uploading = false;
                return;
            }
            else
            {
                Upload.upload({
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    url: '/api/upload/profilePic/' 	+ $scope.user._id + '/'
                    + encodeURIComponent($scope.user.sector_name) + '/'
                    + encodeURIComponent($scope.user.categoria_name) + '/'
                    + encodeURIComponent($scope.user.nombre),
                    file: file
                }).then(function (resp)
                {
                    //upload function returns a promise
                    console.log(resp);

                    if (resp.data.errorCode === 0) {
                        swal("Exito", "Usuario agregado exitosamente");
                        $scope.user = {};
                        $scope.sectors = {};
                        $scope.subSectors = {};
                        $scope.estados = {};

                        $scope.getSectors();
                        $scope.getEstados();
                    }
                    else
                        swal("Error", "Ocurrió un error resconocido, por favor intenta de nuevo más tarde", "error");

                    $scope.uploading = false;

                }, function (resp)
                {
                    //catch error
                    console.log('Error status: ' + resp.status);
                    swal("Error","Revisa el peso de tu imagen, debe ser menor a 10mb","error");

                }, function (evt)
                {
                    // console.log(evt);
                    // console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total));
                    // var progress = parseInt(100.0 * evt.loaded / evt.total);
                    // waitingDialog.update(progress);
                    // $scope.uploading = false;
                });
                //
            };
        };



    });

})();
