'use strict';

describe('Component: CreateUserComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CreateUserComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    CreateUserComponent = $componentController('CreateUserComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
