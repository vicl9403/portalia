'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/createUser', {
        template: '<create-user></create-user>'
      });
  });
