'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/setSuggestedTag', {
        template: '<set-suggested-tag></set-suggested-tag>'
      });
  });
