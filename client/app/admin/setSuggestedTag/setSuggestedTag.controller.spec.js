'use strict';

describe('Component: SetSuggestedTagComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SetSuggestedTagComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    SetSuggestedTagComponent = $componentController('SetSuggestedTagComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
