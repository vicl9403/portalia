'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    var SetSuggestedTagComponent = function SetSuggestedTagComponent() {
        _classCallCheck(this, SetSuggestedTagComponent);
        this.message = 'Hello';
    };

    angular.module('portaliaApp').component('setSuggestedTag', {
        templateUrl: 'app/admin/setSuggestedTag/setSuggestedTag.html',
        controller: SetSuggestedTagComponent
    }).controller('SetSuggestedTagsCtr', function ($scope, $http, $window, Auth) {

        // Verificar que tenga permiso de estar aquí
        Auth.getCurrentUser(function (currentUser) {
            $scope.userData = currentUser; // Your code
            if ($scope.userData.role != 'admin' ) $window.location = '/';
        });

        $scope.sectors = {};
        $scope.tags = [];

        $scope.getSectors = function () {
            $scope.subsectores = {};

            // Obtener los sectores
            $http.get('/api/sectors/').then(function (res) {
                $scope.sectors = res.data;

            });
        };

        // Esperar a que cargue la página
        angular.element(document).ready(function () {
            $scope.getSectors();
        });

        $scope.getTags = function ()
        {
            var filter = { 'category_name' : $("#categoria").val() }

            $http.post('/api/suggestedTags/find', filter)
                .then(function (res)
                {
                    $scope.tags  = res.data.tags;
                }, function (resp)
                {
                    $scope.tags = [];
                });

        }

        // Función para actualizar los subsectores
        $("#sector").change(function ()
        {
            if($("#sector").val() == '')
            {
                $scope.subsectores = [];
                $scope.$apply();
            }
            // Iteramos en todos los sectores
            for (var i = 0; i < $scope.sectors.length; i++) {
                // Detectamos cuando el sector seleccionado corresponda al que se va iterando
                if ($("#sector").val() == $scope.sectors[i]._id) {
                    // Actualizamos los subsectores del sector seleccionado
                    $scope.subsectores = $scope.sectors[i].subsectores;
                    // Actualizamos el scope porque no se está utilizando función de angular
                    $scope.$apply();
                    break;
                }
            }

            $scope.getTags();
        });

        $("#categoria").change(function ()
        {
            $scope.getTags();
        });



        $scope.saveTags = function ()
        {

            if($scope.tags.toString().length <= 0 || $scope.subsectores.length == 0)
                swal("Error","Tienes que escribir al menos una palabra clave y tener una categoria, para poder guardar","error");
            else
            {
                var filter = {'category_name': $("#categoria").val()}
                $http.post('/api/suggestedTags/find', filter)
                    .then(function (res) {
                        var data = {'tags': $scope.tags.toString().split(',')}

                        $http.patch('/api/suggestedTags/' + res.data._id, data)
                            .then(function (res) {
                                swal('Éxito', "Modificación realizada correctamente", "success");
                                $scope.getTags();
                            });

                    })
                    .catch(function (err) {
                        var data = {
                            'category_name': $("#categoria").val(),
                            'tags': $scope.tags.toString().split(',')
                        }
                        $http.post('/api/suggestedTags/', data)
                            .then(function (res) {
                                $scope.data = res.data;
                            })
                    });
            }
            
        }



    });


})();
//# sourceMappingURL=setSuggestedTag.controller.js.map
