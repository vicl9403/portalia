'use strict';

(function(){

class SeguimientoProspectosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('seguimientoProspectos', {
    templateUrl: 'app/admin/seguimiento-prospectos/seguimiento-prospectos.html',
    controller: SeguimientoProspectosComponent,
    controllerAs: 'seguimientoProspectosCtrl'
  }).controller('SeguimientoProspectosController', function ($scope, $http, Auth, $timeout, $window, $location, $rootScope) {

    $scope.getWatchedQuotes = function () {
      $http.get('/api/quotes/watched')
        .then( res =>{
          $scope.quotes = res.data;
        })
    }

    $scope.init = function () {
      $scope.getWatchedQuotes();
    }

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY');
    }

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      if (user.role != 'admin')
        $window.location = '/';

      $scope.user = user;

      $scope.init();

    });

});

})();
