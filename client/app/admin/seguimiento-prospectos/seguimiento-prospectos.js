'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/seguimiento-prospectos', {
        template: '<seguimiento-prospectos></seguimiento-prospectos>'
      });
  });
