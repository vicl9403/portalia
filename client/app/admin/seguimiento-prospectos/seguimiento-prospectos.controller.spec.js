'use strict';

describe('Component: SeguimientoProspectosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SeguimientoProspectosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    SeguimientoProspectosComponent = $componentController('seguimiento-prospectos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
