'use strict';

describe('Component: PanelMailingComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var PanelMailingComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    PanelMailingComponent = $componentController('panelMailing', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
