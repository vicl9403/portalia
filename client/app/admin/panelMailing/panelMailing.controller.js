'use strict';

(function(){

angular.module('portaliaApp')
  .component('panelMailing', {
    templateUrl: 'app/admin/panelMailing/panelMailing.html'
  }).controller('panelMailingCrl', function($scope, $http, $window, Auth, $location) {
    Auth.getCurrentUser(function(res){
      if(res.role != 'admin'){
        $location.path('/')
      }
    });
    $scope.users = { };
		$http.get('/api/users/usuariosDestacados' )
			.then(function (res) {
				$scope.users = res.data;
			})


    $http.get("/api/sectors")
      .then(function(response) {
        console.log(response);
        $scope.sectors = response.data;
      })
      .catch(function (err) {
        console.log(err);
      })

      $http.get("/api/estados")
        .then(function(response) {
          $scope.estados = response.data;
          console.log($scope.estados);
        })

    $scope.query = {}
    $scope.datos = {}
    $scope.mailSend = {}

    $scope.compQuery = function () {
      for (var arraydata in $scope.datos) {
        if ($scope.datos.hasOwnProperty(arraydata)) {
          if ($scope.datos[arraydata] == "" || $scope.datos[arraydata] == undefined) {
            delete $scope.datos[arraydata];
          }
        }
      }
      console.log($scope.datos);
      $http.post("/api/users/queryMailing", $scope.datos)
      .then(function(response) {
        $scope.users = response.data;
        console.log(response);
        $('#mailingQuery').modal('hide');
      })
      .catch(function (err) {
        console.log(err);
      })
    }

    //Funcion de Enviar Mailing
    $scope.enviarMailing = function () {
      $scope.mailSend.email = [];
      console.log("Entro");
        for (var arraydata in $scope.users) {
          var y = [];
          var x = {
            nombre: $scope.users[arraydata].nombre,
            mail: $scope.users[arraydata].email,
            categoria_name: $scope.users[arraydata].categoria_name,
            sector_name: $scope.users[arraydata].sector_name
          }
          $scope.mailSend.email.push(x);
     }
     $scope.mailSend.email.pop();
      console.log($scope.mailSend);
      $http.post("/api/mailings", $scope.mailSend)
      .then(function(response) {
        console.log(response);
        $location.path('/admin/redaccionMail').search({param: response.data._id});
      })
      .catch(function (err) {
        console.log(err);
      })
    }

    $scope.mailingSingle = function(user) {
      $scope.mailSend.email = [];
      $scope.mailSend.bloque = 'Individual';
      var x = {
        nombre: user.nombre,
        mail: user.email,
        categoria_name: user.categoria_name,
        sector_name: user.sector_name
      }
      $scope.mailSend.email.push(x);
      $http.post("/api/mailings", $scope.mailSend)
      .then(function(response) {
        console.log(response);
        $location.path('/admin/redaccionMail').search({param: response.data._id});
      })
      .catch(function (err) {
        console.log(err);
      });
    }

    $scope.datosModal = function () {
      $("#mailingQuery").modal("toggle");
      var radioBtn1 = document.getElementById("1");
      var radioBtn2 = document.getElementById("2");
      radioBtn1.checked = true;
      radioBtn1.value = "and";
      radioBtn2.checked = false;
      $scope.datos.tipoQuery = "and";
    }

    $scope.changeEstado = function(estado) {
      if ($scope.query.estado != "" && $scope.query.estado != undefined) {
        $scope.datos.estado = $scope.estados[estado].name;
        $('#municipio')
        .find('option')
        .remove()
        .end()
        .append('<option value="">Selecciona un municipio</option>')
        .val('{{$index}}');

        $scope.estados[estado].municipios.sort(function(a, b) {
          var textA = a.name.toUpperCase();
          var textB = b.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        for (var x in $scope.estados[estado].municipios) {
          //        $("#municipio").append(new Option($scope.estados[estado].municipios[x].name, $scope.estados[estado].municipios[x]._id));
          $("#municipio").append(new Option($scope.estados[estado].municipios[x].name, x));
        }
      }
      else {
        $scope.datos.estado = null;
        $scope.datos.municipio = null;
      }
    }

    $scope.selectMunicipio = function(estado,municipio){
      if ($scope.query.municipio != "" && $scope.query.municipio != undefined) {
        $scope.datos.municipio = $scope.estados[estado].municipios[municipio].name;
        console.log("Entro al IF");
      }
      else {
        $scope.datos.municipio = null;
      }
    }

    $scope.changeSector = function(sector) {
      if ($scope.query.sector != "" && $scope.query.sector != undefined) {
        $scope.datos.sector = $scope.sectors[sector].name;
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un sub sector</option>')
          .val('');

        for (var x = 0; x < $scope.sectors[sector].subsectores.length; x++) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, x));
        }
      }
      else {
        $scope.datos.sector = null;
        $scope.datos.categoria = null;
      }
    }

    $scope.changeCategoria = function(sector, categoria) {
      if ($scope.query.categoria != "" && $scope.query.categoria != undefined) {
        $scope.datos.categoria = $scope.sectors[sector].subsectores[categoria].name;
      }
      else {
        $scope.datos.categoria = null;
      }
    }

    $scope.restartData = function () {
      delete $scope.query;
      delete $scope.datos;
      $scope.query = {}
      $('#categoria')
        .find('option')
        .remove()
        .end()
        .append('<option value="">Selecciona un sub sector</option>')
        .val('');

        $('#municipio')
        .find('option')
        .remove()
        .end()
        .append('<option value="">Selecciona un municipio</option>')
        .val('');
      $scope.datos = {}
      $http.get('/api/users').then(function(response){
        $scope.users = response.data;
        console.log($scope.users);
      })
      var radioBtn1 = document.getElementById("1");
      var radioBtn2 = document.getElementById("2");
      radioBtn1.checked = true;
      radioBtn1.value = "and";
      radioBtn2.checked = false;
      $scope.datos.tipoQuery = "and";
    }


  });

})();
