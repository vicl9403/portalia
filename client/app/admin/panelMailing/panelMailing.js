'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/panelMailing', {
        template: '<panel-mailing></panel-mailing>'
      });
  });
