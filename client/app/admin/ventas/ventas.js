'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/ventas', {
        template: '<ventas></ventas>'
      });
  });
