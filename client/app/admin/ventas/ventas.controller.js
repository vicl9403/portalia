'use strict';
(function(){

class VentasComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('ventas', {
    templateUrl: 'app/admin/ventas/ventas.html',
    controller: VentasComponent
  });

})();
