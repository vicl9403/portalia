'use strict';

describe('Component: RegistroDeAccesosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RegistroDeAccesosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    RegistroDeAccesosComponent = $componentController('registro-de-accesos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
