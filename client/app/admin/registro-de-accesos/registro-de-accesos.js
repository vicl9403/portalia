'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/registro-de-accesos', {
        template: '<registro-de-accesos></registro-de-accesos>'
      });
  });
