'use strict';

(function(){

class RegistroDeAccesosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('registroDeAccesos', {
    templateUrl: 'app/admin/registro-de-accesos/registro-de-accesos.html',
    controller: RegistroDeAccesosComponent,
    controllerAs: 'registroDeAccesosCtrl'
  }).controller('RegistroDeAccesosController', function($scope, $http, $window, Auth, $location, $routeParams) {


    $scope.filter = function () {
      let since =  moment( $("#since").val() ).format('YYYY-MM-DD');
      let until = moment( $("#until").val() ).format('YYYY-MM-DD');

      $scope.isLoading = true;

      $http.get('/api/logins/' + since + "/" + until )
        .then( res => {
          $scope.registers = res.data;
          $scope.isLoading = false;
        })
    };


    $scope.jsonToCsv = function (objArray) {
      let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
      let str = '';

      for (let i = 0; i < array.length; i++) {
        let line = '';
        for (let index in array[i]) {
          if (line != '') line += ','

          line += array[i][index];
        }

        str += line + '\r\n';
      }

      return str;
    }

    $scope.downloadCsv = function ( csvString ) {
      var a         = document.createElement('a');
      a.href        = 'data:attachment/csv,' +  encodeURIComponent(csvString);
      a.target      = '_blank';
      a.download    = 'reporte-accesos.csv';

      document.body.appendChild(a);
      a.click();
    }

    $scope.exportToCsv = function () {

      $scope.isLoading = true;

      let registersParsed = [];

      for( let i = 0; i < $scope.registers.length ; i ++ ) {

        if( $scope.registers[i].user != null ) {

          if( $scope.registers[i].user.role == 'empresa') {
            registersParsed.push({
              nombre: $scope.registers[i].user.nombre,
              email: $scope.registers[i].user.email,
              telefono: $scope.registers[i].user.telefono,
              fecha: $scope.getDateForHummans($scope.registers[i].startAt),
              id: $scope.registers[i].user._id
            })
          }
        }
      }

      let csvResult = $scope.jsonToCsv( registersParsed );

      $scope.downloadCsv( csvResult );

      $scope.isLoading = false;
    }

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY HH:MM');
    };

    // Verificar que tenga permiso de estar aquí
    Auth.getCurrentUser(function(user) {

      if (user.role != 'admin')
        $window.location = '/';

      $scope.since = null;

      $scope.user = user;

      $scope.isLoading = false;

      $("#since").datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD',
      });
      $("#until").datetimepicker({
        locale: 'es',
        format: 'YYYY-MM-DD',
      });

    });


  });

})();
