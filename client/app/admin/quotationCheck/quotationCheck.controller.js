'use strict';
(function() {

  class QuotationCheckComponent {
    constructor() {
      this.message = 'Hello';
    }
  }

  angular.module('portaliaApp')
    .component('quotationCheck', {
      templateUrl: 'app/admin/quotationCheck/quotationCheck.html'
    }).controller('quotationCheckCrl', function($scope, $http, $window, Auth, $location , $timeout) {

    //Las Apis de este controlador hacen llamadas a
    //las rutas de la carpeta quotation del servidor
    //pueden consultarse las rutas en el archivo index.js

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      if( user.role != 'admin' )
        $window.location.url = '/';

      $scope.user = user;
      $scope.activeQuote = null;

      $http.get('/api/quotes/newQuotes').then(function (res) {
        console.log( res.data );
        $scope.quotes = res.data;
      });


    });

    // Proceso para mostrar una cotización
    $scope.showQuote = function ( id ) {
      $scope.activeQuote  = id;
      $scope.$broadcast ('getQuoteData', id );

      $timeout(function() {
        $("html, body").animate({
          scrollTop: $("#activeQuote").offset().top
        }, "slow");
      }, 200);
    }

    // Proceso para eliminar una cotización
    $scope.deleteQuote = function (id) {

      swal({
          title: "Estas seguro?",
          text: "Si se elimina la cotización no volverá a aparecer en este listado",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, eliminar",
          cancelButtonText: "No, cancelar",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            $http.delete('/api/quotes/' + id)
              .then( (res) => {
                // Obtener nuevamente las cotizaciones
                $http.get('/api/quotes/newQuotes')
                  .then( (res) => {
                    $scope.quotes = res.data;
                    swal("Eliminada!", "Se eliminó correctamente la cotización", "success");
                  })
              })
              .catch(function(err){
                console.log(err);
              });
          }
          if ($scope.toUpdate.active == false) {
            $scope.viewPendientes(1);
          } else {
            $scope.view('rejected', 1)
          }
        });
      }

      $scope.changeStatusQuote = function(quoteId, new_status, active) {
        $scope.toUpdate = {};
        $scope.toUpdate.id = quoteId;
        $scope.toUpdate.status = new_status;
        $scope.toUpdate.active = active;
        $http.post('/api/quotes/changeQuoteStatus', $scope.toUpdate).then(function(response) {
          console.log(response);
          swal('Éxito', 'Esta cotización ha pasado la sección correspondiente', 'success');
          if ($scope.toUpdate.active == false) {
            $scope.viewPendientesCot(1);
          } else {
            swal("Cancelado", "La cotización no se eliminó", "error");
          }
        })
        .catch(function(err){
          console.log(err);
        });
    }

    // Proceso para aprobar una cotización
    $scope.approveQuote = function (quote) {

      console.log( quote );
      swal({
          title: "Estas seguro?",
          text: "Si la cotización se autoriza, se procesará para encontrar usuarios y se mandará por correo electrónico",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#F08F04",
          confirmButtonText: "Si, es una cotización valiosa",
          cancelButtonText: "No, cancelar",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {

            // Procesar el cambio de status de la cotización
            quote.status = 'approved';
            $http.patch( '/api/quotes/' + quote._id , quote )
              .then( (res) => {
                // Obtener nuevamente las cotizaciones
                $http.get('/api/quotes/newQuotes')
                  .then( (res) => {
                    $scope.quotes = res.data;
                    swal( "Éxito" , 'La cotización cambió a su status aprobada' , 'success' );
                  })
              })

          } else {
            swal("Cancelado", "No se hicieron cambios sobre la cotización", "error");
          }
        });

    }


    });
})();
