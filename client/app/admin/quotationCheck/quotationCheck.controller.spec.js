'use strict';

describe('Component: QuotationCheckComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var QuotationCheckComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    QuotationCheckComponent = $componentController('QuotationCheckComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
