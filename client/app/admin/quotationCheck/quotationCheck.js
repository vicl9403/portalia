'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/quotationCheck', {
        template: '<quotation-check></quotation-check>'
      });
  });
