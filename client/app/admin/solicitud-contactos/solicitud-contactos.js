'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/solicitud-contactos', {
        template: '<solicitud-contactos></solicitud-contactos>'
      });
  });
