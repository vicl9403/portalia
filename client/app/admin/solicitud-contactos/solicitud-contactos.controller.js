'use strict';
(function(){

class SolicitudContactosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('solicitudContactos', {
    templateUrl: 'app/admin/solicitud-contactos/solicitud-contactos.html',
    controller: SolicitudContactosComponent
  }).controller('ContactRequestController', function ($scope, $http, $window, Auth) {

    $scope.getContacts = function () {
      $http.get('/api/contacts/not-authorized')
        .then(function (res) {
          $scope.contacts = res.data;
          console.log( res.data );
        });
    };

    // Verificar que tenga permiso de estar aquí
    Auth.getCurrentUser(function (currentUser) {

      $scope.user = currentUser;
      if ($scope.user.role != 'admin')
        $window.location = '/';

      // Obtener las solicitudes actuales
      $scope.getContacts();

      // Variable de control para la visualización del detalle
      $scope.contactDetail = 'test' ;

      $scope.loading = false;

    });

    $scope.showDetail= function ( contact ) {
      $scope.contactDetail = contact ;
    }

    $scope.getDateForHummans = function ( date ) {
      moment.locale('es');
      return moment(date).format('D MMMM YYYY');
    }

    $scope.aproveContact = function ( contact ) {

      // Setear la autorización
      contact.authorized = true;

      $scope.loading = true;

      // Mandar el cambio
      $http.patch('/api/contacts/' + contact._id , contact )
        .then( function (res) {

          console.log( contact );
          $http.post('/api/contacts/sendContactMail', { data: contact } )
            .then( function () {
              $scope.getContacts();
              $scope.loading = false;
              swal( "Éxito", 'La solicitud de contacto se aprobó correctamente' , 'success');
            });
        });

    }

    $scope.rejectContact = function ( contact ) {

      swal({
          title: "Estas seguro?",
          text: "No podrás recuperar esta solicitud de contacto si la eliminas",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#F08F04",
          confirmButtonText: "Eliminar!",
          cancelButtonText: "Cancelar!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {

            $http.delete('/api/contacts/' + contact._id )
              .then( function (res) {
                $scope.getContacts();
                swal( "Éxito" , "La solicitud de contacto se eliminó correctamente" , 'success');
              });

          } else {
            swal("Cancelado", "La solicitud de contacto no se ha eliminado", "error");
          }
        });

    }

  });

})();
