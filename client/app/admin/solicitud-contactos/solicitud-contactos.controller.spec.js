'use strict';

describe('Component: SolicitudContactosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var SolicitudContactosComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    SolicitudContactosComponent = $componentController('SolicitudContactosComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
