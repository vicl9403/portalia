'use strict';

describe('Component: CodigosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CodigosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    CodigosComponent = $componentController('codigos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
