'use strict';

(function() {

  angular.module('portaliaApp')
    .component('codigos', {
      templateUrl: 'app/admin/codigos/codigos.html'
    }).controller('codigosCtrl', function($scope, $http, $window, Auth, $location) {
      Auth.getCurrentUser(function(currentUser) {
        $scope.userData = currentUser // Your code
        if ($scope.userData.role != 'admin') {
          $window.location = '/';
        }
      });

      $scope.datos = {};
      $("#usos").blur(function() {
        if ($("#usos").val() == '1') {
          $("#correo").prop("disabled", false);
        } else {
          $("#correo").prop("disabled", true);
        }
      });

      $scope.generateCode = function() {
        console.log($scope.code);
        if ($scope.code.$valid == false) {
          alert("Para generar el código asegúrate de rellenar los campos necesarios")
        } else {
          var text = "";
          var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

          for (var i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
          }

          $("#codigoBox").val(text);
          $("#nightmareCustom").prop("disabled", false);
          $scope.datos.codigo = text;
          $scope.datos.usosRestantes = $scope.datos.usos
          console.log($("#numeroOf").val());
          console.log($("#unidadOf").val());
          // $scope.datos.unidadOfrecidad
          // console.log($scope.datos);
          $http.post('/api/codigos/', $scope.datos)
            .then(function(response) {
              $scope.datosCustom = response.data;
              alert("Código generado con éxito");
              // console.log($scope.datosCustom);
            })
            .catch(function(err) {
              console.log(err);
            });
        }
      }

      $scope.custom = function() {
        if ($('#codigoBox').is(':disabled') == true) {
          $('#codigoBox').prop('disabled', false);
          document.getElementById('nightmareCustom').innerHTML = "GUARDAR"
          document.getElementById('codigoBox').focus();
        } else {
          $('#codigoBox').prop('disabled', true);
          document.getElementById('nightmareCustom').innerHTML = "PERSONALIZAR"
          $scope.datosCustom.codigo = $("#codigoBox").val();
          console.log($scope.datosCustom);
          $http.patch('/api/codigos/' + $scope.datosCustom._id, $scope.datosCustom)
            .then(function(response) {
              console.log(response);
              alert("Cambio aplicado");
            })
            .catch(function(err) {
              console.log(err);
            });
        }
      }

    });


})();
