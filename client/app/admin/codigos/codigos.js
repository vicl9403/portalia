'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/codigos', {
        template: '<codigos></codigos>'
      });
  });
