'use strict';
(function(){

class UpdateLogsComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('updateLogs', {
    templateUrl: 'app/admin/updateLogs/updateLogs.html',
    controller: UpdateLogsComponent
  }).controller('LogsCtr', function ($scope, $http, $window, Auth) 
  {	
  		Auth.getCurrentUser(function(currentUser) 
  		{
  			$scope.userData = currentUser // Your code
  			if ($scope.userData.role != 'admin') 
  				$window.location = '/';
  		});

  		// Variables para los filtros
  		$scope.start = '';
  		$scope.end = '';

  		// Obtener la fecha actual
  		var today = new Date();

  		// Obtener la fecha de un dia anterior
  		var yesterday = new Date(today);
  		yesterday.setDate(today.getDate() - 1);

  		var days = {
  			start 	: yesterday.toISOString(),
  			end		: today.toISOString(),
  		};

		$http.post('/api/updatesOnUsers/date', days )
			.then(function (res) {
				$scope.logs = res.data;
				console.log(res.data);
			})

		// Inicializar los date pickers
		$(document).ready(function() {
			jQuery('#datePickerStart').datetimepicker({
				locale: 'es',
				format: 'YYYY-MM-DDTHH:mm:SS',
			});
				jQuery('#datePickerEnd').datetimepicker({
				locale: 'es',
				format: 'YYYY-MM-DDTHH:mm:SS',
			});
		});


		$scope.updateLogs = function()
		{
			// Verificar que los filtros estén con algún dato al menos
			if( $("#datePickerEnd").val() != '')
			{
				// Generamos los formatos iso string para poder hacer la consulta
				var start = new Date($("#datePickerStart").val());
				var end   = new Date($("#datePickerEnd").val());

				// Generamos el JSON para la consulta
		  		var days = {
		  			start 	: start.toISOString(),
		  			end		: end.toISOString()
		  		};

				$http.post('/api/updatesOnUsers/date', days )
					.then(function (res) {
						$scope.logs = res.data;
					})
			}
		}
  });

})();
