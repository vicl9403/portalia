'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/updateLogs', {
        template: '<update-logs></update-logs>'
      });
  });
