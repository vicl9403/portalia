'use strict';

describe('Component: UpdateLogsComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var UpdateLogsComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    UpdateLogsComponent = $componentController('UpdateLogsComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
