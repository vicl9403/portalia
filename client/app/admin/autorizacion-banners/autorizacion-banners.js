'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/autorizacion-banners', {
        template: '<autorizacion-banners></autorizacion-banners>'
      });
  });
