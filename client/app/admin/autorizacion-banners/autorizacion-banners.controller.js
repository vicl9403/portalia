'use strict';



(function(){

class AutorizacionBannersComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('autorizacionBanners', {
    templateUrl: 'app/admin/autorizacion-banners/autorizacion-banners.html',
    controller: AutorizacionBannersComponent,
    controllerAs: 'autorizacionBannersCtrl'
  }).controller('AutorizacionBannersController', function ($scope, $http, Auth, $timeout, $window, $location, $rootScope) {

    var DropzoneImg1 = new Dropzone("#img1",
      {
        acceptedFiles: 'image/*',
        url: "/api/upload/image",
        autoProcessQueue: true,
        resizeWidth : 300,
        resizeHeight: 300,
        maxFiles : 1,
        init: function() {
          this.on("sending", function(file, xhr, formData){
            formData.append("path", "/banners/" + $scope.user._id + '/' );
          });
          this.on("success", function(file, responseText) {

            this.removeAllFiles();
            $scope.activeBanner.img1 = responseText;

            $http.patch('/api/banners/' + $scope.activeBanner._id , $scope.activeBanner )
              .then( res =>{
                swal("Éxito","La imágen del banner se actualizó correctamente" , 'success');
              })
          });
        }
      });

    var DropzoneImg2 = new Dropzone("#img2",
      {
        acceptedFiles: 'image/*',
        url: "/api/upload/image",
        autoProcessQueue: true,
        resizeWidth : 400,
        resizeHeight: 150,
        maxFiles : 1,
        init: function() {
          this.on("sending", function(file, xhr, formData){
            formData.append("path", "/banners/" + $scope.user._id + '/' );
          });
          this.on("success", function(file, responseText) {

            this.removeAllFiles();
            $scope.activeBanner.img2 = responseText;

            $http.patch('/api/banners/' + $scope.activeBanner._id , $scope.activeBanner )
              .then( res =>{
                swal("Éxito","La imágen del banner se actualizó correctamente" , 'success');
              })
          });
        }
      });


    $scope.getBannersByStatus = function( status , page ) {

      $http.get(`/api/banners/status/${ status }?page=${ page }`)
        .then( res=> {

          $scope.banners  = res.data.items;
          $scope.totalBanners = res.data.count;

          $scope.activeBanner = null;
        })
    }

    $scope.init = function () {

      $scope.currentPage = 0;

      $scope.getBannersByStatus( 'pending' , 0 );

    };


    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY');
    }


    $("#status").change(function () {
      $scope.getBannersByStatus( this.value , 0 );
    });

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      if (user.role != 'admin')
        $window.location = '/';

      $scope.user = user;

      $scope.pendingBanners = null;
      $scope.approvedBanners = null;

      $scope.activeBanner = null;
      $scope.actualStatus = 'pending';

      $scope.init();


    });

    $scope.viewBanner = function (banner) {
      $scope.activeBanner = banner;
    }

    $scope.operationWithBannerTime = function ( qty , operation , type ) {
      let banner = $scope.activeBanner;

      if( type == '+')
        banner.until = moment(banner.until).add(1, operation );
      else
        banner.until = moment(banner.until).subtract(1, operation );

      $http.patch('/api/banners/' + banner._id , banner )
        .then( res => {
          $scope.activeBanner = res.data;
        })
    }

    $scope.addBanner = function () {
      $window.location = '/perfil-usuario/banners/solicitud-banner'
    }

    $scope.activateBanner = function (banner) {

      banner.isActive = true;

      $http.patch('/api/banners/' + banner._id , banner )
        .then( res => {
          $scope.getBannersByStatus( 'approved' , 0 );
        })
    }

    $scope.approveBanner = function (banner) {

      swal({
          title: "¿Estas seguro?",
          text: "El banner se pasará al bloque de los aprobados",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, aprobar",
          closeOnConfirm: false
        },
        function(){

          banner.status = 'approved';

          $http.patch('/api/banners/' + banner._id , banner )
            .then( res => {
              $scope.getBannersByStatus( 'pending' , 0 );
              swal('Éxito','El banner se aprobó correctamente','success');
            })

        });


    }

    $scope.desactivateBanner = function (banner) {
      banner.isActive = false;

      $http.patch('/api/banners/' + banner._id , banner )
        .then( res => {
          $scope.getBannersByStatus( 'approved' , 0 );
        })
    }

    $scope.deleteTag = function (index) {

      swal({
          title: "¿Estas seguro?",
          text: "Si eliminas esa palabra no la podrás recuperar",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, eliminar",
          closeOnConfirm: false
        },
        function(){

          let banner = $scope.activeBanner;

          if( banner.tags.length > 3 ) {
            banner.tags.splice(index, 1);

            $http.patch('/api/banners/' + banner._id, banner)
              .then(res => {
                $scope.activeBanner = res.data;
                swal("Éxito",'Palabra clave eliminada correctamente', 'success');
              })
          }
          else {
            swal("Advertencia",'No se pueden eliminar todas las palabras clave, tienen que estar al menos 3.' , 'warning' );
          }

        });


    }

  $scope.saveChanges = function () {
    $http.patch('/api/banners/' + $scope.activeBanner._id , $scope.activeBanner )
      .then( res => {
        $scope.activeBanner = res.data;
        swal('Éxito','Cambios guardados correctamente.','success');
      })
  }

});

})();
