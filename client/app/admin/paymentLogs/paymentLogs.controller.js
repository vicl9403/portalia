'use strict';

(function() {

  angular.module('portaliaApp')
    .component('paymentLogs', {
      templateUrl: 'app/admin/paymentLogs/paymentLogs.html'
    }).controller('logsCtrl', function($scope, $http, $window, Auth) {
      //Traer el usuario actualmente logueado
      Auth.getCurrentUser(function(res) {
        $scope.filtering = false; //Variable filtering
        if (res.role == 'admin') { //Si es admin proceder con el código
          //Objeto necesario para hacer el request de datos, pagina y itemsPerPage
          $scope.requestData = {
            itemsPerPage: 10,
            requestedPage: 1
          };

          //Request inicial para la primera página de pagos realizados
          $http.post('/api/paymentLogs/getAll', $scope.requestData)
            .then(function(res) {
              console.log(res);
              $scope.paymentLogs = res.data.documents;
              $scope.totalPages = res.data.totalPages;
              $scope.totalResults = res.data.totalResults;
              $scope.currentPage = res.data.currentPage;
              $scope.updatePagination();
            })
            .catch(function(err) {
              console.log(err);
            });

          //Traer estados para ng-repeat de los filtros
          $http.get("api/estados")
            .then(function(response) {
              $scope.estados = response.data;
            });

          //Traer sectores para ng-repeat de los filtros
          $http.get("api/sectors")
            .then(function(response) {
              $scope.sectors = response.data;
            });
        } else { //Si no es admin expulsarlo
          $window.location.href = '/';
        }
      });

      //Función de filtrado-----------------------
      $scope.filter = function() {
        $scope.toFilter = {};
        $scope.toFilter.itemsPerPage = 10;
        $scope.toFilter.requestedPage = 1;
        if ($scope.filterData.sector != undefined) {
          $scope.toFilter.sector_name = $scope.sectors[$scope.filterData.sector].name;
        }
        if ($scope.filterData.categoria != undefined) {
          $scope.toFilter.categoria_name = $scope.filterData.categoria;
        }
        if ($scope.filterData.estado != undefined) {
          $scope.toFilter.estado_name = $scope.estados[$scope.filterData.estado].name;
        }

        $http.post('/api/paymentLogs/filter', $scope.toFilter)
          .then(function(res) {
            console.log(res);
            $scope.paymentLogs = res.data.documents;
            $scope.totalPages = res.data.totalPages;
            $scope.totalResults = res.data.totalResults;
            $scope.currentPage = res.data.currentPage;
            $scope.updatePagination();
            $scope.filtering = true;
          })
          .catch(function(err) {
            console.log(err);
          });
      }

      //Limpiar la búsqueda y trae todos de nuevo
      $scope.cleanFilter = function() {
        $scope.requestData = {
          itemsPerPage: 10,
          requestedPage: 1
        };

        $http.post('/api/paymentLogs/getAll', $scope.requestData)
          .then(function(res) {
            console.log(res);
            $scope.filtering = false;
            $scope.filterData = {};
            $scope.paymentLogs = res.data.documents;
            $scope.totalPages = res.data.totalPages;
            $scope.totalResults = res.data.totalResults;
            $scope.currentPage = res.data.currentPage;
            $scope.updatePagination();
          })
          .catch(function(err) {
            console.log(err);
          });
      }

      $scope.requestCSV = function() {
        $('#modal-loading').modal('toggle');
        if ($scope.filtering == false) {
          $scope.csvParams = {
            todas: true
          };
          $http.post('/api/paymentLogs/requestCSV', $scope.csvParams)
            .then(function(res) {
              swal('Éxito', 'El CSV se descargará en un momento', 'success');
              window.location = '/assets/uploads/compras.csv';
              console.log(res);
            });
        } else {
          $http.post('/api/paymentLogs/requestCSV', $scope.toFilter)
            .then(function(res) {
              swal('Éxito', 'El CSV se descargará en un momento', 'success');
              window.location = '/assets/uploads/compras.csv';
              console.log(res);
            });
        }
      }

      //Poner en su lugar correspondiente las categorías al seleccionar un sector
      $scope.changeSector = function(sector) {
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona una categoría</option>')
          .val('');

        for (var x = 0; x < $scope.sectors[sector].subsectores.length; x++) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, $scope.sectors[sector].subsectores[x].name));
        }
      };

      //Funciones necesarias para la paginación----------------------------------------------------------------------
      $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
          input.push(i);
        }
        return input;
      };

      $scope.updatePagination = function() {
        $scope.next = parseInt($scope.currentPage) + 1;
        $scope.before = parseInt($scope.currentPage) - 1;

        if ($scope.totalPages >= 10) {
          if ($scope.totalPages - $scope.currentPage >= 5) {
            if ($scope.currentPage - 10 < 0) {
              $scope.lowerLimit = 0;
              $scope.upperLimit = 9;
            } else {
              $scope.upperLimit = parseInt($scope.currentPage) + 4;
              $scope.lowerLimit = parseInt($scope.upperLimit) - 9;
            }
          } else {
            $scope.upperLimit = parseInt($scope.totalPages) - 1;
            $scope.lowerLimit = parseInt($scope.totalPages) - 10;
          }
        } else {
          $scope.lowerLimit = 0;
          $scope.upperLimit = $scope.totalPages - 1;
        }

      }

      $scope.paginate = function(page) {
        if ($scope.filtering == false) {
          $scope.requestData.requestedPage = page;
          $http.post('/api/paymentLogs/getAll', $scope.requestData)
            .then(function(res) {
              console.log(res);
              $scope.paymentLogs = res.data.documents;
              $scope.totalPages = res.data.totalPages;
              $scope.totalResults = res.data.totalResults;
              $scope.currentPage = res.data.currentPage;
              $scope.updatePagination();
            })
            .catch(function(err) {
              console.log(err);
            });
        } else if ($scope.filtering == true) {
          $scope.toFilter.requestedPage = page;
          $http.post('/api/paymentLogs/filter', $scope.toFilter)
            .then(function(res) {
              console.log(res);
              $scope.paymentLogs = res.data.documents;
              $scope.totalPages = res.data.totalPages;
              $scope.totalResults = res.data.totalResults;
              $scope.currentPage = res.data.currentPage;
              $scope.updatePagination();
            })
            .catch(function(err) {
              console.log(err);
            });
        }
      }
      //Funciones necesarias para la paginación-----------------------------------------------------------------------

      //Función que parsea la hora en un formato más entendible
      $scope.getMomentTime = function(time) {
        return moment(time).format('DD/MM/YYYY');
      }
    });

})();
