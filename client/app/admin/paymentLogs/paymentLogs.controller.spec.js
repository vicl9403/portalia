'use strict';

describe('Component: PaymentLogsComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var PaymentLogsComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    PaymentLogsComponent = $componentController('paymentLogs', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
