'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/paymentLogs', {
        template: '<payment-logs></payment-logs>'
      });
  });
