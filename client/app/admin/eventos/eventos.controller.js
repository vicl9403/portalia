'use strict';

(function() {

  angular.module('portaliaApp')
    .component('eventos', {
      templateUrl: 'app/admin/eventos/eventos.html'
    }).controller('eventosCtrl', function($scope, $http, $window, Auth, $location, Upload) {
      Auth.getCurrentUser(function(currentUser) {
        $scope.userData = currentUser // Your code
        if ($scope.userData.role != 'admin') {
          $window.location = '/';
        }
      });
      $scope.datos = {};
      $scope.datos.destacado = false;
      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
        });

      $http.get('/api/events')
        .then(function(resp) {
          $scope.eventos = resp.data;
        });

      $(document).ready(function() {
        jQuery('#datetimepicker').datetimepicker({
          locale: 'es',
          format: 'YYYY-MM-DD HH:mm'
        });
        jQuery('#datetimepickerDos').datetimepicker({
          locale: 'es',
          format: 'YYYY-MM-DD HH:mm'
        });
      });

      $scope.crearEvento = function() {
        $scope.datos.start = $('#datetimepicker').val().replace(/ /g, "T");
        $scope.datos.end = $('#datetimepickerDos').val().replace(/ /g, "T");
        console.log($scope.datos);
        $http.post('/api/events/', $scope.datos)
          .then(function(resp) {
            alert("Evento creado con éxito")
            $http.get('/api/events')
              .then(function(resp) {
                $scope.eventos = resp.data;
              });
          })
      }

      $scope.deleteEvent = function(evento) {
        var r = confirm("¿Seguro que quieres borrar el evento '" + evento.title + "'?");
        if (r == true) {
          $http.delete('/api/events/' + evento._id)
            .then(function(response) {
              alert("Éxito al borrar el evento")
              console.log(response);
              $http.get('/api/events')
                .then(function(resp) {
                  $scope.eventos = resp.data;
                });
            })
        } else {
          alert("Cancelado");
        }
      }

      $scope.destacar = function() {
        if ($scope.datos.destacado == false) {
          $scope.datos.destacado = true;
          $('#destacar').css("background-color", "#f08f04");
        } else {
          $scope.datos.destacado = false;
          $('#destacar').css("background-color", "black");
        }
      }

      $scope.rename = function() {
        $http.post('/api/users/renameSector')
        .then(function(res){
          console.log('hola');
        })
      }

      $scope.destacarList = function(evento) {
        evento.destacado = true;
        $http.patch('/api/events/' + evento._id, evento)
          .then(function(resp) {
            alert("Evento destacdo con éxito")
            $http.get('/api/events')
              .then(function(resp) {
                $scope.eventos = resp.data;
              });
          })
      }

      $scope.noDestacar = function(evento) {
        evento.destacado = false;
        $http.patch('/api/events/' + evento._id, evento)
          .then(function(resp) {
            alert("Este evento ya no será destacado")
            $http.get('/api/events')
              .then(function(resp) {
                $scope.eventos = resp.data;
              });
          })
      }

      $scope.subirImagen = function(file) {
        waitingDialog.show('Subiendo archivo');
        if (file == null || file.size > 20000 * 1000 * 1000) {
          waitingDialog.hide();
          alert("Revisa el peso de tu archivo, debe ser menor a 20mb");
          $('#subirImagen').val('');
          return;
        } else {
          Upload.upload({
              url: '/api/upload/imagenEvento/', //webAPI exposed to upload the file
              data: {
                file: file,
                usuario: 'puto'
              } //pass file as data, should be user ng-model
            })
            .then(function(resp) { //upload function returns a promise
              console.log(resp.data);
              $scope.datos.imagen = resp.data.nombre
              if (resp.data.errorCode === 0) { //validate success
                $('#subirImagen').val('');
                waitingDialog.hide();
                if (file.name.length >= 25) {
                  $scope.fileName = file.name.substring(0, 25) + '...'
                } else {
                  $scope.fileName = file.name;
                }
              } else {
                //$window.alert('an error occured');
                waitingDialog.hide();
                alert("Revisa el peso de tu archivo, debe ser menor a 20mb");
                $('#subirImagen').val('');
              }
            }, function(resp) { //catch error
              console.log('Error status: ' + resp.status);
              waitingDialog.hide();
              alert("Revisa el peso de tu archivo, debe ser menor a 20mb");
              $('#subirImagen').val('');
            }, function(evt) {
              console.log(evt);
              console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total));
              var progress = parseInt(100.0 * evt.loaded / evt.total)
              waitingDialog.update(progress);
            });
        }
      };
    });

})();
