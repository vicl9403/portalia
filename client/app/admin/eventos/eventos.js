'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/eventos', {
        template: '<eventos></eventos>'
      });
  });
