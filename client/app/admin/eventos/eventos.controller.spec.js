'use strict';

describe('Component: EventosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var EventosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    EventosComponent = $componentController('eventos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
