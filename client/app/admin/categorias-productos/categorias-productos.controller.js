'use strict';

(function(){

class CategoriasProductosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('categoriasProductos', {
    templateUrl: 'app/admin/categorias-productos/categorias-productos.html',
    controller: CategoriasProductosComponent,
    controllerAs: 'categoriasProductosCtrl'
  }).controller('CategoriasProductosController', function($scope, $http, $window, Auth) {

    // Verificar que tenga permiso de estar aquí
    Auth.getCurrentUser(function(user) {

      if (user.role != 'admin')
        $window.location = '/';


      $scope.user = user;

      $scope.getProductCategories();

      $scope.newCategory = {
        name          : '',
        subcategories : [],
        search        : []
      };

      $scope.activeCategory = null;

      $scope.newSubcategory = {
        name        : '',
        active      : true,
      };

    });

    $scope.setActiveCategory = function (category) {
      $scope.activeCategory = category;
    }

    $scope.addSubcategory = function (  ) {
      $scope.activeCategory.subCategories.push(
        {
          name: $scope.newSubcategory.name,
          active: true
        }
      );

      $scope.newSubcategory.name = '';
    }

    $scope.getProductCategories = function () {
      $http.get('/api/product_categories')
        .then( function (res) {
          $scope.categories = res.data;
        })
    }

    $scope.createCategorie = function () {
      $http.post('/api/product_categories/' , $scope.newCategory )
        .then( function (res) {
          $scope.getProductCategories();
        })
    }

    $scope.deleteSubcategory = function ( index ) {
      $scope.activeCategory.subCategories.splice( index , 1 );
    }

    $scope.deleteCategory = function ( id ) {

      swal({
          title: "¿Estas seguro?",
          text: "Si eliminas esta categoria no se podrán crear mas productos dentro de esta categoria",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, eliminar!",
          cancelButtonText: "No, cancelar!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {

            $http.delete('/api/product_categories/' + id )
              .then(function (res) {
                $scope.getProductCategories();
                swal('Éxito','Se ha eliminado correctamente la categoria' , 'success' )
              })

          } else {
            swal("Cancelado", "La categoria sigue activa");
          }
        });



    }
    $scope.saveCategory = function () {
      $http.patch('/api/product_categories/subcategories/' + $scope.activeCategory._id , $scope.activeCategory.subCategories )
        .then( function (res) {
          if( res.status == 200 ) {
            $scope.getProductCategories();
            swal('Éxito', 'Categorias modificadas correctamente', 'success');
            $scope.newSubcategory.name = '';
          }
          else {
            swal('Error','Ocurrió un error desconocido, favor de ponerte en contacto con el administrador' , 'error' );
            console.log( res );
          }
        })
    }

  });


})();
