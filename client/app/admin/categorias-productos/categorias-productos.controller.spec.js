'use strict';

describe('Component: CategoriasProductosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CategoriasProductosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    CategoriasProductosComponent = $componentController('categorias-productos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
