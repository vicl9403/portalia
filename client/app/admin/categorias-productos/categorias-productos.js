'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/categorias-productos', {
        template: '<categorias-productos></categorias-productos>'
      });
  });
