'use strict';

angular.module('portaliaApp')

  .controller('ContactController', function($scope, $location, Auth, $window,  $http , $routeParams) {



    Auth.getCurrentUser(function (user) {

      $scope.user = user;

      $scope.contactData = {
        name : ( user != undefined ) ? user.nombre : '' ,
        email: ( user != undefined ) ? user.email : '',
        phone: ( user != undefined ) ? user.telefono : '',
        description: '',
        to: $routeParams.id
      };

      if( $scope.user._id != undefined ) {
        $scope.contactData['from'] = user._id;
      }

    });

    // Función para guardar el contacto con la empresa
    $scope.setContact = function ( isValidForm ) {
      if ( isValidForm ) {
        // Limitar los contactos por día del usuario
        if( $scope.user.contactsPerDay >= 5 ) {
          swal( "¡Oooops!", "Haz eviado muchos contactos el día de hoy, por favor espera a mañana para poder mandar más mensajes de contacto" , 'warning');
          return ;
        }
        $http.post('/api/contacts', $scope.contactData )
          .then(function (res) {

            // Incrementar los contactos por día del usuario
            $scope.incrementContactNumber( $scope.user );

            $('#contact-business').modal('toggle');
            swal("Éxito", "Se generó el contacto exitósamente", "success");
          });
      }
      else
        swal( 'Adevertencia' , 'Todos los campos son requeridos, por favor verifica tu información y vuelve a intentar', 'warning');
    };

    // Función para incrementar los contactos por día del usuario
    // una vez que ya se mandó el contacto a aprobación
    $scope.incrementContactNumber = function ( user ) {
      user.contactsPerDay ++;

      $http.patch('/api/users/' + user._id , user );
    };

    $scope.openLoginModal = function(modo) {
      $scope.type = modo;
      $('#myModal').modal('show');
    };

  });






// myMod.component('myComp', {
//   template: '<div>My name is  test {{$ctrl.name}}</div>',
//   controller: function() {
//     this.name = 'shahar';
//   }
// });

