'use strict';

angular.module('portaliaApp')
  .directive('contactBusiness', function() {
    return {
      restrict: 'E',
      scope: {
        message: '='
      },
      templateUrl: 'app/perfil/components/contact/contact.html',

    };
  });
