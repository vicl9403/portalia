'use strict';

angular.module('portaliaApp').controller('oportunidadesCtrl', function($scope, $location, Auth, $window, $http, appConfig, $rootScope, $routeParams, $timeout) {
  $scope.archivedQuotes = [];
  $scope.newQuotes = [];
  $timeout(function() {
    $http.get('api/quotes/getUserQuotes/' + $scope.userData._id)
      .then(function(response) {
        console.log(response);
        for (var x = 0; x < response.data.length; x++) {
          if ($scope.userData.seenQuotes.indexOf(response.data[x]._id) > -1) {
            $scope.archivedQuotes.push(response.data[x]);
            console.log($scope.archivedQuotes);
          } else {
            $scope.newQuotes.push(response.data[x]);
            console.log($scope.newQuotes);
          }
        }
        $scope.currentID = 'new';
        $scope.currentQuotes = $scope.newQuotes;
      });
  }, 2500);

  $scope.changeCurrentQuotes = function(toDisplay) {
    $scope.currentID = toDisplay;
    if (toDisplay == 'new') {
      $scope.currentQuotes = $scope.newQuotes;
    } else if (toDisplay == 'archived') {
      $scope.currentQuotes = $scope.archivedQuotes;
    }
  }

  $scope.markAsSeen = function(idQuote) {
    $scope.userData.seenQuotes.push(idQuote);
    Auth.updateProfile($scope.userData);
    $http.get('api/quotes/getUserQuotes/' + $scope.userData._id)
      .then(function(response) {
        $scope.archivedQuotes = [];
        $scope.newQuotes = [];
        console.log(response);
        for (var x = 0; x < response.data.length; x++) {
          if ($scope.userData.seenQuotes.indexOf(response.data[x]._id) > -1) {
            $scope.archivedQuotes.push(response.data[x]);
            console.log($scope.archivedQuotes);
          } else {
            $scope.newQuotes.push(response.data[x]);
            console.log($scope.newQuotes);
          }
        }
        $scope.currentID = 'new';
        $scope.currentQuotes = $scope.newQuotes;
      });
  };

  $scope.setModalInfo = function(quote) {
      $scope.contactData = quote;
      $scope.temp = $scope.contactData.date.substring(0, 10);
      $scope.contactData.date = $scope.temp;
      $("#forma :input").prop('readonly', true);
      console.log($scope.contactData);
  }

});
//# sourceMappingURL=shedules.controller.js.map
