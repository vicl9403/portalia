'use strict';

angular.module('portaliaApp')
  .directive('oportunidades', () => ({
    templateUrl: 'app/perfil/components/oportunidades/oportunidades.html',
    restrict: 'E',
    controller: 'oportunidadesCtrl',
    controllerAs: 'nav'
  }));
