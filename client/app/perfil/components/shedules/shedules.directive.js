'use strict';

angular.module('portaliaApp')
  .directive('shedules', () => ({
    templateUrl: 'app/perfil/components/shedules/shedules.html',
    restrict: 'E',
    controller: 'ShedulesController',
    controllerAs: 'nav'
  }));
