'use strict';

angular.module('portaliaApp').controller('ShedulesController', function ($scope, $location, Auth, $window, $http, appConfig, $rootScope, $routeParams) {

  $scope.checkHours = function (start, end) {
    if(start == '' || end == '')
      return true;


    //Obtenemos los valores de las horas
    var hourStart = parseInt(start.split(":")[0]);
    var hourEnd   = parseInt(end.split(":")[0]);

    //Incrementamos en 12 si son pm
    if(start.split(" ")[1] == "PM")
      hourStart += 12
    if(end.split(" ")[1] == "PM")
      hourEnd += 12;

    alert(hourStart);
    alert(hourEnd);

    if(hourStart > hourEnd)
        return false;

    return true;

  }

  $scope.setShedules = function () {
    var invalid = false;



    $scope.user.shedules.forEach(function (item) {

      // //Verificar que seran horas correctas
      // if( $scope.checkHours(item.opening,item.closing) == false )
      // {
      //   swal("Error","El horario del cierre es menor que el de apertura, tienen que tener al menos una hora de diferencia en el "+item.day + " por favor verifícalo","error");
      //   return;
      // }
      // else
      // {

      //Verificar que ningun día abierto esté sin apertura o cierre
      if(item.oppened && (item.opening == '' || item.closing == ''))
          invalid = true;

      //Setear a vacíos todos los días que cierra
      if(! item.oppened) {
          item.opening = '';
          item.closing = '';
      }

      // }


    });

    if(invalid)
      swal("Error","Para poder guardar tus horarios todos los días que abres debes especificar una hora de apertura y cierre","error")
    else
    {
      $http.patch('/api/users/' + $scope.user._id, $scope.user).then(function () {
          swal("Éxito", "horarios modificados correctamente", "success");
          console.log($scope.user.shedules);
      });
    }
  }

  angular.element(document).ready(function () {
      $scope.isMyProfile = false;

      if($scope.userData == undefined) {
          $http.get('/api/users/' + $routeParams.id)
              .then(function (res) {
                console.log(res);
                  $scope.user = res.data;
                  if($scope.user.shedules.length == 0)
                  {
                      $scope.user.shedules =
                          [
                              {
                                  day:  'Lunes',
                                  oppened:  false ,
                                  opening:  '' ,
                                  closing:  ''
                              },
                              {
                                  day:  'Martes' ,
                                  oppened:  false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Miércoles',
                                  oppened: false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Jueves' ,
                                  oppened:  false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Viernes',
                                  oppened: false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Sábado',
                                  oppened: false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Domingo',
                                  oppened: false,
                                  opening: '',
                                  closing: ''
                              }
                          ];
                  }
              })
      }
      else {
        if (window.location.href == '/perfil/me') {
          $scope.user = $scope.userData;
          $scope.isMyProfile = true;

          if($scope.user.shedules.length == 0)
          {
            $scope.user.shedules =
                [
                    {
                        day:  'Lunes',
                        oppened:  false ,
                        opening:  '' ,
                        closing:  ''
                    },
                    {
                        day:  'Martes' ,
                        oppened:  false ,
                        opening: '',
                        closing: ''
                    },
                    {
                        day:  'Miércoles',
                        oppened: false ,
                        opening: '',
                        closing: ''
                    },
                    {
                        day:  'Jueves' ,
                        oppened:  false ,
                        opening: '',
                        closing: ''
                    },
                    {
                        day:  'Viernes',
                        oppened: false ,
                        opening: '',
                        closing: ''
                    },
                    {
                        day:  'Sábado',
                        oppened: false ,
                        opening: '',
                        closing: ''
                    },
                    {
                        day:  'Domingo',
                        oppened: false,
                        opening: '',
                        closing: ''
                    }
                ];
          }

        }
        else {
          $http.get('/api/users/' + $routeParams.id)
              .then(function (res) {
                console.log(res);
                  $scope.user = res.data;
                  if($scope.user.shedules.length == 0)
                  {
                      $scope.user.shedules =
                          [
                              {
                                  day:  'Lunes',
                                  oppened:  false ,
                                  opening:  '' ,
                                  closing:  ''
                              },
                              {
                                  day:  'Martes' ,
                                  oppened:  false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Miércoles',
                                  oppened: false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Jueves' ,
                                  oppened:  false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Viernes',
                                  oppened: false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Sábado',
                                  oppened: false ,
                                  opening: '',
                                  closing: ''
                              },
                              {
                                  day:  'Domingo',
                                  oppened: false,
                                  opening: '',
                                  closing: ''
                              }
                          ];
                  }
              })
        }
      }

  });





});
//# sourceMappingURL=shedules.controller.js.map
