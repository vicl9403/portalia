'use strict';

angular.module('portaliaApp')
  .directive('telefonos', () => ({
    templateUrl: 'app/perfil/components/telefonos/telefonos.html',
    restrict: 'E',
    controller: 'TelefonosController',
    controllerAs: 'nav'
  }));
