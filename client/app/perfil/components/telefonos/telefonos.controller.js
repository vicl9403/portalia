'use strict';

angular.module('portaliaApp').controller('TelefonosController', function($scope, $location, Auth, $window, $http, appConfig, $rootScope, $routeParams) {
  $scope.toAdd = {};
  $scope.editing = false;

  $scope.addPhone = function() {
    if ($scope.toAdd.tel != '' && $scope.toAdd.nombre != '' && $scope.toAdd.tel != undefined && $scope.toAdd.nombre != undefined) {
      console.log($scope.toAdd);
      $scope.userData.telefonos.push($scope.toAdd)
      Auth.updateProfile($scope.userData);
      $scope.toAdd = {};
    } else {
      swal("Error", "Todos los campos son obligatorios", "error")
    }
  }

  $scope.deletePhone = function(index) {
    $scope.userData.telefonos.splice(index, 1);
    Auth.updateProfile($scope.userData);
  }

  $scope.editPhone = function(index, phone) {
    $scope.editing = true;
    $scope.editIndex = index;
    $scope.toAdd.tel = phone.tel;
    $scope.toAdd.nombre = phone.nombre;
  }

  $scope.submitEdit = function() {
    if ($scope.toAdd.tel != '' && $scope.toAdd.nombre != '' && $scope.toAdd.tel != undefined && $scope.toAdd.nombre != undefined) {
      $scope.userData.telefonos[$scope.editIndex] = $scope.toAdd
      Auth.updateProfile($scope.userData);
      $scope.toAdd = {};
      $scope.editing = false;
    } else {
      swal("Error", "Todos los campos son obligatorios", "error")
    }
  }
});
//# sourceMappingURL=shedules.controller.js.map
