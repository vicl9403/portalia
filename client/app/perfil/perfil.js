'use strict';

angular.module('portaliaApp')
  .config(function($routeProvider) {
    $routeProvider
      .when('/perfil/:id/:nombre', {
        template: '<perfil></perfil>'
      });
  });
