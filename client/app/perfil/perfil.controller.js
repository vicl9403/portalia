'use strict';
(function() {

  angular.module('portaliaApp')
    .component('perfil', {
      templateUrl: 'app/perfil/perfil.html'
    })
    .controller('perfilCrl', function($scope, $http, Auth, Upload, $timeout, $window, $routeParams, $location, $rootScope)
    {
      $scope.nombreInfo = $routeParams.id;
      $scope.usuarioFav = {};
      $scope.contaGrl;
      $scope.contaTel;
      $scope.listaPosts = [];
      $scope.shareData = {};
      $scope.listaFavoritos = [];
      $scope.socket = io();

      $scope.hasCreatedChat = false;

      $scope.logged = Auth.isLoggedIn;

      var slider;
      var sliderVid

      $scope.getUserProducts = function () {
        $http.get('/api/products/userProducts/' + $scope.userInfo._id )
          .then( function (res) {
            $scope.products = res.data;
            console.log( res.data );
          })
      }
      function afterUpload() {
        slider.refresh();
        slider.goToNextSlide();
      }

      $scope.getProductImage = function ( product ) {

        if( product.pictures.length == 0)
          return '/assets/images/foto-perfil-default.png';

        for( let i = 0 ; i < product.pictures.length ; i++ ) {
          if( product.pictures[i].mainPicture )
            return product.pictures[i].url.replace('./client','');
        }
        return product.pictures[0].url.replace('./client','')
      }

      $scope.sharetest = function() {
        $('#mainModal').modal('toggle');
      }

      function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

      $scope.openMail = function() {
        $('#mainModal').modal('toggle');
        $('#modalShare').modal('toggle');
      }

      $scope.openFB = function() {
        $('#mainModal').modal('toggle');
        FB.ui({
          method: 'feed',
          name: $scope.userInfo.nombre + ' - Portalia Plus',
          link: window.location.href,
          caption: 'Mira el perfil de esta empresa y muchas más en el mayor directorio indsutrial de México',
          picture: window.location.hostname + '/assets/uploads/' + encodeURIComponent($scope.userInfo.sector_name) + '/' + encodeURIComponent($scope.userInfo.categoria_name) + '/' + $scope.userInfo._id + '/foto-perfil.jpg'
        }, function(response) {});

      }

      $scope.isValidShareEmail = function ( data ) {
        var errors = [];

        if ( !validateEmail( data.tuCorreo ) )
          errors.push('Tu correo es campo requerido y tiene que ser un correo válido');
        if ( !validateEmail( data.suCorreo ) )
          errors.push('El correo destinatario es campo requerido y tiene que ser un correo válido');
        if ( data.tuNombre  == '' || data.tuNombre == undefined )
          errors.push('Es necesario que ingreses tu nombre');
        if ( data.suNombre == '' || data.suNombre == undefined )
          errors.push('Es necesario que ingreses el nombre destinatario');
        if ( validateEmail( data.tuCorreo ) && validateEmail( data.suCorreo ) && data.tuCorreo == data.suCorreo )
          errors.push('Los dos correos no pueden ser iguales');

        if( errors.length > 0)
        {
          swal( "Error! " , errors.join('\n\n') , 'error' );
          return false;
        }
        return true;
      }

      $scope.shareMail = function() {

        if ( $scope.isValidShareEmail( $scope.shareData ))
        {
          $scope.sendingEmail = true;

          $scope.shareData.url = window.location.href;

          $http.post('/api/mailings/shareMail', $scope.shareData)
            .then(function (res) {
              swal("Éxito", "Tu enlace se ha compartido con éxito", "success");
              $scope.sendingEmail = false;
              $('#modalShare').modal('toggle');
            })
            .catch(function (err) {
            })
        }

      }

      function afterUploadVideo() {
        sliderVid.refresh();
        sliderVid.goToNextSlide();
      }

      // Función para ver si ya hay un chat entre estas empresas
      $scope.hasChat = function ( id1, id2 ) {
        $http.get('/api/chat/exists-chat/'+ id1 + '/' + id2 )
          .then( function (res) {
            // Si la respuesta no viene vacía setear el chat como true
            if( res.data.length > 0)
              $scope.hasCreatedChat = true;

          })
      }

      // Función que se encarga de cre
      $scope.createChat = function () {
        // Establecer el contenido del chat inicial
        var content = ( $scope.userData.role != 'empresa' ) ?
          'El usuario ' + $scope.userData.nombre + ' solicita un chat' :
          'La empresa ' + $scope.userData.nombre + ' solicita un chat' ;

        var data = {
          userOrigin : $scope.userData._id,
          userDest : $routeParams.id,
          messages : [{
            from: $scope.userData._id,
            to: $scope.user._id,
            content: content,
          }]
        }

        // Crear el nuevo chat
        $http.post('/api/chats/', data)
          .then(function ( res ) {

            // Mandar el correo de que se generó un nuevo chat
            $http.post('/api/users/nuevoChat/', { destinatario : $scope.user.email })
              .then(function ( res ) {

                // Mandar la notificación al socket
                $scope.socket.emit('chat_created', { id: $scope.user._id });
                $window.location.href = 'perfil-usuario/chat';

              });

          })
          .catch( err => {
          });
      }

      $timeout(function() {
        slider = $("#lightSlider").lightSlider();
        sliderVid = $("#lightSlider-video").lightSlider();
        $window.scrollTo(0, 0);
        $(window).trigger('resize');
      });

      $http.post('https://blog.portaliaplus.com/api/get_recent_posts/')
        .then(function(response) {
          $scope.posts = response.data.posts;
        });

      if ($routeParams.id === 'me') {
        Auth.getCurrentUser(function(res) {

          $scope.title = "Editando mi perfil";
          $scope.description = "Edita tu perfil";


          if (!res.nombre) {
            $location.path("/?login=true");
          }

          $http.post('api/users/calcularCodigos')
            .then(function(response) {
              $scope.information = response.data;

              if (moment().isBefore('2016-11-15') && moment().isAfter('2016-10-01')) {
                $scope.information.total = 0
              }

            });

          $scope.userData = res;
          $scope.fechaFront = moment($scope.userData.fechaCorte).format("DD/MM/YY");
          $scope.getMessages();
          $scope.getFavoritos();
          $scope.getPosts();

          $scope.datamail = {};
          $scope.datamail.to = [];
          $scope.datamail.subject = 'Correo de invitacion a Portalia Plus de ' + $scope.userData.nombre;
          $scope.datamail.name = $scope.userData.nombre;
          $scope.datamail._id = $scope.userData._id;

          $http.get("api/plans")
            .then(function(response) {
              $scope.paquete = response.data[0];

              if ($scope.userData.paquete == "gratis") {
                $scope.imgLimit = parseInt($scope.paquete.beneficios[5].paquete[0].limite);
              }
              if ($scope.userData.paquete == $scope.paquete.beneficios[5].paquete[1].name.toLowerCase()) {
                $scope.imgLimit = parseInt($scope.paquete.beneficios[5].paquete[1].limite);
              }
              if ($scope.userData.paquete == $scope.paquete.beneficios[5].paquete[2].name.toLowerCase()) {
                $scope.imgLimit = parseInt($scope.paquete.beneficios[5].paquete[2].limite);
              }

              if ($scope.userData.paquete == "gratis") {
                $scope.vidLimit = parseInt($scope.paquete.beneficios[6].paquete[0].limite);
              }
              if ($scope.userData.paquete == $scope.paquete.beneficios[6].paquete[1].name.toLowerCase()) {
                $scope.vidLimit = parseInt($scope.paquete.beneficios[6].paquete[1].limite);
              }
              if ($scope.userData.paquete == $scope.paquete.beneficios[6].paquete[2].name.toLowerCase()) {
                $scope.vidLimit = parseInt($scope.paquete.beneficios[6].paquete[2].limite);
              }

              $scope.sliderImages = function() {
                if ($scope.userData.productsImg.length >= $scope.imgLimit) {
                  return 'col-xs-12 col-xs-offset-0 col-sm-12 col-lg-12 col-lg-offset-0'
                } else {
                  return 'col-xs-12 col-xs-offset-0 col-sm-10 col-lg-9 col-lg-offset-1'
                }
              };

              $scope.sliderVideos = function() {
                if ($scope.userData.videos.length >= $scope.vidLimit) {
                  return 'col-xs-12 col-xs-offset-0 col-sm-12 col-lg-12 col-lg-offset-0'
                } else {
                  return 'col-xs-12 col-xs-offset-0 col-sm-10 col-lg-9 col-lg-offset-1'
                }
              };

            });

        });
      } else {

        $http.get('/api/users/' + $routeParams.id)
          .then(function(response) {
            Auth.getCurrentUser(function(res) {

              $scope.userInfo = response.data;
              // $scope.$apply();
              $scope.userData = res;
              $scope.getMessages();

              $scope.getUserProducts();

              // Función que setea hasChat si es que existe un chat entre ambas empresas
              $scope.hasChat($scope.userData._id, $routeParams.id);

              $scope.title = $scope.userInfo.nombre;
              $scope.description = "Ve el perfil de la empresa " + $scope.userInfo.nombre + ". Portalia Plus es el directorio industrial de empresas mas completo en México. Encuentra más empresas como " + $scope.userInfo.nombre;

              // if (response.data == undefined && $scope.nombreInfo != 'me') {
              //   console.log("sakese alv");
              //   $location.path('/')
              // }
              $scope.contaGrl = $scope.userData._id;
              if ($scope.userData.role != 'admin')
              {
                if ($scope.userInfo.visitasGene.length === 0)
                {
                  $scope.userInfo.visitasGene[0] = $scope.contaGrl;
                  $scope.userInfo.numeroVisitasGene++;
                }
                else
                {
                  $scope.userInfo.visitasGene.push($scope.contaGrl);
                  $scope.userInfo.numeroVisitasGene++;
                }
              }

              if ($("#startChat").is(':visible')) {
                // $scope.escondido = true;
              } else {
                if (screen.width > 768) {
                  $scope.escondido = true;
                }
                // $scope.escondido = true;
              }

              $scope.sliderImages = function() {
                return 'col-xs-12 col-xs-offset-0 col-sm-12 col-lg-12 col-lg-offset-0'
              };
              $scope.sliderVideos = function() {
                return 'col-xs-12 col-xs-offset-0 col-sm-12 col-lg-12 col-lg-offset-0'
              };

              Auth.updateProfile($scope.userInfo);
            });
            // $scope.userInfo = response.data;
            // $scope.visitasGenerales();
          })
          .catch(function(err) {

            $location.path('/error-404')
          });
      }

      $scope.sucursales = function() {
        $location.path("/agregarSucursal")
      }

      $scope.gotoPromotion = function() {
        sessionStorage['banerPromo'] = 'false';
        $window.location = "/promocionInvitar";
      }

      $scope.getStars = function(num) {
        return new Array(num);
      }

      $scope.openRate = function() {
        if ($scope.userData.alreadyRated.indexOf($scope.userInfo._id) > -1) {
          swal('', 'Ya has calificado esta empresa', 'info')
        } else {
          $('#rateUser').modal('toggle');
        }
      }

      $scope.rate = function(stars) {
        $scope.promedio = ($scope.userInfo.stars + stars) / 2
        $scope.userInfo.stars = parseInt($scope.promedio);
        Auth.updateProfile($scope.userInfo);
        $scope.userData.alreadyRated.push($scope.userInfo._id);
        Auth.updateProfile($scope.userData);
        swal('¡Éxito al calificar!', 'Tus calificaciones ayudan a destacar a las mejores empresas de Portalia Plus', 'success');
        $('#rateUser').modal('toggle');
      }

      $scope.visitasGenerales = function() {
        $scope.contaGrl = $scope.userData._id;
        if ($scope.userInfo.visitasGene.length === 0) {
          $scope.userInfo.visitasGene[0] = $scope.contaGrl
        } else {
          if ($scope.userInfo.visitasGene.indexOf($scope.contaGrl) > -1) {} else {
            $scope.userInfo.visitasGene.push($scope.contaGrl);
          }
        }
        Auth.updateProfile($scope.userInfo);
      }



      $scope.enviar = function() {

        var emails = document.getElementById('emails').getElementsByTagName('input');
        for (var i = 0; i < emails.length; i++) {
          if (validateEmail(emails[i].value))
            $scope.datamail.to.push(emails[i].value);
        }

        $http.post('/api/email/invitacion', $scope.datamail)
          .then(function(response) {

            if (response.status === 200) {
              $window.alert('Correos enviados');
            } else {
              $window.alert('hubo un problema enviando el correo, intentelo de nuev');
            }
          })
          .catch(function(err) {

          });

      };

      $scope.pedirFavoritos = function(idFavorito, indice) {
        $http.get('/api/users/' + idFavorito)
          .then(function(response) {
            $scope.listaFavoritos[indice] = response.data;
          });
      }

      $scope.pedirPosts = function(idPost, indice) {
        $http.get('https://blog.portaliaplus.com/api/get_post/?post_id=' + idPost)
          .then(function(response) {
            $scope.listaPosts[indice] = response.data;
          });
      }

      $scope.showTel = function() {
        $("#telefono").fadeIn();
        $("#mostrar").fadeOut(300);
        $scope.contaTel = $scope.userData._id;

        if ($scope.userInfo.visitasTel.length === 0) {
          $scope.userInfo.visitasTel[0] = $scope.contaTel
        } else {
          if ($scope.userInfo.visitasTel.indexOf($scope.contaTel) > -1) {} else {
            $scope.userInfo.visitasTel.push($scope.contaTel);
          }
        }
        Auth.updateProfile($scope.userInfo);
      }

      $scope.agregarFavorito = function() {
        // $scope.exists = false; //Variable para ver si existe la empresa ya en favoritos, falsa por defecto
        $scope.idFav = $scope.userInfo._id; //UsuarioFav es lo que se va a parchear al usuario, se le da el valor del id de la empresa a agregar a favoritos

        if (!$scope.userData.favoritos) {
          $('#myModal').modal('show');
        }
        if ($scope.userData.favoritos.length === 0) { //Si aun no hay ningun elemento en favoritos lo pone directamente en la posición 0
          $scope.userData.favoritos[0] = $scope.idFav;
          swal("Exito" , "Agregado a tus favoritos", "success");
        } else { //Si no pasa aquí
          if ($scope.userData.favoritos.indexOf($scope.idFav) > -1) { //Si la variable es verdadera te arroja una alerta de que ya es empresa favorita
            swal("Error", "Esta empresa ya está en tus favoritas", "error");
          } else { //Si no hace push al arreglo y te arroja mensaje de exito
            swal("Exito" , "Agregado a tus favoritos", "success");
            $scope.userData.favoritos.push($scope.idFav);
          }

        }
        Auth.updateProfile($scope.userData); //Hace el update, y se acaba el infierno de if's
      }

      var itsNew = $location.search().new;
      if (itsNew) {
        $('#modal-success').modal('show')
        $('#pasoDos').show();
        $('#finalizar').show();
      }

      $scope.currentImage = '';
      $scope.datos = [];

      $scope.editar = function(campoN, botonN) {
        var campo = document.getElementById(campoN);
        var boton = document.getElementById(botonN);
        if (campo.disabled == true) {
          campo.disabled = false;
          boton.innerHTML = 'Guardar';
          campo.focus();
        } else {
          campo.disabled = true;
          boton.innerHTML = 'Editar';
          campo.blur();
          Auth.updateProfile($scope.userData);
        }
      };

      $scope.editarFactores = function() {
        var editarFactoresBtn = document.getElementById("btnEditFactores");
        var editarFactoresTxt = document.getElementById("ventajasTA");

        if (editarFactoresTxt.disabled == true) {
          editarFactoresTxt.disabled = false;
          editarFactoresBtn.innerHTML = 'Guardar';
          editarFactoresTxt.focus();
        } else {
          editarFactoresTxt.disabled = true;
          editarFactoresBtn.innerHTML = 'Editar';
          editarFactoresTxt.blur();
          Auth.updateProfile($scope.userData);
        }
      }

      //Funcion para subir todo el perfil editado
      $scope.editarAll = function() {
        if ($('#error-url').is(':visible')) {
          alert('Introduce información válida');
          $('#error-url').hide();
        } else {
          $('#btnSaveTags').hide();
          $scope.reviewTags();
          $scope.reviewProducts();
          Auth.updateProfile($scope.userData);
        }
      };

      $scope.updateAdress = function() {

        $scope.userData.sucursales[0].estadoSucursal = $scope.place.address_components[3].long_name;
        $scope.userData.sucursales[0].ciudad = $scope.place.address_components[2].long_name;
        $scope.userData.sucursales[0].numero = 'Sin número';
        $scope.userData.sucursales[0].colonia = $scope.place.address_components[1].long_name;
        $scope.userData.sucursales[0].calle = $scope.place.address_components[0].long_name;
        $scope.userData.sucursales[0].cp = 'Sin Código Postal';
        $scope.userData.sucursales[0].loc[0] = $scope.place.geometry.location.lng();
        $scope.userData.sucursales[0].loc[1] = $scope.place.geometry.location.lat();
        $scope.userData.sucursales[0].nombreSucu = 'Sucursal Principal';
        $scope.userData.sucursales[0].longitud = $scope.place.geometry.location.lng();
        $scope.userData.sucursales[0].latitud = $scope.place.geometry.location.lat();
        Auth.updateProfile($scope.userData);
      };

      $scope.saveUser = function() {
        Auth.updateProfile($scope.userData);
      };

      $scope.reviewTags = function() {
        //Reviso todos los tags nuevos
        var tags = document.getElementById('tags').getElementsByTagName('input');
        var dif = tags.length - $scope.userData.tags.length;
        var newTags = [];
        if (($scope.userData.paquete == 'plus' && tags.length < 100) || ($scope.userData.paquete == 'select' && tags.length < 50) || ($scope.userData.paquete == 'gratis' && tags.length <= 10)) {
          for (var i = 0; i < tags.length; i++) {
            if (tags[i].value !== '') {
              newTags.push(tags[i].value);
            }
          }

          newTags = newTags.filter(function(item, index, inputArray) {
            return inputArray.indexOf(item) == index;
          });

          $scope.userData.tags = newTags;
          //Borro nuevos por que ya se fueron al model y se pintarian dobles
          for (i = 0; i < dif; i++) {
            var last = $('#tags').children(':last-child');
            last.remove();
          }
        } else {
          sweetAlert("Error", "Tu paquete no permite esta cantidad de palabras clave, intenta de nuevo", "error")
        }
      };

      $scope.reviewProducts = function() {
        //Reviso todos los tags nuevos
        var products = document.getElementById('productos').getElementsByTagName('input');
        var dif = products.length - $scope.userData.products.length;
        var newProducts = [];
        if (($scope.userData.paquete == 'plus' && products.length < 100) || ($scope.userData.paquete == 'select' && products.length < 50) || ($scope.userData.paquete == 'gratis' && products.length <= 10)) {
          for (var i = 0; i < products.length; i++) {
            if (products[i].value !== '') {
              newProducts.push(products[i].value);
            }
          }
          newProducts = newProducts.filter(function(item, index, inputArray) {
            return inputArray.indexOf(item) == index;
          });

          $scope.userData.products = newProducts;
          //Borro nuevos por que ya se fueron al model y se pintarian dobles
          for (i = 0; i < dif; i++) {
            var last = $('#productos').children(':last-child');
            last.remove();
          }
        } else {
          sweetAlert("Error", "Tu paquete no permite esta cantidad de productos, intenta de nuevo", "error")
        }
      };


      $scope.editarTextarea = function(campoN, botonN) {
        $('#' + campoN).prop('readonly', function(i, v) {
          return !v;
        });
        $('#' + campoN).toggleClass('editarTextarea');
        $('#' + botonN).parent().toggleClass('editando');
        Auth.updateProfile($scope.userData);
      };


      $scope.uploadProfilePic = function(file) {
        waitingDialog.show('Subiendo imagen');
        if (file == null || file.size > 10000 * 1000 * 1000) {
          waitingDialog.hide();
          alert("Revisa el peso de tu imagen, debe ser menor a 10mb");
          $('#imgPerfil').val('');
          return;
        } else {
          Upload.upload({
              method: 'POST',
              headers: {
                'Content-Type': 'multipart/form-data'
              },
              url: '/api/upload/profilePic/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre), //webAPI exposed to upload the file
              file: file
            })
            .then(function(resp) { //upload function returns a promise

              if (resp.data.errorCode === 0) { //validate success
                waitingDialog.hide();
                $('.profileImg').attr('src', 'assets/uploads/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + $scope.userData._id + '/foto-perfil.jpg' + '?decache=' + Math.random());
                $('#imgPerfil').val('');
                $scope.userData.hasPic = true; //true
                Auth.updateProfile($scope.userData);
              } else {
                waitingDialog.hide();
                $window.alert('an error occured');
                $('#imgPerfil').val('');
              }
            }, function(resp) { //catch error
              waitingDialog.hide();
              alert("Revisa el peso de tu imagen, debe ser menor a 10mb");
              $('#imgPerfil').val('');
            }, function(evt) {
              var progress = parseInt(100.0 * evt.loaded / evt.total)
              waitingDialog.update(progress);
            });
        };
      };

      $scope.uploadProductPic = function(file) {
        waitingDialog.show('Subiendo imagen');
        if (file == null || file.size > 10000 * 1000 * 1000) {
          waitingDialog.hide();
          alert("Revisa el peso de tu imagen, debe ser menor a 10mb");
          $('#fileImg').val('');
          return;
        } else {
          Upload.upload({
              url: '/api/upload/productsPics/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre), //webAPI exposed to upload the file
              data: {
                file: file
              } //pass file as data, should be user ng-model
            })
            .then(function(resp) { //upload function returns a promise
              if (resp.data.errorCode === 0) { //validate success
                $('#fileImg').val('');
                $scope.userData.productsImg.push(resp.data.imgName);
                Auth.updateProfile($scope.userData).then(function() {
                  //$('#videoUpload').trigger('click');
                  afterUpload();
                  waitingDialog.hide();
                  $('#fileImg').val('');
                });
              } else {
                //$window.alert('an error occured');
                waitingDialog.hide();
                alert("Revisa el peso de tu imagen, debe ser menor a 10mb");
                $('#fileImg').val('');
              }
            }, function(resp) { //catch error
              waitingDialog.hide();
              alert("Revisa el peso de tu imagen, debe ser menor a 10mb");
              $('#fileImg').val('');
            }, function(evt) {
              var progress = parseInt(100.0 * evt.loaded / evt.total)
              waitingDialog.update(progress);
            });
        }
      };

      $scope.uploadVideo = function(file) {
        waitingDialog.show('Subiendo video');
        if (file == null || file.size > 100000 * 1000 * 1000) {
          waitingDialog.hide();
          alert("Revisa el peso de tu video, debe ser menor a 100mb");
          $('#fileVid').val('');
          return;
        } else {
          Upload.upload({
              url: '/api/upload/productsVideo/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre), //webAPI exposed to upload the file
              data: {
                file: file
              } //pass file as data, should be user ng-model
            })
            .then(function(resp) { //upload function returns a promise
              if (resp.data.errorCode === 0) { //validate success
                $('#fileVid').val('');
                $scope.userData.videos.push(resp.data.vidName);
                Auth.updateProfile($scope.userData).then(function() {
                  //$('#videoUpload').trigger('click');
                  afterUploadVideo();
                  waitingDialog.hide();
                  $('#fileVid').val('');
                });
              } else {
                //$window.alert('an error occured');
                waitingDialog.hide();
                alert("Revisa el peso de tu video, debe ser menor a 100mb");
                $('#fileVid').val('');
              }
            }, function(resp) { //catch error
              waitingDialog.hide();
              alert("Revisa el peso de tu video, debe ser menor a 100mb");
              $('#fileVid').val('');
            }, function(evt) {
              var progress = parseInt(100.0 * evt.loaded / evt.total)
              waitingDialog.update(progress);
            });
        }
      };

      $scope.uploadCurriculum = function(file) {
        waitingDialog.show('Subiendo archivo');
        if (file == null || file.size > 20000 * 1000 * 1000) {
          waitingDialog.hide();
          alert("Revisa el peso de tu archivo, debe ser menor a 20mb");
          $('#subirCatalogo').val('');
          return;
        } else {
          Upload.upload({
              url: '/api/upload/productsFile/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre), //webAPI exposed to upload the file
              data: {
                file: file
              } //pass file as data, should be user ng-model
            })
            .then(function(resp) { //upload function returns a promise
              if (resp.data.errorCode === 0) { //validate success
                $('#subirCatalogo').val('');
                waitingDialog.hide();
              } else {
                //$window.alert('an error occured');
                waitingDialog.hide();
                alert("Revisa el peso de tu archivo, debe ser menor a 20mb");
                $('#subirCatalogo').val('');
              }
            }, function(resp) { //catch error
              waitingDialog.hide();
              alert("Revisa el peso de tu archivo, debe ser menor a 20mb");
              $('#subirCatalogo').val('');
            }, function(evt) {
              var progress = parseInt(100.0 * evt.loaded / evt.total)
              waitingDialog.update(progress);
            });
        }
      };

      $scope.editarSocialMedia = function() {
        Auth.updateProfile($scope.userData);
        $('.loginScreen').hide();
        $scope.userData = Auth.getCurrentUser();
      };

      $scope.editarRedes = function() {
        $('.loginScreen').toggle();
      };


      $scope.createMessage = function() {
        if ($scope.nombreInfo != 'me') {
          if ($scope.mensajeDos !== '') {
            $scope.data = {
              personsId: [{
                id: $scope.userData._id,
                name: $scope.userData.nombre
              }, {
                id: $scope.userInfo._id,
                name: $scope.userInfo.nombre

              }],
              msj: [{
                idEnv: $scope.userData._id,
                msj: $scope.mensajeDos
              }]
            };
            $http.post('/api/chat/', $scope.data)
              .then(function(response) {
                $scope.datos.push(response.data);
                $scope.currentChat = response.data._id;
                $scope.currentChatPosition = $scope.datos.length - 1;
                $scope.getMessages();
                $('#modal-chat').modal('hide');
                $scope.dataNuevoChat = {
                  "destinatario": $scope.userInfo.email
                }
                $http.post('/api/users/nuevoChat', $scope.dataNuevoChat)
                  .then(function(res) {
                  })
                  .catch(function(err) {
                  })
                $timeout(function() {
                  $("#conversation").scrollTop($("#conversation")[0].scrollHeight);
                }, 50);
              })
              .catch(function(err) {
              });
            $scope.mensajeDos = '';
          }
        } else {
          $scope.data = $scope.datos[$scope.currentChatPosition];
          $scope.data.msj.push({
            idEnv: $scope.userData._id,
            msj: $scope.mensaje
          });
          $http.patch('/api/chat/' + $scope.data._id, $scope.data)
            .then(function(response) {
              $scope.datos[$scope.currentChatPosition] = response.data;
              $timeout(function() {
                $("#conversation").scrollTop($("#conversation")[0].scrollHeight);
              }, 50);
            })
            .catch(function(err) {
            });
          $scope.mensaje = '';
        }

      };

      $scope.changeChat = function(id, pos) {
        $scope.currentChat = id;
        $scope.currentChatPosition = pos;
        $timeout(function() {
          $("#conversation").scrollTop($("#conversation")[0].scrollHeight);
        }, 50);
      };

      $scope.getMessages = function() {
        $http.get('/api/chat/' + $scope.userData._id)
          .then(function(response) {
            $scope.datos = response.data;
            // $scope.socket.syncUpdates('chat', $scope.datos);
            // if ($routeParams.id != 'me') {
            //   for (var x = 0; x < $scope.datos.length; x++) {
            //     console.log("foreando conversaciones alv");
            //     if ($scope.datos[x].personsId[1].id === $scope.userInfo._id) { //kpez
            //       console.log("ya estás conversando con stemen");
            //       $("#startChat").hide();
            //       $("#alreadyChatting").show();
            //     }
            //   }
            // }
          })
          .catch(function(err) {
          });
      };

      $('#mensajeChido').keyup(function(e) {
        if (e.which === 13) {
          // $('#perfilVisitanteCrl').scope().createMessage();
          $scope.createMessage();
        }
      });

      $('#txtMessage').keyup(function(e) {
        if (e.which === 13) {
          // $('#perfilVisitanteCrl').scope().createMessage();
          $scope.createMessage();
        }
      });

      $scope.seeImage = function(img, index) {

        let url = '';

        if( img.includes('/assets/uploads') )
          url = img;
        else
          url = 'assets/uploads/' + encodeURIComponent($scope.userInfo.sector_name) + '/' + encodeURIComponent($scope.userInfo.categoria_name) + '/' + $scope.userInfo._id + '/productos/' + img;

          $('#modal-image').attr('src', url);
          $scope.userInfo.visitaFotos.push(index);
          Auth.updateProfile($scope.userInfo);

      };

      $scope.seeVideo = function(vid) {
        if ($scope.nombreInfo == 'me') {
          $('#video-ventana').attr('src', 'assets/uploads/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + $scope.userData._id + '/productos/' + vid);
          $("#divVid video")[0].load();
        } else {
          $('#video-ventana').attr('src', 'assets/uploads/' + encodeURIComponent($scope.userInfo.sector_name) + '/' + encodeURIComponent($scope.userInfo.categoria_name) + '/' + $scope.userInfo._id + '/productos/' + vid);
          $("#divVid video")[0].load();
        }
      };

      $scope.borrarfoto = function(index) {
        $http.delete('/api/users/borrarfoto/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre) + '/' + index)
          .then(function() {
            $scope.userData.productsImg.splice(index, 1);
            Auth.updateProfile($scope.userData);
            afterUpload();
          });
      };

      $scope.borrarfoto = function(index) {
        $http.delete('/api/users/borrarfoto/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre) + '/' + index)
          .then(function() {
            $scope.userData.productsImg.splice(index, 1);
            Auth.updateProfile($scope.userData);
            afterUpload();
          });
      };

      $scope.borrarVideo = function(index) {
        $http.delete('/api/users/borrarVideo/' + $scope.userData._id + '/' + encodeURIComponent($scope.userData.sector_name) + '/' + encodeURIComponent($scope.userData.categoria_name) + '/' + encodeURIComponent($scope.userData.nombre) + '/' + index)
          .then(function() {
            $scope.userData.videos.splice(index, 1);
            Auth.updateProfile($scope.userData);
            afterUploadVideo();
          });
      };

      $scope.removerFavorito = function(index) {
        $scope.userData.favoritos.splice(index, 1);
        $scope.listaFavoritos.splice(index, 1);
        Auth.updateProfile($scope.userData);
        // $scope.userData = Auth.getCurrentUser();
      };

      $scope.removerArticulo = function(index) {
        $scope.userData.articulosGuardados.splice(index, 1);
        $scope.listaPosts.splice(index, 1);
        Auth.updateProfile($scope.userData);
        // $scope.userData = Auth.getCurrentUser();

      };

      $scope.getFavoritos = function() {
        for (var i = 0; i < $scope.userData.favoritos.length; i++) {
          $scope.pedirFavoritos($scope.userData.favoritos[i], i);
        }
      };

      $scope.getPosts = function() {
        for (var x = 0; x < $scope.userData.articulosGuardados.length; x++) {
          $scope.pedirPosts($scope.userData.articulosGuardados[x], x);
        }
      };

      $scope.agregarEmailExtra = function() {
        $("#emails").append('<div class="row">' +
          '<div class="col-xs-2 bot"><img src="assets/images/icons/destacado-correo.svg" class="img-ico img-center" alt="">' +
          '</div><div class="col-xs-10 bot"><input type="text" class="form-control input-yellow" placeholder="Insertar correo">' +
          '</div></div>');
      }


    })

})();
