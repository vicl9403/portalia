'use strict';

describe('Component: PerfilComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var PerfilComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    PerfilComponent = $componentController('PerfilComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
