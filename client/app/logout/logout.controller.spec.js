'use strict';

describe('Component: LogoutComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var LogoutComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    LogoutComponent = $componentController('LogoutComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
