'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/logout', {
        template: '<logout></logout>'
      });
  });
