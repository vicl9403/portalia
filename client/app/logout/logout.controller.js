'use strict';
(function(){

class LogoutComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('logout', {
    templateUrl: 'app/logout/logout.html',
    controller: LogoutComponent
  }).controller('LogoutController', function ( $scope, $http, Auth, $window ) {

    Auth.logout();
    $window.location.href = "/";


  });

})();
