'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/graciaspromocion', {
        template: '<graciaspromocion></graciaspromocion>'
      });
  });
