'use strict';

(function(){

class GraciaspromocionComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('graciaspromocion', {
    templateUrl: 'app/graciaspromocion/graciaspromocion.html',
    controller: GraciaspromocionComponent,
    controllerAs: 'graciaspromocionCtrl'
  });

})();
