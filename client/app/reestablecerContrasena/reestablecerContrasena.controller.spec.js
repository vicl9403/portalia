'use strict';

describe('Component: ReestablecerContrasenaComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ReestablecerContrasenaComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    ReestablecerContrasenaComponent = $componentController('ReestablecerContrasenaComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
