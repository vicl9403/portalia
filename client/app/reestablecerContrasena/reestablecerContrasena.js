'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/reestablecerContrasena', {
        template: '<reestablecer-contrasena></reestablecer-contrasena>'
      });
  });
