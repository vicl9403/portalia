'use strict';
(function() {

  class ReestablecerContrasenaComponent {
    constructor() {
      this.message = 'Hello';
    }
  }

  angular.module('portaliaApp')
    .component('reestablecerContrasena', {
      templateUrl: 'app/reestablecerContrasena/reestablecerContrasena.html',
      controller: ReestablecerContrasenaComponent
    }).controller("resetCrl", function($scope, $http) {
      $scope.data = {};
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      $scope.enviar = function() {


        // Verificar que haya escrito algo
        if( $scope.data.email == '' || $scope.data.email == undefined )
          swal("Error","por favor escribe una dirección de correo","error");
        else {
            $http.post('/api/users/check', {
                email: $scope.data.email
            })
                .then(function (res) {
                    // $scope.checkedEmail = true;
                    if (res.status == 201) {
                        $scope.isValidEmail = false;
                        swal("Error", "no pudimos enviar el correo de recuperación, por favor verifica los datos", "error");
                    } else if (res.status == 200) {

                        $scope.isValidEmail = true;
                        $http.post('/api/passwordRecoverys', $scope.data)
                            .then(function (response) {
                                $('#modal-success').modal('show');
                                $('#email').val("");
                            })
                            .catch(function (err) {
                                swal("Error", "Ups detectamos un error intenta de nuevo o ponte en contacto con soporte@portaliaplus.com", "error");
                            });
                    }
                })
        }




      };
    });

})();
