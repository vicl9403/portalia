'use strict';

angular.module('portaliaApp')
	.config(function ($routeProvider) {
		$routeProvider
			.when('/cambiarPass/:key', {
				template: '<cambiar-pass></cambiar-pass>'
			});
	});
