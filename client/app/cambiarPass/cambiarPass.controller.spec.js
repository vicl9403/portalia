'use strict';

describe('Component: CambiarPassComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CambiarPassComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    CambiarPassComponent = $componentController('CambiarPassComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
