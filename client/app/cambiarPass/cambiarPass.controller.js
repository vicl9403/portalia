'use strict';
(function () {

	class CambiarPassComponent {
		constructor() {
			this.message = 'Hello';
		}
	}

	angular.module('portaliaApp')
		.component('cambiarPass', {
			templateUrl: 'app/cambiarPass/cambiarPass.html',
			controller: CambiarPassComponent
		}).controller('cambiarCrl', function ($scope, $http, Auth, $routeParams) {
			$scope.data = {};

			$scope.changePassword = function () {
				$http.post('/api/users/reset/' + $routeParams.key, $scope.data)
					.then(function (res) {
						$('#modalExito').modal('show');
						console.log(res);
					})
					.catch(function (err) {
						console.log(err);
					});
			};
		});

})();
