'use strict';
(function() {


  angular.module('portaliaApp')
    .component('registroExitoso', {
      templateUrl: 'app/registroExitoso/registroExitoso.html',
    })
    .controller('registroExitosoCrl', function($scope, $http, $window, Auth) {

      $scope.agregarEmailExtra = function() {
        $("#emails").append('<div class="row">' +
          '<div class="col-xs-2 bot"><img src="assets/images/icons/destacado-correo.svg" class="img-ico img-center" alt="">' +
          '</div><div class="col-xs-10 bot"><input type="text" class="form-control input-yellow" placeholder="Insertar correo">' +
          '</div></div>');
      }

      $scope.data = {};
      $scope.data.to = [];

      Auth.getCurrentUser(function(res) {
        $scope.userData = res;
        $scope.data.subject = 'Correo de invitacion a Portalia Plus de ' + $scope.userData.nombre;
        $scope.data.name = $scope.userData.nombre;
        $scope.data._id = $scope.userData._id;
      });

      function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

      $scope.enviar = function() {

        var emails = document.getElementById('emails').getElementsByTagName('input');
        for (var i = 0; i < emails.length; i++) {
          if (validateEmail(emails[i].value))
            $scope.data.to.push(emails[i].value);
        }

        $http.post('/api/email/invitacion', $scope.data)
          .then(function(response) {
            console.log(response);
            if (response.status === 200) {
              $window.alert('Correos enviados');
            } else {
              //$window.alert('hubo un problema enviando el correo, intentelo de nuev');
            }
          })
          .catch(function(err) {
            console.log(err);
          });

      };

      $scope.redirect = function(){
        $window.location.href = '/perfil-usuario'
      }


    });

})();
