'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/registroExitoso', {
        template: '<registro-exitoso></registro-exitoso>'
      });
  });
