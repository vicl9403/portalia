'use strict';
(function(){

class CotizacionesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')

	.component('cotizaciones', {
    	templateUrl: 'app/cotizaciones/cotizaciones.html',
    	controller: CotizacionesComponent
	}).controller('cotizacionesCtr', function ($scope, $http, $window) {

		$scope.subsectores = {};

		$scope.data = {
			"nombre"  	: "",
			"email"	  	: "",
			"telefono"	: "",
			"sector"	: "",
			"solicitud"	: "",
		}

  		$scope.getSectors = function()
  		{
			// Obtener los sectores
			$http.get('/api/sectors/')
				.then(function(res)
				{
					$scope.sectors = res.data;
				});
  		}
  		$scope.validEmail = function (email) {
  		    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		    return re.test(email);
  		}
      $scope.validName = function (name) {
          var re = /^(?=(?:[^\A-Za-z0-9]*[\A-Za-z0-9]){2})[~,?,!]*\S+(?: \S+){0,}$/;
          return re.test(name);
      }
  		$scope.validateForm = function()
  		{
  			var errores = 0;


        // Validación del nombre
        if($scope.data.nombre == undefined)
        {
          errores ++;
          $scope.invalidNombre = true;
        }
  			else if($scope.data.nombre == '' || $scope.data.nombre.length >= 255 || !$scope.validName($scope.data.nombre))
  			{
  				errores ++;
  				$scope.invalidNombre = true;
  			}
  			else
  				$scope.invalidNombre = false;

        // Validación del mail
        if($scope.data.email == undefined)
        {
          errores ++;
          $scope.invalidEmail = true;
        }
  			else if($scope.data.email == '' || $scope.data.email.length > 255 || !$scope.validEmail($scope.data.email) )
  			{
  				errores ++;
  				$scope.invalidEmail = true;
  			}
  			else
  				$scope.invalidEmail = false;

        // Validación del teléfono
        if($scope.data.telefono == undefined)
        {
          errores ++;
          $scope.invalidTelefono = true;
        }
        else if($scope.data.telefono == '' || $scope.data.telefono.toString().length > 16 )
        {
          errores ++;
          $scope.invalidTelefono = true;
        }
  			else
  				$scope.invalidTelefono = false;

        // Validación de solicitud
        if( $scope.data.solicitud == undefined)
        {
          errores ++;
          $scope.invalidSolicitud = true;
        }
  			else if($scope.data.solicitud == '' || $scope.data.solicitud.length > 1024)
  			{
  				errores ++;
  				$scope.invalidSolicitud = true;
  			}
  			else
  				$scope.invalidSolicitud = false;

  			return errores;
  		}
  		$scope.sendCotizacion = function()
  		{
  			if ($scope.validateForm() > 0)
  				swal("Error en el formulario", "por favor llena los campos marcados con color rojo con el formato indicado", "error");
  			else
  			{
  				$http.post('/api/cotizaciones/nuevaCotizacion', $scope.data).then(function (response) {
					window.location.href = "/cotizaciones/exito";
  				}).catch(function (err) {
  				  swal("Error","Ups detectamos un error intenta de nuevo o ponte en contacto con soporte@portaliaplus.com","error");
  				  console.log(err);
  				});
  			}
  		}

  		angular.element(document).ready(function () {
			$scope.getSectors();
	    });

	});


})();
