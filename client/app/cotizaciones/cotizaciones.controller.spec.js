'use strict';

describe('Component: CotizacionesComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CotizacionesComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    CotizacionesComponent = $componentController('CotizacionesComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
