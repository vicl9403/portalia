'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/cotizaciones', {
        template: '<cotizaciones></cotizaciones>'
      });
  });
