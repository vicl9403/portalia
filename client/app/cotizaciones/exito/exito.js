'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/cotizaciones/exito', {
        template: '<exito></exito>'
      });
  });
