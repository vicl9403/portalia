'use strict';
(function(){

class ExitoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('exito', {
    templateUrl: 'app/cotizaciones/exito/exito.html',
    controller: ExitoComponent
  });

})();
