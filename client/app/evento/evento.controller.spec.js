'use strict';

describe('Component: EventoComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var EventoComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    EventoComponent = $componentController('EventoComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
