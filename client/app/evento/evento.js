'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/evento', {
        template: '<evento></evento>'
      });
  });
