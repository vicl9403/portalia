'use strict';
(function(){

class EventoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('evento', {
    templateUrl: 'app/evento/evento.html',
    controller: EventoComponent
  });

})();
