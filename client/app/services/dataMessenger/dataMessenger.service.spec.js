'use strict';

describe('Service: dataMessenger', function () {

  // load the service's module
  beforeEach(module('portaliaApp'));

  // instantiate service
  var dataMessenger;
  beforeEach(inject(function (_dataMessenger_) {
    dataMessenger = _dataMessenger_;
  }));

  it('should do something', function () {
    expect(!!dataMessenger).toBe(true);
  });

});
