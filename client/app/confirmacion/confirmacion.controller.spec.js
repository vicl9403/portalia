'use strict';

describe('Component: ConfirmacionComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ConfirmacionComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    ConfirmacionComponent = $componentController('ConfirmacionComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
