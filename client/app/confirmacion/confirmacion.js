'use strict';

angular.module('portaliaApp')
	.config(function($routeProvider) {
		$routeProvider
			.when('/confirmacion/:adeudo', {
				template: '<confirmacion></confirmacion>'
			});
	});
