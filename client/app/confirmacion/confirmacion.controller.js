'use strict';
(function () {

	angular.module('portaliaApp')
		.component('confirmacion', {
			templateUrl: 'app/confirmacion/confirmacion.html'
		}).controller('confirmacionCrl', function ($scope, $http, $window, $routeParams) {
			$scope.adeudo = $routeParams.adeudo;
			$scope.data = {
				paymentId: $routeParams.paymentId,
				token: $routeParams.token,
				PayerID: $routeParams.PayerID
			};

			$scope.pagar = function () {
				$http.post('api/paypal', $scope.data)
					.then(function (response) {
						console.log(response);
						$window.location.href = '/';
					}).catch(function (err) {
						console.log(err);
					});
			};
		});

})();
