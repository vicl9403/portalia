'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/contrasenaPreregistro/:key', {
        template: '<contrasena-preregistro></contrasena-preregistro>'
      });
  });

