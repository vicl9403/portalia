'use strict';
(function(){

class ContrasenaPreregistroComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('contrasenaPreregistro', {
    templateUrl: 'app/contrasenaPreregistro/contrasenaPreregistro.html',
    controller: ContrasenaPreregistroComponent
  }).controller('cambiarPreregistroCtr', function ($scope, $http, Auth, $routeParams) {
		$scope.data = {};

		$scope.changePassword = function () {
			$http.post('/api/users/reset/' + $routeParams.key, $scope.data)
				.then(function (res) {
					$('#modalExito').modal('show');
					console.log(res);
				})
				.catch(function (err) {
					console.log(err);
				});
		};


	});

})();
