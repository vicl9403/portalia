'use strict';

describe('Component: ContrasenaPreregistroComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ContrasenaPreregistroComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    ContrasenaPreregistroComponent = $componentController('ContrasenaPreregistroComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
