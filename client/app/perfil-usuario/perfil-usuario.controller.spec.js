'use strict';

describe('Component: PerfilUsuarioComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var PerfilUsuarioComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    PerfilUsuarioComponent = $componentController('PerfilUsuarioComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
