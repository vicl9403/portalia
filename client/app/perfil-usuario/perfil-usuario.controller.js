'use strict';
(function(){

class PerfilUsuarioComponent {
  constructor(  ) {
  }
}

// class PerfilUsuarioComponent extends Base {
//   constructor( $rootScope ) {
//     super($rootScope);
//   }
// }

angular.module('portaliaApp')
  .component('perfilUsuario', {
    templateUrl: 'app/perfil-usuario/perfil-usuario.html',
    controller: PerfilUsuarioComponent,
  })
  .controller('PerfilUsuarioController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope, Upload, FileUploader ) {

    var uploader = $scope.uploader = new FileUploader({
      url: '/upload',
      name: 'imagesFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|xls|docx|doc|pdf|csv'.indexOf(type) !== -1;
      },
    });

    uploader.onBeforeUploadItem = function(item) {

      $scope.isLoading = true;

      item.formData.push({
        path: `${ $scope.user.sector_name }/${ $scope.user.categoria_name }/${ $scope.user._id }/`
      });

      item.formData.push({
        name: 'foto-perfil'
      });

      $scope.user.profilePic = '';

    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
      $scope.isLoading = false;
      swal('Error','Tuvimos un problema al subir la foto, es probable que hayas excedido el tamaño permitido o estes intentando subir un catálogo con formato inválido','error')
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {

      $scope.isLoading = false;

      // Limpiar la url de respuesta
      let url = response;


      $scope.user.hasPic = true;
      $scope.user.profilePic = url;

      $http.patch('/api/users/' + $scope.user._id , $scope.user )
        .then( function ( res ) {

          $scope.user = res.data;

          $('.profile-userpic img').attr('src', url + '?decache=' + Math.random());

          swal( 'Éxito' , 'tu catálogo se ha subido correctamente.' , 'success' );

        })

    };

    // Array necesario para la función de las visitas
    $scope.visits = [];
    $scope.getTrueVisits = function ( visits) {
      $scope.anonymous = 0;
      for ( var i=0; i<visits.length ; i++ )
      {
        if( visits[i] != null )
          $http.get('/api/users/' + visits[i] , $scope.data)
            .then( function (res) {
              $scope.visits.push(res.data);
            })

        else
          $scope.anonymous ++ ;
      }

      // $scope.visits.push(anonymous);

    }


    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      if ( user == undefined )
        $window.location.href = '/';

      $scope.user = user;
      $scope.activeQuote = false;
      // console.log(pushcrew.subscriberId);

      $scope.getTrueVisits( user.visitasGene );
      if(user.pushSuscriberId.length == 0){
         _pe.addProfileId(user._id);
         user.pushSuscriberId = user._id;
         $http.patch('/api/users/' + user._id , user );
      }

    })

    // Función para iterar dentro de un loop
    $scope.range = function(min, max, step) {
      step = step || 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
        input.push(i);
      }
      return input;
    };

    $scope.changeNotifications = function(choice) {
      $scope.user.wantsNotifications = choice;
      $http.patch('/api/users/' + $scope.user._id , $scope.user );
    }

    $scope.uploadProfilePic = function (file) {
      $scope.uploader.uploadAll();
    };

  });

})();
