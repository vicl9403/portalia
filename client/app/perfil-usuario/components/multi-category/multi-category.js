'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/multi-categoria', {
        template: '<multi-category></multi-category>'
      });
  });
