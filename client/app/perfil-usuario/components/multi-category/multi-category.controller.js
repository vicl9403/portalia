'use strict';
(function(){

class MultiCategoryComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('multiCategory', {
    templateUrl: 'app/perfil-usuario/components/multi-category/multi-category.html',
    controller: MultiCategoryComponent
  }).controller('MultiCategoryController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope) {


    $scope.sectors = {};
    $scope.subSectors = {};

    $scope.selectedSector = '';
    $scope.selectedCategory = '';

    $scope.subcategories = [];

    $scope.limit = {
      gratis : 2,
      select : 5,
      plus : 12
    };

    $scope.getSectors = function() {
      // Obtener los sectores
      $http.get('/api/sectors/')
        .then(function(res) {
          $scope.sectors = res.data;
        });
    };

    $scope.$watch('selectedSector', function( selectedSector ) {
      for ( var i=0 ; i < $scope.sectors.length ; i++ ) {
        if ( $scope.sectors[i].name == $.trim(selectedSector) ) {
          $scope.subSectors = $scope.sectors[i].subsectores;
          $scope.selectedCategory = '';
        }
      }
    });

    $scope.addSubcategory = function () {

      if ( $scope.subcategories.length >= $scope.limit[ $scope.user.paquete ] ) {
        swal ( 'Advertencia', 'En este paquete solo puedes tener ' + $scope.limit[ $scope.user.paquete ] + '  subcategorias', 'warning');
        return;
      }
      if ( $scope.selectedCategory == '' )
      {
        swal ( 'Advertencia', 'Por favor selecciona una categoría antes de agregar', 'warning');
        return;
      }
      if( ! $scope.subcategories.includes( $scope.selectedCategory ) && $scope.selectedCategory.trim() != $scope.user.categoria_name)
        $scope.subcategories.push( $scope.selectedCategory );
      else
        swal ( 'Advertencia', 'Ya tienes agregada esa categoría, no la puedes agregar nuevamente', 'warning');
    };

    $scope.deleteSubcategory = function ( index ) {
      $scope.subcategories.splice(index, 1);
    };

    $scope.saveSubcategories = function () {
      var data = {
        subcategories : $scope.subcategories
      };

      $http.patch('/api/users/' + $scope.user._id, data ).then(function (response) {
        if ( response.status == 200 )
          swal( "Exito", 'Se guardaron los cambios correctamente', 'success');
        else
          swal( 'Error', 'No se pudieron guardar los cambios, por favor intenta de nuevo más tarde', 'error');
      });

    };

    $scope.redirect = function ( url ) {
      $window.location.href = url;
    };

    angular.element(document).ready(function () {

      Auth.getCurrentUser(function (user) {

        if( user.role != 'empresa')
          $window.location.href = '/perfil-usuario';

        $scope.user = user;

        $scope.subcategories = user.subcategories;

        $scope.getSectors();

      });

    });

  });

})();
