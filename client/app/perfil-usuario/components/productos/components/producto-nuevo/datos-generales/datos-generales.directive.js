'use strict';

angular.module('portaliaApp')
    .directive('productoDatosGenerales', () => ({
        templateUrl: 'app/perfil-usuario/components/productos/components/producto-nuevo/datos-generales/datos-generales.html',
        restrict: 'E',
        controller: 'ProductoDatosGeneralesController',
        controllerAs: 'nav'
    }));
