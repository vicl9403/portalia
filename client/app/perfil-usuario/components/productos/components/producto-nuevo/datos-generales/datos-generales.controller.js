'use strict';

angular.module('portaliaApp')
  .controller('ProductoDatosGeneralesController', function($scope, $location, $window, $http, Auth, $timeout, $route, $routeParams) {

    $scope.init = function () {
      $("#summernote").summernote({
        height: 300,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true,
        placeholder: 'Ingresa la descripción del producto, recuerda que mientras mejor sea tu descripción tendrás más posibilidades de generar interés en los compradores.'
      });

      $scope.productData = {};

      $scope.getProductCategories();

    }

    $scope.getProductCategories = function () {
      $http.get('/api/product_categories')
        .then( function (res) {
          $scope.categories = res.data;
          $scope.category = $scope.categories[0] ;
          $scope.subCategory = $scope.category.subCategories[0];
        })
    }

    $scope.createProduct = function(isValidForm) {

      if (isValidForm && $("#summernote").summernote('code') != '<p><br></p>' ) {

        $scope.productData.category = $scope.category.name ;
        $scope.productData.subCategory = $scope.subCategory.name ;
        $scope.productData.description = $("#summernote").summernote('code');
        $scope.productData.user = $scope.$parent.user._id;

        $http.post('/api/products' , $scope.productData )
          .then( function (res) {

            // Hacer el push al usuario arreglo productos para procesarlo posteriormente
            if( $scope.user.products == null )
              $scope.user.products = [];
            if( ! $scope.user.products.includes($scope.productData.name ) ) {
              $scope.user.products.push($scope.productData.name);
              $http.patch('/api/users/' + $scope.user._id, $scope.user);
            }

            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

            $('#productForm').addClass('animated bounceOutLeft').one(animationEnd , function () {

              $scope.$parent.activeStep = 2  ;
              $scope.$parent.newProduct = res.data;


              $(this).removeClass('animated ' + 'bounceOutLeft');

              $scope.$apply();

              $('#datos-tecnicos').addClass('animated bounceInRight');

            });

          })

      }
      else  if( $("#summernote").summernote('code') == '<p><br></p>' ) {
        swal("Error", "Tienes que redactar una descripción para tu producto", "error");
      }
      else
        swal("Error", "Hay errores en el formulario, por favor corrige los campos marcados en rojo", "error");

    }

  });
