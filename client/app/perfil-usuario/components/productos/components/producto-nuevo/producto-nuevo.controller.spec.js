'use strict';

describe('Component: ProductoNuevoComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ProductoNuevoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    ProductoNuevoComponent = $componentController('producto-nuevo', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
