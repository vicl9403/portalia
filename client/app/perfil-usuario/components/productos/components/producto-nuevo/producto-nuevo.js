'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/productos/nuevo', {
        template: '<producto-nuevo></producto-nuevo>'
      });
  });
