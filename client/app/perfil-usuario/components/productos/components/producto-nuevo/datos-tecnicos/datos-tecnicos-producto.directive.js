'use strict';

angular.module('portaliaApp')
    .directive('productoDatosTecnicos', () => ({
        templateUrl: 'app/perfil-usuario/components/productos/components/producto-nuevo/datos-tecnicos/datos-tecnicos-producto.html',
        restrict: 'E',
        controller: 'ProductoDatosTecnicosController',
        controllerAs: 'nav'
    }));
