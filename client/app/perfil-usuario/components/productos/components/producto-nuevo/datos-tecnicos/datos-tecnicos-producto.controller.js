'use strict';

angular.module('portaliaApp')
  .controller('ProductoDatosTecnicosController', function($scope, $location, $window, $http, Auth, $timeout, $route, $routeParams, FileUploader) {

    var uploader = $scope.uploader = new FileUploader({
      // La url se setea una vez que se tiene el usuario
      url: 'datasheet',
      name: 'file',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|xls|docx|doc|pdf|csv'.indexOf(type) !== -1;
      },
    });

    uploader.onBeforeUploadItem = function(item) {

      $scope.isLoading = true;

      item.url = '/upload';
      item.formData = [{
        path: '/client/assets/uploads/productos/' + $scope.$parent.newProduct._id
      }]

    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
      $scope.isLoading = false;
      swal('Error','Tuvimos un problema al subir el catálogo, es probable que hayas excedido el tamaño permitido o estes intentando subir un catálogo con formato inválido','error')
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      $scope.isLoading = false;
      $scope.fileName = '';
    };
    uploader.onCompleteAll = function( fileItem, response, status, headers ) {
    };

    $scope.$parent.$watch('newProduct', function(newProduct) {

      if( newProduct != undefined && newProduct != null ) {
        $scope.init();
        $scope.productExists = true;
      }

    });

    $scope.init = function () {

      $scope.isLoading = false;
      $("#price").maskMoney();
      $scope.newProduct = $scope.$parent.newProduct;

    };

    $scope.saveProductInfo = function () {

      $scope.$parent.newProduct.price = $("#price").val().replace(',','');

      console.log( $scope.$parent.newProduct );


      $scope.isLoading = true;

      $http.patch('/api/products/' + $scope.$parent.newProduct._id  , $scope.$parent.newProduct )
        .then( function ( res ) {

          var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

          $('#datos-tecnicos').addClass('bounceOutLeft').one(animationEnd , function () {

            $scope.$parent.activeStep = 3  ;
            $scope.$parent.newProduct = res.data;

            $(this).removeClass('animated ' + 'bounceOutLeft');

            $scope.$apply();

            $('#imagenes-producto').addClass('animated bounceInRight');

          });

        })

      $scope.uploadDatasheet();

    };

    $scope.uploadDatasheet = function () {
      uploader.uploadAll();
    }

  });
