'use strict';

angular.module('portaliaApp')
  .controller('ProductoImagenesController', function($scope, $location, $window, $http, Auth, $timeout, $route, $routeParams) {


    $scope.$parent.$watch('newProduct', function(newProduct) {

      if( newProduct != undefined && newProduct != null ) {

        $scope.newProduct = newProduct;

        $('div#dropzone').dropzone({
          url: '/upload',
          init: function() {
            this.on("sending", function(file, xhr, formData){
              formData.append("path", "/client/assets/uploads/productos/" + $scope.$parent.newProduct._id  );
            });
            this.on("success", function(file, responseText) {

              if( Array.isArray($scope.newProduct.pictures) ){
                $scope.newProduct.pictures.push({
                  url : responseText,
                  mainPicture: false
                });
              }
              else {
                $scope.newProduct.pictures = [
                  {
                    url : responseText,
                    mainPicture: true
                  }
                ];
              }

              $http.patch('/api/products/savePictures/' + $scope.newProduct._id , $scope.newProduct.pictures )
                .then(function (res) {
                  $scope.$parent.newProduct = res.data;
                  swal({
                      title: "¡Listo!",
                      text: "¡Tu producto ha sido agregado exitosamente, en cualquier momento que quieras puedes agregar otro más, o modificar los existentes!",
                      type: "success",
                      confirmButtonText: "Entendido!",
                      closeOnConfirm: false
                    });

                })

            });
          }
        });

      }

    });



  });
