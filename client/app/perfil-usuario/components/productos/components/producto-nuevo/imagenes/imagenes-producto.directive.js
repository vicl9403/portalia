'use strict';

angular.module('portaliaApp')
    .directive('imagenesProducto', () => ({
        templateUrl: 'app/perfil-usuario/components/productos/components/producto-nuevo/imagenes/imagenes-producto.html',
        restrict: 'E',
        controller: 'ProductoImagenesController',
        controllerAs: 'nav'
    }));
