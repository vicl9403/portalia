'use strict';

(function(){

class ProductoNuevoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('productoNuevo', {
    templateUrl: 'app/perfil-usuario/components/productos/components/producto-nuevo/producto-nuevo.html',
    controller: ProductoNuevoComponent,
    controllerAs: 'productoNuevoCtrl'
  }).controller('NuevoProductoController' ,function($scope, $http, Auth, Upload, $timeout, $window, $routeParams, FileUploader){

    $scope.init = function () {

      Auth.getCurrentUser( function (user) {
        $scope.user = user;
      })

      $scope.newProduct = null;
      $scope.activeStep = 1;
    }

    $scope.changeToStep = function ( newStep ) {

      if( newStep != 1 && $scope.newProduct == null ) {
        // swal( "Advertencia" , 'Para poder avanzar es necesario que captures los datos generales.','warning');
        // return;
      }
      $scope.activeStep = newStep;
    }

  });



})();
