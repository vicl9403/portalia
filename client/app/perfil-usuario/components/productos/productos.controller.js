'use strict';

(function(){

class ProductosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('productos', {
    templateUrl: 'app/perfil-usuario/components/productos/productos.html',
    controller: ProductosComponent,
    controllerAs: 'productosCtrl'
  }).controller('ProductosController' ,function($scope, $http, Auth, Upload, $timeout, $window, $routeParams, FileUploader){

    Auth.getCurrentUser( function (user) {

      $scope.user = user;

      $scope.getUserProducts( user );


    })

    $scope.getUserProducts = function ( user ) {
      $http.get('/api/products/userProducts/' + user._id )
        .then( function (res) {
          $scope.products = res.data;

          setTimeout(function(){
            $("#products-table").dataTable({
              "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
              }
            });
            }, 100);


        })
    }

    $scope.deleteProduct = function ( id ) {
      swal({
          title: "¿Estás seguro?",
          text: "Si eliminas este producto no se visualizará más en tu perfil",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, eliminar",
          closeOnConfirm: false
        },
        function(){

          $http.delete( '/api/products/' + id )
            .then( function (res) {

              $http.get('/api/products/userProducts/' + $scope.user._id )
                .then( function (res) {

                  let products = res.data;
                  let newProducts = [];

                  for( let i = 0 ; i < products.length ; i ++ ) {
                    newProducts.push( products[i].name );
                  }

                  $scope.user.products = newProducts;

                  $http.patch('/api/users/' + $scope.user._id , $scope.user )
                    .then( res => {
                      $window.location = '/perfil-usuario/productos';
                    });

                })


            })
        });
    }

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY');
    }
  });

})();
