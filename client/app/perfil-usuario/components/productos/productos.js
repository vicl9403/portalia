'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/productos', {
        template: '<productos></productos>'
      });
  });
