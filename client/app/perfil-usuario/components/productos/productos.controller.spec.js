'use strict';

describe('Component: ProductosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ProductosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    ProductosComponent = $componentController('productos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
