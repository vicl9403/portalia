'use strict';

(function(){

class EditarProductoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('editarProducto', {
    templateUrl: 'app/perfil-usuario/components/productos/editar-producto/editar-producto.html',
    controller: EditarProductoComponent,
    controllerAs: 'editarProductoCtrl'
  }).controller('EditarProductoController' ,function($scope, $http, Auth, Upload, $timeout, $window, $routeParams, FileUploader){

    Auth.getCurrentUser( function (user) {
      $scope.user = user;

      $http.get('/api/products/' + $routeParams.id )
        .then( function (res) {

          $scope.product = res.data;
          $scope.newProduct = res.data;

          $scope.getProductCategories();

          $("#summernote").summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true,
            placeholder: 'Ingresa la descripción del producto, recuerda que mientras mejor sea tu descripción tendrás más posibilidades de generar interés en los compradores.'
          });
          $('#summernote').summernote('code', $scope.product.description);

          $("#price").maskMoney();
          $("#price").val( $scope.product.price );
          $("#price").focus();

        });



    })



    $scope.$watch('categories.selectedOption', function (value) {
      if( value != undefined ) {
        for (let i = 0; i < $scope.categories.options.length; i++) {
          if ($scope.categories.options[i].name == value.name ) {

            $scope.subCategories = {
              options: $scope.categories.options[i].subCategories,
              selectedOption: { name:  $scope.product.subCategory } //This sets the default value of the select in the ui
            };
          }
        }
      }
    })

    $scope.getProductCategories = function () {
      $http.get('/api/product_categories')
        .then( function (res) {

          $scope.categories = {
            options: res.data,
            selectedOption: { name:  $scope.product.category } //This sets the default value of the select in the ui
          };

        })
    }

    $scope.setAsMainImage = function (url) {

      for( let i = 0; i < $scope.product.pictures.length ; i++ ) {
        if( $scope.product.pictures[i].url == url ) {
          $scope.product.pictures[i].mainPicture = true;
        }
        else {
          $scope.product.pictures[i].mainPicture = false;
        }
      }

    }

    $scope.deleteImage = function ( index ) {
      $scope.product.pictures.splice(index, 1);
    }

    $scope.saveChanges = function () {

      $scope.product.category = $scope.categories.selectedOption.name;
      $scope.product.subCategory = $scope.subCategories.selectedOption.name;
      $scope.product.price = $("#price").val().replace(',','');
      $scope.product.description = $("#summernote").summernote('code');

      $http.patch('/api/products/' + $scope.product._id , $scope.product )
        .then( function (res) {
          $scope.product = res.data;

          // Hacer el push al usuario arreglo productos para procesarlo posteriormente
          if( $scope.user.products == null )
            $scope.user.products = [];
          if( ! $scope.user.products.includes( $scope.product.name ) ) {
            $scope.user.products.push($scope.product.name);
            $http.patch('/api/users/' + $scope.user._id, $scope.user);
          }

          swal("Éxito",'Los cambios se guardaron correctamente', 'success');
        })


      $http.patch('/api/products/savePictures/' + $scope.product._id , $scope.product.pictures )
        .then( function (res) {
        })
    }
  });

})();
