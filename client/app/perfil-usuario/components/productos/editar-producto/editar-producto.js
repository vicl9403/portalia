'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/editar-producto/:id', {
        template: '<editar-producto></editar-producto>'
      });
  });
