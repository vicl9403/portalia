'use strict';
(function(){

class ImagenesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('imagenes', {
    templateUrl: 'app/perfil-usuario/components/imagenes/imagenes.html',
    controller: ImagenesComponent
  }).controller('UserImgController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope, FileUploader)
  {
    var uploader = $scope.uploader = new FileUploader({
      url: '/upload',
      name: 'imagesFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|xls|docx|doc|pdf|csv'.indexOf(type) !== -1;
      },
    });



    Auth.getCurrentUser(function (user) {

      $scope.user = user;

    });


    $scope.deleteImage = function(index) {

      swal({
          title: "Estas seguro?",
          text: "No podrás recuperar la imágen una vez que la elimines",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, eliminar!",
          closeOnConfirm: false
        },
        function(){

          $scope.user.productsImg.splice(index, 1);

          Auth.updateProfile($scope.user);

          swal("Eliminada!", "Imagen eliminada exitosamente", "success");

        });


    };



    // a sync filter
    uploader.filters.push({
      name: 'syncFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        console.log('syncFilter');
        return this.queue.length < 10;
      }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
      // console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
      // console.log( fileItem);
      // console.info('onAfterAddingFile', fileItem);
      if ( $scope.user.paquete == 'gratis' && $scope.user.productsImg.length >= 5)
      {
        swal( "Error", "Este plan no te permite subir más de 5 imágenes", "error");
        fileItem.remove();
      }

      if ( $scope.user.paquete == 'select' && $scope.user.productsImg.length >= 15)
      {
        swal( "Error", "Este plan no te permite subir más de 5 imágenes", "error");
        fileItem.remove();
      }

    };
    uploader.onAfterAddingAll = function(addedFileItems) {
      // console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {

      $scope.isLoading = true;

      item.formData.push({
        path: `${ $scope.user.sector_name }/${ $scope.user.categoria_name }/${ $scope.user._id }/imagenes/`
      });

    };
    uploader.onProgressItem = function(fileItem, progress) {
      // console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
      // console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      // console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
      // console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
      // console.info('onCancelItem', fileItem, response, status, headers);
      swal( 'Cancalación!' , 'Se cancelo la subida de las imágenes, por favor verifica las imágenes que si lograron subirse a tu perfil', 'warning');
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
      console.log( fileItem );
      console.log( response );
      console.log( status );
      console.log( headers );

      if( $scope.user.productsImg == undefined )
        $scope.user.productsImg = [];

      $scope.user.productsImg.push( response );
      $http.patch('/api/users/' + $scope.user._id , $scope.user )
        .then( res => {
          console.log( res );
        });

      $http.get('/api/users/' + $scope.user._id )
        .then( function (res) {
          $scope.user = res.data;
          // swal( 'Exito!' , 'Las imágenes se subieron correctamente', 'success');
        });
    };
    uploader.onCompleteAll = function( fileItem, response, status, headers ) {

      var actualImages = $scope.user.productsImg;

      $http.get('/api/users/' + $scope.user._id )
        .then( function (res) {
          $scope.user = res.data;
          if( actualImages != $scope.user.productsImg )
            swal( 'Exito!' , 'Las imágenes se subieron correctamente', 'success');
        });

    };

    // console.info('uploader', uploader);



  });


})();
