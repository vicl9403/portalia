'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/imagenes', {
        template: '<imagenes></imagenes>'
      });
  });
