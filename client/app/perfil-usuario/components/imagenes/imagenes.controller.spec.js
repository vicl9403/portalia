'use strict';

describe('Component: ImagenesComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ImagenesComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    ImagenesComponent = $componentController('ImagenesComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
