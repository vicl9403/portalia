'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/mis-cotizaciones/:id?', {
        template: '<mis-cotizaciones></mis-cotizaciones>'
      });
  });
