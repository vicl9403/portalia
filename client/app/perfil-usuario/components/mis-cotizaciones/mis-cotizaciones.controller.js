'use strict';
(function(){

class MisCotizacionesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('misCotizaciones', {
    templateUrl: 'app/perfil-usuario/components/mis-cotizaciones/mis-cotizaciones.html',
    controller: MisCotizacionesComponent
  }).controller('MisCotizacionesController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope) {

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;
      $scope.activeQuote = false;

      $http.get('/api/quotes/getUserQuotes/' + user._id ).then(function (res) {
        $scope.quotes = res.data;
        $timeout(function() {
          $("#quotesTable").dataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            }
          });
        }, 100);

      });

      $http.get('/api/contacts/userContacts/' + user._id ).then(function (res) {
        $scope.contacts = res.data;

        $timeout(function() {

          $("#contactTable").dataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            }
          });

        }, 100);

      });

      $scope.activeContact = null;

    });

    $scope.showQuote = function ( id ) {
      $scope.activeQuote  = true;
      $scope.$broadcast ('getQuoteData', id );

      $timeout(function() {
        $("html, body").animate({
          scrollTop: $("#activeQuote").offset().top
        }, "slow");
      }, 200);

    }

    $scope.showContact = function ( contact ) {
      $scope.activeContact = contact;
      $timeout(function() {
        $("html, body").animate({
          scrollTop: $("#activeContact").offset().top
        }, "slow");
      }, 200);
    }

  })

})();
