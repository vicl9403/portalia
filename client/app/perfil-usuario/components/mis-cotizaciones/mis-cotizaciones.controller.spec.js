'use strict';

describe('Component: MisCotizacionesComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var MisCotizacionesComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    MisCotizacionesComponent = $componentController('MisCotizacionesComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
