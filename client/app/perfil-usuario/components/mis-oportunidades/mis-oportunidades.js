'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/mis-oportunidades/:page?', {
        template: '<mis-oportunidades></mis-oportunidades>'
      });
  });
