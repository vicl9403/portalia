'use strict';

describe('Component: MisOportunidadesComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var MisOportunidadesComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    MisOportunidadesComponent = $componentController('mis-oportunidades', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
