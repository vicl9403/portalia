'use strict';

(function(){

class MisOportunidadesComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('misOportunidades', {
    templateUrl: 'app/perfil-usuario/components/mis-oportunidades/mis-oportunidades.html',
    controller: MisOportunidadesComponent,
    controllerAs: 'misOportunidadesCtrl'
  }).controller('MisOportunidadesController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope, $routeParams) {

    moment.locale('es');

    $scope.range = function(min, max, step) {
      step = step || 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
        input.push(i);
      }
      return input;
    };

    $scope.getUserQuotes = function ( page ) {

      $scope.isLoading = true;

      $http.get('/api/quotation_user/getUserQuotes/' + $scope.user._id + `?page=${page}` )
        .then(function (res) {
          $scope.documents = res.data;

          $scope.isLoading = false;
        });
    };

    $scope.getTotalQuotes = function () {
      $http.get('/api/quotation_user/countModel')
        .then(function (res) {
          $scope.totalQuotes = res.data.count;
        });
    }

    $scope.getDateForHummans = function (date) {

      if (date == null)
        return 'No se especificó';

      return( moment(date).format('D MMMM YYYY') );
    }

    $scope.showQuote = function (quote) {
      $scope.activeQuote  = true;
      $scope.$broadcast ('getQuoteData', quote._id );

      if( $scope.user.paquete == 'gratis' ) {
        swal({
            title: "Te invitamos a probar uno de nuestros beneficios de clientes que ya cuentan con membresía.",
            text: "Por tiempo lomotado podrás ver el contacto del solicitante de tu cotización, que ya cuentan con su membresía.",
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "¡Entendido!",
            closeOnConfirm: false
          },
          function(){
            swal("Gracias!", "Recuerda que este beneficio es por tiempo limitado si no cuentas con una membresía", "success");
            $timeout(function () {
              $("html, body").animate({
                scrollTop: $("#activeQuote").offset().top
              }, "slow");
            }, 200);
          });
      }
      else {
        $timeout(function () {
          $("html, body").animate({
            scrollTop: $("#activeQuote").offset().top
          }, "slow");
        }, 200);
      }
    }

    $scope.quoted = function (entity) {
      entity.quoted = new Date();
      $http.patch('/api/quotation_user/' + entity._id , entity )
        .then( res=> {
          swal( 'Gracias por cotizar' , 'Muchas gracias por cotizar en nuestro portal, esperamos que logres cerrar el negocio exitosamente', 'success');
          $scope.getUserQuotes( ($routeParams.page != undefined) ? $routeParams.page - 1 : 0 );
        })
    }

    Auth.getCurrentUser(function (user) {

      $scope.user = user;
      $scope.activeQuote = false;

      $scope.isLoading = false;
      $scope.getUserQuotes( ($routeParams.page != undefined) ? $routeParams.page - 1 : 0 );

      $scope.getTotalQuotes();

    });

  });

})();
