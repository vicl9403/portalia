'use strict';
(function(){

class MisDatosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('misDatos', {
    templateUrl: 'app/perfil-usuario/components/mis-datos/mis-datos.html',
    controller: MisDatosComponent
  }).controller('MisDatosController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope) {
      $scope.editing = false;

    // Método para obtener los estados
    $scope.allStates = function () {
      // Obtener los estados
      $http.get('/api/estados/').then(function (res) {
        var states = res.data;

        $scope.states = [];
        //Obtener el array simple de subsectores
        for (var c = 0; c < states.length; c++) {
          $scope.states.push(states[c].name);
        }
      });
    };


    $scope.editShedules = function () {
      $scope.allowShedulesEdit    = true;
    };

    $scope.editProfileInfo = function () {
      $scope.allowProfilInfoEdit = true;
    };

    $scope.updateUser = function ( title, message, type ) {

      $http.patch('/api/users/' + $scope.user._id , $scope.user ).then(function (response) {
        swal(title, message, type);
        // Permitir editar los datos generales
        $scope.allowGeneralDataEdit = false;
        $scope.allowShedulesEdit    = false;
        $scope.allowProfilInfoEdit  = false;
        $scope.allowSocialMediaEdit = false;
      });

    }

    $scope.sucursales = function() {
      $window.location.href = '/agregarSucursal';
    }

    $scope.initShedules = function () {
      if( $scope.user.shedules.length == 0 )
      {
        $scope.user.shedules =
          [
            {
              day:  'Lunes',
              oppened:  false ,
              opening:  '' ,
              closing:  ''
            },
            {
              day:  'Martes' ,
              oppened:  false ,
              opening: '',
              closing: ''
            },
            {
              day:  'Miércoles',
              oppened: false ,
              opening: '',
              closing: ''
            },
            {
              day:  'Jueves' ,
              oppened:  false ,
              opening: '',
              closing: ''
            },
            {
              day:  'Viernes',
              oppened: false ,
              opening: '',
              closing: ''
            },
            {
              day:  'Sábado',
              oppened: false ,
              opening: '',
              closing: ''
            },
            {
              day:  'Domingo',
              oppened: false,
              opening: '',
              closing: ''
            }
          ];
      }
    }

    // Permitir editar los datos generales
    $scope.allowGeneralDataEdit = false;
    $scope.allowShedulesEdit    = false;
    $scope.allowProfilInfoEdit  = false;
    $scope.allowSocialMediaEdit = false;

    angular.element(document).ready(function () {

      Auth.getCurrentUser(function (user) {

        $scope.user = user;

        $scope.allStates();

        $scope.initShedules();

      });

    });

    $scope.saveGeneralData = function ( isValidForm ) {
      if ( isValidForm  ){
        $scope.updateUser( 'Exito', 'Datos actualizados correctamente', 'success');
        $scope.allowGeneralDataEdit = false;
        $scope.allowSocialMediaEdit = false;
      }
      else
        swal('Error', 'Error al intentar actualizar los datos, por favor verifica los campos en color rojo', 'error');
    }
    $scope.saveShedules = function ( isValidForm ) {
    if ( isValidForm  ) {

      for ( var i=0; i< $scope.user.shedules.length ; i++) {
        if ( $("#check_" + i ).is(":checked") ) {

          // Verificar que si está abierto se pongan tanto apertura como cierre
          if( $("#opening_"+i).val() == '' || $("#closing_"+i).val() == '' )
          {
            swal('Error', 'Debes establecer un horario de apertura y cierre para el '+ $("#day_"+i).text().trim(), 'error');
            return;
          }
          $scope.user.shedules[i].day = $("#day_"+i).text();
          $scope.user.shedules[i].opening = $("#opening_"+i).val();
          $scope.user.shedules[i].closing = $("#closing_"+i).val();
          $scope.user.shedules[i].oppened = true;
        }
        else {
          $scope.user.shedules[i].day = $("#day_"+i).text();
          $scope.user.shedules[i].opening = "";
          $scope.user.shedules[i].closing = "";
          $scope.user.shedules[i].oppened = false;
        }
      }
      $scope.updateUser( 'Exito', 'Datos actualizados correctamente', 'success');
      $scope.allowShedulesEdit = false;
    }
    else
      swal('Error', 'error al intentar actualizar los datos, por favor verifica los campos en color rojo', 'error');
  }
    $scope.saveProfileInfo = function ( isValidForm ) {
      if ( isValidForm ){
        if( $("#tags").val() != '')
        {
          $scope.user.tags = $("#tags").val().split(",");
          var len = $scope.user.tags.length;
          for ( var i=0; i< len ; i++ )
          {
            if( $scope.user.tags[ i ].trim() == '') {
              $scope.user.tags.splice(i, 1)
              len--;
              i--;
            }
          }
        }

        else
          $scope.user.tags = [];

        if( $("#products").val() != '')
        {
          $scope.user.products = $("#products").val().split(",");
          var len = $scope.user.products.length;
          for ( var i=0; i< len ; i++ )
          {
            if( $scope.user.products[ i ].trim() == '') {
              $scope.user.products.splice(i, 1)
              len--;
              i--;
            }
          }
        }
        else
          $scope.user.products = [];

        $scope.updateUser( 'Exito', 'Datos actualizados correctamente', 'success');
        $scope.allowProfilInfoEdit = false;
      }
      else
        swal('Error', 'error al intentar actualizar los datos, por favor verifica los campos en color rojo', 'error');
  }

  $scope.toAdd = {};
  $scope.editing = false;

  $scope.addPhone = function() {
    if ($scope.toAdd.tel != '' && $scope.toAdd.nombre != '' && $scope.toAdd.tel != undefined && $scope.toAdd.nombre != undefined) {
      console.log($scope.toAdd);
      $scope.user.telefonos.push($scope.toAdd)
      Auth.updateProfile($scope.user);
      $scope.toAdd = {};
    } else {
      swal("Error", "Todos los campos son obligatorios", "error")
    }
  }

  $scope.deletePhone = function(index) {
    $scope.user.telefonos.splice(index, 1);
    Auth.updateProfile($scope.user);
  }

  $scope.editPhone = function(index, phone) {
    $scope.editing = true;
    $scope.editIndex = index;
    $scope.toAdd.tel = phone.tel;
    $scope.toAdd.nombre = phone.nombre;
  }

  $scope.submitEdit = function() {
    if ($scope.toAdd.tel != '' && $scope.toAdd.nombre != '' && $scope.toAdd.tel != undefined && $scope.toAdd.nombre != undefined) {
      $scope.user.telefonos[$scope.editIndex] = $scope.toAdd
      Auth.updateProfile($scope.user);
      $scope.toAdd = {};
      $scope.editing = false;
    } else {
      swal("Error", "Todos los campos son obligatorios", "error")
    }
  }


  });

})();
