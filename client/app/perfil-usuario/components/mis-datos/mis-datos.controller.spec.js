'use strict';

describe('Component: MisDatosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var MisDatosComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    MisDatosComponent = $componentController('MisDatosComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
