'use strict';

angular.module('portaliaApp')
  .directive('userdata', () => ({
    templateUrl: 'app/perfil-usuario/components/mis-datos/blocks/user-data/user-data.html',
    restrict: 'E',
    controller: 'UserDataController',
    controllerAs: 'nav'
  }));
