'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/mis-datos', {
        template: '<mis-datos></mis-datos>'
      });
  });
