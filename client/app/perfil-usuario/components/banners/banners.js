'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/banners', {
        template: '<banners></banners>'
      });
  });

