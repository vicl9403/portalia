'use strict';

(function(){

class SolicitudBannerComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('solicitudBanner', {
    templateUrl: 'app/perfil-usuario/components/banners/solicitud-banner/solicitud-banner.html',
    controller: SolicitudBannerComponent,
    controllerAs: 'solicitudBannerCtrl'
  }).controller('SolicitudBannerController', function ($scope, $http, Auth, $timeout, $window, $location, $rootScope, FileUploader) {

    $scope.uploadComplete = 0;

    $scope.img1 = '';
    $scope.img2 = '';

    var DropzoneImg1 = new Dropzone("#img1",
      {
        acceptedFiles: 'image/*',
        url: "/api/upload/image",
        autoProcessQueue: false,
        resizeWidth : 300,
        resizeHeight: 300,
        init: function() {
          this.on("addedfile", function (file) {
            console.log( file );
          });
          this.on("sending", function(file, xhr, formData){
            formData.append("path", "/banners/" + $scope.user._id + '/' );
          });
          this.on("success", function(file, responseText) {

            $scope.img1 = responseText;
            $scope.completeBannerCreation();
          });
        }
      });

    var DropzoneImg2 = new Dropzone("#img2",
      {
        acceptedFiles: 'image/*',
        url: "/api/upload/image",
        autoProcessQueue: false,
        resizeWidth : 400,
        resizeHeight: 150,
        init: function() {
          this.on("addedfile", function (file) {
            console.log( file );
          });
          this.on("sending", function(file, xhr, formData){
            formData.append("path", "/banners/" + $scope.user._id + '/' );
          });
          this.on("success", function(file, responseText) {

            $scope.img2 = responseText;
            $scope.completeBannerCreation();
          });
        }
      });

    $scope.completeBannerCreation = function () {


      if( $scope.img1 != '' && $scope.img2 != '' ) {

        $scope.banner.img1 = $scope.img1;
        $scope.banner.img2 = $scope.img2;

        $http.patch('/api/banners/' + $scope.banner._id , $scope.banner )
          .then( res =>{
            $window.location = '/perfil-usuario/activar-banner/' + $scope.banner._id
          })
      }

    }
    $scope.init = function () {
      let today = moment().format("MM/DD/YYYY");
      let nextMonth = moment().add(30,'d').format("MM/DD/YYYY");

      $scope.bannerTime = today + ' - ' + nextMonth;

      $scope.since = moment(today);
      $scope.until = moment(nextMonth);
      $scope.newTag = '';

      $scope.isLoading = false;

      $scope.url = '' ;

      $('#daterange').daterangepicker(  {
        autoApply: true,
        "locale": {
          "format": "MM/DD/YYYY",
          "separator": " - ",
          "applyLabel": "Aplicar",
          "cancelLabel": "Cancelar",
          "fromLabel": "Desde",
          "toLabel": "Hasta",
          "customRangeLabel": "Custom",
          "daysOfWeek": [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
          ],
          "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
          ],
          "firstDay": 1
        },
        startDate: today,
        endDate: nextMonth
      },function(start, end) {
        $scope.since = start;
        $scope.until = end;
      });


      $scope.tags = $("#tags").tagsinput({
        maxTags: ($scope.user.role == 'admin') ? 3 : 6,
        tagClass : 'label label-warning'
      });

      $("#tags").tagsinput('removeAll');

      $('#tags').on('beforeItemAdd', function(event) {
        var tag = event.item;

        $scope.isLoading = true;
        $http.get('/api/banners/findKeyword/' + tag )
          .then( (res) =>{
            $scope.isLoading = false;
            if( res.data.length >= 3 ) {
              swal('Lo sentimos...',`La palabra ${ tag } ya la tiene 3 asignaciones, no la podemos agregar nuevamente.`,'warning');
              $('#tags').tagsinput('remove', tag, {preventPost: true});
            }
          });
      });

    }

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;

      $scope.init();

    });


    $scope.addTag = function (tag) {
      if( $("#tags").tagsinput('items').length >= 5 ) {
        swal('Advertencia', 'Solo se pueden agregar 5 palabras clave','warning');
        return;
      }
      $("#tags").tagsinput('add', tag);
      $scope.newTag = '';

    }

    $scope.sendRequest = function () {


      if ( $("#tags").tagsinput('items').length > 0 && $scope.url != '' ) {

        let data = {
          tags: $("#tags").tagsinput('items'),
          since: new Date( $scope.since ),
          until: new Date( $scope.until ),
          url: $scope.url,
          user: ($scope.userBanner != undefined ) ? $scope.userBanner : $scope.user._id,
          status: ( $scope.user.role == 'admin' ) ? "approved" : "pending"
        };

        $scope.isLoading = true;

        $http.post('/api/banners' , data )
          .then( res =>{
            $scope.banner = res.data;

            DropzoneImg1.processQueue();
            DropzoneImg2.processQueue();
            //$scope.isLoading = false;
            //swal("Éxito", "se mando correctamente la solicitud del banner." , 'success');
            //$scope.init();
          });

      }
      else {
        swal( "Advertencia", "Tienes que seleccionar al menos una palabra clave y una URL" , "warning" );
      }
    }

  })

})();
