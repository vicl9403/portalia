'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/banners/solicitud-banner', {
        template: '<solicitud-banner></solicitud-banner>'
      });
  });
