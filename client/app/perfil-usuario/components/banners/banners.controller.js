'use strict';

(function(){

class BannersComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('banners', {
    templateUrl: 'app/perfil-usuario/components/banners/banners.html',
    controller: BannersComponent,
    controllerAs: 'bannersCtrl'
  }).controller('BannerController', function ($scope, $http, Auth, $timeout, $window, $location, $rootScope) {


    var DropzoneImg1 = new Dropzone("#img1",
      {
        acceptedFiles: 'image/*',
        url: "/api/upload/image",
        autoProcessQueue: true,
        resizeWidth : 300,
        resizeHeight: 300,
        maxFiles : 1,
        init: function() {
          this.on("sending", function(file, xhr, formData){
            formData.append("path", "/banners/" + $scope.user._id + '/' );
          });
          this.on("success", function(file, responseText) {

            this.removeAllFiles();
            $scope.activeBanner.img1 = responseText;

            $http.patch('/api/banners/' + $scope.activeBanner._id , $scope.activeBanner )
              .then( res =>{
                swal("Éxito","La imágen del banner se actualizó correctamente" , 'success');
              })
          });
        }
      });

    var DropzoneImg2 = new Dropzone("#img2",
      {
        acceptedFiles: 'image/*',
        url: "/api/upload/image",
        autoProcessQueue: true,
        resizeWidth : 400,
        resizeHeight: 150,
        maxFiles : 1,
        init: function() {
          this.on("sending", function(file, xhr, formData){
            formData.append("path", "/banners/" + $scope.user._id + '/' );
          });
          this.on("success", function(file, responseText) {

            this.removeAllFiles();
            $scope.activeBanner.img2 = responseText;

            $http.patch('/api/banners/' + $scope.activeBanner._id , $scope.activeBanner )
              .then( res =>{
                swal("Éxito","La imágen del banner se actualizó correctamente" , 'success');
              })
          });
        }
      });

    $scope.init = function () {

      $scope.activeBanner = null;

      $http.get('/api/banners/user/' + $scope.user._id )
        .then( res =>{
          $scope.banners = res.data;
        })
    }

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY');
    }

    $scope.getStatusText = function (status) {

      if( status == 'pending' ) {
        return 'Pendiente'
      }

      if( status == 'approved') {
        return 'Aprobado'
      }

      if( status == 'active' ) {
        return 'activo'
      }

      if( status == 'rejected' ) {
        return 'Rechazado'
      }

      return '';

    }

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;

      $scope.init();

    });

    $scope.deleteBanner = function (banner) {

      swal({
          title: "¿Estás seguro?",
          text: "No podrás activar ese banner si lo eliminas",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, eliminar",
          closeOnConfirm: false
        },
        function(){
          banner.status = 'deleted';
          $http.patch('/api/banners/' + banner._id , banner )
            .then( res=>{
              banner = res.data;
              $scope.init();
              swal("Eliminado", "El banner se eliminó correctamente.", "success");
            })


        });


    }
    $scope.showBanner = function (banner) {
      $scope.activeBanner = banner;
    }
    $scope.saveChanges = function () {
      $http.patch('/api/banners/' + $scope.activeBanner._id , $scope.activeBanner )
        .then( res => {
          $scope.activeBanner = res.data;
          swal('Éxito','Cambios guardados correctamente.','success');
        })
    }

    $scope.activateBanner = function ( banner ) {
      $scope.bannerToActivate = banner;
      $('#modal-activate-banner').modal('show');
    }
});

})();
