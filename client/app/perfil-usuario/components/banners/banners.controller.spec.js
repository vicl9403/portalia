'use strict';

describe('Component: BannersComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var BannersComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    BannersComponent = $componentController('banners', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
