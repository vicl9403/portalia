'use strict';

function getCleanedString(cadena){
  // Definimos los caracteres que queremos eliminar
  var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

  // Los eliminamos todos
  for (var i = 0; i < specialChars.length; i++) {
    cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
  }

  // Lo queremos devolver limpio en minusculas
  cadena = cadena.toLowerCase();

  // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
  cadena = cadena.replace(/ /g,"_");

  // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
  cadena = cadena.replace(/á/gi,"a");
  cadena = cadena.replace(/é/gi,"e");
  cadena = cadena.replace(/í/gi,"i");
  cadena = cadena.replace(/ó/gi,"o");
  cadena = cadena.replace(/ú/gi,"u");
  cadena = cadena.replace(/ñ/gi,"n");
  return cadena;
}

(function(){

class ActivarBannerComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('activarBanner', {
    templateUrl: 'app/perfil-usuario/components/banners/activar-banner/activar-banner.html',
    controller: ActivarBannerComponent,
    controllerAs: 'activarBannerCtrl'
  }).controller('ActivarBannerController', function ($scope, $http, Auth, $timeout, $window, $location, $routeParams ) {

    $window.location = '/perfil-usuario/banners';

    var pricePerHit = .10;

    $scope.minPrice = 2;

    $scope.totalToPay = 0;

    $scope.pricePerTag = [];

    $scope.getTotal = function ( arr ) {
      let total = 0;

      let numDays = $scope.diffinDays( $scope.banner.until , $scope.banner.since  );

      for( let c = 0 ; c < arr.length ; c ++ ) {
        total += arr[c].price * numDays;
      }

      $scope.totalToPay = total;
    }

    $scope.getBannerPrice = function () {
      $http.get('/api/algolia/getPopularWords')
        .then( res => {

          let topWords = res.data.topSearches ;

          for( let i = 0 ; i < topWords.length ; i ++ ) {

            let word = getCleanedString(topWords[i].query);

            let numDays = $scope.diffinDays( $scope.banner.until , $scope.banner.since  );

            for( let c = 0 ; c < $scope.pricePerTag.length ; c ++ ) {
              if( $scope.pricePerTag[c].cleanedTag == word ) {

                $scope.pricePerTag [ c ].price = ( topWords[i].count * pricePerHit ) / 30;
                if( ( $scope.pricePerTag [ c ].price ) < $scope.minPrice )
                  $scope.pricePerTag [ c ].price = $scope.minPrice ;
              }
            }
          }

          $scope.getTotal( $scope.pricePerTag );
        })
    }

    $scope.getBanner = function (id) {

      $http.get('/api/banners/' + id )
        .then( res => {
          $scope.banner = res.data;

          // Asignar el arreglo para poder tener los precios por palabra
          for( let i=0 ; i < $scope.banner.tags.length ; i ++ ) {
            $scope.pricePerTag.push({
              price       : $scope.minPrice,
              tag         : $scope.banner.tags[i],
              cleanedTag  : getCleanedString($scope.banner.tags[i])
            });
          }

          $scope.getBannerPrice();

          var card = new Card({
            form: 'form#cardForm',
            container: '.card-wrapper',

            width: 450,

            placeholders: {
              number: '**** **** **** ****',
              name: 'Nombre',
              expiry: '**/****',
              cvc: '***'
            },

            messages: {
              validDate: 'expire\ndate',
              monthYear: 'mm/yy'
            }

          });

        })
    }

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY');
    }

    $scope.diffinDays = function (since, until) {
      since = moment( since );
      until= moment( until );

      return since.diff( until , 'days' );
    }

    $scope.getUserCards = function () {
      $http.get('/api/openpay/user/cards')
        .then( res => {
          $scope.userCards = res.data;
        })
    }

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;

      if( $scope.user.openpayID == null )
      {
        let data = {
          'name' : user.nombre,
          'email' : user.email,
        }

        $http.post('/api/openpay/customers' , data )
          .then( res => {
            $scope.user = res.data;
            $scope.getBanner($routeParams.id);
            $scope.getUserCards();
          })
      }
      else
      {
        $scope.getBanner($routeParams.id);
        $scope.getUserCards();
      }

    });

    $scope.addNewCard = function () {
      $("#modal-add-card").modal('show');
    }

    $scope.isActiveBanner = function (banner) {
      return moment().format('X') <= moment(banner.since).format('X');
    }
    $scope.saveCard = function () {

      $scope.pendingTransaction = true;

      let cardRequest = {
        'card_number' : $scope.card.number.replace(/\s/g, ""),
        'holder_name' : $scope.card.name,
        'expiration_year' : $scope.card.expiration.split('/')[1].replace(/\s/g, ""),
        'expiration_month' : $scope.card.expiration.split('/')[0].replace(/\s/g, ""),
        'cvv2' : $scope.card.cvv
      }

      $http.post('/api/openpay/newCard', cardRequest )
        .then( res => {
          swal('Éxito','tu tarjeta se agregó correctamente','success');
          $("#modal-add-card").modal('hide');
          $scope.pendingTransaction = false;
          $scope.getUserCards();
        })
        .catch( res => {
          swal('Error', 'No pudimos agregar esa tarjeta, por favor verifica tus datos o intenta con otra tarjeta' , 'error');
          console.log( res );
          $scope.pendingTransaction = false;
        })
    }

    $scope.buyBanner = function ( card ) {


      swal({
          title: "Estas seguro?",
          text: "Realizaremos un cargo a tu tarjeta por la cantidad de " + $scope.totalToPay.toFixed(2),
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, confirmar",
          closeOnConfirm: true
        },
        function(){

          $scope.pendingTransaction = true;

          $http.get('/api/openpay/keys')

            .then( res => {
              OpenPay.setId( res.data.id );
              OpenPay.setApiKey( res.data.pbk );
              OpenPay.setSandboxMode( res.data.sandboxMode );

              let deviceSessionId = OpenPay.deviceData.setup();

              let chargeRequest = {
                'source_id' : card.id,
                'method' : 'card',
                'amount' : 20.00,
                'currency' : 'MXN',
                'description' : 'Compra de banner de publicidad',
                'device_session_id' : deviceSessionId,
              };

              $http.post('/api/openpay/banner/' + $scope.banner._id , chargeRequest )
                .then( res => {
                  swal('Éxito','Tu pago se realizó correctamente, el banner estará activo hasta su fecha de fin', 'success');
                  $scope.getBanner($routeParams.id);
                  $scope.pricePerTag = {};
                  $scope.pendingTransaction = false;
                })
                .catch( err => {
                  swal('Error','Ocurrió un error al procesar el pago, por favor intenta de nuevo', 'error');
                  console.log( err );
                  $scope.pendingTransaction = false;
                })

            });





        });

    }

    $scope.sendtoBusinessList = function (tag) {
      $window.location = '/resultadosObtenidos/pagina-1/buscar-' + tag + '/tipo-nombre/'
    }

})

})();
