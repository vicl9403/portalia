'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/activar-banner/:id', {
        template: '<activar-banner></activar-banner>'
      });
  });
