'use strict';
(function(){

class ChatComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('chat', {
    templateUrl: 'app/perfil-usuario/components/chat/chat.html',
    controller: ChatComponent
  }).controller( 'ChatController', function ( $scope, $http, Auth, $timeout, $window, $location, socket, $httpParamSerializerJQLike ) {

    $scope.chatMessage = '';

    $scope.socket = io();

    $scope.userChats = [];
    $scope.activeChat = {};
    $scope.chatWith = {};
    $scope.unreadMessages = {};

    $scope.getUnreadChats = function ( chat , user ) {//lol

      var messages = chat.messages;
      var unreadMessages = 0;

      for ( var i=0 ; i<messages.length ; i++ ) {
        if( messages[i].to == user._id && ! messages[i].hasReadDest )
          unreadMessages ++;
      }

      return unreadMessages;
    }

    $scope.setReadChats = function ( chat , user ) {

      $scope.unreadMessages[ chat ] = 0;

      for( var c=0; c<$scope.userChats.length; c++ ) {
        if ( $scope.userChats[c]._id == chat )
        {
          for ( var i=0; i< $scope.userChats[ c ].messages.length; i++ )
          {
            if ( $scope.userChats[ c ].messages[ i ].to == user._id  )
              $scope.userChats[ c ].messages[ i ].hasReadDest = true;
          }
          $http.patch('/api/chats/' + chat , $scope.userChats[ c ])
            .then(function (res) {
              // $scope.chatMessage = '';
            })
        }
      }



    }

    $scope.getUserChats = function ( user ) {


      $http.get('/api/chats/user/' + user._id )
        .then( function ( res ) {

          $scope.userChats = res.data;

          $scope.activeChat = $scope.userChats[0];

          // Iterar sobre todos los chats del usuario
          for ( var i=0; i < $scope.userChats.length ; i++ )
          {
            var aux = i;
            // Evento de escucha para cada uno de los chats
            $scope.socket.on('chat_message_' + $scope.userChats[i]._id , function(msg){
              var id = msg.id;
              msg = msg.message;


              if ( msg.to == $scope.user._id )
              {
                for( var c=0; c< $scope.userChats.length ; c++ )
                {
                  if( $scope.userChats[c]._id == id )
                  {
                    // Agregar el nuevo mensaje a la lista
                    $scope.userChats[c].messages.push(msg);
                    // Obtener la notificación de un nuevo mensaje sin leer
                    $scope.unreadMessages[ $scope.userChats[c]._id ] =  $scope.getUnreadChats( $scope.userChats[c] , user ) ;
                    // Actualizar el scope
                    $scope.$apply();
                    // Hacer scroll al último chat
                    $timeout(function() {
                      $(".scroller").animate({scrollTop: $('.lastElement').offset().top + ( $scope.activeChat.messages.length * 50 ) });
                    }, 100);

                  }
                }
              }
              else
              {
                $timeout(function() {
                  $(".scroller").animate({scrollTop: $('.lastElement').offset().top + ( $scope.activeChat.messages.length * 50 ) });
                }, 100);
              }

            });

            // Obtener los mensajes sin leer del chat
            $scope.unreadMessages[ $scope.userChats[i]._id ] =  $scope.getUnreadChats( $scope.userChats[i] , user ) ;
          }


        });

    }



    // Setea el usuario cone l que se está chateando cuando cambie el chat activo
    $scope.$watch('activeChat', function() {


      if( ! jQuery.isEmptyObject( $scope.activeChat ) ) {
        // Obtener los mensajes sin leer del chat activo

        if( $scope.user._id == $scope.activeChat.userDest[0]._id  )
          $scope.chatWith = $scope.activeChat.userOrigin[0];
        else
          $scope.chatWith = $scope.activeChat.userDest[0];

      }
    });


    Auth.getCurrentUser(function (user) {

        $scope.user = user;

        $scope.getUserChats( user );

        // Evento que escucha la generación de un nuevo chat
        $scope.socket.on('chat_created' + $scope.user._id , function( chat ){

          // Actualizar el listado de chats del usuario
          $scope.getUserChats( $scope.user );

        });

    });


    $scope.sendChat = function () {

      if ($scope.chatMessage != '') {

        // Generar el origen
        var org = $scope.user._id;
        var dest = $scope.chatWith._id;

        // Variable para el nuevo mensaje
        var newMessage = {
          from: org,
          to: dest,
          content: $scope.chatMessage,
          hasReadOrigin: true,
          hasreadDest: false,
          created_at: Date.now,
        };

        // Hacer push del chat que se quiere modificar
        $scope.activeChat.messages.push(newMessage);
        $http.patch('/api/chats/' + $scope.activeChat._id, $scope.activeChat)
          .then(function (res) {
            $scope.chatMessage = '';
            if($scope.chatWith.pushSuscriberId && $scope.chatWith.pushSuscriberId.length != 0){
            var settings = {
              "url": "https://api.pushengage.com/apiv1/notifications",
              "method": "POST",
              "headers": {
                "api_key": "Mz2LkrECnf0uFi4Pxj17974Jl9O8sTym",
                "content-type": "application/x-www-form-urlencoded"
              },
              "data": {
                "notification_title": "Tienes un nuevo mensaje de chat en Portalia Plus",
                "notification_message": "Haz click para ir al sitio y leerlo en la sección 'Chat' de tu perfil",
                "notification_url": "https://portaliaplus.com",
                "profile_id[0]": $scope.chatWith._id,
                "image_url": 'https://portaliaplus.com/assets/images/icons/chat-message.png'
              }
            }

            $.ajax(settings).done(function (response) {
              console.log(response);
            });
            }
          })

        // Mandar el evento al servidor
        $scope.socket.emit('chat_message', {id: $scope.activeChat._id, message: newMessage});
      }

    }

    $scope.changeChat = function ( id ) {

      // Marcar como leídos el chat que se clickea
      $scope.setReadChats( id , $scope.user );

      for ( var i=0; i< $scope.userChats.length; i++ )
      {
        if( $scope.userChats[i]._id == id ) {
          $scope.activeChat = $scope.userChats[i];
          $(".scroller").animate({scrollTop: $('.lastElement').offset().top + ( $scope.activeChat.messages.length * 50 ) });
        }
      }
    }
    // Detectar el enter del textbox
    $scope.onMessageKeyPress = function (keyEvent) {
      if (keyEvent.which === 13)
        $scope.sendChat();
    }

  });



})();
