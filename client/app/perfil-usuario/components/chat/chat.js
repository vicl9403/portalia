'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/chat', {
        template: '<chat></chat>'
      });
  });
