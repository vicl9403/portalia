'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/oportunidades-de-negocio/:sector', {
        template: '<oportunidades-de-negocio-user></oportunidades-de-negocio-user>'
      });
  });
