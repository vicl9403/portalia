'use strict';

describe('Component: OportunidadesDeNegocioUserComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var OportunidadesDeNegocioUserComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    OportunidadesDeNegocioUserComponent = $componentController('OportunidadesDeNegocioUserComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
