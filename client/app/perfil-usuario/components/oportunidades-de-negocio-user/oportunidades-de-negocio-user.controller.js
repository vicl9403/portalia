'use strict';
(function(){

class OportunidadesDeNegocioUserComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('oportunidadesDeNegocioUser', {
    templateUrl: 'app/perfil-usuario/components/oportunidades-de-negocio-user/oportunidades-de-negocio-user.html',
    controller: OportunidadesDeNegocioUserComponent
  }).controller('OportunidadesUserController', function ( $scope, $http, Auth, $timeout, $window, $location, $rootScope) {

    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;
      $scope.activeQuote = false;

      $http.get('/api/quotes/getSectorQUotes/' + user.sector_name ).then(function (res) {
        $scope.quotes = res.data;
        $timeout(function() {
          $("#quotesTable").dataTable({
            "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            }
          });
        }, 100);

      });


    });

    $scope.showQuote = function ( id ) {
      $scope.activeQuote  = true;
      $scope.$broadcast ('getQuoteData', id );

      if( $scope.user.paquete == 'gratis' ) {
        swal({
            title: "Te invitamos a probar uno de nuestros beneficios de clientes que ya cuentan con membresía.",
            text: "Por tiempo lomotado podrás ver el contacto del solicitante de tu cotización, que ya cuentan con su membresía.",
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "¡Entendido!",
            closeOnConfirm: false
          },
          function(){
            swal("Gracias!", "Recuerda que este beneficio es por tiempo limitado si no cuentas con una membresía", "success");
            $timeout(function () {
              $("html, body").animate({
                scrollTop: $("#activeQuote").offset().top
              }, "slow");
            }, 200);
          });
      }
      else {
        $timeout(function () {
          $("html, body").animate({
            scrollTop: $("#activeQuote").offset().top
          }, "slow");
        }, 200);
      }
    }

  });

})();
