'use strict';

describe('Component: MembresiaComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var MembresiaComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    MembresiaComponent = $componentController('membresia', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
