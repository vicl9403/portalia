'use strict';

(function(){

class MembresiaComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('membresia', {
    templateUrl: 'app/perfil-usuario/components/membresia/membresia.html',
    controller: MembresiaComponent,
    controllerAs: 'membresiaCtrl'
  }).controller('MembresiaController', function ($scope, $http, Auth, $timeout, $window, $location, $rootScope) {

    $scope.getDateForHummans = function (date) {
      moment.locale('es');
      return moment(date).format('DD MMMM YYYY');
    }

    $scope.getClientSubscriptions = function () {
      if( $scope.user.openpayID != null ) {
        $http.get('/api/openpay/card')
          .then( res => {
            $scope.subscriptions = res.data;
          })
      }
    }
    // Obtener al usuario loggeado
    Auth.getCurrentUser(function (user) {

      $scope.user = user;

      var card = new Card({
        form: 'form#cardForm',
        container: '.card-wrapper',

        width: 450,

        placeholders: {
          number: '**** **** **** ****',
          name: 'Nombre',
          expiry: '**/****',
          cvc: '***'
        },

        messages: {
          validDate: 'expire\ndate',
          monthYear: 'mm/yy'
        }

      });

      $scope.getClientSubscriptions();

      $scope.activeSubscription = null;

      $scope.updatingSubscription = false;

    });

    $scope.setCardToUpdate = function (  subscription ) {
      $scope.activeSubscription = subscription;
      $scope.card = {};
    }

    $scope.cancelCardUpdate = function () {
      $scope.activeSubscription = null ;
    }

    $scope.isValidCard = function () {

      if( $scope.card== undefined ) {
        swal('Advertencia', 'Los datos proporcionados son inválidos, por favor verifica tu información', 'warning');
        return false;
      }

      if( $scope.card.number == undefined || $scope.card.expiration == undefined || $scope.card.cvv == undefined ) {
        swal('Advertencia', 'Los datos proporcionados son inválidos, por favor verifica tu información', 'warning');
        return false;
      }

      if( ! $.payment.validateCardNumber( $scope.card.number )) {
        swal('Advertencia','El número de tarjeta ingresado es incorrecto', 'warning');
        return false;
      }


      if( ! $.payment.validateCardExpiry( $scope.card.expiration.split('/')[0] , $scope.card.expiration.split('/')[1] )) {
        swal('Advertencia', 'La fecha de expiración de tu tarjeta es incorrecta, o tiene un formato inválido' , 'warning');
        return false;
      }

      if( ! $.payment.validateCardCVC($scope.card.cvv) ) {
        swal('Advertencia', 'El código de seguridad de tu tarjeta es inválido', 'warning');
        return false;
      }

      return true ;

    }

    $scope.updateCard = function () {

      // if( $scope.isValidCard() ) {
      //   console.log( $scope.card );
      // }

      $scope.updatingCard = true;

      let subscriptionRequest = {
        'card' : {
          'card_number': $scope.card.number.replace(/\s/g, ""),
          'holder_name': $scope.card.name,
          'expiration_year': $scope.card.expiration.split('/')[1].replace(/\s/g, ""),
          'expiration_month': $scope.card.expiration.split('/')[0].replace(/\s/g, ""),
          'cvv2': $scope.card.cvv
        }
      }

      $http.patch('/api/openpay/subscription/' + $scope.activeSubscription.id , subscriptionRequest )
        .then( res => {
          swal('Éxito', 'tu tarjeta se ha actualizado correctamente.');
          $scope.getClientSubscriptions();
          $scope.updatingCard = false;
          $scope.activeSubscription = null ;
        })
        .catch( err => {
          swal('Error','No pudimos dar de alta tu tarjeta, por favor intenta con otra, o verifica los datos de la misma.' , 'error');
          $scope.updatingCard = false;
          console.log( err );
        })
    }

    $scope.cancelSubscription = function ( subscription ) {

      swal({
          title: "Estas seguro?",
          text: "No podrás recuperar la subscripción si la eliminas",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, eliminar",
          closeOnConfirm: true
        },
        function(){
          $scope.cancellingSubscription = subscription;

          $http.delete('/api/openpay/subscription/' + subscription.id )
            .then( res => {
              $scope.cancellingSubscription = null;

              $scope.getClientSubscriptions();

              console.log( res );
            })
            .catch( err => {
              $scope.cancellingSubscription = null;
              console.log( err )
            })
        });



    }
  });

})();
