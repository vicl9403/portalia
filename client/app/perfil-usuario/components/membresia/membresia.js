'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/membresia', {
        template: '<membresia></membresia>'
      });
  });
