'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/catalogo-de-productos', {
        template: '<catalogo-de-productos></catalogo-de-productos>'
      });
  });
