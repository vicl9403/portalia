'use strict';

(function(){

class CatalogoDeProductosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('catalogoDeProductos', {
    templateUrl: 'app/perfil-usuario/components/catalogo-de-productos/catalogo-de-productos.html',
    controller: CatalogoDeProductosComponent,
    controllerAs: 'catalogoDeProductosCtrl'
  }).controller('CatalogoDeProductosController' ,function($scope, $http, Auth, Upload, $timeout, $window, $routeParams, FileUploader)
  {

    var uploader = $scope.uploader = new FileUploader({
      // La url se setea una vez que se tiene el usuario
      url: 'images',
      name: 'imagesFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|xls|docx|doc|pdf|csv'.indexOf(type) !== -1;
      },
    });

    // CALLBACKS UPLOADER
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
      // console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onBeforeUploadItem = function(item) {

      $scope.isLoading = true;

      item.formData.push({
        path: '/client/assets/uploads/catalogos/' + $scope.user._id 
      });

    };
    uploader.onAfterAddingFile = function(fileItem) {
      // Código para saber cuando se agrega un archivo
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
      $scope.isLoading = false;
      swal('Error','Tuvimos un problema al subir el catálogo, es probable que hayas excedido el tamaño permitido o estes intentando subir un catálogo con formato inválido','error')
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

      $scope.isLoading = false;

      // Limpiar la url de respuesta
      let url = response.replace('./client','');

      if( $scope.user.productsCatalog == undefined ) {
        $scope.user.productsCatalog = [];
      }

      $scope.user.productsCatalog.push({
        name : $scope.fileName ,
        url: url
      });

      $scope.fileName = '';

      $http.patch('/api/users/' + $scope.user._id , $scope.user )
        .then( function ( res ) {
          $scope.user = res.data;
          swal( 'Éxito' , 'tu catálogo se ha subido correctamente.' , 'success' );
        })


    };
    uploader.onCompleteAll = function( fileItem, response, status, headers ) {
      $scope.isLoading = false;
      $http.get('/api/users/' + $scope.user._id )
        .then( function ( user ) {
          $scope.user = user.data;
          swal( 'Éxito' , 'tu catálogo se ha subido correctamente.' , 'success' );
        })
    };

    Auth.getCurrentUser( function (user) {
      $scope.user = user;

      // Ajustando la url del uploader
      uploader.url = '/upload' ;
      $scope.fileName = '';
      $scope.isLoading = false;

    })

    $scope.uploadCatalog = function () {
      if( $scope.fileName != '' ) {
        $scope.uploader.uploadAll();
      }
      else {
        swal( 'Adevertencia' , 'Para poder subir el catálogo es necesario que establezcas su nombre','warning');
        $scope.isLoading = false;
      }


    }

    $scope.deleteCatalog = function ( catalog ) {

      swal({
          title: "Estas seguro?",
          text: "Si eliminas este catalogo no se visualizará en tu perfil",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si, eliminar",
          closeOnConfirm: false
        },
        function(){

          let activeCatalogs = [];
          for( let i = 0; i < $scope.user.productsCatalog.length ; i ++ ) {
            if( catalog != $scope.user.productsCatalog[i] )
              activeCatalogs.push( $scope.user.productsCatalog[i] );
          }

          $scope.user.productsCatalog = activeCatalogs;

          $http.patch('/api/users/' + $scope.user._id , $scope.user )
            .then( function (res) {
              swal('Éxito', 'Se eliminó correctamente el catálogo' , 'success');
            })

        });

    }

  });

})();
