'use strict';

describe('Component: CatalogoDeProductosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CatalogoDeProductosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    CatalogoDeProductosComponent = $componentController('catalogo-de-productos', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
