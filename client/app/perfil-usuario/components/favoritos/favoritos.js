'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario/favoritos', {
        template: '<favoritos></favoritos>'
      });
  });
