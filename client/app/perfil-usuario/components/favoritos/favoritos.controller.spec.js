'use strict';

describe('Component: FavoritosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var FavoritosComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    FavoritosComponent = $componentController('FavoritosComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
