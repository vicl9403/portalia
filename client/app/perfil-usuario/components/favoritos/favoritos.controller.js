'use strict';
(function(){

class FavoritosComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('favoritos', {
    templateUrl: 'app/perfil-usuario/components/favoritos/favoritos.html',
    controller: FavoritosComponent
  }).controller('FavoritesController', function ( $scope, $http, Auth, $timeout, $window ) {

    $scope.listaFavoritos = [];



    $scope.pedirFavoritos = function(idFavorito, indice) {
      $http.get('/api/users/' + idFavorito)
        .then(function(response) {
          $scope.listaFavoritos[indice] = response.data;
        });
    };

    $scope.getFavoritos = function() {
      for (var i = 0; i < $scope.user.favoritos.length; i++) {
        $scope.pedirFavoritos($scope.user.favoritos[i], i);
      }
    };

    $scope.deleteFavorite = function ( id ) {
      var newFavorites = [];

      for( var i=0; i < $scope.user.favoritos.length ; i ++ ) {
        if ( $scope.user.favoritos[i] != id )
          newFavorites.push($scope.user.favoritos[i]);
      }

      $scope.user.favoritos = newFavorites;

      $http.patch('/api/users/' + $scope.user._id , $scope.user )
        .then(function(response) {
          $scope.listaFavoritos = [];
          $scope.getFavoritos();
          swal( 'Exito' , 'Tus usuarios favoritos se actualizaron correctamente' , 'success' );
        });

    };

    Auth.getCurrentUser(function (user) {
      $scope.user = user;

      $scope.getFavoritos();

    });



  });

})();
