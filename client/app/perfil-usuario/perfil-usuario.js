'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/perfil-usuario', {
        template: '<perfil-usuario></perfil-usuario>',
        styleUrls: ['/assets/metronic/apps/css/inbox.css'],
      });
  });
