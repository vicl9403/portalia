'use strict';

angular.module('portaliaApp',
[
    'portaliaApp.constants',
    'portaliaApp.auth',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'btford.socket-io',
    'ui.bootstrap',
    'ngFileUpload',
    'ngSanitize',
    'MassAutoComplete',
    'vcRecaptcha',
    'updateMeta',
    'angularFileUpload',
  ])
  .config(function($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .when('/_=_', {
        redirectTo: '/'
      })
      .otherwise({
        redirectTo: '/error-404'
      });
    $httpProvider.defaults.useXDomain = true;
    $locationProvider.html5Mode(true);
  })
  .filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
  })
  .filter('filtroABC', function() {
    return function(items, letter) {

      var filtered = [];
      var letterMatch = new RegExp(letter, 'i');
      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        if (letterMatch.test(item.nombre.substring(0, 1))) {
          filtered.push(item);
        }
      }
      return filtered;
    };
  })
  .filter('decodeURI', function() {
    return window.decodeURI;
  })

  .filter('startFrom', function() {
      return function(input, start) {
          if(input) {
              start = +start; //parse to int
              return input.slice(start);
          }
          return [];
      }
  })


.run(function ($rootScope) {
  $rootScope.$watch('canonical', function(newVal){
      if(newVal != '') {
          var link = angular.element('<link>');
          link.attr('rel', 'canonical');
          link.attr('href', newVal);
          link.attr('id', 'canonical');
          angular.element('head').find(document.querySelector('#canonical')).remove();
          angular.element('head').append(link);
      }
  })
})

.factory('socket', function(socketFactory) {
  return socketFactory();
});
