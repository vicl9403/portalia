'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/agregarSucursal', {
        template: '<agregar-sucursal></agregar-sucursal>'
      });
  });
