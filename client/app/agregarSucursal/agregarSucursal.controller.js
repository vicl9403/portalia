'use strict';
(function() {

  angular.module('portaliaApp')
    .component('agregarSucursal', {
      templateUrl: 'app/agregarSucursal/agregarSucursal.html'
    }).controller('agregarSucursalCrl', function($scope, $http, Auth, Upload, $timeout, $window, $routeParams, $location) {
      Auth.getCurrentUser(function(res) {
        $scope.editing = false;
        $scope.userData = res;
        if (res.role == 'visitante') {
          $location.path('/');
        }
      });
      // $scope.userData = Auth.getCurrentUser();
      $scope.sucursales = {};

      $scope.agregarSucursal = function() {
        var checkEstado = $.trim($("#estado").val());
        var checkCiudad = $.trim($("#ciudad").val());
        var checkColonia = $.trim($("#colonia").val());
        var checkCalle = $.trim($("#calle").val());

        if (checkEstado.length > 0 && checkCiudad.length > 0 && checkColonia.length > 0 && checkCalle.length > 0) {
          $scope.sucursales = {};
          $scope.sucursales.loc = [];
          $scope.sucursales.estadoSucursal = document.getElementById("estado").value
          $scope.sucursales.ciudad = document.getElementById("ciudad").value
          $scope.sucursales.numero = document.getElementById("numero").value
          $scope.sucursales.colonia = document.getElementById("colonia").value
          $scope.sucursales.calle = document.getElementById("calle").value
          $scope.sucursales.cp = document.getElementById("cp").value
          $scope.sucursales.latitud = document.getElementById("lat").value
          $scope.sucursales.longitud = document.getElementById("long").value
          $scope.sucursales.loc[0] = document.getElementById("long").value
          $scope.sucursales.loc[1] = document.getElementById("lat").value

          $scope.sucursales.nombreSucu = document.getElementById("nombreSucursal").value

          if ($scope.userData.sucursales.length === 0)
            $scope.userData.sucursales[0] = $scope.sucursales;
          else
            $scope.userData.sucursales.push($scope.sucursales);

          Auth.updateProfile($scope.userData);
          alert("Sucursal agregada con éxito")
          $scope.sucursales = {};
        } else {
          alert("Introduce los datos requeridos para agregar tu sucursal")
        }
      }

      $scope.removerSucursal = function(indice) {
        $scope.userData.sucursales.splice(indice, 1);
        Auth.updateProfile($scope.userData);
      }

      $scope.goProfile = function() {
        $window.location.href = '/perfil-usuario/mis-datos';
      }

      $scope.activateEdit = function(indice, sucursal){
        $scope.indiceEdit = indice;
        $scope.editing = true;
        document.getElementById("estado").value = sucursal.estadoSucursal;
        document.getElementById("ciudad").value = sucursal.ciudad;
        document.getElementById("numero").value = sucursal.numero;
        document.getElementById("colonia").value = sucursal.colonia;
        document.getElementById("calle").value = sucursal.calle;
        if (sucursal.cp != undefined) {
          document.getElementById("cp").value = sucursal.cp;
        }
        document.getElementById("lat").value = sucursal.latitud;
        document.getElementById("long").value = sucursal.longitud;
        document.getElementById("nombreSucursal").value = sucursal.nombreSucu;
        setMarker(sucursal.latitud, sucursal.longitud);
      }

      $scope.saveEdit = function(){
        var checkEstado = $.trim($("#estado").val());
        var checkCiudad = $.trim($("#ciudad").val());
        var checkColonia = $.trim($("#colonia").val());
        var checkCalle = $.trim($("#calle").val());

        if (checkEstado.length > 0 && checkCiudad.length > 0 && checkColonia.length > 0 && checkCalle.length > 0) {
          $scope.sucursales = {};
          $scope.sucursales.loc = [];
          $scope.sucursales.estadoSucursal = document.getElementById("estado").value
          $scope.sucursales.ciudad = document.getElementById("ciudad").value
          $scope.sucursales.numero = document.getElementById("numero").value
          $scope.sucursales.colonia = document.getElementById("colonia").value
          $scope.sucursales.calle = document.getElementById("calle").value
          $scope.sucursales.cp = document.getElementById("cp").value
          $scope.sucursales.latitud = document.getElementById("lat").value
          $scope.sucursales.longitud = document.getElementById("long").value
          $scope.sucursales.loc[0] = document.getElementById("long").value
          $scope.sucursales.loc[1] = document.getElementById("lat").value
          $scope.sucursales.nombreSucu = document.getElementById("nombreSucursal").value
          $scope.userData.sucursales[$scope.indiceEdit] = $scope.sucursales;
          Auth.updateProfile($scope.userData);
          alert("Sucursal actualizada con éxito")
          $scope.sucursales = {};
          $scope.editing = false;
        } else {
          alert("Introduce los datos requeridos para agregar tu sucursal")
        }
      }
    });
})();
