'use strict';

describe('Component: AgregarSucursalComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var AgregarSucursalComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    AgregarSucursalComponent = $componentController('AgregarSucursalComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
