'use strict';

(function() {

  class MainController {

    constructor($http, $scope, $window, appConfig, Auth, $location, $rootScope) {
      $scope.userData = Auth.getCurrentUser();
      $scope.dataQuery = {};
      $scope.dataQuery.tipoBusqueda = "nombre";
      $scope.dataQuery.keyword = undefined;
      $scope.dataQuery.ubicacion = undefined;
      $scope.dataQuery.sector = undefined;
      $scope.dataQuery.categoria = undefined;
      this.$http = $http;
      $scope.postsPerPage = 25;
      $scope.currentPage = 1;
      this.awesomeThings = [];
      $scope.dataEvent = [];

      var mode = appConfig.mode || "portalito";
      $scope.mode = mode;

      $rootScope.canonical = 'https://portaliaplus.com';


      $http.get("api/sectors")
        .then(function(response) {
          console.log(response.data);
          $scope.sectors = response.data;
          $scope.autocomplete_options = {
            suggest: suggest_state,
            on_select: selectedItem
          };

          document.querySelector('.autocompleteMain').addEventListener('keypress', function(e) {
            var parametro = $(".autocompleteMain").val();
            var key = e.which || e.keyCode;
            if (key === 13) { // 13 is enter
              // code for enter
              if ($(".autocompleteMain").val().length > 3) {
                $location.path('/resultadosObtenidos').search({
                  keyword: parametro
                });
              }
            }
          });
        });



      var states = [];
      $http.get('api/estados')
        .then(function(res){
          console.log(res.data);
          for(var x = 0; x < res.data.length; x++){
            states.push(res.data[x].name);
            for(var y = 0; y < res.data[x].municipios.length; y++){
              states.push(res.data[x].municipios[y].name);
            }
          }
          console.log(states);
        })

      function selectedItem(selected) {
        if (selected.type == "empresa") {
          var empresa = selected.obj;
          $scope.busquedaLog = {};
          $scope.busquedaLog.idUsuario = $scope.userData._id
          $scope.busquedaLog.rolUsuario = $scope.userData.role
          $scope.busquedaLog.termino = $(".autocompleteMain").val()
          $http.post('/api/busquedas/', $scope.busquedaLog)
            .then(function(res) {
              console.log(res);
            })
            .catch(function(err) {
              console.log(err);
            })
          $location.path('/resultadosObtenidos').search({
            keyword: selected.label
          });
        } else {
          var sector = selected.obj;
          var subsector = sector.subsectores[selected.index]
          $window.location.href = "/anuncios/" + sector.name + "/" + subsector.name;
        }
      }


      function suggest_state(term) {
        var q = term.toLowerCase().trim();
        var results = [];

        // Find first 10 states that start with `term`.
        for (var i = 0; i < states.length && results.length < 10; i++) {
          var state = states[i];
          if (state.toLowerCase().indexOf(q) === 0)
            results.push({
              label: state,
              value: state
            });
        }
        console.log(results);

        return results;
      }

      $http.get('/api/topUsers/')
        .then(function(res){
          $scope.topUsers = [];
          for(var x = 0; x < res.data.length; x++){
            $scope.topUsers.push(res.data[x].idUser[0]);
          }
        })

      $scope.isSelect = function () {
        var logged = Auth.isLoggedIn;

        if( logged() )
        {
          var user = Auth.getCurrentUser() ;
          return ( user.paquete == 'plus' || user.paquete == 'select' )
        }
        return false;
      }

      $http.post('/api/quotes/getFeaturedQuotes')
        .then(function(response){
          $scope.topQuotes = response.data;
        })


      $scope.changeTipo = function (value) {
        $scope.dataQuery.tipoBusqueda = value;
      }

      $http.get('/api/busquedas/count')
        .then(function(response){
          $scope.topSearch = response.data;
        })

      $scope.changeSector = function(sector) {
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Categoría</option>')
          .val('');

        for (var x in $scope.sectors[sector].subsectores) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, x));
        }
      };
      
      angular.element(document).ready(function() {

        $http.get('api/events')
          .then(function(res) {
            $('#calendar').fullCalendar({
              locale: 'es',
              height: "parent",
              displayEventTime: false,
              customButtons: {
                anterior: {
                  text: 'Ant.',
                  click: function() {
                    $('#calendar').fullCalendar('prev');
                  }
                },
                siguiente: {
                  text: 'Sig.',
                  click: function() {
                    $('#calendar').fullCalendar('next');
                  }
                }
              },
              header: {
                left: 'anterior,siguiente',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
              },
              height: 600,
              eventColor: '#F9B800',
              events: res.data,
              eventClick: function(calEvent, jsEvent, view) {
                $('#titulo').html(calEvent.title);
                $('#description').html(calEvent.description);
                $("#img").attr("src", "/assets/uploads/eventos/" + calEvent.imagen);
                $('#modal-evento').modal();
              }
            })
          })

        $http.get('/api/events/destacados')
          .then(function(resp) {
            $scope.destacados = resp.data;
            $scope.activeEvent = 0;
          })
      });

      $scope.getStars = function(num) {
        return new Array(num);
      }


      $scope.searchProducts = function () {
        if( $scope.dataQuery.product != undefined && $scope.dataQuery.product.trim() != '' )
          $window.location = '/listado-productos/' + $scope.dataQuery.product + '/1';
        else
          swal('Cuidao','Para poder buscar dentro de productos es necesario que escribas algo.','warning');
      }

      $scope.prueba = function() {

        $scope.dataQuery.tipoBusqueda = 'nombre';



        $scope.url = "/resultadosObtenidos/pagina-1/";
        if ($scope.dataQuery.sector != undefined) {
          $scope.url = $scope.url + "sector-" + $scope.sectors[$scope.dataQuery.sector].name + "/";
        }
        if ($scope.dataQuery.categoria != undefined) {
          $scope.url = $scope.url + "categoria-" + $scope.sectors[$scope.dataQuery.sector].subsectores[$scope.dataQuery.categoria].name + "/";
        }
        if ($scope.dataQuery.keyword != undefined) {
          $scope.url = $scope.url + "buscar-" + $scope.dataQuery.keyword + "/";
        }
        if ($scope.dirty != undefined) {
          $scope.url = $scope.url + "ubicacion-" + $scope.dirty.value + "/";
        }
        if ( $("#state").val() != '' ) {
          $scope.url = $scope.url + "ubicacion-" + $("#state").val() + "/";
        }

        if ($scope.dataQuery.tipoBusqueda != undefined) {
          $scope.url = $scope.url + "tipo-" + $scope.dataQuery.tipoBusqueda + "/";
        }

        if ($scope.url != "/resultadosObtenidos/tipo-producto/" && $scope.url != "/resultadosObtenidos/tipo-nombre/") {
          $window.location = $scope.url;
          // $location.path($scope.url)
        } else {
          sweetAlert("Error", "Debes rellenar al menos algún campo para realizar una búsqueda", "error");
        }
      }


      $scope.sendToQuote = function (id) {
        swal({
            title: "Te invitamos a probar uno de nuestros beneficios de clientes que ya cuentan con membresía.",
            text: "Por tiempo limitado podrás ver el contacto del solicitante de tu cotización, que ya cuentan con su membresía.",
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "¡Entendido!",
            closeOnConfirm: false
          },
          function(){
            $window.location = '/detalle-cotizacion/' + id
          });
      }
    }
  }

  Array.prototype.MatchInArray = function(value) {

    var i;

    for (i = 0; i < this.length; i++) {
      value = value.toUpperCase();
      this[i] = this[i].toUpperCase();
      if (this[i].match(value)) {

        return i;

      }

    }

    return -1;

  };

  angular.module('portaliaApp')
    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController
    });

})();
