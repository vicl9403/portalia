'use strict';

angular.module('portaliaApp')
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        template: '<main></main>',
        resolve: {
          "check": function(Auth, $location) {
            Auth.isLoggedIn(function(response) {
              console.log(response);
              if (!response) {
                $location.path('/'); //redirect user to home.
              }
            });
          }
        }
      });
  });
