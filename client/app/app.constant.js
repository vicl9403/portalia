(function(angular, undefined) {
'use strict';

angular.module('portaliaApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin'],mode:'portalia',OPENPAY_ID:'my1qymxtirofw0vduzb4',OPENPAY_PUBLICKEY:'pk_b791f0a43df74360b316021350961f63'})

;
})(angular);