'use strict';

(function() {

  angular.module('portaliaApp')
    .component('calendario', {
      templateUrl: 'app/calendario/calendario.html'
    }).controller('calendarioCrl', function($scope, $http, Auth, $routeParams, $timeout) {
      $http.get('api/events')
        .then(function(res) {
          console.log(res);
          $('#calendar').fullCalendar({
            locale: 'es',
            height: "parent",
            displayEventTime: false,
            customButtons: {
              anterior: {
                text: 'Ant.',
                click: function() {
                  $('#calendar').fullCalendar('prev');
                }
              },
              siguiente: {
                text: 'Sig.',
                click: function() {
                  $('#calendar').fullCalendar('next');
                }
              }
            },
            header: {
              left: 'anterior,siguiente',
              center: 'title',
              right: 'month,agendaWeek,agendaDay'
            },
            height: 600,
            eventColor: '#F9B800',
            events: res.data,
            eventClick: function(calEvent, jsEvent, view) {
              $('#titulo').html(calEvent.title);
              $('#description').html(calEvent.description);
              $('#inicio').html($scope.getMomentTime(calEvent.start));
              $('#final').html($scope.getMomentTime(calEvent.end));
              $('#uerl').html(calEvent.uerl);
              $('#lugar').html(calEvent.lugar)
              $('#sector').html(calEvent.sector);
              $("#img").attr("src", "/assets/uploads/eventos/" + calEvent.imagen);
              $('#modal-evento').modal();
            }
          })
        })

      $scope.getMomentTime = function(time) {
        return moment(time).format('YYYY-DD-MM');
      }

      if ($routeParams.date) {
        $timeout(function() {
          //alert('fired')
          console.log($scope.getMomentTime($routeParams.date));
          $('#calendar').fullCalendar('gotoDate', $scope.getMomentTime($routeParams.date))
        }, 3000);
      }
    });

})();
