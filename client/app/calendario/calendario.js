'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/calendario', {
        template: '<calendario></calendario>'
      });
  });
