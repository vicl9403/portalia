'use strict';

describe('Component: CalendarioComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var CalendarioComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    CalendarioComponent = $componentController('calendario', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
