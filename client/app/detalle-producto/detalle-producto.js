'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/detalle-producto/:nombre/:id', {
        template: '<detalle-producto></detalle-producto>'
      });
  });
