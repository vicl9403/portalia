'use strict';

(function(){

class DetalleProductoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

  Number.prototype.formatMoney = function(c, d, t){
    var n = this,
      c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

angular.module('portaliaApp')
  .component('detalleProducto', {
    templateUrl: 'app/detalle-producto/detalle-producto.html',
    controller: DetalleProductoComponent,
    controllerAs: 'detalleProductoCtrl'
  }).controller('DetalleProductoController', function($scope, $http, $window, Auth, $routeParams) {


    $scope.getProductInfo = function () {

      $http.get('/api/products/' + $routeParams.id )
        .then( function (res) {
          $scope.product = res.data;

          $http.get('/api/users/' + $scope.product.user )
            .then( function (res) {
              $scope.user = res.data;
            })
        })
    }

    angular.element(document).ready(function () {

      $scope.getProductInfo();

    });



  });

})();
