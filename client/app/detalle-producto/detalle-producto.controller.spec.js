'use strict';

describe('Component: DetalleProductoComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var DetalleProductoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    DetalleProductoComponent = $componentController('detalle-producto', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
