'use strict';

describe('Filter: filtroVid', function () {

  // load the filter's module
  beforeEach(module('portaliaApp'));

  // initialize a new instance of the filter before each test
  var filtroVid;
  beforeEach(inject(function ($filter) {
    filtroVid = $filter('filtroVid');
  }));

  it('should return the input prefixed with "filtroVid filter:"', function () {
    var text = 'angularjs';
    expect(filtroVid(text)).toBe('filtroVid filter: ' + text);
  });

});
