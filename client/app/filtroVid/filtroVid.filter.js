'use strict';

function filtroVidFilter() {
  return function (input) {
    return 'filtroVid filter: ' + input;
  };
}


angular.module('portaliaApp')
  .filter('filtroVid', filtroVidFilter);
