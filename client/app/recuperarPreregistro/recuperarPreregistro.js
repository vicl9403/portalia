'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/recuperarPreregistro', {
        template: '<recuperar-preregistro></recuperar-preregistro>'
      });
  });
