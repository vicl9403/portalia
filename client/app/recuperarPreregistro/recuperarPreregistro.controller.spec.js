'use strict';

describe('Component: RecuperarPreregistroComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RecuperarPreregistroComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    RecuperarPreregistroComponent = $componentController('RecuperarPreregistroComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
