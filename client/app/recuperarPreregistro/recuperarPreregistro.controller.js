'use strict';
(function(){

class RecuperarPreregistroComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('recuperarPreregistro', {
    templateUrl: 'app/recuperarPreregistro/recuperarPreregistro.html',
    controller: RecuperarPreregistroComponent
  }).controller("recuperarPreregistroCrl", function($scope, $http) {

      $scope.data = {};
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      $scope.enviar = function() {
        //alert($scope.data.email);
        $http.post('/api/users/check', {
            email: $scope.data.email
          })
          .then(function(res) {
            console.log(res);
            // $scope.checkedEmail = true;
            if (res.status == 201) {
              $scope.isValidEmail = false;
                sweetAlert("Oops...", "Este correo no está registrado, introduce un email válido", "error");
            } else if (res.status == 200) {
              $scope.isValidEmail = true;
              $http.post('/api/passwordRecoverys/promotion', $scope.data)
                .then(function(response) {
                  $('#modal-success').modal('show');
                  $('#email').val("");
                  console.log(response);
                })
                .catch(function(err) {
                  alert("Ups detectamos un error intenta de nuevo o ponte en contacto con soporte@portaliaplus.com");
                  console.log(err);
                });
            }
          })
          .catch(function(err) {
            console.log(err);
          })

      };
    });

})();
