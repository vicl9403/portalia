'use strict';
(function(){

class OportunidadesDeNegocioComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('oportunidadesDeNegocio', {
    templateUrl: 'app/oportunidades-de-negocio/oportunidades-de-negocio.html',
    controller: OportunidadesDeNegocioComponent
  }).controller('oportunidadesController', function($scope, $http, $window, Auth, $timeout, $routeParams, $route) {

    $scope.quotes = [];

    // Función para obtener todas las cotizaciones
    $scope.getQuotes = function ( filters = null)
    {
      if (filters == null)
        filters = $routeParams

      $http.post('/api/quotes/getAllQuotes', filters)
        .then(function(res) {

          $scope.quotes = res.data.documents[0];
          $scope.totalPages = res.data.totalPages;
          $scope.totalResults = res.data.totalResults;
          $scope.currentPage = res.data.currentPage;
          $scope.updatePagination();

          $scope.sectors = res.data.sectors;
          $scope.categories = res.data.categories;
          $scope.states = res.data.states;

          console.log(res.data);

        });
    }

    $("#sector").change( function () {
      let sector = $("#sector").val();
      $scope.setFilter(sector , 'sector' );
    });

    $("#category").change( function () {
      let sector = $("#category").val();
      $scope.setFilter(sector , 'category' );
    });

    $("#state").change( function () {
      let sector = $("#state").val();
      $scope.setFilter(sector , 'state' );
    });


    $scope.range = function(min, max, step) {
      step = step || 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
        input.push(i);
      }
      return input;
    };

    $scope.updatePagination = function() {
    $scope.next = parseInt($scope.currentPage) + 1;
    $scope.before = parseInt($scope.currentPage) - 1;

    if ($scope.totalPages >= 10)
    {
      if ($scope.totalPages - $scope.currentPage >= 5)
      {
        if ($scope.currentPage - 10 < 0)
        {
          $scope.lowerLimit = 0;
          $scope.upperLimit = 9;
        }
        else
        {
          $scope.upperLimit = parseInt($scope.currentPage) + 4;
          $scope.lowerLimit = parseInt($scope.upperLimit) - 9;
        }
      }
      else
      {
        $scope.upperLimit = parseInt($scope.totalPages) - 1;
        $scope.lowerLimit = parseInt($scope.totalPages) - 10;
      }
    }
    else
    {
      $scope.lowerLimit = 0;
      $scope.upperLimit = $scope.totalPages - 1;
    }

  }

    $scope.setFilter = function(data, key) {
      //Variable auxiliar para los parámetros
      var param = {};

      if (data != 'requestedPage')
        param['requestedPage'] = 1;

      //Setear el parametro que se mando
      param[key] = data;

      // $scope.alphabetical = '';

      //Actualizar los parámetros de la URL
      $route.updateParams(param);

      //Obtener los nuevos resultados
      $scope.getQuotes();
      $("html, body").animate({
        scrollTop: 300
      }, "slow");

    }


    $scope.sendToQuote = function (id) {
      swal({
          title: "Te invitamos a probar uno de nuestros beneficios de clientes que ya cuentan con membresía.",
          text: "Por tiempo limitado podrás ver el contacto del solicitante de tu cotización, que ya cuentan con su membresía.",
          type: "warning",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "¡Entendido!",
          closeOnConfirm: false
        },
        function(){
          $window.location = '/detalle-cotizacion/' + id
        });
    }

    $scope.isSelect = function () {
      var logged = Auth.isLoggedIn;

      if( logged() )
      {
        var user = Auth.getCurrentUser() ;
        return ( user.paquete == 'plus' || user.paquete == 'select' )
      }
      return false;
    }

    // Inicialización del sitio
    angular.element(document).ready(function () {

      $timeout(function() {

        // Obtener todas las cotizaciones
        $scope.getQuotes()

      }, 100);



    });


  });

})();
