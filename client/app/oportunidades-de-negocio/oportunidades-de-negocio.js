'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/oportunidades-de-negocio', {
        template: '<oportunidades-de-negocio></oportunidades-de-negocio>'
      });
  });
