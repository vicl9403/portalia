'use strict';

describe('Component: OportunidadesDeNegocioComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var OportunidadesDeNegocioComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    OportunidadesDeNegocioComponent = $componentController('OportunidadesDeNegocioComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
