'use strict';
(function() {

  angular.module('portaliaApp')
    .component('buscarZonaGeografica', {
      templateUrl: 'app/buscarZonaGeografica/buscarZonaGeografica.html'
    }).controller('zonaGeograficaClr', function($scope, $http, $window, $routeParams, Auth) {
      $scope.prueba = "MAX AMMO";
      $scope.data = {};
      $scope.datos = {};
      $scope.posicionSector;
      $scope.datos.sector = {};
      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
          if ($routeParams.sector != undefined || $routeParams.categoria != undefined) {
            // var indexCat = response.data.name.indexOf($routeParams.sector)
            for (var x = 0; x < response.data.length; x++) { //For a traves de todo favoritos
              if ($scope.exists === true) { //Si el valor existe rompe el ciclo
                break;
              } else if (response.data[x].name == $routeParams.sector) { // Si no pasa aquí, y si el valor actual en favoritos es igual al que quieres mandar entra
                $scope.exists = true; //le da true al exists para que al siguiente loop salga del for
                $scope.indexCat = response.data.indexOf(response.data[x])
              }
            }
            // $('#sector option[value="' + indexCat + '"]').prop("selected", true);
          }
        });

      $http.get("api/estados")
        .then(function(response) {
          $scope.estados = response.data;
        });

      window.onload = function() {
        $('#sector option[value="' + $scope.indexCat + '"]').prop("selected", true);
        $scope.changeSector($scope.indexCat);
        $scope.buscar();
      };

      $scope.changeSector = function(sector) {
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un sub sector</option>')
          .val('');

        for (var x in $scope.sectors[sector].subsectores) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, x));
        }

        $scope.datos.sector._id = $scope.sectors[sector]._id
        $scope.posicionSector = sector

        if ($routeParams.categoria != undefined) {
          for (var i = 0; i < document.getElementById("categoria").length; ++i) {
            if (document.getElementById("categoria").options[i].text == $routeParams.categoria) {
              $scope.indexSec = document.getElementById("categoria").options[i].value
              $('#categoria option[value="' + $scope.indexSec + '"]').prop("selected", true);
              $scope.datos.index = $scope.indexSec;
            }
          }
        }
        // $scope.temp.sector_position = sector;
        // $scope.temp.sector = $scope.sectors[sector]._id;
        // $scope.temp.sector_name = $scope.sectors[sector].name;
      };


      $scope.changeCategoria = function(categoria) {
        $scope.datos.index = categoria
      };

      $scope.setRadio = function(radio) {
        $scope.datos.radio = radio
      }

      $scope.changeEstado = function(estado) {
        $('#municipio')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Municipio de Búsqueda</option>')
          .val('');

        $('#longitude').val("");
        $('#latitude').val("");

        $scope.estados[estado].municipios.sort(function(a, b) {
          var textA = a.name.toUpperCase();
          var textB = b.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        for (var x in $scope.estados[estado].municipios) {
          $("#municipio").append(new Option($scope.estados[estado].municipios[x].name, $scope.estados[estado].municipios[x]._id));
        }
      };

      $scope.$watch('datos.sector._id', function() {
        if ($scope.datos.sector._id != undefined) {
          $scope.buscar();
        }
      }, true);

      $scope.$watch('datos.index', function() {
        if ($scope.datos.index != undefined) {
          $scope.buscar();
        }
      }, true);

      $scope.$watch('datos.radio', function() {
        if ($scope.datos.radio != undefined) {
          $('#estado').val("");
          $scope.datos.estado = undefined;

          $('#municipio')
            .find('option')
            .remove()
            .end()
            .append('<option disabled selected value="">Municipio de Búsqueda</option>')
            .val('');

          $scope.datos.municipio = undefined;
          $scope.buscar();
        }
      }, true);

      $scope.$watch('datos.estado', function() {
        if ($scope.datos.estado != undefined) {
          $scope.datos.radio = undefined;
          $('#radio').val("");
          $('#longitude').val("");
          $('#latitude').val("");
          $('#pac-input').val("");
          $scope.datos.estado_name = $scope.estados[$scope.datos.estado].name;
          //alert($scope.datos.estado_name);
          $scope.buscar();
        }
      }, true);

      $scope.$watch('datos.municipio', function() {
        if ($scope.datos.municipio != undefined) {
          $scope.datos.radio = undefined;
          $('#radio').val("");
          $('#longitude').val("");
          $('#latitude').val("");
          $('#pac-input').val("");
          $scope.datos.municipio_name = $scope.estados[$scope.datos.estado].municipios[$("#municipio").prop('selectedIndex') - 1].name;
          // alert($scope.datos.municipio_name);
          $scope.buscar();
        }
      }, true);

      $scope.buscar = function() {
        $scope.datos.sector_name = $("#sector option:selected").text();
        $scope.datos.categoria_name = $("#categoria option:selected").text();
        console.log($scope.datos);
        $scope.datos.longitude = $('#longitude').val();
        $scope.datos.latitude = $('#latitude').val();
        if ($scope.datos.latitude.length == 0 && $scope.datos.latitude.length == 0 && $scope.datos.estado == undefined) {
          sweetAlert("Faltan datos para efectuar la búsqueda, puedes autocompletar la dirección si no dispones de GPS o selecciona un estado y/o municipio")
          return;
        }
        $http.post('/api/users/busquedaGeografica', $scope.datos)
          .then(function(res) {
            console.log(res);
            $scope.items = res.data;
            if ($scope.items.length > 0) {
              if ($scope.items.length == 1) {
                sweetAlert("Mostrando empresas", "Se encontró una empresa", "success");
              } else {
                sweetAlert("Mostrando empresas", "Se encontraron " + $scope.items.length + " empresas", "success");
              }
            } else {
              swal({
                title: "Opps!",
                text: "No se econtraron sucursales con estos criterios pero existen si más empresas listadas en el directorio: <a target='_blank' href='/resultadosObtenidos?sector=" + $scope.datos.sector_name + "&categoria=" + $scope.datos.categoria_name + "&ubicacion=" + $scope.datos.estado_name + "'>ver listado</a>",
                html: true
              });
            }
            sendResults();
          })
          .catch(function(err) {
            console.log(err);
          });
      }
    });

})();
