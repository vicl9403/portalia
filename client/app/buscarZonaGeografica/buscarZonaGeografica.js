'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/buscarZonaGeografica', {
        template: '<buscar-zona-geografica></buscar-zona-geografica>'
      });
  });
