'use strict';
(function(){

class VerificarEmpresaComponent {
  constructor() {
    
  }
}

angular.module('portaliaApp')
  .component('verificarEmpresa', {
    templateUrl: 'app/verificarEmpresa/verificarEmpresa.html',
    controller: VerificarEmpresaComponent
  }).controller('verificarEmpresaCrl', function( $scope, $http, $routeParams, $timeout)
  	{	
  		$scope.idEmp = $routeParams.id;
  		$scope.hiddenEmail = "";

  		$http.get('/api/sectors/')
  			.then(function(res)
  			{
  				$scope.sectors = res.data;
  			});

  		$http.get('/api/estados/')
  			.then(function(res)
    			{
    				$scope.estados = res.data;
    			});

      angular.element(document).ready(function () {
          $scope.data = {};
          $scope.data.email = '';
          $http.get('/api/users/' + $scope.idEmp)
            .then(function(response) {
              // Guardamos la respuesta en el scope
              $scope.empresa = response.data;
              // Agregamos el nombre de la empresa por defecto
              $scope.data.nombre = $scope.empresa.nombre;
              // Mandamos llamar una función que se encargará de cortar el mail
              $scope.hiddenEmail = hideEmail($scope.empresa.email);

              $scope.data.sector = $scope.empresa.sector_name;
            })

          $("html, body").animate({ scrollTop: 0 }, "slow");
      });

  		$scope.changeEstado =  function(estado)
  		{
  			$scope.municipios = $scope.estados[estado].municipios ;
  		}

      $scope.isValid = function()
      {
        // Conteo de errores 
        var errors = 0;
        if($scope.data.email == '' || $scope.data.email == undefined)
        {
          $scope.invalidEmail = true;
          errors ++;
        }
        else
          $scope.invalidEmail = false;

        if($scope.data.nombre == '' || $scope.data.nombre == undefined)
        {
          $scope.invalidName = true;
          errors ++;
        }
        else
          $scope.invalidName = false;

        if($scope.data.sector == '' || $scope.data.sector == undefined)
        {
          $scope.invalidSector = true;
          errors ++;
        }
        else
          $scope.invalidSector = false;

        if($scope.data.estado == '' || $scope.data.estado == undefined)
        {
          $scope.invalidEstado = true;
          errors ++;
        }
        else
          $scope.invalidEstado = false;

        if($scope.data.municipio == '' || $scope.data.municipio == undefined)
        {
          $scope.invalidMunicipio = true;
          errors ++;
        }
        else
          $scope.invalidMunicipio = false;

        if($scope.data.telefono == '' || $scope.data.telefono == undefined)
        {
          $scope.invalidPhone = true;
          errors ++;
        }
        else
          $scope.invalidPhone = false;

        return errors;
      }

      $scope.sendMail = function()
      {
        if($scope.isValid() == 0)
        {
          $timeout(function () {
            $scope.isSending =true;
            $http.post('/api/verificacionEmpresas', $scope.data)
              .then(function(response) {

                swal({
                  title: "Gracias por verificar",
                  text: "Hemos recibido tu información, nos pondremos en contacto a la brevedad para continuar el proceso de validación",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#F08F04",
                  confirmButtonText: "Ir al inicio",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) 
                    window.location.href = '/';

                });

              })
              .catch(function(err) {
                alert("Ups detectamos un error intenta de nuevo o ponte en contacto con soporte@portaliaplus.com");
                console.log(err);
              });
          }, 500);
        }
        else
          swal("Error en el formulario","por favor llena todos los campos requeridos", 'error');

      }
  		  
  		  

  	});

  	// Función para no mostrar el email completo, recibe un email
  	// y te regresa solo una porcion del mismo
	function hideEmail (email)
	{
		var result = email.split("@");
		return result[0].substring(0,3)+'..........@'+result[1];
	}

})();
