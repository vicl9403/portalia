'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/verificarEmpresa/:id', {
        template: '<verificar-empresa></verificar-empresa>'
      });
  });
