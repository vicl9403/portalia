'use strict';

describe('Component: VerificarEmpresaComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var VerificarEmpresaComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    VerificarEmpresaComponent = $componentController('VerificarEmpresaComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
