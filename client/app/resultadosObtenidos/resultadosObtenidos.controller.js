'use strict';
(function() {


  angular.module('portaliaApp')
    .component('resultadosObtenidos', {
      templateUrl: 'app/resultadosObtenidos/resultadosObtenidos.html'
    }).controller('resultadosObtenidosCrl', function($scope, $routeParams, $route, $http, $window, Auth, $location, $timeout, $rootScope) {
      $rootScope.canonical = 'https://portaliaplus.com/resultadosObtenidos';
    });

})();
