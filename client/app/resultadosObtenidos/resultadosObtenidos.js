'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/resultadosObtenidos/:param1?/:param2?/:param3?/:param4?/:param5?/:param6?/:param7?', {
        template: '<resultados-obtenidos></resultados-obtenidos>'
      });
  });
