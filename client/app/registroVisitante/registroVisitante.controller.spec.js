'use strict';

describe('Component: RegistroVisitanteComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RegistroVisitanteComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    RegistroVisitanteComponent = $componentController('RegistroVisitanteComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
