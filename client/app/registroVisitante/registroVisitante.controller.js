'use strict';
(function() {

  angular.module('portaliaApp')
    .component('registroVisitante', {
      templateUrl: 'app/registroVisitante/registroVisitante.html'
    })
    .controller('registroVCrl', function($scope, $http, $window, Auth, $location, $timeout) {
      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
        });

      $timeout(function() {

        console.log("this is your app's controller");
        $scope.response = null;
        $scope.widgetId = null;
        $scope.model = {
          key: '6Lf90yQTAAAAAMjWsw00jYqQi1bW5VddSKjVe4ut'
        };
        $scope.setResponse = function(response) {
          console.info('Response available');
          $scope.response = response;
        };
        $scope.setWidgetId = function(widgetId) {
          //alert("LOL")
          console.info('Created widget ID: %s', widgetId);
          $scope.widgetId = widgetId;
        };
        $scope.cbExpiration = function() {
          console.info('Captcha expired. Resetting response object');
          vcRecaptchaService.reload($scope.widgetId);
          $scope.response = null;
        };

      });


      //$("#contenedorMadre").css("height", "101px");
      $scope.data = {};
      $scope.data.suscripcion = {};
      $scope.data.suscripcion.selecciondas = [];
      $scope.temp = {
        sector: ""
      };
      $scope.data.role = "visitante";
      // $scope.data.suscripcion.activa = false;

      var codigo = $location.search().codigo;
      if (codigo)
        $scope.data.codigo_invitacion = codigo;

      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
        });


      $http.get("api/estados")
        .then(function(response) {
          $scope.estados = response.data;
        });

      $scope.changeSector = function(sector) {
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un sub sector</option>')
          .val('');

        for (var x in $scope.sectors[sector].subsectores) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, x));
        }
        $scope.temp.sector_position = sector;
        $scope.temp.sector = $scope.sectors[sector]._id;
        $scope.temp.sector_name = $scope.sectors[sector].name;
      };


      $scope.changeCategoria = function(categoria) {
        $scope.temp.categoria = $scope.sectors[$scope.temp.sector_position].subsectores[categoria]._id;
        $scope.temp.categoria_name = $scope.sectors[$scope.temp.sector_position].subsectores[categoria].name;
      };

      $scope.blog = function() {
        if ($('#blog').is(':checked')) {
          $("#boxes").show();
          $scope.data.suscripcion.activa = true
        } else if (!$('#blog').is(':checked')) {
          $("#boxes").hide();
          $scope.data.suscripcion.activa = false
        }
      }

      $scope.uno = function() {
        if ($('#uno').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[0]._id)
        } else if (!$('#uno').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[0]._id), 1);
        }
      }

      $scope.dos = function() {
        if ($('#dos').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[1]._id)
        } else if (!$('#dos').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[1]._id), 1);
        }
      }

      $scope.tres = function() {
        if ($('#tres').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[2]._id)
        } else if (!$('#tres').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[2]._id), 1);
        }
      }

      $scope.cuatro = function() {
        if ($('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[3]._id)
        } else if (!$('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[3]._id), 1);
        }
      }

      $scope.cinco = function() {
        if ($('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[4]._id)
        } else if (!$('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[4]._id), 1);
        }
      }

      $scope.seis = function() {
        if ($('#seis').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[5]._id)
        } else if (!$('#seis').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[5]._id), 1);
        }
      }

      $scope.changeEstado = function(estado) {
        $('#municipio')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un municipio</option>')
          .val('');

        $scope.estados[estado].municipios.sort(function(a, b) {
          var textA = a.name.toUpperCase();
          var textB = b.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        for (var x in $scope.estados[estado].municipios) {
          $("#municipio").append(new Option($scope.estados[estado].municipios[x].name, $scope.estados[estado].municipios[x]._id));
        }
        $scope.temp.estado = $scope.estados[estado]._id;
      };

      $scope.checkEmail = function() {

        $http.post('/api/users/check', {
            email: $scope.data.email
          })
          .then(function(res) {
            console.log(res);
            $scope.checkedEmail = true;
            if (res.status == 201) {
              $scope.isValidEmail = true;
              document.getElementById("boton").disabled = false;
              $("#boton").css("border", "#f08f04");
              $("#boton").css("background-color", "#f08f04");
            } else if (res.status == 200) {
              $scope.isValidEmail = false;
              document.getElementById("boton").disabled = true;
              $("#boton").css("border", "grey");
              $("#boton").css("background-color", "grey");
            }
          })
          .catch(function(err) {
            console.log(err);
          })
      }

      $(document).ready(function() {
        $("#formEmail").focusout(function() {
          $scope.checkEmail();
        });
      });

      $scope.registrar = function() {
        if (document.getElementById('terms').checked) {
          if (!$scope.registro.$invalid) {
            if ($scope.data.password == $scope.data.password2 && $scope.data.email == $scope.data.email2) {
              if ($scope.response == null || $scope.response.length > 0)
                $scope.captcha = true;
              else
                $scope.captcha = false;

              if ($scope.captcha == true) {

                Auth.createUser($scope.data)
                  .then(function(response) {
                    $http.post('/api/mailConfirmations', $scope.data)
                      .then(function(response) {
                        console.log(response);
                      })
                      .catch(function(err) {
                        console.log(err);
                      });
                    console.log(response);

                    var data = {};
                    data.subject = 'Correo de confirmacion Portalia Plus';
                    data.email = $scope.data.email;

                    $http.post('/api/email/confirmacion', data)
                      .then(function(response) {
                        console.log(response);
                        if (response.status === 200) {

                        } else {
                          //$window.alert('hubo un problema enviando el correo, intentelo de nuev');
                        }
                      })
                      .catch(function(err) {
                        console.log(err);
                      });
                    $window.location.href = '/registroExitosoVisitante';
                  })
                  .catch(function(err) {
                    console.log(err);
                  });

              } else {
                alert("Debes verificar el Captcha");
              }
            } else {
              //grecaptcha.reset();
              alert("Los correos o contraseñas no coinciden");
            }
          } else {
            //grecaptcha.reset();
            alert("Debes verificar el Captcha");
          }
        } else {
          //grecaptcha.reset();
          alert("Debes aceptar los Términos y Condiciones")
        }
      };

    });


})();
