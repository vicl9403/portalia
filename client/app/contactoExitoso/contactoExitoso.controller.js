'use strict';

(function(){

class ContactoExitosoComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('contactoExitoso', {
    templateUrl: 'app/contactoExitoso/contactoExitoso.html',
    controller: ContactoExitosoComponent,
    controllerAs: 'contactoExitosoCtrl'
  });

})();
