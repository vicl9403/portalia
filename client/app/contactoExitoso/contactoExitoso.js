'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/contactoExitoso', {
        template: '<contacto-exitoso></contacto-exitoso>'
      });
  });
