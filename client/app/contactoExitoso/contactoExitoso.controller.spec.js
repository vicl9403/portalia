'use strict';

describe('Component: ContactoExitosoComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var ContactoExitosoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    ContactoExitosoComponent = $componentController('contactoExitoso', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
