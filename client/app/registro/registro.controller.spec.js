'use strict';

describe('Component: RegistroComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RegistroComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    RegistroComponent = $componentController('RegistroComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
