'use strict';
(function() {

  angular.module('portaliaApp')
    .component('registro', {
      templateUrl: 'app/registro/registro.html'
    })
    .controller('registroCrl', function($scope, $http, $window, Auth, $location, vcRecaptchaService, $timeout, $routeParams) {


      $scope.tags = [];
      $scope.commonRegisters = [];

      $scope.getCommonRegisters = function () {
            $http.get('/api/commonRegisters/').then(function (res) {
                $scope.commonRegisters = res.data;
            }, function (resp) {
                //
            });
        };

      $scope.addCountRegister = function () {
          var id=  $( "#commonRegister option:selected" ).val();
          $http.patch('/api/commonRegisters/incrementCount/'+ id ).then(function (res) {

          }, function (resp) {
              //
          });
      }

      $scope.getTags = function () {
        var filter = { 'category_name': $("#categoria option:selected").text() };

        if(filter != '') {
            $http.post('/api/suggestedTags/find', filter).then(function (res) {
                $scope.tags = res.data.tags;
            }, function (resp) {
                $scope.tags = [];
            });
        }
      };

      $scope.removeTag = function(item) {
        if($scope.tags.length <= 1)
          swal("Error", "Debes dejar al menos una palabra clave","error")
        else
        {
          var index = $scope.tags.indexOf(item);
          $scope.tags.splice(index, 1);
        }
      }

      angular.element(document).ready(function () {

          $("html, body").animate({ scrollTop: 0 }, "slow");

          $scope.data = {};
          $scope.data.email = "";

          $scope.getCommonRegisters();

      });

      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
        });

      if ($routeParams.email) {
        $scope.datosPre = {};
        $scope.datosPre.email = $routeParams.email;
        console.log($scope.datosPre);
        $http.post('/api/preUsers/busqueda', $scope.datosPre)
          .then(function(res) {
            console.log(res.data);
            $scope.data = res.data;
            $scope.data.suscripcion = {};
            $scope.data.datosDeFacturacion = {};
            $scope.data.suscripcion.selecciondas = [];
            $scope.data.role = "empresa";
            delete $scope.data._id
            $( "#formEmail" ).focus();
            $timeout(function() {
              $scope.changeEstado($scope.data.estado)
            }, 3000);
          })
      }

      if ($routeParams.id) {
        $http.get('/api/preUsers/' + $routeParams.id)
          .then(function(res) {
            console.log(res.data);
            $scope.data = res.data;
            $scope.data.suscripcion = {};
            $scope.data.datosDeFacturacion = {};
            $scope.data.suscripcion.selecciondas = [];
            $scope.data.role = "empresa";
            delete $scope.data._id
            $( "#formEmail" ).focus();
            $timeout(function() {
              $scope.changeEstado($scope.data.estado)
            }, 3000);
          })
      }

      $scope.getCode = function() {
        $scope.typed = true;
        console.log($scope.datosCupon);
        $http.post('/api/codigos/busqueda/', $scope.datosCupon)
          .then(function(response) {
            console.log(response.data);
            if (response.data != null) {
              if (response.data.fechaInicial < new Date().toISOString() && response.data.fechaFinal > new Date().toISOString()) {
                if (response.data.usosRestantes == 0) {
                  $scope.usado = true;
                } else {
                  if (response.data.email != undefined) {
                    if (response.data.email == $scope.data.email) {
                      $scope.codigoObtenido = response.data
                    } else {
                      $scope.correoDif = true;
                    }
                  } else {
                    $scope.codigoObtenido = response.data
                      // $("#aplicar").prop("disabled", false);
                  }
                }
              } else {
                $scope.vigenciaIncorrecta = true;
              }
            } else {
              $scope.codigoObtenido = null;
              $scope.vigenciaIncorrecta = false;
              $scope.usado = false;
              $scope.yaUsado = false;
              $scope.correoDif = false;
            }
          });
      }

      $timeout(function() {

        $scope.response = null;
        $scope.widgetId = null;
        $scope.model = {
          key: '6Lf90yQTAAAAAMjWsw00jYqQi1bW5VddSKjVe4ut'
        };
        $scope.setResponse = function(response) {
          console.info('Response available');
          $scope.response = response;
        };
        $scope.setWidgetId = function(widgetId) {
          console.info('Created widget ID: %s', widgetId);
          $scope.widgetId = widgetId;
        };
        $scope.cbExpiration = function() {
          console.info('Captcha expired. Resetting response object');
          vcRecaptchaService.reload($scope.widgetId);
          $scope.response = null;
        };

      });

      $scope.data = {};
      $scope.data.suscripcion = {};
      $scope.data.datosDeFacturacion = {};
      $scope.data.suscripcion.selecciondas = [];
      $scope.temp = {
        sector: ""
      };
      $scope.data.role = "empresa";

      var codigo = $location.search().codigo;
      if (codigo)
        $scope.data.codigo_invitacion = codigo;

      $http.get("api/sectors")
        .then(function(response) {
          $scope.sectors = response.data;
          if ($routeParams.email || $routeParams.id) {
            var index = -1;
            for (var i = 0; i < response.data.length; i++) {
              if (response.data[i].name.toUpperCase() === $scope.data.sector_name.toUpperCase()) {
                index = i;
                $timeout(function() {
                  $("#sector option[value='" + i + "']").prop('selected', true);
                  $scope.changeSector(i)
                }, 3000);
                console.log(index);
                break;
              }
            }
          }
        });


      $http.get("api/estados")
        .then(function(response) {
          $scope.estados = response.data;
        });

      $scope.changeSector = function(sector) {
        $('#categoria')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un sub sector</option>')
          .val('');

        for (var x = 0; x < $scope.sectors[sector].subsectores.length; x++) {
          $("#categoria").append(new Option($scope.sectors[sector].subsectores[x].name, x));
        }
        $scope.temp.sector_position = sector;
        $scope.temp.sector = $scope.sectors[sector]._id;
        $scope.temp.sector_name = $scope.sectors[sector].name;
        if($routeParams.email || $routeParams.id) {
          $("#categoria option[value='" + $scope.data.categoria + "']").prop('selected', true);
          $scope.changeCategoria($scope.data.categoria);
        }

        //Actualizar las tags sugeridas
        $scope.getTags();

      };

      $scope.goToVerificacion = function(id)
      {
        window.location.href = "/verificarEmpresa/"+id;
      }

      $("#categoria").on("change", function () {
          //Actualizar las tags sugeridas
          $scope.getTags();
      });


      $scope.changeCategoria = function(categoria) {
        //Actualizar tags recomendadas
        $scope.getTags();

        $scope.temp.categoria = $scope.sectors[$scope.temp.sector_position].subsectores[categoria]._id;
        $scope.temp.categoria_name = $scope.sectors[$scope.temp.sector_position].subsectores[categoria].name;
      };

      $scope.blog = function() {
        if ($('#blog').is(':checked')) {
          $("#boxes").show();
          $scope.data.suscripcion.activa = true
        } else if (!$('#blog').is(':checked')) {
          $("#boxes").hide();
          $scope.data.suscripcion.activa = false
        }
      };

      $scope.uno = function() {
        if ($('#uno').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[0]._id)
        } else if (!$('#uno').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[0]._id), 1);
        }
      }

      $scope.dos = function() {
        if ($('#dos').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[1]._id)
        } else if (!$('#dos').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[1]._id), 1);
        }
      }

      $scope.tres = function() {
        if ($('#tres').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[2]._id)
        } else if (!$('#tres').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[2]._id), 1);
        }
      }

      $scope.cuatro = function() {
        if ($('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[3]._id)
        } else if (!$('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[3]._id), 1);
        }
      }

      $scope.cinco = function() {
        if ($('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[4]._id)
        } else if (!$('#cuatro').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[4]._id), 1);
        }
      }

      $scope.seis = function() {
        if ($('#seis').is(':checked')) {
          $scope.data.suscripcion.selecciondas.push($scope.sectors[5]._id)
        } else if (!$('#seis').is(':checked')) {
          $scope.data.suscripcion.selecciondas.splice($scope.data.suscripcion.selecciondas.indexOf($scope.sectors[5]._id), 1);
        }
      }

      $scope.changeEstado = function(estado) {
        $('#municipio')
          .find('option')
          .remove()
          .end()
          .append('<option value="">Selecciona un municipio</option>')
          .val('');

        $scope.estados[estado].municipios.sort(function(a, b) {
          var textA = a.name.toUpperCase();
          var textB = b.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        for (var x in $scope.estados[estado].municipios) {
          $("#municipio").append(new Option($scope.estados[estado].municipios[x].name, $scope.estados[estado].municipios[x]._id));
        }
        $scope.temp.estado = $scope.estados[estado]._id;
      };

      $scope.checkEmail = function() {

        $http.post('/api/users/check', {
            email: $scope.data.email
          })
          .then(function(res) {
            console.log(res);
            $scope.checkedEmail = true;
            if (res.status == 201) {
              $scope.isValidEmail = true;
              document.getElementById("boton").disabled = false;
              $("#boton").css("border", "#f08f04");
              $("#boton").css("background-color", "#f08f04");
            }
            else if (res.status == 200)
            {
              $scope.isValidEmail = false;
              document.getElementById("boton").disabled = true;
              $("#boton").css("border", "grey");
              $("#boton").css("background-color", "grey");

              $scope.data.email =  "";
              swal({
                title: "Ya tenemos registrado ese correo",
                text: "No podemos registrar otra persona con ese correo, prueba con otro correo o restablece tu contraseña",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f08f04",
                confirmButtonText: "Restablecer contraseña",
                closeOnConfirm: false
              },
              function(){
                window.location.href = '/reestablecerContrasena';
              });

              return false;
            }
          })
          .catch(function(err) {
            console.log(err);
          })
      };

      $scope.isEqualPass = function()
      {
        if($scope.data.password == $scope.data.password2)
          $scope.equalPass = true;
        else
          $scope.equalPass = false;

        return $scope.equalPass;
      }
      $scope.isEqualEmail = function()
      {

        if($scope.data.email == $scope.data.email2)
          $scope.equalEmail = true;
        else
          $scope.equalEmail = false;

        return $scope.equalEmail;
      };

      $scope.validateName = function(nombre)
      {
        $scope.empresaNombre = nombre;
         if ($scope.empresaNombre.length<2)
         {
           swal('Error' ,'Ingresa al menos 2 caracteres para que sea un nombre valido','error');
           return;
         }
         else
         {
           var valid = /^(?=(?:[^\A-Za-z0-9]*[\A-Za-z0-9]){2})[~,?,!]*\S+(?: \S+){0,}$/;
           return valid.test($scope.empresaNombre);
         }
      };

      $scope.checkBussines = function()
      {
        $scope.verifying = $scope.validateName($scope.data.nombre);
        if (!$scope.verifying)
        {
          swal('Error' ,'Nombre de la empresa invalido, contiene caracteres extraños','error');
          return;
        }
        if($scope.data.nombre == '' || $scope.data.nombre == undefined)
        {
          swal('Error' ,'Tienes que ingresar un nombre de la empresa y verificarlo','error');
          $scope.verifying = false;
        }
        else
        {
          //Tiempo de espera para mejores efectos visuales
          $timeout(function()
          {
            //Arreglo para poder buscar
            var nombre = { 'keyword' :  $scope.data.nombre}

            //Mandamos llamar el api para buscar el nombre de la empresa
            $http.post('/api/users/busquedaTags', nombre)
              .then(function(res) {
                if (res.data.length == 0)
                {
                  //Significa que el nombre de la empresa no existe
                  $scope.verifyied = true;
                }
                else
                {
                  $scope.coincidencias = res.data;

                  // Primero traemos todos los estados de la base de datos
                  $http.get('/api/estados')
                  .then(function(res){

                    // Recuperamos la variable de resultado
                    var estados = res.data;

                    // Seteamos variable de control a falso
                    var showConcidencias = false;

                    // Iteramos sobre todas las coincidencias encontradas con el nombre de la empresa
                    for(var i=0; i<$scope.coincidencias.length; i++)
                    {

                      // Iteramos sobre todos los estados
                      for(var c=0; c < estados.length; c++)
                      {
                        // Iteramos sobre todos los municipios de cada estado
                        for(var d=0; d < estados[c].municipios.length; d++)
                        {

                          // Cuando el municipio en iteración contiene el mismo índice que
                          // el índice de la coincidencia en cuestion, lo remplaza con el
                          // nombre del municipio para que en la vista se vea el nombre, no el id
                          if(res.data[c].municipios[d]._id.toString() == $scope.coincidencias[i].municipio.toString())
                            $scope.coincidencias[i].municipio = res.data[c].municipios[d].name;

                        }
                      }

                      // Obtener el índice del estado al que corresponde
                      var indexEstado = $scope.coincidencias[i].estado;

                      // Remplazar la posición del estado por el nombre del estado
                      $scope.coincidencias[i].estado = res.data[indexEstado].name;

                      if($scope.coincidencias[i].nombre.toUpperCase() == $scope.data.nombre.toUpperCase() )
                        showConcidencias = true
                    }

                    if(showConcidencias== true)
                    {
                      $("#modalVerify").modal('show');

                      // Negamos la verificación
                      $scope.verifyied = false;
                      return;
                    }

                  })


                  //Si acabo el ciclo quiere decir que no encontro coincidencia exacta
                  // por lo tanto no es necesario verificar
                  $scope.verifyied = true;
                }


              })
              .catch(function(err) {
                console.log(err);
              })

            $scope.verifying = false;

          }, 500);


        }

      };


      $scope.enviar = function() {
        $http.post('/api/mailConfirmations', $scope.data)
          .then(function(response) {
            $window.location.href = '/registroExitoso';
          })
          .catch(function(err) {
            console.log(err);
          });
      };

      $scope.registrar = function() {
        if($scope.verifyied != true)
        {
          swal('Error' ,'Tienes que ingresar un nombre de la empresa y verificarlo','error');
          return;
        }
        if (document.getElementById('terms').checked) {
          if (!$scope.registro.$invalid) {
            if ($scope.data.password == $scope.data.password2 && $scope.data.email == $scope.data.email2) {
              if ($scope.response == null || $scope.response.length > 0)
                $scope.captcha = true;
              else
                $scope.captcha = false;

              if ($scope.captcha == true) {
                if ($scope.codigoObtenido != null) {
                  $scope.codigoObtenido.usosRestantes = $scope.codigoObtenido.usosRestantes - 1;
                  $scope.data.paquete = $scope.codigoObtenido.paquete;
                  if ($scope.codigoObtenido.unidadOfrecida == 'd') {
                    $scope.data.fechaCorte = moment().add($scope.codigoObtenido.numeroOfrecido, 'days')
                    $scope.data.fechaCorte = $scope.data.fechaCorte.toISOString();
                  } else if ($scope.codigoObtenido.unidadOfrecida == 'm') {
                    $scope.data.fechaCorte = moment().add($scope.codigoObtenido.numeroOfrecido, 'months')
                    $scope.data.fechaCorte = $scope.data.fechaCorte.toISOString();
                  } else if ($scope.codigoObtenido.unidadOfrecida == 'y') {
                    $scope.data.fechaCorte = moment().add($scope.codigoObtenido.numeroOfrecido, 'years')
                    $scope.data.fechaCorte = $scope.data.fechaCorte.toISOString();
                  }
                }

                //Agregar las tags por defecto
                $scope.data.tags = $scope.tags;

                $scope.data.sector = $scope.temp.sector;
                $scope.data.sector_name = $scope.temp.sector_name;
                $scope.data.categoria_name = $scope.temp.categoria_name;
                $scope.data.emailSec = $scope.data.email
                $scope.data.datosDeFacturacion.razon = $scope.data.razon
                $scope.data.datosDeFacturacion.rfc = $scope.data.rfc
                $scope.data.datosDeFacturacion.correoFactu = $scope.data.email;
                $scope.data.estado_name = $scope.estados[$scope.data.estado].name;
                for(var x = 0; x < $scope.estados[$scope.data.estado].municipios.length; x++){
                  if($scope.estados[$scope.data.estado].municipios[x]._id == $scope.data.municipio){
                    $scope.data.municipio_name = $scope.estados[$scope.data.estado].municipios[x].name;
                  }
                }
                console.log($scope.data);
                Auth.createUser($scope.data)
                  .then(function(response) {

                    // Se agrega una cuenta a los registros comunes
                    $scope.addCountRegister();

                    $http.post('/api/mailConfirmations', $scope.data)
                      .then(function(response) {})
                      .catch(function(err) {
                        console.log(err);
                      });

                    var data = {};
                    data.subject = 'Correo de confirmacion Portalia Plus';
                    data.email = $scope.data.email;

                    $http.post('/api/email/confirmacion', data)
                      .then(function(response) {
                        console.log(response);
                        if (response.status === 200) {

                        } else {
                          //$window.alert('hubo un problema enviando el correo, intentelo de nuev');
                        }
                      })
                      .catch(function(err) {
                        console.log(err);
                      });
                    $window.location.href = '/registroExitoso';
                  })
                  .catch(function(err) {
                    console.log(err);
                  });

              } else {
                alert("Debes verificar el Captcha");
              }
            } else {
              //grecaptcha.reset();
              alert("El correo o la contraseña no coinciden");
            }
          } else {
            //grecaptcha.reset();
            swal("Error","completa todos los campos en rojo y verifica la recaptcha" , 'error');
          }
        } else {
          //grecaptcha.reset();
          alert("Debes aceptar los Términos y Condiciones")
        }
      };

      $scope.testFunction = function()
      {
      };

    });


})();
