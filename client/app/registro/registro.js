'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/registro', {
        template: '<registro></registro>',
        resolve: {
          "check": function (Auth, $location) {
            Auth.isLoggedIn(function (response) {
              console.log(response);
              if (response) {
                $location.path('/'); //redirect user to home.
              }
            });
          }
        }
      });
  });
