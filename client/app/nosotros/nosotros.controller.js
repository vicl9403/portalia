'use strict';
(function(){

class NosotrosComponent {
  constructor() {
    this.message = 'Hello';

  }
}

angular.module('portaliaApp')
  .component('nosotros', {
    templateUrl: 'app/nosotros/nosotros.html',
    controller: NosotrosComponent
  }).controller('NosotrosCtr', function ($scope, $http, $window, Auth) {


    $http.get('/api/users/countsectors/Agroindustria')
      .then(function (res) {
      $scope.agroindustria = res.data;
    });

		$http.get('/api/users/countsectors/Construcción')
  			.then(function (res) {
				$scope.construccion = res.data;
			});

    $http.get('/api/users/countsectors/Electricidad y agua')
      .then(function (res) {
      $scope.electricidad = res.data;
    });

    $http.get('/api/users/countsectors/Industrias manufactureras')
      .then(function (res) {
      $scope.industrias = res.data;
    });

		$http.get('/api/users/countsectors/Servicios especializados')
  			.then(function (res) {
				$scope.servicios = res.data;
			});

		$http.get('/api/users/countsectors/Turismo')
  			.then(function (res) {
				$scope.turismo = res.data;
			});





      $scope.adjustIframe = function(frame, div)
  		{
  			// Obtener el texto del Iframe
  	  		var videoHeigh = $("#"+frame).height();
  	  		var textHeigh = $("#"+div).height();

  	  		if( videoHeigh < textHeigh )
          {
            // Sacar la diferencia del teto con el video
            var dif = textHeigh - videoHeigh;

            // Ajustar el div
            if(dif > 0)
              $("#"+frame).css('margin-top',dif/2);
          }
  	  		else
          {
            // Sacar la diferencia del teto con el video
            var dif = videoHeigh - textHeigh;

            // Ajustar el div
            if(dif > 0)
              $("#"+div).css('margin-top',dif/2);
          }


  		}

  	  	angular.element(document).ready(function () {

          $scope.adjustIframe("nosotros-video","nosotros-text");
          $scope.adjustIframe("compromiso-video","compromiso-text");

  	    });

  	    angular.element($window).bind('resize', function(){

  	    	$scope.adjustIframe("nosotros-video","nosotros-text");
  	    	$scope.adjustIframe("compromiso-video","compromiso-text");

  		});

  });

})();


