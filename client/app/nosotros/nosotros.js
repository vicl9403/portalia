'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/nosotros', {
        template: '<nosotros></nosotros>'
      });
  });
