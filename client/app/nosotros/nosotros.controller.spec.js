'use strict';

describe('Component: NosotrosComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var NosotrosComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    NosotrosComponent = $componentController('NosotrosComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
