'use strict';

describe('Component: TopComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var TopComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    TopComponent = $componentController('top', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
