'use strict';

(function(){

angular.module('portaliaApp')
  .component('top', {
    templateUrl: 'app/top/top.html'
  }).controller('topCtrl', function($scope, $http, $window, Auth, $location){
    $scope.userData = Auth.getCurrentUser();
    $http.get('/api/topUsers/')
      .then(function(res){
        $scope.usuariosDestacados = [];
        console.log(res);
        for(var x = 0; x < res.data.length; x++){
          $scope.usuariosDestacados.push(res.data[x].idUser[0]);
        }
      })
      .catch(function(err){
        console.log(err); //refresh
      });

    $http.get('/api/busquedas/count')
    .then(function(response){
      $scope.busquedasPopulares = response.data;
      console.log($scope.busquedasPopulares);
    })
    .catch(function(error){
      console.log(error);
    });

    $scope.isSelectClass = function(paquete) {
      if (paquete == "select")
        return "thumbnail fondo-select"
      else
        return "thumbnail"
    }

    $scope.goToTermino = function(termino){
      $window.location = "/resultadosObtenidos/pagina-1/buscar-" + termino + "/tipo-producto/";
    }

  });

})();
