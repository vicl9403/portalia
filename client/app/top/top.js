'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/top', {
        template: '<top></top>'
      });
  });
