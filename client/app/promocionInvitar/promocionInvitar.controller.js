'use strict';
(function() {

  angular.module('portaliaApp')
    .component('promocionInvitar', {
      templateUrl: 'app/promocionInvitar/promocionInvitar.html'
    }).controller('PromocionInvitarCrtl', function($scope, $http, Auth, $location) {
      Auth.getCurrentUser(function(res) {
        if (res._id) {
          console.log(res);
          $scope.userData = res;

          $scope.datamail = {};
          $scope.datamail.to = [];
          $scope.datamail.subject = 'Correo de invitacion a Portalia Plus de ' + $scope.userData.nombre;
          $scope.datamail.name = $scope.userData.nombre;
          $scope.datamail._id = $scope.userData._id;

          $http.post('api/users/calcularCodigos')
            .then(function(response) {
              $scope.information = response.data;
              if (moment().isBefore('2016-11-15') && moment().isAfter('2016-10-01')) {
                $scope.information.total = 0
              }

            });

          var action = $location.search().action;
          if (action)
            $('#modal-amigos').modal('show')

        } else {
          $scope.ocultar = true;

          var action = $location.search().action;
          if (action)
            $('#myModal').modal('show')
        }
      });

      function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

      $scope.enviar = function() {

        var emails = document.getElementById('emails').getElementsByTagName('input');
        for (var i = 0; i < emails.length; i++) {
          if (validateEmail(emails[i].value))
            $scope.datamail.to.push(emails[i].value);
        }

        $http.post('/api/email/invitacion', $scope.datamail)
          .then(function(response) {
            console.log(response);
            if (response.status === 200) {
              $window.alert('Correos enviados');
            } else {
              $window.alert('hubo un problema enviando el correo, intentelo de nuev');
            }
          })
          .catch(function(err) {
            console.log(err);
          });

      };
    });

})();
