'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/promocionInvitar', {
        template: '<promocion-invitar></promocion-invitar>'
      });
  });
