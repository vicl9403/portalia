'use strict';

angular.module('portaliaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/registroExitosoVisitante', {
        template: '<registro-exitoso-visitante></registro-exitoso-visitante>'
      });
  });
