'use strict';

describe('Component: RegistroExitosoVisitanteComponent', function () {

  // load the controller's module
  beforeEach(module('portaliaApp'));

  var RegistroExitosoVisitanteComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    RegistroExitosoVisitanteComponent = $componentController('registroExitosoVisitante', {});
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
