'use strict';

(function(){

class RegistroExitosoVisitanteComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('portaliaApp')
  .component('registroExitosoVisitante', {
    templateUrl: 'app/registroExitosoVisitante/registroExitosoVisitante.html',
    controller: RegistroExitosoVisitanteComponent,
    controllerAs: 'registroExitosoVisitanteCtrl'
  });

})();
