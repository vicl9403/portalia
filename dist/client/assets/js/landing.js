$(document).ready(function() {

  var video = $("#video");
  video.on('click', function(e) {
    if ($(window).width() < 768) {
      var vid = video[0];
      vid.play();
      if (vid.requestFullscreen) {
        vid.requestFullscreen();
      } else if (vid.mozRequestFullScreen) {
        vid.mozRequestFullScreen();
      } else if (vid.webkitRequestFullscreen) {
        vid.webkitRequestFullscreen();
      }
    }
  });
  // Estilo necesario para landing
  // $(".ng-scope").css("height", '100%');
  // $(".footer").addClass('hidden');

  var scrollTop = $('.bloque-1').offset().top;
  var scrollBeneficios = $('.bloque-2').offset().top;
  var scrollLanzamiento = $('.bloque-3').offset().top;

  // Seteamos el primer activo de los controles
  if (window.pageYOffset < scrollBeneficios)
    setActive($('.first'), null);
  if (window.pageYOffset >= scrollBeneficios - 10 && window.pageYOffset < scrollLanzamiento)
    setActive($('.second'), $("#toBeneficios"));
  if (window.pageYOffset >= scrollLanzamiento - 300)
    setActive($('.third'), $("#toLanzamiento"));

  // Funcion para que se actualicen con scroll
  $(window).scroll(function() {
    if (window.pageYOffset < scrollBeneficios)
      setActive($('.first'), null);
    if (window.pageYOffset >= scrollBeneficios - 10 && window.pageYOffset < scrollLanzamiento)
      setActive($('.second'), $("#toBeneficios"));
    if (window.pageYOffset >= scrollLanzamiento - 300) {
      setActive($('.third'), $("#toLanzamiento"));

      //$("#video")[0].play();
    }
    // if(   (window.pageYOffset >= scrollBeneficios-20 && window.pageYOffset <= scrollBeneficios + 20 ) ||
    //       (window.pageYOffset >= scrollLanzamiento-20 && window.pageYOffset <= scrollLanzamiento + 20 ) ||
    //       window.pageYOffset == scrollTop)
    // {
    //   $('.navbar').removeClass('nav-dark');
    // }
    // else
    //   $('.navbar').addClass('nav-dark');

  });

  // Acciones de los controles y los links
  $(".first").click(function() {
    $('body').animate({
      scrollTop: $('.bloque-1').offset().top
    }, 'slow');
  });
  $(".second").click(function() {
    $('body').animate({
      scrollTop: $('.bloque-2').offset().top
    }, 'slow');
  });
  $(".third").click(function() {
    $('body').animate({
      scrollTop: $('.bloque-3').offset().top
    }, 'slow');
  });
  $("#btnConocenos").click(function() {
    $(".second").click();
  });
  $("#toBeneficios").click(function() {
    $(".second").click();
  });
  $("#toLanzamiento").click(function() {
    $(".third").click();
  });
  $(".to-lanzamiento").click(function() {
    $(".third").click();
  });





  $('.navbar-collapse a').click(function(e) {
    $('.navbar-collapse').collapse('toggle');
  });

  // Contador
  $(".soon").soon().create({
    due: "2016-08-27",
    layout: "group tight",
    face: "flip color-light corners-sharp shadow-soft",
    separateChars: false,
    labelsYears: 'Año,Años',
    labelsDays: 'Dia,Dias',
    labelsHours: 'Hora,Horas',
    labelsMinutes: 'Minuto,Minutos',
    labelsSeconds: 'Segundo,Segundos',
    padding: true,
    eventComplete: function() {
      //alert("done!");
    }
  });


});

function setActive(component, link) {
  $(".first").removeClass('bg-white');
  $(".second").removeClass('bg-white');
  $(".third").removeClass('bg-white');
  $("#toBeneficios").removeClass('nav-link-active');
  $("#toLanzamiento").removeClass('nav-link-active');

  if (link != null)
    link.addClass('nav-link-active');
  component.addClass('bg-white');
}

$(function() {
  $('[data-toggle=popover]').popover({
    trigger: 'hover',
    html: true,
  })
});


function makeFullScreen(divObj) {
  //Use the specification method before using prefixed versions
  if (divObj.requestFullscreen) {
    divObj.requestFullscreen();
  } else if (divObj.msRequestFullscreen) {
    divObj.msRequestFullscreen();
  } else if (divObj.mozRequestFullScreen) {
    divObj.mozRequestFullScreen();
  } else if (divObj.webkitRequestFullscreen) {
    divObj.webkitRequestFullscreen();
  } else {
    console.log("Fullscreen API is not supported");
  }

}
