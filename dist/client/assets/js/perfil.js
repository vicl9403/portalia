var tags;
var paquete;

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    (document.getElementById('pac-input')), {
      types: ['geocode']
    });

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  var place = autocomplete.getPlace();
  newLatLang = {
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng()
  }
  $('[ng-controller="perfilCrl"]').scope().userData.direccion = $('#pac-input').val();
  $('[ng-controller="perfilCrl"]').scope().place = place;
  $('[ng-controller="perfilCrl"]').scope().newLatLang = newLatLang;
  $('[ng-controller="perfilCrl"]').scope().updateAdress();
}


$(window).load(function() {
  tags = $('[ng-controller="perfilCrl"]').scope().userData.tags;
  paquete = $('[ng-controller="perfilCrl"]').scope().userData.paquete;
});

$(document).ready(function() {


  $('#tags').bind("cut copy paste", function(z) {
    z.preventDefault();
  });
  $('#tags').keydown(function(v) {
    if (v.keyCode === 13) {
      var inputs = $("#tags").find($("input"));
      if ((paquete == 'plus' && inputs.length < 100) || (paquete == 'select' && inputs.length < 50) || (paquete == 'gratis' && inputs.length < 10)) {
        $("#btnAddTag").trigger("click");
      } else {
        alert("Tu plan no permite añadir más tags")
      }
    } else if (v.keyCode === 188) {
      var inputs = $("#tags").find($("input"));
      v.preventDefault();
      v.stopPropagation();
      if ((paquete == 'plus' && inputs.length < 100) || (paquete == 'select' && inputs.length < 50) || (paquete == 'gratis' && inputs.length < 10)) {
        $("#btnAddTag").trigger("click");
      } else {
        alert("Tu plan no permite añadir más tags")
      }
    }
  });

  $('#productos').bind("cut copy paste", function(i) {
    i.preventDefault();
  });
  $('#productos').keydown(function(r) {
    if (r.keyCode === 13) {
      // alert("Enter");
      var inputsProd = $("#productos").find($("input"));
      if ((paquete == 'plus' && inputsProd.length < 100) || (paquete == 'select' && inputsProd.length < 50) || (paquete == 'gratis' && inputsProd.length < 10)) {
        $("#btnAddProd").trigger("click");
      } else {
        alert("Tu plan no permite añadir más tags")
      }
    } else if (r.keyCode === 188) {
      r.preventDefault();
      r.stopPropagation();
      // alert("Coma");
      var inputsProd = $("#productos").find($("input"));
      if ((paquete == 'plus' && inputsProd.length < 100) || (paquete == 'select' && inputsProd.length < 50) || (paquete == 'gratis' && inputsProd.length < 10)) {
        $("#btnAddProd").trigger("click");
      } else {
        alert("Tu plan no permite añadir más tags")
      }
    }
  });
  autosize($('textarea'));
  $("#portalito").addClass('navbar-fixed-top');
  // Habilitar input para su edición
  // $('#btnEditTitle').click(function () {
  // 	console.log("ñaklsdj");
  // 	$("#txtTitle").prop('disabled', function (i, v) {
  // 		return !v;
  // 	});
  // 	$("#txtTitle").focus();
  // });


  // Deshabilitar input, para terminar la edición
  // $("#txtTitle").on("blur", function () {
  // 	$(this).prop('disabled', true);
  // });

  // Agregar una tag
  $('#btnAddTag').on('click', function() {
    $('#btnSaveTags').show();
    var newTag = '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-0 micro-top item-tag" ng-repeat="t in userData.tags">' +
      '<div class="inner-addon right-addon ">' +
      '<i onclick="removeComponent(this,' + "'.item-tag'" + ')" class="glyphicon glyphicon-remove white remove-tag"></i> ' +
      '<input type="text" class="btn-warning micro-top form-control tag " value="" ng-model="t">' +
      '</div>' +
      '</div>';
    $("#tags").append(newTag);
    $('#tags input:last').focus();
  });

  $('#btnAddProd').on('click', function() {
    $("#btnEditProductos").text("Guardar");
    $("#btnEditProductos-xs i").removeClass("fa-edit");
    $("#btnEditProductos-xs i").addClass("fa-save");
    var newProd = '<div class="col-lg-3 item-prod" >' +
      '<div class="inner-addon right-addon ">' +
      '<i onclick="removeComponent(this,' + "'.item-prod'" + ')" class="glyphicon glyphicon-remove remove-prod"></i>' +
      '<input type="text"  class=" micro-top form-control input-productos" value="">' +
      '</div>' +
      '</div>';
    $("#productos").append(newProd);
    // $("#productos .item-prod div i input").focus();
    $("#productos input").each(function() {
      $(this).focus();
    });

  });




  // Eventos para remover
  $(".remove-tag").on("click", function() {
    removeComponent(this, '.item-tag');
  });
  $(".remove-prod").on("click", function() {
    removeComponent(this, '.item-prod');
  });

  // Evento para habilitar edicion
  $("#btnEditDesc").click(function() {
    edit($(this), $("#descripcion textarea"));

    // if($("#descripcionTA").val() == 'Escribe tu descripción aquí')
    // {
    //   $("#descripcionTA").val('');
    // }
  });

  // Evento para habilitar edicion
  // $("#btnEditFactores").click(function() {
  //   edit($(this), $("#favoritos textarea"));
  // });

  // Evento para habilitar edicion
  $("#btnEditDatos").click(function() {
    edit($(this), $("#datos input"));
  });

  // Eventos de validación de datos
  $("#datosUrl").on('blur', function() {

    if (isValidUrl($(this).val()) == false) {
      $(this).addClass('invalid');
      $("#error-url").removeClass('hidden');
    } else {
      $(this).removeClass('invalid');
      $("#error-url").addClass('hidden');
    }
  });
  $("#datosTelefono").on('blur', function() {

    if (isValidPhone($(this).val()) == false) {
      $(this).addClass('invalid');
      $("#error-telefono").removeClass('hidden');
    } else {
      $(this).removeClass('invalid');
      $("#error-telefono").addClass('hidden');
    }
  });
  // Eventos de validación de datos

  // Evento para habilitar edicion
  $("#btnEditProductos").click(function() {
    edit($(this), $("#productos input"));
  });

  // Evento para habilitar edicion
  $("#btnEditFavoritos").click(function() {
    if ($(this).text() == 'Editar') {
      $("#favoritos .favorite-free").each(function(i, obj) {
        $(obj).attr('src', 'assets/images/icons/error.svg');
        // Clase para poder eliminar cara uno
        $(obj).addClass('delete-favorite');
      });
      $(this).text('Guardar')
    } else {
      $("#favoritos .favorite-free").each(function(i, obj) {
        $(obj).attr('src', 'assets/images/icons/favorito-after.svg');
        // Quitar clase de eliminar
        $(obj).removeClass('delete-favorite');
      });
      $(this).text('Editar')
    }
  });
  $("#btnEditArticulos").click(function() {
    if ($(this).text() == 'Editar') {
      $("#guardados .favorite-free").each(function(i, obj) {
        $(obj).attr('src', 'assets/images/icons/error.svg');
        // Clase para poder eliminar cara uno
        $(obj).addClass('delete-favorite');
      });
      $(this).text('Guardar')
    } else {
      $("#guardados .favorite-free").each(function(i, obj) {
        $(obj).attr('src', 'assets/images/icons/favorito-after.svg');
        // Quitar clase de eliminar
        $(obj).removeClass('delete-favorite');
      });
      $(this).text('Editar')
    }
  });
  $(".favorite-free").on("click", function() {
    if ($(this).hasClass('delete-favorite'))
      removeComponent(this, '.item-favorito');
  });


  // Eventos para xs y sm
  $("#btnEditTitle-xs").click(function() {
    editXS($("#btnEditTitle-xs i"), $("#txtTitle input"));
  });
  $("#btnEditDesc-xs").click(function() {
    editXS($("#btnEditDesc-xs i"), $("#descripcion textarea"));
  });
  $("#btnEditFactores-xs").click(function() {
    editXS($("#btnEditFactores-xs i"), $("#favoritos textarea"));
  });
  $("#btnEditDatos-xs").click(function() {
    editXS($("#btnEditDatos-xs i"), $("#datos input"));
  });
  $("#btnEditProductos-xs").click(function() {
    editXS($("#btnEditProductos-xs i"), $("#productos input"));
  });

  $("#btnEditFavoritos-xs").click(function() {
    if ($("#btnEditFavoritos-xs i").hasClass('fa-edit')) {
      $("#favoritos .favorite-free").each(function(i, obj) {
        $(obj).attr('src', 'assets/images/icons/error.svg');
        // Clase para poder eliminar cara uno
        $(obj).addClass('delete-favorite');
      });
      $("#btnEditFavoritos-xs i").removeClass('fa-edit');
      $("#btnEditFavoritos-xs i").addClass('fa-save');
    } else {
      $("#favoritos .favorite-free").each(function(i, obj) {
        $(obj).attr('src', 'assets/images/icons/favorito-after.svg');
        // Quitar clase de eliminar
        $(obj).removeClass('delete-favorite');
      });
      $("#btnEditFavoritos-xs i").removeClass('fa-save');
      $("#btnEditFavoritos-xs i").addClass('fa-edit');
    }
  });
  // Eventos para xs y sm

  // Espacio para el chat
  $("#btnSend").click(function() {
    var message = $("#txtMessage").val();
    setChat(message, 'left');
    $("#conversation").animate({
      scrollTop: $('#conversation').prop("scrollHeight")
    }, 1000);
  });
  $("#txtMessage").keypress(function(e) {
    if (e.which == 13) {
      //     $("#btnSend").click();
      var message = $("#txtMessage").val();
      setChat(message, 'right');
      $("#conversation").animate({
        scrollTop: $('#conversation').prop("scrollHeight")
      }, 1000);
    }
  });

  // Este bloque setea el color de cada item de nuevo chat
  var counter = 1;
  $(".chat-list").each(function(i, obj) {
    if (counter % 2 == 0)
      $(this).addClass('bg-light');
    else
      $(this).addClass('bg-white');

    counter++;
  });
  // Este bloque setea el color de cada item de nuevo chat
  $(".chat-list").click(function() {
    $(".chat-list p").each(function(i, obj) {
      $(obj).removeClass('yellow');
    });
    $(this).children(":first").addClass('yellow');
  });

  // Espacio para el chat



  $('.fdi-Carousel .item').each(function() {
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    if (next.next().length > 0) {
      next.next().children(':first-child').clone().appendTo($(this));
    } else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    }
  });

  $("#imgUpload").click(function() {
    $("#fileImg").click();
  });
  $("#lightSlider-videoDos").lightSlider();
  $("#videoUpload").click(function() {
    $("#fileVid").click();
  });
  $("#videoUploadDos").click(function() {
    $("#fileVidDos").click();
  });

  $(".uploadPicture").click(function() {
    $("#imgPerfil").click();
  });

  $("#curri").click(function() {
    $("#subirCatalogo").click();
  });
  $("#curri").css('cursor', 'pointer');

  $("#closeBlog").click(function() {
    $("#offcanvas").click();
  });

  $("#btnSubirCatalogo").click(function() {
    $("#subirCatalogo").click();
  });
});

// Funciones genericas
function removeComponent(component, removeClass) {

  $(component).closest(removeClass).fadeOut("slow", function() {
    $(component).closest(removeClass).remove();
  });
}

function edit(btn, div) {
  if ($(btn).text() == "Editar") {
    $(btn).text('Guardar')
    $(div).each(function(i, obj) {
      $(obj).attr('disabled', false);
    });
    $(div).first().focus();
  } else {
    $(btn).text('Editar')
    $(div).each(function(i, obj) {
      $(obj).attr('disabled', true);
    });
  }
}

function editXS(btn, div) {
  console.log($(btn).attr("class").split(' '));
  if ($(btn).hasClass("fa-edit")) {
    $(btn).removeClass('fa-edit');
    $(btn).addClass('fa-save');
    $(div).each(function(i, obj) {
      $(obj).attr('disabled', false);
    });
    $(div).first().focus();
  } else {
    $(btn).removeClass('fa-save');
    $(btn).addClass('fa-edit');
    $(div).each(function(i, obj) {
      $(obj).attr('disabled', true);
    });
  }
}
// Funciones genericas

// Funciones para el chat
function setChat(text, side) {
  var offset = '';

  if (side == 'right')
    offset = 'col-lg-offset-7 col-md-offset-6 ';

  $("#txtMessage").val('');

  $('.hr-' + side).each(function(i, obj) {
    $(this).remove();
  });

  var tiempo = new Date();
  var msg = '<div class="row col-lg-5 col-md-6 ' + offset + ' micro-top ">' +
    '<div class="message-' + side + '">' +
    '<p>' + text + '</p>' +
    '</div>' +
    '<p class="hour text-' + side + ' hr-' + side + '">' + tiempo.getHours() + ':' + tiempo.getMinutes() + '</p>' +
    '</div>' + '<div class="col-lg-12 col-md-12"></div>'
  $("#conversation").append(msg);


}



function isValidUrl(url) {
  return /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(url);
}

function isValidPhone(phoneNumber) {
  //  var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  var phoneNumberPattern = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;


  return phoneNumberPattern.test(phoneNumber);
}
