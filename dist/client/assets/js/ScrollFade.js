$(document).ready(function() {

  // $(window).scroll(function() {
  //   /* Check the location of each desired element */
  //   $('.scrollFade').each(function(i) {
  //     var opacity = $(this).css("opacity");
  //     var begin_to_show_object = $(this).offset().top + $(this).outerHeight() / 4;
  //     var bottom_of_window = $(window).scrollTop() + $(window).height();
  //     if ($(window).width() <= 1200) {
  //       begin_to_show_object = $(this).offset().top;
  //     }
  //     if (bottom_of_window > begin_to_show_object && opacity == 0) {
  //       $(this).stop(true, true);
  //       $(this).animate({
  //         'opacity': '1',
  //         left: "0"
  //       }, 400);
  //     }
  //     if (bottom_of_window < begin_to_show_object && opacity == 1) {
  //       $(this).stop(true, true);
  //       $(this).animate({
  //         'opacity': '0',
  //         left: "-=100"
  //       }, 400);
  //     }
  //   });
  // });





  $('.plans').children().click(function() {
    $('.col-md-4').removeClass('selected-plan');
    $(this).toggleClass('selected-plan');
    if ($('#free').hasClass('selected-plan')) {
      $('.time-span').css('display', 'none');
    } else {
      $('.time-span').css('display', 'block');
    }
  });


  $('.cerrar').click(function() {
    $('.loginScreen').hide();
  });


  $('#iniciar-sesion').click(function() {
    $('.loginScreen').show();
  });


  $('#mensaje').keydown(function(e) {
    if (e.keyCode == 13) {
      createMessage();

    }
  });

  /*Llevar el chat al ultimo mensaje*/
  var reference = (function thename() {
    if ($('.message-list').length != 0) {
      var messageList = $('.message-list')[0].scrollHeight;
      $('.message-list').scrollTop(messageList);
    }
    return thename;

  }());

});
/*fin document ready*/


function showRegistro() {
  $('.iniciar-sesion').hide();
  $('.registro').show();
};

function showLogin() {
  $('.iniciar-sesion').show();
  $('.registro').hide();
};

function editarNombreUsuario() {
  $('#nombre-usuario').parent().toggleClass('editando');
  $('.nombre-usuario').prop("disabled", function(i, v) {
    return !v;
  });
  $('.nombre-usuario').toggleClass('editando');

};
// function eliminarFavorito(){
//     $('.favorito').toggleClass('eliminar-favorito');
//     $('#eliminar-favorito').parent().toggleClass('editando');
//
//      $('.favorito.eliminar-favorito').click(function(){
//         $(this).parent().remove();
//     });
//
// };
function editarDescripcionSomos() {
  $('#somos').prop("readonly", function(i, v) {
    return !v;
  });
  $('#somos').toggleClass('editarTextarea');
  $('#descripcion-somos').parent().toggleClass('editando');
};

function editarDescripcionVentajas() {
  $('#ventajas').prop("readonly", function(i, v) {
    return !v;
  });
  $('#ventajas').toggleClass('editarTextarea');
  $('#descripcion-ventajas').parent().toggleClass('editando');
};

function editarProductos() {
  $('.producto').prop("disabled", function(i, v) {
    return !v;
  });
  $('#editar-productos').parent().toggleClass('editando');
  $('.producto').toggleClass('editando-producto');
};

function createMessage() {

  var message = $("textarea[name='message']").val();
  if (message != '') {
    var messagein = '<div class="msg"><div class="message in"><span>' + message + '</span></div></div>';
    $(messagein).appendTo('.message-list');
    $("textarea[name='message']").val('');
    var messageList = $('.message-list')[0].scrollHeight;;
    $('.message-list').scrollTop(messageList);
  }


};



function editarTags() {
  $('#editar-tags').parent().toggleClass('editando');
  if ($('#editar-tags').parent().hasClass('editando')) {
    $('.tag').prepend('<div class="eliminar-tag"></div>');
  } else {
    $('.eliminar-tag').remove();
  }
  $('.eliminar-tag').click(function() {
    $(this).parent().remove();
  });
  $(".agregarTag").toggle();


};

function nuevaTag() {
  var tag = $('#nueva-tag').val();
  var nuevaTag = '<span class="tag"><div class="eliminar-tag"></div>' + tag + '</span>';
  if (tag != '') {
    $(nuevaTag).appendTo('.tagContainer');
  }
  $('.eliminar-tag').click(function() {
    $(this).parent().remove();
  });
  $('#nueva-tag').val('');
};

function editarDatos() {
  $('#editar-datos-info').parent().toggleClass('editando');
  $('.datosContainer input').prop("disabled", function(i, v) {
    return !v;
  });
  $('.datosContainer input').toggleClass('editarDatos');
};

function editarSocialMedia() {
  var facebook = $('#facebook').val();
  var twitter = $('#twitter').val();
  var youtube = $('#youtube').val();
  var linkedin = $('#linkedin').val();
  var googleplus = $('#googleplus').val();
  $('.facebook').attr('href', facebook);
  $('.twitter').attr('href', twitter);
  $('.youtube').attr('href', youtube);
  $('.linkedin').attr('href', linkedin);
  $('.googleplus').attr('href', googleplus);
  $('.loginScreen').hide();
};

function editarRedes() {

  var facebook = $('.facebook').attr('href');
  $('.loginScreen').toggle();
  var twitter = $('.twitter').attr('href');
  var youtube = $('.youtube').attr('href');
  var linkedin = $('.linkedin').attr('href');
  var googleplus = $('.googleplus').attr('href');

  $('#facebook').val(facebook);
  $('#twitter').val(twitter);
  $('#youtube').val(youtube);
  $('#linkedin').val(linkedin);
  $('#googleplus').val(googleplus);
};


$('#nueva-tag').keydown(function(e) {
  if (e.keyCode == 13) {
    nuevaTag();

  }
});
