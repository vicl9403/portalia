$(document).ready(function() {
  $(".plan-free").on("mouseover", function() {
    $("#premiere").removeClass('premiere');
  });
  $(".plan-free").on("mouseleave", function() {
    $("#premiere").addClass('premiere');
  });
  $(".plan-select").on("mouseover", function() {
    $("#premiere").removeClass('premiere');
  });
  $(".plan-select").on("mouseleave", function() {
    $("#premiere").addClass('premiere');
  });


  $('.plans').children().mouseover(function() {
    if ($(this).hasClass('hidden-lg') == false)
      $(this).addClass('active-plan');
  }).mouseleave(function() {
    $(this).removeClass('active-plan');
  });

  $("#panel-free").on("click", function() {
    cleanAll();
    $("#panel-free").addClass('selected-plan');
  });
  $("#panel-plus").on("click", function() {
    cleanAll();
    $("#panel-plus").addClass('selected-plan');
  });
  $("#panel-select").on("click", function() {
    cleanAll();
    $("#panel-select").addClass('selected-plan');
  });

  $(".btn-next").on("click", function() {
    $("#mobile").val("true");
    if ($("#actual").val() == 'plus') {
      $("#panel-plus").fadeOut("medium", function() {
        $("#panel-plus").addClass('visible-lg');
        $("#panel-select").removeClass('visible-lg');
        $("#panel-select").css('display', 'none');
        $("#panel-select").fadeIn("medium", function() {});
        //Cambio de color de botones
        $(".btn-next").addClass('bg-black');
        $(".btn-before").addClass('bg-black');
      });
      $("#actual").val('select');

    } else if ($("#actual").val() == 'select') {
      $("#panel-select").fadeOut("medium", function() {
        $("#panel-select").addClass('visible-lg');
        $("#panel-free").removeClass('visible-lg');
        $("#panel-free").css('display', 'none');
        $("#panel-free").fadeIn("medium", function() {});
        //Cambio de color de botones
        $(".btn-next").removeClass('bg-black');
        $(".btn-before").removeClass('bg-black');
        $(".btn-next").addClass('bg-white');
        $(".btn-before").addClass('bg-white');

      });
      $("#actual").val('free');

    } else if ($("#actual").val() == 'free') {
      $("#panel-free").fadeOut("medium", function() {
        $("#panel-free").addClass('visible-lg');
        $("#panel-plus").removeClass('visible-lg');
        $("#panel-plus").css('display', 'none');
        $("#panel-plus").fadeIn("medium", function() {});
        //Cambo de color de botones
        $(".btn-next").removeClass('bg-white');
        $(".btn-before").removeClass('bg-white');
      });
      $("#actual").val('plus');

    }
  });

  $(".btn-before").on("click", function() {
    $("#mobile").val("true");
    if ($("#actual").val() == 'plus') {
      $("#panel-plus").fadeOut("medium", function() {
        $("#panel-plus").addClass('visible-lg');
        $("#panel-free").removeClass('visible-lg');
        $("#panel-free").css('display', 'none');
        $("#panel-free").fadeIn("medium", function() {});
        //Cambio de color de botones
        $(".btn-next").addClass('bg-white');
        $(".btn-before").addClass('bg-white');
      });
      $("#actual").val('free');

    } else if ($("#actual").val() == 'select') {
      $("#panel-select").fadeOut("medium", function() {
        $("#panel-select").addClass('visible-lg');
        $("#panel-plus").removeClass('visible-lg');
        $("#panel-plus").css('display', 'none');
        $("#panel-plus").fadeIn("medium", function() {});
        //Cambio de color de botones
        $(".btn-next").removeClass('bg-black');
        $(".btn-before").removeClass('bg-black');
        $(".btn-next").removeClass('bg-white');
        $(".btn-before").removeClass('bg-white');

      });
      $("#actual").val('plus');

    } else if ($("#actual").val() == 'free') {
      $("#panel-free").fadeOut("medium", function() {
        $("#panel-free").addClass('visible-lg');
        $("#panel-select").removeClass('visible-lg');
        $("#panel-select").css('display', 'none');
        $("#panel-select").fadeIn("medium", function() {});
        //Cambo de color de botones
        $(".btn-next").removeClass('bg-white');
        $(".btn-before").removeClass('bg-white');
        $(".btn-next").addClass('bg-black');
        $(".btn-before").addClass('bg-black');
      });
      $("#actual").val('select');

    }
  });

  $("#panel-free-xs").on("click", function() {
    cleanAll();
    $(this).addClass('selected-plan-xs');
  });
  $("#panel-plus-xs").on("click", function() {
    cleanAll();
    $(this).addClass('selected-plan-xs');
  });
  $("#panel-select-xs").on("click", function() {
    cleanAll();
    $(this).addClass('selected-plan-xs');
  });
  /*
  $('.plans').children().click(function () {
    $('.col-md-4').removeClass('selected-plan');
    $(this).toggleClass('selected-plan');
    if ($('#free').hasClass('selected-plan')) {
      $('.time-span').css('display', 'none');
    } else {
      $('.time-span').css('display', 'block');
    }
  });
  */
  /* Every time the window is scrolled ...
  $(window).scroll( function(){

      $('.scrollFade').each( function(i){
          var opacity = $(this).css("opacity");
          var begin_to_show_object = $(this).offset().top + $(this).outerHeight()/4;
          var bottom_of_window = $(window).scrollTop() + $(window).height();
          if ($(window).width() <= 1200){
              begin_to_show_object = $(this).offset().top;
          }
          if( bottom_of_window > begin_to_show_object && opacity == 0){
              $(this).stop(true, true);
              $(this).animate({'opacity':'1',left: "0"},400);
          }
          if( bottom_of_window < begin_to_show_object && opacity == 1)
          {
              $(this).stop(true, true);
              $(this).animate({'opacity':'0',left: "-=100"},400);
          }
      });
  });*/
});

function cleanAll() {
  $("#panel-free").removeClass('selected-plan');
  $("#panel-plus").removeClass('selected-plan');
  $("#panel-select").removeClass('selected-plan');

  $("#panel-free-xs").removeClass('selected-plan-xs');
  $("#panel-plus-xs").removeClass('selected-plan-xs');
  $("#panel-select-xs").removeClass('selected-plan-xs');
}
