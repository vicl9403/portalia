$(document).ready(function() {

  // Estilo necesario para landing
  // $(".ng-scope").css("height", '100%');
  // $("#videodiv").addClass('hidden');
  //$(".ng-scope").css("height", '100%');

  // Contador
  $(".soon").soon().create({
    locate: "es-MX",
    due: "2016-09-09T08:05-05:00",
    layout: "group tight",
    face: "flip color-light corners-sharp shadow-soft",
    separateChars: false,
    labelsYears: 'Año,Años',
    labelsDays: 'Dia,Dias',
    labelsHours: 'Hora,Horas',
    labelsMinutes: 'Minuto,Minutos',
    labelsSeconds: 'Segundo,Segundos',
    padding: true,
    eventComplete: function() {
      // $("#videodiv").removeClass('hidden');
      // $(".bloque-1").addClass('hidden');
      // $("#streamingbtn").attr('disabled', false);
      // $("#streamingbtn").trigger();
      // construirVideo();
    }
  });


});
