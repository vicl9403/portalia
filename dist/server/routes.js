/**
 * Main application routes
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (app, passport) {
  // Insert routes below
  app.use('/api/banners', require('./api/banner'));
  app.use('/api/quotation_user', require('./api/quotation_user'));
  app.use('/api/product_categories', require('./api/product_category'));
  app.use('/api/products', require('./api/product'));
  app.use('/api/logins', require('./api/login'));
  app.use('/api/quotes_configurations', require('./api/quotes_configuration'));
  app.use('/api/contacts', require('./api/contact'));
  app.use('/api/topUsers', require('./api/topUser'));
  app.use('/api/paymentLogs', require('./api/paymentLog'));
  app.use('/api/quotes', require('./api/quotation'));
  app.use('/api/mailings', require('./api/mailing'));
  app.use('/api/commonRegisters', require('./api/commonRegister'));
  app.use('/api/suggestedTags', require('./api/suggestedTags'));
  app.use('/api/cotizaciones', require('./api/cotizacion'));
  app.use('/api/updatesOnUsers', require('./api/updatesOnUser'));
  app.use('/api/verificacionEmpresas', require('./api/verificacionEmpresa'));
  app.use('/api/events', require('./api/event'));
  app.use('/api/preUsers', require('./api/preUser'));
  app.use('/api/candidates', require('./api/candidate'));
  app.use('/api/codigos', require('./api/codigo'));
  app.use('/api/finances', require('./api/finance'));
  app.use('/api/busquedas', require('./api/busqueda'));
  app.use('/api/mailConfirmations', require('./api/mailConfirmation'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/plans', require('./api/plan'));
  app.use('/api/estados', require('./api/estado'));
  app.use('/api/passwordRecoverys', require('./api/passwordRecovery'));
  app.use('/api/chats', require('./api/chat'));
  app.use('/api/sectors', require('./api/sector'));
  app.use('/api/paypal', require('./api/paypal'));
  app.use('/api/catalogo_formularios', require('./api/catalogo_formulario'));
  app.use('/api/catalogo_campos_formularios', require('./api/catalogo_campos_formulario'));
  app.use('/auth', require('./auth').default);
  app.use('/api/users', require('./api/user'));
  app.use('/api/email', require('./api/email'));
  app.use('/api/upload', require('./api/upload'));
  app.use('/api/chat', require('./api/chat'));

  app.use('/api/algolia', require('./api/algolia'));

  // app.use(myLogger);

  app.route('/sitemap.xml').get(function (req, res) {
    res.sendFile(_path2.default.resolve(app.get('appPath') + '/sitemap.xml'));
  });

  //function will check if a directory exists, and create it if it doesn't
  function checkDirectory(directory, callback) {
    var fs = require('fs');

    fs.stat(directory, function (err, stats) {
      //Check if error defined and the error code is "not exists"
      if (err && err.errno === 34) {
        //Create the directory, call the callback.
        fs.mkdir(directory, callback);
      } else {
        //just in case there was a different error:
        callback(err);
      }
    });
  }

  // Para subir archivos se tiene que mandar
  // path : String que dice donde se va a guardar
  app.post('/upload', function (req, res) {

    var fs = require('fs');

    if (!req.files) return res.status(400).send('No hay archivos.');

    var allowedExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.pdf', '.doc', '.docx', '.ppt', '.pptx', '.pps', '.ppsx', '.odt', '.xls', '.xlsx'];

    // Preparar arreglo con todos los archivos
    var files = [];
    var fileKeys = Object.keys(req.files);

    // Obtener todos los archivos del request y ponerlos en un array
    fileKeys.forEach(function (key) {
      files.push(req.files[key]);
    });

    // Obtener el path del guardado del request
    var completePath = './client/assets/uploads/' + req.body.path;

    // Toto este bloque de aquí es para crear los directorios en caso de que no se hayan creado
    // anteriormente, recorre todo el path y va creando las carpetas en caso que no existan
    var path = req.body.path;
    var pathSplitted = path.split('/');
    var partial = './client/assets/uploads';

    // Este es el ciclo que hace lo descrito en los comentarios anteriores
    for (var i = 0; i < pathSplitted.length; i++) {

      partial += '/' + pathSplitted[i];

      if (!fs.existsSync(partial)) {
        fs.mkdirSync(partial);
        console.log('test');
      }
    }

    var name = req.body.name;

    // Iterar sobre todos los archivos
    files.forEach(function (file) {

      var fileExtension = file.name.split('.').pop();

      if (allowedExtensions.indexOf(fileExtension)) {

        var uniqueName = '';

        if (name == undefined) {
          var aux = new Date().getTime().toString() + '-' + file.name;
          uniqueName = completePath + aux;
          path = path + aux;
        } else {
          uniqueName = completePath + name + '.' + fileExtension;
          path += name + '.' + fileExtension;
        }

        file.mv(uniqueName, function (err) {
          if (err) {
            console.log(err);
            return res.status(500);
          } else {
            return res.status(200).send(uniqueName.replace('./client', ''));
          }
        });
      }
    });

    return res.status(200);
  });

  // Rutas del dashboard metronic
  app.route('/perfil-usuario').get(function (req, res) {
    res.sendFile(_path2.default.resolve(app.get('appPath') + '/dashboard.html'));
  });
  app.route('/perfil-usuario/*').get(function (req, res) {
    res.sendFile(_path2.default.resolve(app.get('appPath') + '/dashboard.html'));
  });

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(_errors2.default[404]);

  // All other routes should redirect to the index.html
  app.route('/*').get(function (req, res) {
    res.sendFile(_path2.default.resolve(app.get('appPath') + '/index.html'));
  });
};

var _errors = require('./components/errors');

var _errors2 = _interopRequireDefault(_errors);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _auth = require('./auth/auth.service');

var auth = _interopRequireWildcard(_auth);

var _express = require('express');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _express.Router();
//# sourceMappingURL=routes.js.map
