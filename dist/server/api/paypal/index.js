'use strict';

var express = require('express');
var controller = require('./paypal.controller');

var router = express.Router();

router.get('/:adeudo', controller.create);
router.post('/', controller.execute);

module.exports = router;
//# sourceMappingURL=index.js.map
