'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.create = create;
exports.execute = execute;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _paypalRestSdk = require('paypal-rest-sdk');

var _paypalRestSdk2 = _interopRequireDefault(_paypalRestSdk);

var _promise = require('promise');

var _promise2 = _interopRequireDefault(_promise);

var _os = require('os');

var _os2 = _interopRequireDefault(_os);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_paypalRestSdk2.default.configure({
  'host': _environment2.default.auth.paypal.URL,
  'client_id': _environment2.default.auth.paypal.ID,
  'client_secret': _environment2.default.auth.paypal.KEY
});

var create_payment_json = {
  "intent": "sale",
  "payer": {
    "payment_method": "paypal"
  },
  "redirect_urls": {
    "return_url": "",
    "cancel_url": "http://localhost:/" + _environment2.default.port || 9000
  },
  "transactions": [{
    "amount": {
      "currency": "MXN",
      "total": ""
    },
    "description": "This is the payment description."
  }]
};

// Generates a new paypal link payment
function create(req, res) {
  console.log(req.params);
  create_payment_json.transactions[0].amount.total = req.params.adeudo;
  create_payment_json.redirect_urls.return_url = _environment2.default.domain + "/confirmacion/" + req.params.adeudo;
  _paypalRestSdk2.default.payment.create(create_payment_json, function (err, response) {
    if (err) {
      console.log(err);
    } else if (response) {
      console.log("..::Respuesta::..", response);
      console.log("..::Link::..", response.links[2].href);
      res.end(response.links[1].href);
    }
  });
}

function execute(req, res) {

  var params = {
    "payer_id": req.body.PayerID
  };
  _paypalRestSdk2.default.payment.execute(req.body.paymentId, params, function (err, payment) {
    if (err) {
      console.error(err);
    } else {
      console.log(payment);
      res.end(payment.state);
    }
  });
}
//# sourceMappingURL=paypal.controller.js.map
