'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var suggestedTagsCtrlStub = {
  index: 'suggestedTagsCtrl.index',
  show: 'suggestedTagsCtrl.show',
  create: 'suggestedTagsCtrl.create',
  update: 'suggestedTagsCtrl.update',
  destroy: 'suggestedTagsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var suggestedTagsIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './suggestedTags.controller': suggestedTagsCtrlStub
});

describe('SuggestedTags API Router:', function () {

  it('should return an express router instance', function () {
    suggestedTagsIndex.should.equal(routerStub);
  });

  describe('GET /api/suggestedTags', function () {

    it('should route to suggestedTags.controller.index', function () {
      routerStub.get.withArgs('/', 'suggestedTagsCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/suggestedTags/:id', function () {

    it('should route to suggestedTags.controller.show', function () {
      routerStub.get.withArgs('/:id', 'suggestedTagsCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/suggestedTags', function () {

    it('should route to suggestedTags.controller.create', function () {
      routerStub.post.withArgs('/', 'suggestedTagsCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/suggestedTags/:id', function () {

    it('should route to suggestedTags.controller.update', function () {
      routerStub.put.withArgs('/:id', 'suggestedTagsCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/suggestedTags/:id', function () {

    it('should route to suggestedTags.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'suggestedTagsCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/suggestedTags/:id', function () {

    it('should route to suggestedTags.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'suggestedTagsCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
