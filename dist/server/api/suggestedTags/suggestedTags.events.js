/**
 * SuggestedTags model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _suggestedTags = require('./suggestedTags.model');

var _suggestedTags2 = _interopRequireDefault(_suggestedTags);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SuggestedTagsEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
SuggestedTagsEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _suggestedTags2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    SuggestedTagsEvents.emit(event + ':' + doc._id, doc);
    SuggestedTagsEvents.emit(event, doc);
  };
}

exports.default = SuggestedTagsEvents;
//# sourceMappingURL=suggestedTags.events.js.map
