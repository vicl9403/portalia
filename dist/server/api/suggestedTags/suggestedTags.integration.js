'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newSuggestedTags;

describe('SuggestedTags API:', function () {

  describe('GET /api/suggestedTags', function () {
    var suggestedTagss;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/suggestedTags').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        suggestedTagss = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      suggestedTagss.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/suggestedTags', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/suggestedTags').send({
        name: 'New SuggestedTags',
        info: 'This is the brand new suggestedTags!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newSuggestedTags = res.body;
        done();
      });
    });

    it('should respond with the newly created suggestedTags', function () {
      newSuggestedTags.name.should.equal('New SuggestedTags');
      newSuggestedTags.info.should.equal('This is the brand new suggestedTags!!!');
    });
  });

  describe('GET /api/suggestedTags/:id', function () {
    var suggestedTags;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/suggestedTags/' + newSuggestedTags._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        suggestedTags = res.body;
        done();
      });
    });

    afterEach(function () {
      suggestedTags = {};
    });

    it('should respond with the requested suggestedTags', function () {
      suggestedTags.name.should.equal('New SuggestedTags');
      suggestedTags.info.should.equal('This is the brand new suggestedTags!!!');
    });
  });

  describe('PUT /api/suggestedTags/:id', function () {
    var updatedSuggestedTags;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/suggestedTags/' + newSuggestedTags._id).send({
        name: 'Updated SuggestedTags',
        info: 'This is the updated suggestedTags!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedSuggestedTags = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedSuggestedTags = {};
    });

    it('should respond with the updated suggestedTags', function () {
      updatedSuggestedTags.name.should.equal('Updated SuggestedTags');
      updatedSuggestedTags.info.should.equal('This is the updated suggestedTags!!!');
    });
  });

  describe('DELETE /api/suggestedTags/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/suggestedTags/' + newSuggestedTags._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when suggestedTags does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/suggestedTags/' + newSuggestedTags._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=suggestedTags.integration.js.map
