'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SuggestedTagsSchema = new _mongoose2.default.Schema({
    category_name: String,
    tags: []

});

exports.default = _mongoose2.default.model('SuggestedTags', SuggestedTagsSchema);
//# sourceMappingURL=suggestedTags.model.js.map
