/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/suggestedTags              ->  index
 * POST    /api/suggestedTags              ->  create
 * GET     /api/suggestedTags/:id          ->  show
 * PUT     /api/suggestedTags/:id          ->  update
 * DELETE  /api/suggestedTags/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.findByField = findByField;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _suggestedTags = require('./suggestedTags.model');

var _suggestedTags2 = _interopRequireDefault(_suggestedTags);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of SuggestedTagss
function index(req, res) {
  return _suggestedTags2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single SuggestedTags from the DB
function show(req, res) {
  return _suggestedTags2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new SuggestedTags in the DB
function create(req, res) {
  return _suggestedTags2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing SuggestedTags in the DB
function update(req, res) {

  if (req.body._id) {
    delete req.body._id;
  }
  _suggestedTags2.default.findById(req.params.id, function (err, entity) {
    if (!entity) return next(new Error('Could not load Document'));else {
      // do your updates here
      entity.tags = req.body.tags;

      entity.save(function (err) {
        if (err) return respondWithResult(res, 200);else return respondWithResult(res, 500);
      });
    }
  }).then(respondWithResult(res));

  // return SuggestedTags.findById(req.params.id).exec()
  //   .then(handleEntityNotFound(res))
  //   .then(saveUpdates({ 'tags' : ['test' ] }))
  //   .then(respondWithResult(res))
  //   .catch(handleError(res));
}

// Deletes a SuggestedTags from the DB
function destroy(req, res) {
  return _suggestedTags2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

// findByField Function
function findByField(req, res) {
  return _suggestedTags2.default.findOne(req.body).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}
//# sourceMappingURL=suggestedTags.controller.js.map
