/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/paymentLogs              ->  index
 * POST    /api/paymentLogs              ->  create
 * GET     /api/paymentLogs/:id          ->  show
 * PUT     /api/paymentLogs/:id          ->  upsert
 * PATCH   /api/paymentLogs/:id          ->  patch
 * DELETE  /api/paymentLogs/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.upsert = upsert;
exports.patch = patch;
exports.destroy = destroy;
exports.getAllLogs = getAllLogs;
exports.filter = filter;
exports.generateCSV = generateCSV;

var _fastJsonPatch = require('fast-json-patch');

var _fastJsonPatch2 = _interopRequireDefault(_fastJsonPatch);

var _paymentLog = require('./paymentLog.model');

var _paymentLog2 = _interopRequireDefault(_paymentLog);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var pagination = require('mongoose-pagination');
var json2csv = require('json2csv');

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function patchUpdates(patches) {
  return function (entity) {
    try {
      _fastJsonPatch2.default.apply(entity, patches, /*validate*/true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of PaymentLogs
function index(err, res) {
  _paymentLog2.default.find({}, function (err, logs) {
    _user2.default.populate(logs, {
      path: "user"
    }, function (err, logs) {
      res.status(200).send(logs);
    });
  }).catch(handleError(err));
}

// Gets a single PaymentLog from the DB
function show(req, res) {
  return _paymentLog2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new PaymentLog in the DB
function create(req, res) {
  return _paymentLog2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Upserts the given PaymentLog in the DB at the specified ID
function upsert(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _paymentLog2.default.findOneAndUpdate(req.params.id, req.body, {
    upsert: true,
    setDefaultsOnInsert: true,
    runValidators: true
  }).exec().then(respondWithResult(res)).catch(handleError(res));
}

// Updates an existing PaymentLog in the DB
function patch(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _paymentLog2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(patchUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a PaymentLog from the DB
function destroy(req, res) {
  return _paymentLog2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

//Get paymentLogs with pagination
function getAllLogs(req, res) {
  return _paymentLog2.default.find().paginate(req.body.requestedPage, req.body.itemsPerPage, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'documents': docs
    };
    res.status(200).json(respuesta);
  });
}

//Search function
function filter(req, res) {
  var and = [];
  if (req.body.estado_name != undefined) {
    and.push({
      userState: new RegExp(req.body.estado_name, 'i')
    });
  }

  if (req.body.sector_name != undefined) {
    and.push({
      userSector: new RegExp(req.body.sector_name)
    });
  }

  if (req.body.categoria_name != undefined) {
    and.push({
      userCategoria: new RegExp(req.body.categoria_name)
    });
  }

  var query = {
    $and: and
  };

  return _paymentLog2.default.find(query).paginate(req.body.requestedPage, req.body.itemsPerPage, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'documents': docs
    };
    res.status(200).json(respuesta);
  });
}

//Funcion que genera el CSV
function generateCSV(req, res) {
  if (req.body.todas == true) {
    return _paymentLog2.default.find({}, 'userId userName userEmail userSector userCategoria paquete periodo numeroPagos precioPorPago total paymentDate endDate -_id').exec().then(function (logs) {
      console.log(logs);
      var fields = ['userId', 'userName', 'userEmail', 'userSector', 'userCategoria', 'paquete', 'periodo', 'numeroPagos', 'precioPorPago', 'total', 'paymentDate', 'endDate'];
      var csv = json2csv({
        data: logs,
        fields: fields
      });

      console.log(csv);

      fs.writeFile('./client/assets/uploads/compras.csv', csv, function (err) {
        if (err) throw err;
        console.log('file saved');
        res.status(200).json('compras.csv');
      });
    });
  } else {
    var and = [];
    if (req.body.estado_name != undefined) {
      and.push({
        userState: new RegExp(req.body.estado_name, 'i')
      });
    }

    if (req.body.sector_name != undefined) {
      and.push({
        userSector: new RegExp(req.body.sector_name)
      });
    }

    if (req.body.categoria_name != undefined) {
      and.push({
        userCategoria: new RegExp(req.body.categoria_name)
      });
    }

    var query = {
      $and: and
    };

    return _paymentLog2.default.find(query, 'userId userName userEmail userSector userCategoria paquete periodo numeroPagos precioPorPago total paymentDate endDate -_id').exec().then(function (logs) {
      console.log(logs);
      var fields = ['userId', 'userName', 'userEmail', 'userSector', 'userCategoria', 'paquete', 'periodo', 'numeroPagos', 'precioPorPago', 'total', 'paymentDate', 'endDate'];
      var csv = json2csv({
        data: logs,
        fields: fields
      });

      console.log(csv);

      fs.writeFile('./client/assets/uploads/compras.csv', csv, function (err) {
        if (err) throw err;
        console.log('file saved');
        res.status(200).json('compras.csv');
      });
    });
  }
}
//# sourceMappingURL=paymentLog.controller.js.map
