'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var PaymentLogSchema = new _mongoose2.default.Schema({
  paquete: String,
  periodo: String,
  numeroPagos: Number,
  precioPorPago: Number,
  total: Number,
  userId: String,
  userName: String,
  userEmail: String,
  userSector: String,
  userCategoria: String,
  userState: String,
  paymentDate: {
    type: Date,
    default: Date.now
  },
  endDate: Date
});

exports.default = _mongoose2.default.model('PaymentLog', PaymentLogSchema);
//# sourceMappingURL=paymentLog.model.js.map
