/**
 * PaymentLog model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _paymentLog = require('./paymentLog.model');

var _paymentLog2 = _interopRequireDefault(_paymentLog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PaymentLogEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
PaymentLogEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _paymentLog2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    PaymentLogEvents.emit(event + ':' + doc._id, doc);
    PaymentLogEvents.emit(event, doc);
  };
}

exports.default = PaymentLogEvents;
//# sourceMappingURL=paymentLog.events.js.map
