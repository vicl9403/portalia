/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/busquedas              ->  index
 * POST    /api/busquedas              ->  create
 * GET     /api/busquedas/:id          ->  show
 * PUT     /api/busquedas/:id          ->  update
 * DELETE  /api/busquedas/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.count = count;
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.killme = killme;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _busqueda = require('./busqueda.model');

var _busqueda2 = _interopRequireDefault(_busqueda);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _randomstring = require('randomstring');

var _randomstring2 = _interopRequireDefault(_randomstring);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fonts = {
  montserrat: {
    normal: './client/assets/fonts/montserrat/Montserrat-Regular.ttf',
    bold: './client/assets/fonts/montserrat/Montserrat-Bold.ttf',
    italics: './client/assets/fonts/montserrat/Montserrat-Regular.ttf',
    bolditalics: './client/assets/fonts/montserrat/Montserrat-Regular.ttf'
  }
};

var _jade = require('jade');

var http = require('http');

var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);
var fs = require('fs');
var mkdirp = require('mkdirp');

var transporter = _nodemailer2.default.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  }
});

function pdfFactura(response, req, folio) {
  // console.log(req.body);
  if (req.body.method == 'bank_account') {
    var metodo = '03 Transferencia';
  } else if (req.body.method == 'card') {
    if (req.body.cardType == 'debit') {
      var metodo = '28 Tarjeta de Debito';
    } else {
      var metodo = '04 Tarjeta de Credito';
    }
  }
  // console.log(response);
  var timestamp = (0, _moment2.default)({}).format('DDMMYYhhmmssSSS');
  var randomNumber = _randomstring2.default.generate({
    length: 5,
    charset: 'numeric'
  });
  console.log(response);
  var test34 = response.split("\\\"");
  var noCert = test34[21],
      cert = test34[23],
      cfd = test34[109],
      sat = test34[115],
      fecha = test34[111],
      regimen = test34[61];
  var precioLetra = NumeroALetras(req.body.precioMes);
  var precioConIva = req.body.precioMes - (req.body.precioMes - 16 / 100 * req.body.precioMes);
  var lol = precioConIva.toFixed(2);
  var precioConIvaDos = String(lol);
  var precioUnitario = req.body.precioMes;
  if (req.body.descuento) {
    var string = "con descuento";
  } else {
    var string = "sin descuento";
  }
  var dd = {
    defaultStyle: {
      font: 'montserrat'
    },
    pageMargins: [40, 80, 40, 40],
    header: {
      margin: 18,
      columns: [{
        // usually you would use a dataUri instead of the name for client-side printing
        // sampleImage.jpg however works inside playground so you can play with it
        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAAB4CAIAAAAIfDudAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDQ5NjY1Nzk2RTNGMTFFNjkwMTBERDYwMUYzOUMxREUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDQ5NjY1N0E2RTNGMTFFNjkwMTBERDYwMUYzOUMxREUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpENDk2NjU3NzZFM0YxMUU2OTAxMERENjAxRjM5QzFERSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpENDk2NjU3ODZFM0YxMUU2OTAxMERENjAxRjM5QzFERSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlT0sWUAABRGSURBVHja7J13XBRH/8d3706EAwIKFlQUDRYURbGBJSbYQIO/xIoNNBp7LzEmeTSJJvnZfWnsisYCdozdJxoTKxAVY8OuiIqgoAIGEe72+RxD1vXuPG4PBEy+75d/DHs7u3M7n/mWmdmT4wiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIIhiCy/r7B07dtSrV0+j0ehfheezs7PT09OfPn16//7906dPHzp06MqVKy9evJDbIBcHzsOFd7ThsjRcXAoXmyBkacyt27p162nTptnY2Bi2EPz111/Pnj1LTk5Gw6Kiok6dOpWSkiI9oW7dusuXL7e2tjZa3fArKxSKDRs2zJ492/SZdnZ2P/3007Fjx+bNmyf3aaDu3r173dzc0OwBAwacOXPGzIpOTk4//PBD8+bNnz9/PnTo0OjoaLm37tOnT3BwcEhISEJCQhGL9Ny5c4J5ZGRkLF68uFq1arKu72zH/T5e+XS+iv17NEf1UX0ZA6NHjx5ZWVlmtjAyMjIoKAjSEau3bNlSkMmqVavybFWnTp0wJq9du1amTBm5D/ydd95Br+NGGNJonozR6+ICobNGtmvXTu597e3tjx8/jrpQ3psQkkrW2aL1gp2A5VCpXlZHE/Gno6OjlZUV/oTNwCDz8fHBcDl//ryZ1y/3Du9e9qXOSii5uhX5HWcFM6ujdyE7NEOr1aKfpB+heUqlEsZDPNK0adOwsLAaNWrAKqAWjmRmZj5+/LhEiRKozs5BAbazZMmS7OL4yjBy4hUgWZxvukmoDrOBW7u7u0N/5sjU6DPH3c2xwSJoOaqIZbk3/eCDD5o1a4YCui8iIiItLa0oZSd2IXzZ0aNH8TT1ZOfg4IDn27lzZ7QbBxs0aLBy5crAwMCkpCTD69SrxNcsx+06JzzPyj1SwYFTvGrdytjrxCf62RbufFl7LuIs7maqhTdu3Bg0aJDeQajE1ta2YsWKjRs37tatGwY0NPT1118nJiYuW7YMJ8TGxn700UdMtWKH4cwRI0agDAc3adIkqZMFcXFxpp9VkyZNPvzwQ1YeM2ZMeHg4fH0xD7zQrePHj2flVq1avf/++7t27SrKBiEeQoejM/z8/EycBm8yZ84cnMaM/NSpU424Hi/+4lTVg5mqpb2VjmqusRu/oIfi0tcq0cOyf/dmqPaPUg5tpbC35jo34K9PVyXMVC0IUtiVNHLfLl26oFNxR7TTRPNgvRAFwuux5mFIVK9e/XUnDxs2jJ22Z88euY8LN0JUJ3XKsHxynSyUjYpPnjxh5sdcv1Gu3G+//cZu2qZNG1k37dChAyyl2Obdu3er1eqCFZLCsmrwHSY+ffjwIQwDYmH2J4Z7qVKlJHaC6+DJ/xikrFSKs7Hiejbmj01QbRmkDPFVVHTM8ZVaLjNbZ+Fg0iAv32r8d/+niJykmtdNWcaOU1txIT6KWV2UVkpT41XqDfWAM0XGM3z4cITbbJCIBsnEN5VGFGZSp04dhJvSI0OGDJFGk8Uxx+T5kSNHSv1YQEAAbHaxkJ05YRbiGARDOtdZoYKHh4f4UTVnHqJxkOjWtTRXSq2T2tl4YcUx7YSt2pDVmkHrNT/s1x64JDxM55QKDhp1lAy5Xk34Ye/nq/FHjhzZunWrGMpYoKo8Qf+xuDA1NZWFaPXr10cEUpxl17ZtW2ZWEe+i2Sw4GTVq1NshO4Ck/enTp8xTwOaLx61VnJOt/skx8cK4LVr/hRpobvUJ7b6LwvYYYcYBbfflmp4rNeujtBlZBg7IOl/Ng6lDMpure1fXApddrVq1mMJYmMFsP4JL5IYs6yqeUd0nn3yC/kIZifDEiRPhGVD29/dv1KjR2yE79CtLvjBcpEb7SqIwKUKb/OzlmVBYL6YtY9N8f9wWhodrJ27VPv47FtcKukRk5TFtPlsoZrvIXk04ZcsYO3Ys67+LFy+GhoYuX76cpZYI0lm+VQyBnWvfvj0K6Lh169ah2WzCD5EG8qG3Q3Y1atRgExYZGRnM7OX6Xy23LlIbfTs3F42IEUZt0tx/msfV1kVpP9umSdUFY9yzTG7xb9o8q+RJ1apVWeHx48eypifyBEFFp06dmKnbvHkzvBWiSWZcocVevXoVQ4MH69C3b19HR0eW1KPZGCdbtmxh4UG7du0QIbwFssPDdXBwYBkG0kbpR7Ur8HVcdIWricJn2zVpz8264ObTwpoTOguHRKRVjfwaJ/j9wMBAVj537pw4y1UgDBw4sHz58ijcvHkTySybflu0aFFuFt+pU8OGDYub7Ly9vT/++GNWXrZsGYvLN2zYcOXKFZZ44UsVsexMdxIcVteuXXv37s2ytlOnTt2+fVt6gn9tvnJpnW7mHxKS5MxELjysjUsWVAou0Is3DBClk4imZ/YQ6Y8fPx4Pmv25Z88eC+ZUTZh5MTVG1nLnzh1W3rVrV0xMDAqwKEFBQQXu1vMJkm5nZ2cUrl+/DrWxgykpKeHh4bm95u9fu3btIpMdekitViMGLyEBXgPxctmyZeFfvvzyS4QybNIkKSlJHOXuZTj/OnyfpooejXRP/OYj4eBleZ0Nje69oNNTjbL8hHaKNh58C3e+pMpIC40OBqgNLYTafvzxR8TL7Pju3bsRPhdg/yGTgPJQePDgAZ6DeBz2A/dl5e7du9esWbP4aA4NxkhgZTRSuvqyevVqNnLefffdLl26FMjtLEnfkB8gXkbsKU0U0KmQnYuLS506ddigYQ8aEmQxjbMttypYUd/1pdCPXhceP5N99//GCoNa6pYuhrVSDGulO9J/Tfb2s6+cg8x07ty5egYPphdmBvEckjI0lR28cePGpEmT2ExBgYBbw8yzclhYGJys9FMYvEuXLsFmwAUjCJkyZUoxkR0aU6lSJRTi4uK2bdsm/QiDZ82aNaypPXv2RMwg2u9Cgq1SmAkcinSOqsW7XNz3r6xAjGltia2t6szfm/HKdWblBiQvVynMZO/evfXq1TN9O/hidvKBAwfMad7w4cPZ+XBPRndCjBgxgp2QmJjIetoEhbNKUaFCBWiLnfzZZ58Zev+KFSvCa7ET8AWLXUqRnp5+69atnTt3Dh48GPHp9u3bxY/UJfW/THqmJbdIe/5yDZdhKzMphA2G4AYMGNCnTx8kEwX49WHmxbh73bp1eqZOjCNZkA53HxISUhxMXb9+/djEKhq8Y8cOw7D43r17K1euZOVPP/3Ugq00BeBk0Sy4sBMnTiDCg3V5/vw5/Bf6EppLS0vDuEzPQb+zMzm9b2Nv0XyvvTVvXeJVIRrI9+7duzNmzGDLx3igsFhsKic2Nnb69Omw2QkJCQW+q4LLWc1kswywDWIYpweGJQK+2bNnw6hA+kuXLk1OTi5CzUmHCrR19epVo6fho759+8I8e3l5BQQErF27tghkd/Dgwf3798uqdf2hgCRUbcUrFZxKwdwlZ6XkXsicL6vmzNn8LTu2dHvaINKA/5L2OpIbtrxTuXJl9PHrnmw+Qbw4cuRIVkaw2L9//6ysLMMVWIxSJBOZmZnW1tZVqlSBwcMYLlpTh2ZwOUvVCDq/+OILwwV39DiyRtGUDBs2DBl6frbSWCI7DFNpMmEmiancsDCtd2XeUc0NbMG7OfEt3XXlJJlG54OaOuFCbZtOaaNuobpwMNZI0oNGis7i+++/hx1yd3eHMmDt/vzzT4QyBd5/HTt2FFeQcK/JkyebM0MLRw/jZ+gcCgfEjrBhbGwgzTdzg0zTpk2RUEZERBTH6WJDLiYI66K0Cw9r15wUcuwWDw3J8wh2XMe6uirXkoQvdmjXRmr3XxSy85qEgfH76quvWBnKGDp0aIF/NZVKNXr0aAsqwsAgiywqUwfN1apVy4KKY8aMscD05Mva5Z9fYoWBzXWbSia1Vxy+ojHf4I32U1R14jVabt8F4WmGjDtu27Zt06ZNbBvS2LFjkfScPn26AL9RYGAgbIAu0ExLW7FixYULF0zvDYOr7d27t5+fH2xMUFDQ5s2bERMXci84OTnh1myZ7ujRo2vWrIHfN3F+RkYGNIpk0cHBwcfHBwZP3NtWGBMoiNPhUPJlGxTc+k9y35kIDVbqpQiv4+MG/INZuirx/69qVs2ImRQnUGJiYgxnAby9vePj49kswC+//IJgxZybmjOBAieFC4qTMmbuqGvSpAl8K6q8ePGie/fuhT+BgrASt2YvvjRv3tzM+AojVtz++XY42dxb8lyPRgq/v91rF29+YZBu/6YJEMz1aMTP76ZkyYRdSW5oK0UpmTtez5w5s2TJElaGmSnAFUYMQrYREv23fv16M9fZoqOjmbXAAOjZs6f0PY9CgC3QsbGHMXP8+HEzs0kYRRaJ+vr6tm7d+q2RnUd5fmYXha1kV3r3hvy2IcqAOkaWWW2suJrl+FldFIt7KcVtnhBuJy9+bBvZjV+1ahV7vjBIEyZMcHNzK5CoDhES2+OEZEU6VZknCxcuZKvbAQEBzEcXGjBv7I0yDBI0w/yKsHbsvcnSpUsHBwdbFuEVgez+yuIMwzKvSvzGT5Vr+ysn+yu6NeT9avEdPPkh7ynmdVPuG6Uc0FyhMmjpI/nJH3KLGTNmsK2L1apV+/zzz/O/u7Nly5bi/jnkpGyjvJmcPHmSuSpEeBZ3oQXgdjD2LBg4dOjQ77//bn5djBM4DWbRIVzLtn8WgexuPRImbtM8kUz6nL8nsBWLFu785/6KlX2VEUOU4QOVMzorejbONYHn7grSVY31UbpNyBbcfdeuXWFhYawM1+bv75/P/oOrYmvQSCM2btwoqzq6cNGiRczgITD18vIqnC6AZWXROZzmggUL5L5FD4vOXpIqX748olILRkvRvE6y57wwYqPmfk7qFhYt9A7V9lypWXFMe98gmcvM1m0kHrhO02e1dtwWDdtgHHpcOzlCm/bcwrtPmTKFLWbDM06ePLls2bIWfxFPT09x48b8+fMR28m9Apw+m3i3tbUdMmTI63ZDwboU1JuOuMWoUaNYVHf48OEjR47IvQJkOmfOHFZGPs6227wFEyg6q3NOiEvRuJbijlwTIKC4ZO7kTeHXy8KKvkrpy4ibTmm/+lnLnDLOuflIU1qtq2L4aoX53L1797vvvmPvxiI9REI3a9Ysy6K6Xr16saju0qVLlk2fQqmIOBHbwWZAwXPnzr18+bLhaWq1+ptvvklISHhdmmxlZXX9+nXYzjzfGG/cuDHb+awbwKGhlu2+wVBBhOft7Y2UGXZ6+vTpb1ArMTExLHk28YZffqhbkX8w85XdJVM/lGGPu3btitAKzUNob3oTpY2NzZ49e8StIib2oSDzEOdc9HMjDw/0MftUXBazLKk8cOAAu47eSi40jUFi5oYaSJ8tc7EJFERs7Hjbtm2lcz3h4eHs+NGjR/Nj6fv168euA9fh6ur6Bp2sONf1hl72TEwVriQKGS90a19wrw/TuQv3BVnmR/wpjDxtzLfffst2A5QqVQp5hrgD73Vfmb16KO2/wYMHszcPrl69+vPPP1v8rZ88eYKgkP0gBpJi6Q5eDB4z5xe5nPduxPVAaUXpCEQCy7by4+TNmzcb/bUGM8FQYVPu0Bya/Qad7MyZMytVqoQQGOHzm5BdUhrX7ydtw8qcnTWv0XC3koUTN2TIDq2aNm0a/JHeHnqj4JENGDDAx8cHiS00BLvC3h7QAyZh9uzZzJboRUjnz59HaAg5Ij6Lj4/PzxeHg4aTdXFxQWOk+sbwmDp1KsSd51wgxhvaIHpYjKjVq1dHRkbC/N+4cUMaI8IhYnwmJyfrbeeUCzz+uHHjkMUj1MvzRzkIgiAIgiAIgiAKDnm7LF1L87ZWup8gIYobKoXulaj7T15uerW2tnZyckKSjjSc/Qhkamqq3q4+5O9eXl5//PGHrKXkAmitrLOX9lK5OfECya74oVBwNx8KgzZkQ3k5fyo6dOhQoUKFpKQklNk0HoQICUJkZ8/mvldsb2/v4+ODP4u17M7fEx7/RV1cLGXHc3EpQmZWrklQq9V2dnZQ2LVr19h0McRnY2NTvnz5gIAAFxeXffv2cTnTeGxdp7Bts6yz/7MzW6mgLi6maLSc9LUS6AmmTu8/Qbhz587NmzcHDx588eLFwn6532LZZWm4LA3171sStvO80d2Ejx49On78OHyroeycnZ0zMzMN3yB2cHCAj0ZF0TW7uroicMzKynr48OGDBw+MLvCYss3UPf9CYAWN/gp2s2bNjP4gkKenp6+vLytDbcHBwe+9916ZMmXc3Nw6duyIKnJ/vUpFffAvxMrKymg8ZyLIEz9q3759fHz8zp07cwWkUlnwO+5k7f6NNGzYULo/QKqt18lRPA5HLN0Ikp2dnZqaKjcpIdn9Y2GvlhrMsygCAwNLly4dFRVl2WWjo6NDQkJq166dn81v5GT/mSDYt7Ozg1Vj2QD7v6+gNg8Pj4yMjCVLlrDtfWYilS/0CgvXuXNnHIyNjT1z5gxSE7nWjmT3jzV1iLrgENPT09krNvgTgtu0aZPprYFGBQThikkx+0+OQJUqVVq0aDF8+PBff/314MGDsn78mWT3j00aUlJSIiMj7969a34tyMvoa2ANGjQw3Iccl8O+fftGjx599epVoz/mR7Hdvw4IyPTvsBjy7Nkzd3d3vdkQuGZfX182M2c4EQhxQ3D29vaUUhC52YM5p0lFduLECVdXV+m7w9WrVx80aNDJkyfZoi3ixX79+kn/dwNPT09USUxMpNiO0EVg4v+CZDpXkL5+i0AwNDS0R48e48aNS01NVavVcNZwo+xHUbmcVzQQ1fn5+SGqg/3DCdbW1tu3b5crO4IgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgzOV/AgwA9TDYj/A4XlsAAAAASUVORK5CYII=',
        width: 100
      }]
    },
    content: [{
      columns: [{
        // auto-sized columns have their widths based on their content
        width: '*',
        text: [{
          text: '\nCMG INTERNATIONAL S DE RL DE CV\n' + '\nPERSONA MORAL RÉGIMEN GENERAL DE LEY DE LAS PERSONAS MORALES TITULO II  LISR\n' + 'RFC Emisor: CIN1211201R6\n' + 'Domicilio Fiscal del Emisor:\n' + 'Calle Ave. San Francisco No. Exterior 3429 Oficina 10 Colonia Jardines de los Arcos Ciudad Guadalajara Estado Jalisco, México CP 45048 \n\n' + 'RFC Receptor:' + req.body.rfc + '\n' + req.body.razon + '\n' + 'Calle ' + req.body.calle.toUpperCase() + ' No. Exterior ' + req.body.numeroCalle.toUpperCase() + ' Colonia ' + req.body.colonia.toUpperCase() + ' Municipio ' + req.body.ciudad.toUpperCase() + ' Estado ' + req.body.estado.toUpperCase() + ' México CP ' + req.body.codigoPostal.toUpperCase() + "\n\n\n",
          fontSize: 10
        }]
      }, {
        // auto-sized columns have their widths based on their content
        width: '*',
        text: [{
          text: '\nFolio Fiscal:\n' + cfd + ' \n' + 'No Serie del CSD:\n' + noCert + "\n" + 'Lugar, fecha, y hora de emisión: Gdl ' + fecha + '\n' + 'Efecto del Comprobante: Ingreso \n' + 'Folio y Serie:' + folio + ' \n' + 'Regimen Fiscal:\n' + regimen + '\n\n\n',
          fontSize: 10
        }]
      }],
      // optional space between columns
      columnGap: 10 //
    }, {
      table: {
        // headers are automatically repeated if the table spans over multiple pages
        // you can declare how many rows should be treated as headers
        headerRows: 1,
        widths: ['*', '*', '*', '*', '*', '*'],
        body: [[{
          text: 'CANTIDAD',
          fontSize: 9
        }, {
          text: 'UNIDAD DE MEDIDA',
          fontSize: 9
        }, {
          text: 'NÚMERO DE IDENTIFICACIÓN',
          fontSize: 9
        }, {
          text: 'DESCRIPCIÓN',
          fontSize: 9
        }, {
          text: 'PRECIO UNITARIO',
          fontSize: 9
        }, {
          text: 'IMPORTE',
          fontSize: 9
        }], [{
          text: String(1),
          fontSize: 9
        }, {
          text: 'SERVICIO',
          fontSize: 9
        }, {
          text: 'N/A',
          fontSize: 9
        }, {
          text: "Pago de paquete " + req.body.planSolicitado + " de Portalia " + string,
          fontSize: 9
        }, {
          text: String(precioUnitario),
          fontSize: 9
        }, {
          text: precioUnitario + ".00",
          fontSize: 9
        }]]
      }
    }, {
      columns: [{
        // auto-sized columns have their widths based on their content
        width: '*',
        text: [{
          text: '\n\nMoneda: MXN\n' + 'Forma de Pago: PAGO EN UNA SOLA EXHIBICIÓN \n' + 'Método de Pago:' + metodo + '\n' + 'Condiciones de Pago: N/A \n\n',
          fontSize: 10
        }]
      }, {
        // auto-sized columns have their widths based on their content
        width: '*',
        text: [{
          text: '\n\nSubtotal:    $' + String(req.body.precioMes - 16 / 100 * req.body.precioMes) + '\n\n' + 'Impuestos Trasladados \n\n' + 'IVA: 16%     $' + precioConIvaDos + '\n\n' + 'TOTAL        $' + String(req.body.precioMes) + '\n\n',
          fontSize: 10
        }]
      }],
      // optional space between columns
      columnGap: 0
    }, {
      columns: [{
        // auto-sized columns have their widths based on their content
        width: '*',
        text: [{
          text: '\n\nTOTAL CON LETRA \n' + precioLetra + ', 00/100 MN \n\n SELLO DIGITAL DEL CFDI \n',
          fontSize: 10
        }, {
          text: cfd.replace(/(.{130})/g, "$1\n") + '\n\n',
          fontSize: 7
        }, {
          text: 'SELLO DEL SAT\n',
          fontSize: 10
        }, {
          text: sat.replace(/(.{130})/g, "$1\n") + '\n\n',
          fontSize: 7
        }, {
          text: 'Cadena Original del Complemento de Certificado Digital del SAT\n',
          fontSize: 10
        }, {
          text: cert.replace(/(.{110})/g, "$1\n") + '\n\n',
          fontSize: 6
        }, {
          text: 'No. de Serie del Certificado del SAT                ' + noCert + '\n\n' + 'Fecha y hora de certificación        ' + fecha,
          fontSize: 10
        }]
      }],
      // optional space between columns
      columnGap: 0
    }]

    // pdfMake.createPdf(dd).open(); //Hello!
  };var pdfDoc = printer.createPdfKitDocument(dd);
  var path = './client/assets/uploads/' + req.user.sector_name + '/' + req.user.categoria_name + '/' + req.user._id;
  var fileXML = path + '/' + timestamp + "-" + randomNumber + '.xml';
  var file = path + '/' + timestamp + "-" + randomNumber + '.pdf';
  var objectReplace = {
    '&lt;': '<',
    '&gt;': '>'
  };

  var str1 = String(response);
  var str2 = str1.split('"xml":"').pop();
  var str3 = str2.split('"}</getStampingResult>')[0];
  var str4 = str3.replace(/&lt;/g, '<');
  var str5 = str4.replace(/&gt;/g, '>');
  var str6 = str5.replace(/\\/g, '');

  mkdirp(path, function (err) {
    if (err) {
      if (err.code === 'EEXIST') {
        pdfDoc.pipe(fs.createWriteStream(file));
        // fs.createWriteStream(fileXML)
        fs.writeFile(fileXML, str6, function (err) {
          if (err) {
            return console.log(err);
          }
        });
        pdfDoc.end();
        var template = './server/views/factura.jade';

        // get template from file system
        fs.readFile(template, 'utf8', function (err, file) {
          if (err) {
            //handle errors
            console.log(err);
          } else {
            //compile jade template into function
            var compiledTmpl = _jade.compile(file, {
              filename: template
            });
            // set context to be used in template
            var data = {
              nombre: req.user.nombre,
              pdf: 'https://www.portaliaplus.com/assets/uploads/' + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + '/' + timestamp + '-' + randomNumber + '.pdf',
              xml: 'https://www.portaliaplus.com/assets/uploads/' + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + '/' + timestamp + '-' + randomNumber + '.xml'
            };
            // get html back as a string with the context applied;
            var diseño = compiledTmpl(data);

            return transporter.sendMail({
              from: 'hola@portaliaplus.com', // sender address
              to: req.user.email, // list of receivers
              subject: "Factura de PortaliaPlus", // Subject line
              html: diseño // html body
            }, function (error, info) {
              if (error) {
                console.log(error);
              } else {
                console.log('Message sent: ' + info.response);
              }
            });
          }
        });

        transporter.sendMail({
          from: 'hola@portaliaplus.com', // sender address
          to: 'soporte@portaliaplus.com', // list of receivers
          subject: "Factura de PortaliaPlus", // Subject line
          html: "Hola " + req.user.nombre + ", haz requerido una factura de tu pago en PortaliaPlus, sigue estos enlaces para descargarla:<br>" + "PDF: https://www.portaliaplus.com/assets/uploads/" + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + "/" + timestamp + "-" + randomNumber + '.pdf <br>' + "XML: https://www.portaliaplus.com/assets/uploads/" + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + "/" + timestamp + "-" + randomNumber + '.xml <br>' // html body
        }, function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });
      } else {
        res.sendStatus(500);
        return;
      } // something else went wrong
    } else {
      pdfDoc.pipe(fs.createWriteStream(file));
      // fs.createWriteStream(fileXML)
      fs.writeFile(fileXML, str6, function (err) {
        if (err) {
          return console.log(err);
        }
      });
      pdfDoc.end();
      var template = './server/views/factura.jade';

      // get template from file system
      fs.readFile(template, 'utf8', function (err, file) {
        if (err) {
          //handle errors
          console.log(err);
        } else {
          //compile jade template into function
          var compiledTmpl = _jade.compile(file, {
            filename: template
          });
          // set context to be used in template
          var data = {
            nombre: req.user.nombre,
            pdf: 'https://www.portaliaplus.com/assets/uploads/' + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + '/' + timestamp + '-' + randomNumber + '.pdf',
            xml: 'https://www.portaliaplus.com/assets/uploads/' + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + '/' + timestamp + '-' + randomNumber + '.xml'
          };
          // get html back as a string with the context applied;
          var diseño = compiledTmpl(data);

          return transporter.sendMail({
            from: 'hola@portaliaplus.com', // sender address
            to: req.user.email, // list of receivers
            subject: "Factura de PortaliaPlus", // Subject line
            html: diseño // html body
          }, function (error, info) {
            if (error) {
              console.log(error);
            } else {
              console.log('Message sent: ' + info.response);
            }
          });
        }
      });

      transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: 'soporte@portaliaplus.com', // list of receivers
        subject: "Factura de PortaliaPlus", // Subject line
        html: "Hola " + req.user.nombre + ", haz requerido una factura de tu pago en PortaliaPlus, sigue estos enlaces para descargarla:<br>" + "PDF: https://www.portaliaplus.com/assets/uploads/" + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + "/" + timestamp + "-" + randomNumber + '.pdf <br>' + "XML: https://www.portaliaplus.com/assets/uploads/" + encodeURIComponent(req.user.sector_name) + '/' + encodeURIComponent(req.user.categoria_name) + '/' + req.user._id + "/" + timestamp + "-" + randomNumber + '.xml <br>' // html body
      }, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });
    } // successfully created folder
  });
}

function Unidades(num) {
  switch (num) {
    case 1:
      return "UN";
    case 2:
      return "DOS";
    case 3:
      return "TRES";
    case 4:
      return "CUATRO";
    case 5:
      return "CINCO";
    case 6:
      return "SEIS";
    case 7:
      return "SIETE";
    case 8:
      return "OCHO";
    case 9:
      return "NUEVE";
  }

  return "";
} //Unidades()

function Decenas(num) {

  var decena = Math.floor(num / 10);
  var unidad = num - decena * 10;

  switch (decena) {
    case 1:
      switch (unidad) {
        case 0:
          return "DIEZ";
        case 1:
          return "ONCE";
        case 2:
          return "DOCE";
        case 3:
          return "TRECE";
        case 4:
          return "CATORCE";
        case 5:
          return "QUINCE";
        default:
          return "DIECI" + Unidades(unidad);
      }
    case 2:
      switch (unidad) {
        case 0:
          return "VEINTE";
        default:
          return "VEINTI" + Unidades(unidad);
      }
    case 3:
      return DecenasY("TREINTA", unidad);
    case 4:
      return DecenasY("CUARENTA", unidad);
    case 5:
      return DecenasY("CINCUENTA", unidad);
    case 6:
      return DecenasY("SESENTA", unidad);
    case 7:
      return DecenasY("SETENTA", unidad);
    case 8:
      return DecenasY("OCHENTA", unidad);
    case 9:
      return DecenasY("NOVENTA", unidad);
    case 0:
      return Unidades(unidad);
  }
} //Unidades()

function DecenasY(strSin, numUnidades) {
  if (numUnidades > 0) return strSin + " Y " + Unidades(numUnidades);

  return strSin;
} //DecenasY()

function Centenas(num) {
  var centenas = Math.floor(num / 100);
  var decenas = num - centenas * 100;

  switch (centenas) {
    case 1:
      if (decenas > 0) return "CIENTO " + Decenas(decenas);
      return "CIEN";
    case 2:
      return "DOSCIENTOS " + Decenas(decenas);
    case 3:
      return "TRESCIENTOS " + Decenas(decenas);
    case 4:
      return "CUATROCIENTOS " + Decenas(decenas);
    case 5:
      return "QUINIENTOS " + Decenas(decenas);
    case 6:
      return "SEISCIENTOS " + Decenas(decenas);
    case 7:
      return "SETECIENTOS " + Decenas(decenas);
    case 8:
      return "OCHOCIENTOS " + Decenas(decenas);
    case 9:
      return "NOVECIENTOS " + Decenas(decenas);
  }

  return Decenas(decenas);
} //Centenas()

function Seccion(num, divisor, strSingular, strPlural) {
  var cientos = Math.floor(num / divisor);
  var resto = num - cientos * divisor;

  var letras = "";

  if (cientos > 0) if (cientos > 1) letras = Centenas(cientos) + " " + strPlural;else letras = strSingular;

  if (resto > 0) letras += "";

  return letras;
} //Seccion()

function Miles(num) {
  var divisor = 1000;
  var cientos = Math.floor(num / divisor);
  var resto = num - cientos * divisor;

  var strMiles = Seccion(num, divisor, "UN MIL", "MIL");
  var strCentenas = Centenas(resto);

  if (strMiles == "") return strCentenas;

  return strMiles + " " + strCentenas;
} //Miles()

function Millones(num) {
  var divisor = 1000000;
  var cientos = Math.floor(num / divisor);
  var resto = num - cientos * divisor;

  var strMillones = Seccion(num, divisor, "UN MILLON DE", "MILLONES DE");
  var strMiles = Miles(resto);

  if (strMillones == "") return strMiles;

  return strMillones + " " + strMiles;
} //Millones()

function NumeroALetras(num) {
  var data = {
    numero: num,
    enteros: Math.floor(num),
    centavos: Math.round(num * 100) - Math.floor(num) * 100,
    letrasCentavos: "",
    letrasMonedaPlural: 'PESOS', //"PESOS", 'Dólares', 'Bolívares', 'etcs'
    letrasMonedaSingular: 'PESO', //"PESO", 'Dólar', 'Bolivar', 'etc'

    letrasMonedaCentavoPlural: "CENTAVOS",
    letrasMonedaCentavoSingular: "CENTAVO"
  };

  if (data.centavos > 0) {
    data.letrasCentavos = "CON " + function () {
      if (data.centavos == 1) return Millones(data.centavos) + " " + data.letrasMonedaCentavoSingular;else return Millones(data.centavos) + " " + data.letrasMonedaCentavoPlural;
    }();
  };

  if (data.enteros == 0) return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
  if (data.enteros == 1) return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;else return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
} //NumeroALetras()

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function count(req, res) {
  return _busqueda2.default.find({}, 'termino').exec().then(function (busquedas) {
    var array_elements = [];
    var current = null;
    var cnt = 0;
    var counts = {};

    for (var x = 0; x < busquedas.length; x++) {
      array_elements.push(busquedas[x].termino);
    }

    array_elements.sort();

    array_elements.forEach(function (x) {
      counts[x] = (counts[x] || 0) + 1;
    });

    var arr = [];
    for (var prop in counts) {
      if (counts.hasOwnProperty(prop)) {
        arr.push({
          'key': prop,
          'value': counts[prop]
        });
      }
    }
    arr.sort(function (a, b) {
      return a.value - b.value;
    });
    //arr.sort(function(a, b) { a.value.toLowerCase().localeCompare(b.value.toLowerCase()); }); //use this to sort as strings
    res.status(200).json(arr);
  });
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Busquedas
function index(req, res) {
  return _busqueda2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Busqueda from the DB
function show(req, res) {
  return _busqueda2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Busqueda in the DB
function create(req, res) {
  return _busqueda2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Busqueda in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _busqueda2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Busqueda from the DB
function destroy(req, respuesta) {
  return _busqueda2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

function killme(req, resp) {
  // req.body.precioMes = 1;
  var folio = Math.floor(Math.pow(10, 9 - 1) + Math.random() * 9 * Math.pow(10, 9 - 1));
  console.log("Me hice popo");
  var webserUrl = '162.221.187.226';
  var popo = req;
  if (req.body.descuento) {
    var string = "con descuento";
  } else {
    var string = "sin descuento";
  }
  var yeison = {
    "accountNumber": req.body.idOrden,
    "authorID": Math.floor(Math.pow(10, 9 - 1) + Math.random() * 9 * Math.pow(10, 9 - 1)),
    //"creationDate": "\/Date(" + moment({}).subtract(5, 'hours') + ")\/",
    "customerAddress": req.body.calle,
    "customerCity": req.body.ciudad,
    "customerColony": req.body.colonia,
    "customerCountry": "México",
    "customerEmail": req.body.correoFactu,
    "customerExtNumber": req.body.numeroCalle,
    "customerIntNumber": "",
    "customerName": req.body.razon,
    "customerRFC": req.body.rfc,
    "customerState": req.body.estado,
    "customerStreet": req.body.calle,
    "customerZipCode": req.body.codigoPostal,
    "detail": [{
      "iva": req.body.precioMes * 0.16,
      "orderDetailID": Math.floor(Math.pow(10, 9 - 1) + Math.random() * 9 * Math.pow(10, 9 - 1)),
      "orderID": Math.floor(Math.pow(10, 9 - 1) + Math.random() * 9 * Math.pow(10, 9 - 1)),
      "price": req.body.precioMes,
      "productCode": "N\/A",
      "productName": "Pago de paquete " + req.body.planSolicitado + " en portaliaplus.com",
      "quantity": 1,
      "total": req.body.precioMes,
      "unitMeasure": "SERVICIO"
    }],
    "folio": folio,
    "invoiceID": Math.floor(Math.pow(10, 9 - 1) + Math.random() * 9 * Math.pow(10, 9 - 1)),
    "iva": 16,
    "paymentCondition": "PAGO EN UNA SOLA EXHIBICIÓN",
    "paymentMethodSat": "98",
    "subTotal": req.body.precioMes,
    "total": req.body.precioMes
  };

  var soapRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">' + '<soapenv:Header/>' + '<soapenv:Body>' + '<tem:getStamping>' + '<tem:invoice>' + JSON.stringify(yeison) + '</tem:invoice>' + '<tem:companyID>' + 1 + '</tem:companyID>' + '</tem:getStamping>' + '</soapenv:Body>' + '</soapenv:Envelope>';

  var postData = soapRequest;

  var options = {
    hostname: webserUrl,
    method: 'POST',
    path: '/stamping//stamping.asmx',
    headers: {
      'Content-Type': 'text/xml'
    }
  };
  var response = "";

  // console.log(yeison);

  var req = http.request(options, function (res) {
    console.log('STATUS: ' + res.statusCode);
    // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      // console.log(`BODY: ${chunk}`);
      response += '' + chunk;
    });
    res.on('end', function () {
      console.log('No more data in response.');
      pdfFactura(response, popo, folio);
      if (resp != undefined) {
        resp.json(response).end();
      }
    });
  });

  req.on('error', function (e) {
    console.log('problem with request: ' + e.message);
    // console.log(e);
  });

  // write data to request body
  req.write(postData);
  req.end();
  // return res.json(req).end();
}
//# sourceMappingURL=busqueda.controller.js.map
