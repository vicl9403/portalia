'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BusquedaSchema = new _mongoose2.default.Schema({
  termino: String,
  idUsuario: {
    type: String,
    default: 'Anónimo'
  },
  rolUsuario: {
    type: String,
    default: 'Anónimo'
  },
  timestamp: {
    type: Date,
    default: Date.now
  }
});

exports.default = _mongoose2.default.model('Busqueda', BusquedaSchema);
//# sourceMappingURL=busqueda.model.js.map
