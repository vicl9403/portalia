/**
 * Busqueda model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _busqueda = require('./busqueda.model');

var _busqueda2 = _interopRequireDefault(_busqueda);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BusquedaEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
BusquedaEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _busqueda2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    BusquedaEvents.emit(event + ':' + doc._id, doc);
    BusquedaEvents.emit(event, doc);
  };
}

exports.default = BusquedaEvents;
//# sourceMappingURL=busqueda.events.js.map
