'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

// var User = mongoose.model('User');
// var Quotation = require('../quotation/quotation.model.js');
//
// mongoose.model('Quotation');


var QuotationUserSchema = new _mongoose2.default.Schema({

  user: { type: Schema.ObjectId, ref: "User", required: true },

  quote: { type: Schema.ObjectId, ref: "Quotation", required: true },

  status: { type: String },

  wasSend: { type: Boolean, default: false },

  sendDate: { type: Date },

  tracing: { type: String },

  quoted: { type: Date }

});

exports.default = _mongoose2.default.model('QuotationUser', QuotationUserSchema);
//# sourceMappingURL=quotation_user.model.js.map
