/**
 * QuotationUser model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _quotation_user = require('./quotation_user.model');

var _quotation_user2 = _interopRequireDefault(_quotation_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var QuotationUserEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
QuotationUserEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _quotation_user2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    QuotationUserEvents.emit(event + ':' + doc._id, doc);
    QuotationUserEvents.emit(event, doc);
  };
}

exports.default = QuotationUserEvents;
//# sourceMappingURL=quotation_user.events.js.map
