'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newQuotationUser;

describe('QuotationUser API:', function () {

  describe('GET /api/quotation_user', function () {
    var quotationUsers;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/quotation_user').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        quotationUsers = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      quotationUsers.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/quotation_user', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/quotation_user').send({
        name: 'New QuotationUser',
        info: 'This is the brand new quotationUser!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newQuotationUser = res.body;
        done();
      });
    });

    it('should respond with the newly created quotationUser', function () {
      newQuotationUser.name.should.equal('New QuotationUser');
      newQuotationUser.info.should.equal('This is the brand new quotationUser!!!');
    });
  });

  describe('GET /api/quotation_user/:id', function () {
    var quotationUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/quotation_user/' + newQuotationUser._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        quotationUser = res.body;
        done();
      });
    });

    afterEach(function () {
      quotationUser = {};
    });

    it('should respond with the requested quotationUser', function () {
      quotationUser.name.should.equal('New QuotationUser');
      quotationUser.info.should.equal('This is the brand new quotationUser!!!');
    });
  });

  describe('PUT /api/quotation_user/:id', function () {
    var updatedQuotationUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/quotation_user/' + newQuotationUser._id).send({
        name: 'Updated QuotationUser',
        info: 'This is the updated quotationUser!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedQuotationUser = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedQuotationUser = {};
    });

    it('should respond with the updated quotationUser', function () {
      updatedQuotationUser.name.should.equal('Updated QuotationUser');
      updatedQuotationUser.info.should.equal('This is the updated quotationUser!!!');
    });
  });

  describe('DELETE /api/quotation_user/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/quotation_user/' + newQuotationUser._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when quotationUser does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/quotation_user/' + newQuotationUser._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=quotation_user.integration.js.map
