'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var quotationUserCtrlStub = {
  index: 'quotationUserCtrl.index',
  show: 'quotationUserCtrl.show',
  create: 'quotationUserCtrl.create',
  update: 'quotationUserCtrl.update',
  destroy: 'quotationUserCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var quotationUserIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './quotation_user.controller': quotationUserCtrlStub
});

describe('QuotationUser API Router:', function () {

  it('should return an express router instance', function () {
    quotationUserIndex.should.equal(routerStub);
  });

  describe('GET /api/quotation_user', function () {

    it('should route to quotationUser.controller.index', function () {
      routerStub.get.withArgs('/', 'quotationUserCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/quotation_user/:id', function () {

    it('should route to quotationUser.controller.show', function () {
      routerStub.get.withArgs('/:id', 'quotationUserCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/quotation_user', function () {

    it('should route to quotationUser.controller.create', function () {
      routerStub.post.withArgs('/', 'quotationUserCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/quotation_user/:id', function () {

    it('should route to quotationUser.controller.update', function () {
      routerStub.put.withArgs('/:id', 'quotationUserCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/quotation_user/:id', function () {

    it('should route to quotationUser.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'quotationUserCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/quotation_user/:id', function () {

    it('should route to quotationUser.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'quotationUserCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
