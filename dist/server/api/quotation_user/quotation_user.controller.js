/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/quotation_user              ->  index
 * POST    /api/quotation_user              ->  create
 * GET     /api/quotation_user/:id          ->  show
 * PUT     /api/quotation_user/:id          ->  update
 * DELETE  /api/quotation_user/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.getProspects = getProspects;
exports.sendQuotesMail = sendQuotesMail;
exports.getUserQuotes = getUserQuotes;
exports.getCountModel = getCountModel;
exports.getProspectsByDate = getProspectsByDate;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _quotation_user = require('./quotation_user.model');

var _quotation_user2 = _interopRequireDefault(_quotation_user);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _quotationModel = require('../quotation/quotation.model.js');

var _quotationModel2 = _interopRequireDefault(_quotationModel);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mailin = require("mailin-api-node-js");

var client = new Mailin("https://api.sendinblue.com/v2.0", "wFKdNjXJx3ZYUL8M");

var ObjectId = require('mongodb').ObjectID;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    if (entity) {
      var updated = _lodash2.default.merge(entity, updates);
      return updated.save().then(function (updated) {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of QuotationUsers
function index(req, res) {
  return _quotation_user2.default.find().exec().then(respondWithResult(res));
}

// Gets a single QuotationUser from the DB
function show(req, res) {
  return _quotation_user2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res));
}

// Creates a new QuotationUser in the DB
function create(req, res) {
  return _quotation_user2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing QuotationUser in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _quotation_user2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res));
}

// Deletes a QuotationUser from the DB
function destroy(req, res) {
  return _quotation_user2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res));
}

function getProspects(req, res) {

  return _quotation_user2.default.find({
    quote: req.params.id
  }).populate('quote').populate('user', '-password -salt').exec().then(handleEntityNotFound(res)).then(respondWithResult(res));
}

function sendQuotesMail(req, res) {

  _quotation_user2.default.find({
    $and: [{ "wasSend": false }, { "status": { $ne: 'rejected' } }]
  }).populate('user', '-password -salt').populate('quote').exec().then(function (res) {

    // Setear el lenguaje de las fechas a español
    _moment2.default.locale('es');

    var listOfQuotes = res;

    var quotesPerUser = {};

    // Este ciclo va a obtener todas las cotizaciones en las cuales el usuario resultó
    // estar como activo, y las va a poner en un índice particular
    for (var i = 0; i < listOfQuotes.length; i++) {

      if (quotesPerUser[listOfQuotes[i].user._id] == undefined) {
        quotesPerUser[listOfQuotes[i].user._id] = [];
      }
      quotesPerUser[listOfQuotes[i].user._id].push({
        quote: listOfQuotes[i].quote,
        user: listOfQuotes[i].user
      });
    }

    // En este punto quotesPerUser ya contiene todas las cotizaciones organizadas por usuario

    var _loop = function _loop(idUser) {

      var user = quotesPerUser[idUser][0].user;

      var emailUser = '';

      if (user.emailSec != null) emailUser = user.emailSec;else emailUser = user.email;

      var oportunidades = '';

      for (var _i = 0; _i < quotesPerUser[idUser].length; _i++) {
        oportunidades += '-' + (_i + 1) + ' ' + quotesPerUser[idUser][_i].quote.product + ' ' + (0, _moment2.default)(quotesPerUser[idUser][_i].quote.date).format('D MMMM YYYY') + '<br><br>';
      }

      var data = {
        "id": 70,
        "to": emailUser,
        // "to": 'lopez.victor94@gmail.com',
        "from": "cotizaciones@portaliaplus.mx",
        "attr": {
          "MES": (0, _moment2.default)().format('MMMM'),
          "EMPRESA": user.nombre,
          "OPORTUNIDAD": oportunidades,
          "NUMEROOPORTUNIDADES": quotesPerUser[idUser].length,
          "FECHA": (0, _moment2.default)().format('D MMMM YYYY')
        },
        "headers": {
          "Content-Type": "text/html;charset=iso-8859-1",
          "X-param1": "value1",
          "X-param2": "value2",
          "X-Mailin-custom": "Oportunidades de negocio",
          "X-Mailin-tag": "Oportunidades de negocio"
        }
      };

      client.send_transactional_template(data).on('complete', function (data) {
        var dataJSON = JSON.parse(data);
        // console.log( dataJSON );

        for (var _i2 = 0; _i2 < quotesPerUser[idUser].length; _i2++) {
          _quotation_user2.default.findOne({
            "user": user._id,
            "quote": quotesPerUser[idUser][_i2].quote._id
          }).exec().then(function (entity) {
            entity.wasSend = true;
            entity.sendDate = new Date();
            entity.save();
          });
        }
      });
    };

    for (var idUser in quotesPerUser) {
      _loop(idUser);
    }
  });

  return res.status(200).send('ok');
}

function getUserQuotes(req, res) {

  var elementsToTake = 10;

  var page = 0;
  if (req.query.page != null) page = req.query.page;

  _quotation_user2.default.find({
    $and: [{ user: req.params.id }, { status: { $ne: 'rejected' } }]
  }).limit(elementsToTake).skip(page * elementsToTake).populate('user', '-salt -password').populate('quote').sort('-_id').exec(function (err, quotes) {

    if (err) {
      return res.status(500).send('Error inesperado');
    }

    _quotation_user2.default.count({
      $and: [{ user: req.params.id }, { status: { $ne: 'rejected' } }]
    }).exec(function (err, count) {

      if (err) {
        return res.status(500).send('Error ineseperado');
      }

      var response = {
        totalResults: count,
        numPages: Math.ceil(count / elementsToTake),
        quotes: quotes,
        actualPage: parseInt(page)
      };
      res.status(200).send(response);
    });
  });
}

function getCountModel(req, res) {

  return _quotation_user2.default.count().exec(function (err, count) {

    if (err) {
      return res.status(500).send('Error ineseperado');
    }
    return res.status(200).send({ count: count });
  });
}

function getProspectsByDate(req, res) {

  _quotation_user2.default.find({
    "_id": {
      $gte: ObjectId(Math.floor(new Date(req.params.since) / 1000).toString(16) + "0000000000000000"),
      $lt: ObjectId(Math.floor(new Date(req.params.until) / 1000).toString(16) + "0000000000000000")
    }
  }).populate('user', '-salt -password').populate('quote').sort('-_id').exec(function (err, items) {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }

    return res.status(200).send(items);
  });
}
//# sourceMappingURL=quotation_user.controller.js.map
