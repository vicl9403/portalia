/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/sectors              ->  index
 * POST    /api/sectors              ->  create
 * GET     /api/sectors/:id          ->  show
 * PUT     /api/sectors/:id          ->  update
 * DELETE  /api/sectors/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.busqueda = busqueda;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _sector = require('./sector.model');

var _sector2 = _interopRequireDefault(_sector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Sectors
function index(req, res) {
  return _sector2.default.find({}, null, { sort: { name: 1 } }).exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Sector from the DB
function show(req, res) {
  return _sector2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Sector in the DB
function create(req, res) {
  return _sector2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Sector in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _sector2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Sector from the DB
function destroy(req, res) {
  return _sector2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

function busqueda(req, res) {
  console.log(req.body);
  return _sector2.default.find({
    'name': req.body.sectorName
  }).exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}
//# sourceMappingURL=sector.controller.js.map
