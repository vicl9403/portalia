'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var sectorCtrlStub = {
  index: 'sectorCtrl.index',
  show: 'sectorCtrl.show',
  create: 'sectorCtrl.create',
  update: 'sectorCtrl.update',
  destroy: 'sectorCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var sectorIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './sector.controller': sectorCtrlStub
});

describe('Sector API Router:', function () {

  it('should return an express router instance', function () {
    sectorIndex.should.equal(routerStub);
  });

  describe('GET /api/sectors', function () {

    it('should route to sector.controller.index', function () {
      routerStub.get.withArgs('/', 'sectorCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/sectors/:id', function () {

    it('should route to sector.controller.show', function () {
      routerStub.get.withArgs('/:id', 'sectorCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/sectors', function () {

    it('should route to sector.controller.create', function () {
      routerStub.post.withArgs('/', 'sectorCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/sectors/:id', function () {

    it('should route to sector.controller.update', function () {
      routerStub.put.withArgs('/:id', 'sectorCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/sectors/:id', function () {

    it('should route to sector.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'sectorCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/sectors/:id', function () {

    it('should route to sector.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'sectorCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
