/**
 * Sector model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _sector = require('./sector.model');

var _sector2 = _interopRequireDefault(_sector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SectorEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
SectorEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _sector2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    SectorEvents.emit(event + ':' + doc._id, doc);
    SectorEvents.emit(event, doc);
  };
}

exports.default = SectorEvents;
//# sourceMappingURL=sector.events.js.map
