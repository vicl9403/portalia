'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var User = _mongoose2.default.model('User');

var ContactSchema = new _mongoose2.default.Schema({

  name: { type: String, required: true },
  email: { type: String, required: true },
  phone: { type: String, required: true },
  description: { type: String, required: true },

  from: [{ type: Schema.ObjectId, ref: "User" }],
  to: [{ type: Schema.ObjectId, ref: "User" }],

  created_at: { type: Date, default: new Date() },

  authorized: { type: Boolean, default: false },

  active: Boolean

});

exports.default = _mongoose2.default.model('Contact', ContactSchema);
//# sourceMappingURL=contact.model.js.map
