/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/contacts              ->  index
 * POST    /api/contacts              ->  create
 * GET     /api/contacts/:id          ->  show
 * PUT     /api/contacts/:id          ->  update
 * DELETE  /api/contacts/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.authorized = authorized;
exports.notAuthorized = notAuthorized;
exports.index = index;
exports.show = show;
exports.userContacts = userContacts;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.sendContactMail = sendContactMail;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _contact = require('./contact.model');

var _contact2 = _interopRequireDefault(_contact);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var _jade = require('jade');

var transporter = _nodemailer2.default.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  },
  tls: {
    rejectUnauthorized: false
  }
});

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Obtener los contactos autorizados
function authorized(req, res) {

  return _contact2.default.find({
    $and: [{
      "authorized": true
    }]
  }).populate('from').exec().then(respondWithResult(res));
}

// Obtener los contactos no autorizados
function notAuthorized(req, res) {

  _contact2.default.find({
    "authorized": false
  }).populate('from to').exec(function (err, results) {
    res.status(200).send(results);
  });
}

// Gets a list of Contacts
function index(req, res) {

  return _contact2.default.find({
    $and: [{
      "authorized": false
    }]
  }).exec().then(respondWithResult(res));
}

// Gets a single Contact from the DB
function show(req, res) {
  return _contact2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

function userContacts(req, res) {
  return _contact2.default.find({
    $and: [{ "authorized": true }, { "to": req.params.id }]
  }).populate('from').exec().then(respondWithResult(res));
}

// Creates a new Contact in the DB
function create(req, res) {
  return _contact2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Contact in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _contact2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res));
}

// Deletes a Contact from the DB
function destroy(req, res) {
  return _contact2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

function sendContactMail(req, res) {

  var template = './server/views/simpleContact.jade';

  var data = req.body.data;

  // get template from file system
  fs.readFile(template, 'utf8', function (err, file) {
    if (err) {
      //handle errors
      console.log(err);
      return res.send('ERROR!');
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {
        data: data
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);
      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: data.to[0].email, // list of receivers
        cc: 'tmk@portaliaplus.com',
        //to: 'pruebas@portaliaplus.com',
        subject: "Solicitud de cotizacion", // Subject line
        html: html // html body
      }, function (error, info) {
        if (error) return false;else return res.send(200);
      });
    }
  });
}
//# sourceMappingURL=contact.controller.js.map
