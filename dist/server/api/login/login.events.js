/**
 * Login model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _login = require('./login.model');

var _login2 = _interopRequireDefault(_login);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoginEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
LoginEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _login2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    LoginEvents.emit(event + ':' + doc._id, doc);
    LoginEvents.emit(event, doc);
  };
}

exports.default = LoginEvents;
//# sourceMappingURL=login.events.js.map
