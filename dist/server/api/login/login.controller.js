/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/logins              ->  index
 * POST    /api/logins              ->  create
 * GET     /api/logins/:id          ->  show
 * PUT     /api/logins/:id          ->  update
 * DELETE  /api/logins/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.closeUserLoggins = closeUserLoggins;
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.getLogginsByDate = getLogginsByDate;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _login = require('./login.model');

var _login2 = _interopRequireDefault(_login);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    if (entity) {
      var updated = _lodash2.default.merge(entity, updates);
      return updated.save().then(function (updated) {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function closeUserLoggins(req, res) {
  var idUser = req.params.idUser;

  _login2.default.find({
    $and: [{ user: idUser }, { finishedAt: null }]
  }).exec().then(function (loggins) {
    if (loggins.length > 0) {

      for (var i = 0; i < loggins.length; i++) {
        loggins[i].finishedAt = new Date();
        loggins[i].save();
      }
    }
    return res.send(200);
  });
}

// Gets a list of Logins
function index(req, res) {
  return _login2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Login from the DB
function show(req, res) {
  return _login2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Login in the DB
function create(req, res) {
  return _login2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Updates an existing Login in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _login2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Login from the DB
function destroy(req, res) {
  return _login2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

function getLogginsByDate(req, res) {

  _login2.default.find({
    "startAt": {
      $gte: new Date(req.params.since),
      $lt: new Date(req.params.until)
    }
  }).populate('user', '-salt -password').sort('-_id').exec(function (err, items) {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }

    return res.status(200).send(items);
  });
}
//# sourceMappingURL=login.controller.js.map
