'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newLogin;

describe('Login API:', function () {

  describe('GET /api/logins', function () {
    var logins;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/logins').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        logins = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      logins.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/logins', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/logins').send({
        name: 'New Login',
        info: 'This is the brand new login!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newLogin = res.body;
        done();
      });
    });

    it('should respond with the newly created login', function () {
      newLogin.name.should.equal('New Login');
      newLogin.info.should.equal('This is the brand new login!!!');
    });
  });

  describe('GET /api/logins/:id', function () {
    var login;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/logins/' + newLogin._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        login = res.body;
        done();
      });
    });

    afterEach(function () {
      login = {};
    });

    it('should respond with the requested login', function () {
      login.name.should.equal('New Login');
      login.info.should.equal('This is the brand new login!!!');
    });
  });

  describe('PUT /api/logins/:id', function () {
    var updatedLogin;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/logins/' + newLogin._id).send({
        name: 'Updated Login',
        info: 'This is the updated login!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedLogin = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedLogin = {};
    });

    it('should respond with the updated login', function () {
      updatedLogin.name.should.equal('Updated Login');
      updatedLogin.info.should.equal('This is the updated login!!!');
    });
  });

  describe('DELETE /api/logins/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/logins/' + newLogin._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when login does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/logins/' + newLogin._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=login.integration.js.map
