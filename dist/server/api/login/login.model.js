'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var User = _mongoose2.default.model('User');

var LoginSchema = new _mongoose2.default.Schema({

  user: { type: Schema.ObjectId, ref: "User" },

  startAt: { type: Date, default: new Date() },

  finishedAt: { type: Date },

  active: Boolean

});

exports.default = _mongoose2.default.model('Login', LoginSchema);
//# sourceMappingURL=login.model.js.map
