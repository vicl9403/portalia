'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PlanSchema = new _mongoose2.default.Schema({
  beneficios: [{ //Beneficios es un array de objetos que contiene los beneficios que se imprimen en "Pricing"
    description: { //Description es el beneficio como tal, por ejemplo "Perfil Empresarial amigable y autoadministrable"
      type: String,
      default: 'Descripcion del Beneficio Portalia'
    },
    moreInfo: { //moreInfo no se utiliza en el diseño actual, era para el diseño anterior donde había un botón de más información que te abría un modal con una imagen describiendo el beneficio
      exists: { //Booleano que define si el beneficio tiene o no "más información"
        type: Boolean,
        default: false
      },
      source: { //El source de la imagen que va a obtener para el mmodal
        type: String,
        default: 'www.facebook.com' //URL Dummy, falta mandarla a un asset
      }
    },
    paquete: [{ //Paquete es una copia del array de paquetes, para definir si cada uno tiene el beneficio o no
      name: {
        type: String,
        default: 'nombre del paquete' //Ejemplo: "Plus"
      },
      available: { //Define la disponibilidad
        type: Boolean,
        default: false //Ejemplo: En true el plan "Plus" sí tendrá el beneficio "equis"
      },
      limite: { //límite se usa para los beneficios que no son solo un "sí" o "no" para establecer límites (Dejar en el default de 0 si no se necesita)
        type: Number,
        default: 0 //Ejemplo: El plan "Plus" tiene un límite de 50 en el beneficio "Contenido multimedia"
      }
    }]
  }],

  pricing: { //El objeto pricing contiene un array de objetos con los paquetes como tal, para añadir uno nuevo solo tienes que añadir un nuevo objeto con los valores correspondientes, su nombre y su precio
    paquete: [{
      nombre: String,
      costo: Number
    }]
  }
});

exports.default = _mongoose2.default.model('Plan', PlanSchema);
//# sourceMappingURL=plan.model.js.map
