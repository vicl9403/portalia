/**
 * Plan model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _plan = require('./plan.model');

var _plan2 = _interopRequireDefault(_plan);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PlanEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
PlanEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _plan2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    PlanEvents.emit(event + ':' + doc._id, doc);
    PlanEvents.emit(event, doc);
  };
}

exports.default = PlanEvents;
//# sourceMappingURL=plan.events.js.map
