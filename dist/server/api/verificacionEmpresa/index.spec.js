'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var verificacionEmpresaCtrlStub = {
  index: 'verificacionEmpresaCtrl.index',
  show: 'verificacionEmpresaCtrl.show',
  create: 'verificacionEmpresaCtrl.create',
  update: 'verificacionEmpresaCtrl.update',
  destroy: 'verificacionEmpresaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var verificacionEmpresaIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './verificacionEmpresa.controller': verificacionEmpresaCtrlStub
});

describe('VerificacionEmpresa API Router:', function () {

  it('should return an express router instance', function () {
    verificacionEmpresaIndex.should.equal(routerStub);
  });

  describe('GET /api/verificacionEmpresas', function () {

    it('should route to verificacionEmpresa.controller.index', function () {
      routerStub.get.withArgs('/', 'verificacionEmpresaCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/verificacionEmpresas/:id', function () {

    it('should route to verificacionEmpresa.controller.show', function () {
      routerStub.get.withArgs('/:id', 'verificacionEmpresaCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/verificacionEmpresas', function () {

    it('should route to verificacionEmpresa.controller.create', function () {
      routerStub.post.withArgs('/', 'verificacionEmpresaCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/verificacionEmpresas/:id', function () {

    it('should route to verificacionEmpresa.controller.update', function () {
      routerStub.put.withArgs('/:id', 'verificacionEmpresaCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/verificacionEmpresas/:id', function () {

    it('should route to verificacionEmpresa.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'verificacionEmpresaCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/verificacionEmpresas/:id', function () {

    it('should route to verificacionEmpresa.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'verificacionEmpresaCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
