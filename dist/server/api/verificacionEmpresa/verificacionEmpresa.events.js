/**
 * VerificacionEmpresa model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _verificacionEmpresa = require('./verificacionEmpresa.model');

var _verificacionEmpresa2 = _interopRequireDefault(_verificacionEmpresa);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VerificacionEmpresaEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
VerificacionEmpresaEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _verificacionEmpresa2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    VerificacionEmpresaEvents.emit(event + ':' + doc._id, doc);
    VerificacionEmpresaEvents.emit(event, doc);
  };
}

exports.default = VerificacionEmpresaEvents;
//# sourceMappingURL=verificacionEmpresa.events.js.map
