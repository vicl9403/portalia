'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.processAnalysis = processAnalysis;

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _quotation = require('./quotation.model');

var _quotation2 = _interopRequireDefault(_quotation);

var _quotation_user = require('../quotation_user/quotation_user.model');

var _quotation_user2 = _interopRequireDefault(_quotation_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var plural = require('pluralize-es');

var QuoteAnalysis = function () {
  function QuoteAnalysis(minScore) {
    _classCallCheck(this, QuoteAnalysis);

    // Score Mínimo para poder mandar la petición
    this.minScore = minScore;

    // Artículos a eliminar en el preprocesamiento de las palabras
    // this.articles = [
    //   'el',
    //   'la',
    //   'los',
    //   'las',
    //   'un',
    //   'una',
    //   'unos',
    //   'unas',
    //   'lo',
    //   'de'
    // ]
  }

  _createClass(QuoteAnalysis, [{
    key: 'removeSpecials',
    value: function removeSpecials(str) {
      // Definimos los caracteres que queremos eliminar
      var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

      // Los eliminamos todos
      for (var i = 0; i < specialChars.length; i++) {
        str = str.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
      }

      // Cambiar a lower case todo
      str = str.toLowerCase();

      // Quitar los acentos
      str = str.replace(/á/gi, "a");
      str = str.replace(/é/gi, "e");
      str = str.replace(/í/gi, "i");
      str = str.replace(/ó/gi, "o");
      str = str.replace(/ú/gi, "u");
      return str;
    }
  }, {
    key: 'removeArticles',
    value: function removeArticles(str) {
      var _this = this;

      var arrayStr = str.split(' ');
      var withoutArticles = '';

      withoutArticles = arrayStr.map(function (word) {
        if (_this.articles.indexOf(word) == -1) {
          return word;
        }
        return '';
      });

      withoutArticles.join(' ');

      return withoutArticles;
    }
  }, {
    key: 'findUsersToApply',
    value: function findUsersToApply(word) {

      return _user2.default.find({
        $text: {
          $search: word,
          $diacriticSensitive: false,
          $language: 'es'
        }
      }, {
        "score": { $meta: "textScore" }
      }).exec().then(function (users) {
        return users;
      });
    }
  }, {
    key: 'findValueInArrayByKey',
    value: function findValueInArrayByKey(arr, key, id) {

      for (var i = 0; i < arr.length; i++) {
        if (arr[i][key].toString() == id.toString()) {
          return true;
        }
      }

      return false;
    }
  }, {
    key: 'setUserProspects',
    value: function setUserProspects(idQuote, users) {
      var customScore = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;


      var minScore = this.minScore;

      _quotation_user2.default.remove({
        quote: idQuote
      }).exec().then(function (res) {
        var _loop = function _loop(i) {

          // Checar si cumple con el score mínimo
          if (users[i].score >= minScore) {
            // Ver si la cotización ya se le había asignado al usuario anteriormente
            _quotation_user2.default.findOne({
              $and: [{ user: users[i]._id }, { quote: idQuote }]
            }).exec().then(function (res) {
              if (res == null) {
                var usrQuote = new _quotation_user2.default({
                  user: users[i]._id,
                  quote: idQuote,
                  status: 'approved',
                  wasSend: false
                });

                usrQuote.save();
              }
            });
          }
        };

        for (var i = 0; i < users.length; i++) {
          _loop(i);
        }
      });
    }
  }]);

  return QuoteAnalysis;
}();

function getActiveQuotes() {
  return _quotation2.default.find({
    "status": "approved"
  }).exec().then(function (quotes) {
    return quotes;
  });
}

function processAnalysis(minScore) {
  var customQuote = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  var analysis = new QuoteAnalysis(minScore);
  var quotes = void 0;

  if (customQuote == null) {

    getActiveQuotes().then(function (data) {
      quotes = data;
      //  Iterar sobre todas las cotizaciones
      quotes.forEach(function (quote) {

        // Obtener el producto con el texto original
        var product = quote.product;

        if (quote.score == null || quote.score == 0) {
          quote.score = minScore;
          quote.save();
        }
        // Analizar la cotización con el texto original
        analysis.findUsersToApply(product).then(function (res) {
          var users = res;

          // Eliminar las relaciones anteriores
          _quotation_user2.default.remove({
            'quote_id': quote._id
          }).exec().then(function (res) {
            if (users.length > 0) {
              analysis.setUserProspects(quote._id, users, true);
            }
          });
        });
      });
    });
  } else {

    var quote = customQuote;

    // Obtener el producto con el texto original
    var product = quote.product;

    // Analizar la cotización con el texto original
    analysis.findUsersToApply(product).then(function (res) {

      var users = res;

      // Eliminar las relaciones anteriores
      _quotation_user2.default.remove({
        'quote_id': quote._id
      }).exec().then(function (res) {
        if (users.length > 0) {
          analysis.setUserProspects(quote._id, users, true);
        }
      });
    });
  }
}
//# sourceMappingURL=quotationAnalysis.js.map
