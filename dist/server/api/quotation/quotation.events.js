/**
 * Quotation model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _quotation = require('./quotation.model');

var _quotation2 = _interopRequireDefault(_quotation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var QuotationEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
QuotationEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _quotation2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    QuotationEvents.emit(event + ':' + doc._id, doc);
    QuotationEvents.emit(event, doc);
  };
}

exports.default = QuotationEvents;
//# sourceMappingURL=quotation.events.js.map
