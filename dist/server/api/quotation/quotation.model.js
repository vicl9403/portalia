'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var User = _mongoose2.default.model('User');

var QuotationSchema = new Schema({

  product: String,
  sector: String,
  category: String,
  state: String,
  quantity: String,
  unity: String,
  frecuency: String,
  date: Date,
  createdAt: {
    type: Date,
    default: Date.now
  },
  product_use: String,
  comments: String,
  bussiness_name: String,
  contact_name: String,
  contact_lastname: String,
  featured: {
    type: Boolean,
    default: false
  },
  contact_email: String,
  contact_phone: {
    type: { type: String },
    lada: { type: String },
    number: { type: String }
  },
  user: [{ type: Schema.ObjectId, ref: "User" }],
  status: String,
  segmentId: String,

  prospects: [],

  watched: { type: Boolean, default: false },

  // Variable utilizada para el análisis de prospectos
  score: { type: Number, default: 0 },

  active: {
    type: Boolean,
    default: true
  }

});

QuotationSchema.virtual('profile').get(function () {
  return {
    'name': this.name,
    'role': this.role
  };
});

exports.default = _mongoose2.default.model('Quotation', QuotationSchema);
//# sourceMappingURL=quotation.model.js.map
