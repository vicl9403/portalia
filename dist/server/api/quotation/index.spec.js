'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var quotationCtrlStub = {
  index: 'quotationCtrl.index',
  show: 'quotationCtrl.show',
  create: 'quotationCtrl.create',
  update: 'quotationCtrl.update',
  destroy: 'quotationCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var quotationIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './quotation.controller': quotationCtrlStub
});

describe('Quotation API Router:', function () {

  it('should return an express router instance', function () {
    quotationIndex.should.equal(routerStub);
  });

  describe('GET /api/quotes', function () {

    it('should route to quotation.controller.index', function () {
      routerStub.get.withArgs('/', 'quotationCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/quotes/:id', function () {

    it('should route to quotation.controller.show', function () {
      routerStub.get.withArgs('/:id', 'quotationCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/quotes', function () {

    it('should route to quotation.controller.create', function () {
      routerStub.post.withArgs('/', 'quotationCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/quotes/:id', function () {

    it('should route to quotation.controller.update', function () {
      routerStub.put.withArgs('/:id', 'quotationCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/quotes/:id', function () {

    it('should route to quotation.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'quotationCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/quotes/:id', function () {

    it('should route to quotation.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'quotationCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
