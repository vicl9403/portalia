/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/quotes              ->  index
 * POST    /api/quotes              ->  create
 * GET     /api/quotes/:id          ->  show
 * PUT     /api/quotes/:id          ->  update
 * DELETE  /api/quotes/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendQuotesAnalysis = sendQuotesAnalysis;
exports.sendQuotesAnalysisForOneQuote = sendQuotesAnalysisForOneQuote;
exports.index = index;
exports.show = show;
exports.newQuotes = newQuotes;
exports.approvedQuotes = approvedQuotes;
exports.quotesByCategory = quotesByCategory;
exports.create = create;
exports.update = update;
exports.setQuotationFinished = setQuotationFinished;
exports.updateProspects = updateProspects;
exports.destroy = destroy;
exports.activeContacts = activeContacts;
exports.activeQuotes = activeQuotes;
exports.searchContacts = searchContacts;
exports.searchQuotes = searchQuotes;
exports.changeContactStatus = changeContactStatus;
exports.changeQuoteStatus = changeQuoteStatus;
exports.getUserQuotes = getUserQuotes;
exports.getSectorQuotes = getSectorQuotes;
exports.getFeaturedQuotes = getFeaturedQuotes;
exports.getAllQuotes = getAllQuotes;
exports.getQuotesByDate = getQuotesByDate;
exports.getWatchedQuotes = getWatchedQuotes;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _quotation = require('./quotation.model');

var _quotation2 = _interopRequireDefault(_quotation);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _quotationAnalysis = require('./quotationAnalysis');

var quotationAnalysis = _interopRequireWildcard(_quotationAnalysis);

var _quotes_configuration = require('../quotes_configuration/quotes_configuration.model');

var _quotes_configuration2 = _interopRequireDefault(_quotes_configuration);

var _quotation_user = require('../quotation_user/quotation_user.model');

var _quotation_user2 = _interopRequireDefault(_quotation_user);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var quotationConfiguration = require('../quotes_configuration/quotes_configuration.controller');

var pagination = require('mongoose-pagination');
var util = require('util');
var fs = require('fs');
var _jade = require('jade');
var mailin = require("mailin-api-node-js");
var client = new Mailin("https://api.sendinblue.com/v2.0", "wFKdNjXJx3ZYUL8M");
var moment = require('moment');
var pagination = require('mongoose-pagination');
var asyncLoop = require('node-async-loop');
var request = require("request");
var transporter = _nodemailer2.default.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portalia0plus.com',
    pass: 'Qwerty123$%'
  },
  tls: {
    rejectUnauthorized: false
  }
});

var ObjectId = require('mongodb').ObjectID;

var schedule = require('node-schedule');

// Este cron se ejecuta diario y genera el match entre
// las cotizaciones que estén activas y los usuarios que apliquen para
// las cotizaciones
var quotesCron = schedule.scheduleJob('0 0 * * *', function () {

  _quotes_configuration2.default.findOne().sort('-_id').exec().then(function (res) {
    quotationAnalysis.processAnalysis(res.min_score);
  });
});

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function sendQuotesAnalysis(req, res) {

  // Si viene el parámetro utilizarlo
  if (req.params.minScore != undefined) {
    console.log('with param' + req.params.minScore);
    quotationAnalysis.processAnalysis(req.params.minScore);
  } else {
    _quotes_configuration2.default.findOne().sort('-_id').exec().then(function (res) {
      quotationAnalysis.processAnalysis(res.min_score);
    });
  }

  res.status(200).send('ok');
}

function sendQuotesAnalysisForOneQuote(req, res) {

  _quotation2.default.findById(req.params.quote).exec().then(function (quote) {
    quotationAnalysis.processAnalysis(req.params.minScore, quote);
    quote.score = req.params.minScore;
    quote.save();
  });

  res.status(200).send('ok');
}

// Esta función manda un mail automáticamente al usuario que se contacto
function sendQuoteMailToSpecificUser(quote) {

  if (quote.user[0] != undefined) {
    var idUser = quote.user[0]._id;
    _user2.default.findById(idUser).exec().then(function (user) {

      var data = {
        "id": 70,
        "to": user.email,
        // "to": 'lopez.victor94@gmail.com',
        "attr": {
          "MES": moment().format('MMMM'),
          "EMPRESA": user.nombre,
          "OPORTUNIDAD": quote.product,
          "NUMEROOPORTUNIDADES": 1,
          "FECHA": moment(new Date()).format('D MMMM YYYY')
        },
        "headers": {
          "Content-Type": "text/html;charset=iso-8859-1",
          "X-param1": "value1",
          "X-param2": "value2",
          "X-Mailin-custom": "Oportunidades de negocio",
          "X-Mailin-tag": "Oportunidades de negocio"
        }
      };

      client.send_transactional_template(data).on('complete', function (data) {
        var dataJSON = JSON.parse(data);
        // console.log( dataJSON );
      });
    });
  }
}

function saveUpdates(updates) {

  // Si tiene un usuario y cambia a aprobada se manda automáticamente el mail
  if (updates.user != undefined && updates.status == 'approved') {
    // Este bloque se ejecuta porque fué un contacto directo dentro de bussinesslist
    sendQuoteMailToSpecificUser(updates);
  }

  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Quotations
function index(err, res) {

  _quotation2.default.find({}, function (err, quotes) {
    _user2.default.populate(quotes, {
      path: "user"
    }, function (err, quotes) {
      res.status(200).send(quotes);
    });
  });
}

// Gets a single Quotation from the DB
function show(req, res) {
  return _quotation2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Quotation from the DB
function newQuotes(err, res) {

  return _quotation2.default.find({ "status": null }).populate('user', 'nombre , _id').exec().then(respondWithResult(res));
}

function approvedQuotes(err, res) {

  _quotation2.default.find({
    "status": 'approved'
  }, function (err, quotes) {
    _user2.default.populate(quotes, {
      path: "user"
    }, function (err, quotes) {
      res.status(200).send(quotes);
    });
  });
}

function quotesByCategory(req, res) {
  _quotation2.default.find({
    $and: [{ "status": 'approved' }, { "category": req.params.category }]
  }, function (err, quotes) {
    res.status(200).send(quotes);
  });
}

// Creates a new Quotation in the DB
function create(req, res) {
  return _quotation2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Quotation in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _quotation2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res));
}

function setQuotationFinished(req, res) {
  return _quotation2.default.findById(req.params.id).exec().then(function (res) {
    res.status = 'done';
    res.save().then(respondWithResult("good"));
  });
}

function updateProspects(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _quotation2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(function (entity) {
    entity.prospects = req.body;
    entity.save().then(respondWithResult(res));
  });
}

// Deletes a Quotation from the DB
function destroy(req, res) {
  return _quotation2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

//Trae los contactos activas
function activeContacts(req, res) {
  return _quotation2.default.find({
    $and: [{
      'active': req.body.active
    }, {
      'user': {
        $not: {
          $size: 0
        }
      }
    }]
  }).paginate(req.body.requestedPage, 10, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'documents': docs
    };
    res.status(200).json(respuesta);
  });
}

//Trae los quotes activas
function activeQuotes(req, res) {
  return _quotation2.default.find({
    $and: [{
      'active': req.body.active
    }, {
      'user': {
        $size: 0
      }
    }]
  }).paginate(req.body.requestedPage, 10, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'documents': docs
    };
    res.status(200).json(respuesta);
  });
}

//Trae las contactos en un estado específico
function searchContacts(req, res) {
  return _quotation2.default.find({
    $and: [{
      'status': req.body.status
    }, {
      'user': {
        $not: {
          $size: 0
        }
      }
    }]
  }).paginate(req.body.requestedPage, 10, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'documents': docs
    };
    res.status(200).json(respuesta);
  });
}

//Trae las cotizaciones en un estado específico
function searchQuotes(req, res) {
  return _quotation2.default.find({
    $and: [{
      'status': req.body.status
    }, {
      'user': {
        $size: 0
      }
    }]
  }).paginate(req.body.requestedPage, 10, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'documents': docs
    };
    res.status(200).json(respuesta);
  });
}

//Cambia el estado de la cotización
function changeContactStatus(req, res) {
  return _quotation2.default.findById(req.body.id).exec().then(function (quote) {
    quote.status = req.body.status;
    quote.active = req.body.active;
    quote.save();
    if (req.body.status == 'approved') {
      sendMail(quote);
    }
    res.status(200).json(quote);
  }).catch(handleError(res));
}

//Cambia el estado de la cotización
function changeQuoteStatus(req, res) {
  console.log(req.body);
  return _quotation2.default.findById(req.body.id).exec().then(function (quote) {
    quote.status = req.body.status;
    quote.active = req.body.active;
    quote.save();
    if (req.body.status == 'approved') {
      sendMasive(quote);
    }
    res.status(200).json(quote);
  }).catch(function (err) {
    console.log(err);
    res.status(500).end();
  });
}

//Trae las cotizaciones dirigidas a un usuario
function getUserQuotes(req, res) {
  return _quotation2.default.find({
    $and: [{
      'status': 'approved'
    }, {
      'user': req.params.id
    }]
  }).exec().then(function (quotes) {
    res.status(200).json(quotes);
  }).catch(function (err) {
    console.log(err);
    res.status(500).end();
  });
}

//Trae las cotizaciones dirigidas a un usuario
function getSectorQuotes(req, res) {
  return _quotation2.default.find({
    $and: [{
      'status': 'approved'
    }, {
      'sector': req.params.sector
    }]
  }).exec().then(function (quotes) {
    res.status(200).json(quotes);
  }).catch(function (err) {
    console.log(err);
    res.status(500).end();
  });
}

//Trae las destacadas
function getFeaturedQuotes(req, res) {
  return _quotation2.default.find({
    $or: [{ 'featured': true }, { 'status': 'done' }]
  }).sort('-_id').limit(20).exec().then(function (quotes) {
    res.status(200).json(quotes);
  }).catch(function (err) {
    console.log(err);
    res.status(500).end();
  });
}

//Trae todas con paginado
function getAllQuotes(req, res) {
  var and = [];
  var categorias;
  var estados;
  var sectores;

  if (req.body.state != undefined) {
    and.push({
      state: new RegExp(req.body.state, 'i')
    });
  }

  if (req.body.sector != undefined) {
    and.push({
      sector: new RegExp(req.body.sector, 'i')
    });
  }

  if (req.body.category != undefined) {
    and.push({
      category: new RegExp(req.body.category, 'i')
    });
  }

  if (req.body.sortDate != undefined) {
    if (req.body.sortDate == 'asc') {
      var sorting = 'createdAt';
    } else if (req.body.sortDate == 'desc') {
      var sorting = '-createdAt';
    }
  } else {
    var sorting = 'createdAt';
  }

  if (and.length == 0) {
    var query = {
      $or: [{ 'status': 'approved' }, { 'status': 'done' }]
    };

    _quotation2.default.distinct('state').exec().then(function (results) {
      estados = results;
    });

    _quotation2.default.distinct('sector').exec().then(function (results) {
      sectores = results;
    });

    _quotation2.default.distinct('category').exec().then(function (results) {
      categorias = results;
    });
  } else {
    _quotation2.default.distinct('state', {
      $and: and
    }).exec().then(function (results) {
      estados = results;
    });

    _quotation2.default.distinct('sector', {
      $and: and
    }).exec().then(function (results) {
      sectores = results;
    });

    _quotation2.default.distinct('category', {
      $and: and
    }).exec().then(function (results) {
      categorias = results;
    });
  }

  and.push({
    $or: [{ status: 'approved' }, { status: 'done' }]

  });

  var query = {
    $and: and
  };

  _quotation2.default.find(query).sort(sorting).paginate(req.body.requestedPage, req.body.itemsPerPage, function (err, docs, total) {
    var respuesta = {
      'totalResults': total,
      'currentPage': req.body.requestedPage,
      'totalPages': Math.ceil(total / 10),
      'categories': categorias,
      'sectors': sectores,
      'states': estados,
      'documents': [docs]
    };

    res.status(200).json(respuesta);
  });
}

//Esta función se encarga de enviar los correos cuando se aprueba una cotización
function sendMail(quote) {
  console.log('Sendmail triggered');
  _user2.default.populate(quote, {
    path: "user"
  }, function (err, quote) {
    switch (quote.sector) {
      case 'Electricidad y agua':
        quote.url = 'elecagua.jpg';
        break;
      case 'Turismo':
        quote.url = 'turismo.jpg';
        break;
      case 'Agroindustria':
        quote.url = 'agroindustria.jpg';
        break;
      case 'Logistica y transportación':
        quote.url = 'logistica.jpg';
        break;
      case 'Industrias manufactureras':
        quote.url = 'manufactura.jpg';
        break;
      case 'Construcción':
        quote.url = 'construccion.jpg';
        break;
      case 'Servicios especializados':
        quote.url = 'especializados.jpg';
        break;
    }
    var template = './server/views/cotizacionAdmin.jade';
    var templateDos = './server/views/cotizacionCopy.jade';

    // get template from file system
    fs.readFile(template, 'utf8', function (err, file) {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {
        data: quote
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: quote.user[0].email, // list of receivers
        subject: "Cotización de Portalia Plus", // Subject line
        cco: 'mkt@portaliaplus.com',
        html: html // html body
      }, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });
    });

    // get template from file system
    fs.readFile(templateDos, 'utf8', function (err, file) {
      //compile jade template into function
      var compiledTmplDos = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var contextDos = {
        data: quote
      };
      // get html back as a string with the context applied;
      var htmlDos = compiledTmplDos(contextDos);

      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: quote.contact_email, // list of receivers
        subject: "Tu cotización de Portalia Plus ha sido enviada", // Subject line
        html: htmlDos // html body
      }, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });
    });
  });
}

//Función de difusión masiva con send in blue
function sendMasive(quote) {
  return _user2.default.find({
    'categoria_name': quote.category
  }).exec().then(function (users) {
    asyncLoop(users, function (user, next) {
      var telefono = quote.contact_phone.lada + quote.contact_phone.number;
      var data = {
        "id": 5,
        "to": user.email,
        "attr": {
          "DATE": moment(quote.date).format('DD/MM/YYYY'),
          "PRODUCT": quote.product,
          "QUANTITY": quote.quantity,
          "FREQUENCY": quote.frecuency,
          "COMMENTS": quote.comments,
          "CONTACTNAME": quote.contact_name,
          "CONTACTPHONE": telefono,
          "CONTACTEMAIL": quote.contact_email,
          "CONTACTESTADO": quote.state,
          "CATEGORIA": quote.category
        },
        "headers": {
          "Content-Type": "text/html;charset=iso-8859-1",
          "X-param1": "value1",
          "X-param2": "value2",
          "X-Mailin-custom": "my custom value",
          "X-Mailin-tag": "my tag value"
        } //lol

      };client.send_transactional_template(data).on('complete', function (data) {
        var dataJSON = JSON.parse(data);
        console.log(dataJSON);
        if (user.wantsNotifications == true) {
          request({
            url: "https://api.pushengage.com/apiv1/notifications",
            method: "POST",
            headers: {
              "api_key": "Mz2LkrECnf0uFi4Pxj17974Jl9O8sTym"
            },
            form: {
              "notification_title": "Hay nuevas oportunidades de negocio para ti en Portalia Plus",
              "notification_message": "¡Revisa tu correo electrónico para más información!",
              "notification_url": "https://portaliaplus.com",
              "profile_id[0]": user._id,
              "image_url": 'https://portaliaplus.com/assets/images/icons/chat-message.png'
            }
          }, function (err, res, body) {
            next();
            if (!err) {
              console.log(res.body);
            } else {
              console.log(err);
            }
          });
        } else {
          next();
        }
      });
    }, function (err) {
      if (err) {
        console.error('Error: ' + err.message);
        return;
      }

      console.log('Finished!');
    });
  }).catch(function (err) {
    console.log(err);
  });
}

function getOldPrespectsForAllQuotes() {

  _quotation2.default.find().exec().then(function (res) {
    var quotes = res;

    var _loop = function _loop(i) {
      if (quotes[i].prospects != null) {

        if (quotes[i].prospects.length > 0) {
          var _loop2 = function _loop2(c) {
            _quotation_user2.default.findOne({
              quote: quotes[i]._id,
              user: quotes[i].prospects[c].user
            }).exec().then(function (res) {

              if (res == null) {

                // Esto significa que no existe esta relación, por lo tanto se va a asignar
                _user2.default.findById(quotes[i].prospects[c].user).exec().then(function (res) {
                  if (res != null) {
                    var newRelation = new _quotation_user2.default({
                      quote: quotes[i]._id,
                      user: quotes[i].prospects[c].user,
                      wasSend: quotes[i].prospects[c].wasSend,
                      status: quotes[i].status
                    });
                    newRelation.save();
                  }
                });
              }
            });
          };

          for (var c = 0; c < quotes[i].prospects.length; c++) {
            _loop2(c);
          }
        }
      }
    };

    for (var i = 0; i < quotes.length; i++) {
      _loop(i);
    }
  });
}

function getQuotesByDate(req, res) {

  _quotation2.default.find({
    $and: [{
      "_id": {
        $gte: ObjectId(Math.floor(new Date(req.params.since) / 1000).toString(16) + "0000000000000000"),
        $lt: ObjectId(Math.floor(new Date(req.params.until) / 1000).toString(16) + "0000000000000000")
      }
    }, { status: { $ne: null } }]

  }).sort('-_id').exec(function (err, quotes) {
    if (err) {
      return res.status(402).send(err);
    }
    return res.status(200).send(quotes);
  });
}

function getWatchedQuotes(req, res) {
  _quotation2.default.find({
    "watched": true
  }).exec(function (err, quotes) {
    if (err) return res.status(402).send(err);

    return res.status(200).send(quotes);
  });
}
// Esta función solamente se debe de llamar si se desea analizar de nuevo las cotizacions con la funcionalidad anterior
//getOldPrespectsForAllQuotes();
//# sourceMappingURL=quotation.controller.js.map
