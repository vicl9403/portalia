'use strict';

var express = require('express');
var controller = require('./quotation.controller');

var router = express.Router();

router.get('/watched', controller.getWatchedQuotes);

router.get('/newQuotes', controller.newQuotes);
router.get('/approvedQuotes', controller.approvedQuotes);
router.get('/category/:category', controller.quotesByCategory);
router.post('/sendQuotesAnalysis/:minScore*?', controller.sendQuotesAnalysis);
router.post('/sendQuotesAnalysisForOneQuote/:quote/:minScore', controller.sendQuotesAnalysisForOneQuote);
router.patch('/setQuotationFinished/:id', controller.setQuotationFinished);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/getUserQuotes/:id', controller.getUserQuotes);
router.get('/getSectorQUotes/:sector', controller.getSectorQuotes);
router.patch('/updateProspects/:id', controller.updateProspects);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/activeContacts', controller.activeContacts);
router.post('/activeQuotes', controller.activeQuotes);
router.post('/searchContacts', controller.searchContacts);
router.post('/searchQuotes', controller.searchQuotes);
router.post('/changeContactStatus', controller.changeContactStatus);
router.post('/changeQuoteStatus', controller.changeQuoteStatus);
router.post('/getFeaturedQuotes', controller.getFeaturedQuotes);
router.post('/getAllQuotes', controller.getAllQuotes);

router.get('/date/:since/:until', controller.getQuotesByDate);

module.exports = router;
//# sourceMappingURL=index.js.map
