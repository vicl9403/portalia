/**
 * TopUser model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _topUser = require('./topUser.model');

var _topUser2 = _interopRequireDefault(_topUser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TopUserEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
TopUserEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _topUser2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    TopUserEvents.emit(event + ':' + doc._id, doc);
    TopUserEvents.emit(event, doc);
  };
}

exports.default = TopUserEvents;
//# sourceMappingURL=topUser.events.js.map
