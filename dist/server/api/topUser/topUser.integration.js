'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newTopUser;

describe('TopUser API:', function () {
  describe('GET /api/topUsers', function () {
    var topUsers;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/topUsers').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        topUsers = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      topUsers.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/topUsers', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/topUsers').send({
        name: 'New TopUser',
        info: 'This is the brand new topUser!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newTopUser = res.body;
        done();
      });
    });

    it('should respond with the newly created topUser', function () {
      newTopUser.name.should.equal('New TopUser');
      newTopUser.info.should.equal('This is the brand new topUser!!!');
    });
  });

  describe('GET /api/topUsers/:id', function () {
    var topUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/topUsers/' + newTopUser._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        topUser = res.body;
        done();
      });
    });

    afterEach(function () {
      topUser = {};
    });

    it('should respond with the requested topUser', function () {
      topUser.name.should.equal('New TopUser');
      topUser.info.should.equal('This is the brand new topUser!!!');
    });
  });

  describe('PUT /api/topUsers/:id', function () {
    var updatedTopUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/topUsers/' + newTopUser._id).send({
        name: 'Updated TopUser',
        info: 'This is the updated topUser!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedTopUser = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedTopUser = {};
    });

    it('should respond with the original topUser', function () {
      updatedTopUser.name.should.equal('New TopUser');
      updatedTopUser.info.should.equal('This is the brand new topUser!!!');
    });

    it('should respond with the updated topUser on a subsequent GET', function (done) {
      (0, _supertest2.default)(app).get('/api/topUsers/' + newTopUser._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        var topUser = res.body;

        topUser.name.should.equal('Updated TopUser');
        topUser.info.should.equal('This is the updated topUser!!!');

        done();
      });
    });
  });

  describe('PATCH /api/topUsers/:id', function () {
    var patchedTopUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).patch('/api/topUsers/' + newTopUser._id).send([{ op: 'replace', path: '/name', value: 'Patched TopUser' }, { op: 'replace', path: '/info', value: 'This is the patched topUser!!!' }]).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        patchedTopUser = res.body;
        done();
      });
    });

    afterEach(function () {
      patchedTopUser = {};
    });

    it('should respond with the patched topUser', function () {
      patchedTopUser.name.should.equal('Patched TopUser');
      patchedTopUser.info.should.equal('This is the patched topUser!!!');
    });
  });

  describe('DELETE /api/topUsers/:id', function () {
    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/topUsers/' + newTopUser._id).expect(204).end(function (err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when topUser does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/topUsers/' + newTopUser._id).expect(404).end(function (err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=topUser.integration.js.map
