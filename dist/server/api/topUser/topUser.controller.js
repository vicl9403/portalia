/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/topUsers              ->  index
 * POST    /api/topUsers              ->  create
 * GET     /api/topUsers/:id          ->  show
 * PUT     /api/topUsers/:id          ->  upsert
 * PATCH   /api/topUsers/:id          ->  patch
 * DELETE  /api/topUsers/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.upsert = upsert;
exports.patch = patch;
exports.destroy = destroy;
exports.deleteDis = deleteDis;

var _fastJsonPatch = require('fast-json-patch');

var _fastJsonPatch2 = _interopRequireDefault(_fastJsonPatch);

var _topUser = require('./topUser.model');

var _topUser2 = _interopRequireDefault(_topUser);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function patchUpdates(patches) {
  return function (entity) {
    try {
      _fastJsonPatch2.default.apply(entity, patches, /*validate*/true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of TopUsers
function index(err, res) {
  return _topUser2.default.find({}, function (err, docs) {
    _user2.default.populate(docs, {
      path: "idUser"
    }, function (err, users) {
      res.status(200).json(users);
    });
  });
}

// Gets a single TopUser from the DB
function show(req, res) {
  return _topUser2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new TopUser in the DB
function create(req, res) {
  _topUser2.default.count({}, function (err, count) {
    if (count == 12) {
      return res.status(200).json('Limite alcanzado');
    } else {
      return _topUser2.default.create(req.body).then(respondWithResult(res, 201));
      // .catch(handleError(res));
    }
  });
}

// Upserts the given TopUser in the DB at the specified ID
function upsert(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _topUser2.default.findOneAndUpdate(req.params.id, req.body, {
    upsert: true,
    setDefaultsOnInsert: true,
    runValidators: true
  }).exec().then(respondWithResult(res)).catch(handleError(res));
}

// Updates an existing TopUser in the DB
function patch(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _topUser2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(patchUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a TopUser from the DB
function destroy(req, res) {
  return _topUser2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}

function deleteDis(req, res) {
  console.log('kjfslkdjfslk');
  return _topUser2.default.findOne({
    idString: req.body.idString
  }).exec().then(function (doc) {
    console.log(doc);
    doc.remove();
    res.status(200).end();
  });
}
//# sourceMappingURL=topUser.controller.js.map
