'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var topUserCtrlStub = {
  index: 'topUserCtrl.index',
  show: 'topUserCtrl.show',
  create: 'topUserCtrl.create',
  upsert: 'topUserCtrl.upsert',
  patch: 'topUserCtrl.patch',
  destroy: 'topUserCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var topUserIndex = proxyquire('./index.js', {
  express: {
    Router: function Router() {
      return routerStub;
    }
  },
  './topUser.controller': topUserCtrlStub
});

describe('TopUser API Router:', function () {
  it('should return an express router instance', function () {
    topUserIndex.should.equal(routerStub);
  });

  describe('GET /api/topUsers', function () {
    it('should route to topUser.controller.index', function () {
      routerStub.get.withArgs('/', 'topUserCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/topUsers/:id', function () {
    it('should route to topUser.controller.show', function () {
      routerStub.get.withArgs('/:id', 'topUserCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/topUsers', function () {
    it('should route to topUser.controller.create', function () {
      routerStub.post.withArgs('/', 'topUserCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/topUsers/:id', function () {
    it('should route to topUser.controller.upsert', function () {
      routerStub.put.withArgs('/:id', 'topUserCtrl.upsert').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/topUsers/:id', function () {
    it('should route to topUser.controller.patch', function () {
      routerStub.patch.withArgs('/:id', 'topUserCtrl.patch').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/topUsers/:id', function () {
    it('should route to topUser.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'topUserCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
