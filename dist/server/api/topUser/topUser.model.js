'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var User = _mongoose2.default.model('User');

var TopUserSchema = new _mongoose2.default.Schema({
  idUser: [{ type: Schema.ObjectId, ref: "User" }],
  idString: String
});

exports.default = _mongoose2.default.model('TopUser', TopUserSchema);
//# sourceMappingURL=topUser.model.js.map
