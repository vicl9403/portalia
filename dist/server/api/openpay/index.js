'use strict';

var _auth = require('../../auth/auth.service');

var auth = _interopRequireWildcard(_auth);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var express = require('express');
var controller = require('./openpay.controller');


var router = express.Router();

router.post('/card', auth.isAuthenticated(), controller.card);
router.post('/bank', auth.isAuthenticated(), controller.bank);
router.post('/subscription', auth.isAuthenticated(), controller.subscription);
router.post('/webhook', controller.webhook);

module.exports = router;
//# sourceMappingURL=index.js.map
