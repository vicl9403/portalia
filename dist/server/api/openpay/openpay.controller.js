'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bank = bank;
exports.card = card;
exports.subscription = subscription;
exports.webhook = webhook;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _busqueda = require('../busqueda/busqueda.controller');

var billing = _interopRequireWildcard(_busqueda);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var randomstring = require("randomstring");
var Openpay = require('openpay');
var openpay = new Openpay("mkx9erv2qm0efa14gvvz", "sk_29f6320a9c734292a044d861885c8385", true);
var _jade = require('jade');
var fs = require('fs');
var P = require('bluebird');

var transporter = _nodemailer2.default.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use S
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  },
  tls: {
    rejectUnauthorized: false
  }
});

// Creates a new Email in the DB
function enviarRecibo(nombre, plan, pago, fechaInicio, fechaFin, tipo, email) {
  var template = './server/views/recibo.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function (err, file) {
    if (err) {
      //handle errors
      console.log(err);
    } else {
      //compile jade template into function
      var compiledTmpl = _jade.compile(file, {
        filename: template
      });
      // set context to be used in template
      var context = {
        nombre: nombre,
        plan: plan,
        tipo_pago: tipo,
        fecha_inicial: fechaInicio,
        fecha_final: fechaFin
      };
      // get html back as a string with the context applied;
      var html = compiledTmpl(context);

      return transporter.sendMail({
        from: 'hola@portaliaplus.com', // sender address
        to: email, // list of receivers
        subject: "Pago realizado con exito y paquete activado", // Subject line
        html: html // html body
      }, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
      });
    }
  });
}

function addCustomer(req, cb) {
  var customerRequest = {
    'name': req.user.nombre,
    'email': req.user.email,
    'requires_account': false
  };
  openpay.customers.create(customerRequest, function (error, customer) {
    if (error) {
      cb(error);
      return;
    } else {
      _user2.default.findById(req.user._id).then(function (usr) {
        usr.openpayID = customer.id;
        usr.save().then(function () {
          cb(false);
        });
      });
    }
  });
};

function createSubscription(planS, plan, req, cb) {
  var subscriptionRequest = {
    'source_id': req.body.card,
    'plan_id': plan
  };
  _user2.default.findById(req.user._id).exec().then(function (user) {
    openpay.customers.subscriptions.create(user.openpayID, subscriptionRequest, function (error, subscription) {
      if (error) {
        cb(error);
        return;
      } else {
        user.subscriptionCounter = req.body.payments;
        user.subscription = subscription;
        user.subscription.plan = planS;
        user.save().then(function () {
          cb(false);
        });
      }
    });
  });
}

function getSubscription(uid, id, cb) {
  openpay.customers.subscriptions.get(uid, id, function (error, subscription) {
    if (error) {
      console.log('Line 122:', error);
      cb(false);
      return;
    }
    openpay.plans.get(subscription.plan_id, function (err, plan) {
      if (err) {
        console.log('Line 128:', err);
        cb(false);
        return;
      }
      cb(plan);
    });
  });
}

function cancelSubscription(uid, id, cb) {
  openpay.customers.subscriptions.delete(uid, id, function (err) {
    if (err) {
      console.log('Line 140:', err);
      cb(false);
      return;
    }
    cb(true);
  });
}

function verifyCharge(id, callback) {
  openpay.charges.get(id, function (error, charge) {
    if (error) {
      callback(false);
      return;
    } else {
      // console.log('Line 154', charge);
      if (charge.status === 'completed') {
        callback(charge);
      } else {
        callback(false);
      }
    }
  });
}

function updateUserPlan(res, charge) {
  _user2.default.findOne({
    'planSolicitado.openPayResponse.order_id': charge.order_id
  }).exec().then(function (user) {
    if (!user) {
      console.log("No en esta DB");
      res.status(200).end();
    }
    user.fechaPago = (0, _moment2.default)({});
    if (user.planSolicitado.plan == user.paquete) {
      var timeRemaining = parseInt((0, _moment2.default)(user.fechaCorte).diff((0, _moment2.default)({}), 'days'));
      if (timeRemaining > 0) {
        user.fechaCorte = (0, _moment2.default)({}).add(user.planSolicitado.period, 'M').add(timeRemaining, 'd');
      } else {
        user.fechaCorte = (0, _moment2.default)({}).add(user.planSolicitado.period, 'M');
      }
    } else {
      user.fechaCorte = (0, _moment2.default)({}).add(user.planSolicitado.period, 'M');
    }
    user.paquete = user.planSolicitado.plan;
    user.noPaquete = 2;
    // if (user.datosDeFacturacion.requested == true) {
    //   var dataFactura = {};
    //   dataFactura.user = user;
    //   dataFactura.body = user.datosDeFacturacion
    //   dataFactura.body.precioMes = user.planSolicitadoPricing.price
    //   dataFactura.body.meses = user.planSolicitadoPricing.period
    //   dataFactura.body.planSolicitado = user.planSolicitadoPricing.plan
    //   dataFactura.body.idOrden = charge.order_id
    //   dataFactura.body.descuento = user.planSolicitadoPricing.discount
    //   dataFactura.body.total = user.planSolicitadoPricing.price * user.planSolicitadoPricing.period
    //   dataFactura.body.method = charge.method
    //   if (charge.method == 'card') {
    //     dataFactura.body.cardType = charge.card.type
    //   }
    //   billing.killme(dataFactura)
    // }
    user.planSolicitado = undefined;
    user.save().then(function () {
      //TODO Mandar correo para paco y para mi cuando alguien paga y recibe el webhook
      var tipo = "Otro";
      if (charge.method == "bank_account") tipo = "Transferencia bancaria";else if (charge.method == "card") {
        var lastFour = charge.card.card_number.substr(charge.card.card_number.length - 4);
        tipo = "Tarjeta " + charge.card.bank_name + " terminación: " + lastFour;
      }
      enviarRecibo(user.nombre, user.paquete.toUpperCase(), charge.amount, (0, _moment2.default)(user.fechaPago).format("DD/MM/YY hh:mm a"), (0, _moment2.default)(user.fechaCorte).format("DD/MM/YY hh:mm a"), tipo, user.email);
      console.log("Pago recibido y aplicado para -" + user.nombre);
      res.status(200).end();
    });
  }).catch(function (err) {
    console.log(err);
  });
}

function updateUserPlanSub(res, charge) {
  console.log("sub");
  var cid = charge.description.split(" ")[charge.description.split(" ").length - 1];
  console.log("SAD", cid);
  var sub = true;
  var query = {
    'openpayID': cid
  };

  _user2.default.findOne(query).exec().then(function (user) {
    if (!user) {
      console.log("No en esta DB");
      res.status(200).end();
    }
    user.fechaPago = (0, _moment2.default)({});
    // console.log("1");
    getSubscription(user.openpayID, charge.subscription_id, function (plan) {
      if (!plan) {
        res.status(200).end();
        return;
      }
      user.paquete = user.subscription.plan;
      user.fechaCorte = (0, _moment2.default)({}).add(plan.repeat_every, 'M');
      user.subscriptionCounter = user.subscriptionCounter - 1;
      if (user.subscriptionCounter == 0) {
        user.subscriptionCounter = undefined;
        user.subscription = undefined;
        cancelSubscription(user.openpayID, charge.subscription_id, function (err) {
          if (!err) {
            console.log("Error en cancelar", err);
          } else {
            console.log("sin error");
          }
        });
      }
      // console.log("2");
      if (user.datosDeFacturacion.requested == true) {
        console.log('lol matame');
        console.log(charge);
        var dataFactura = {};
        dataFactura.user = user;
        dataFactura.body = user.datosDeFacturacion;
        dataFactura.body.precioMes = user.planSolicitadoPricing.frequentCost;
        if (user.planSolicitadoPricing.periodo == 'year') {
          dataFactura.body.meses = 12;
        } else {
          dataFactura.body.meses = 6;
        }
        dataFactura.body.planSolicitado = user.planSolicitadoPricing.plan;
        dataFactura.body.idOrden = charge.authorization;
        dataFactura.body.method = charge.method;
        if (charge.method == 'card') {
          dataFactura.body.cardType = charge.card.type;
        }
        billing.killme(dataFactura);
      }
      user.planSolicitado = undefined;
      // console.log("3");
      user.save().then(function () {
        // console.log("4");
        //TODO Mandar correo para paco y para mi cuando alguien paga y recibe el webhook
        var tipo = "Otro";
        if (charge.method == "bank_account") tipo = "Transferencia bancaria";else if (charge.method == "card") {
          var lastFour = charge.card.card_number.substr(charge.card.card_number.length - 4);
          tipo = "Tarjeta " + charge.card.bank_name + " terminación: " + lastFour;
        }
        enviarRecibo(user.nombre, user.paquete.toUpperCase(), charge.amount, (0, _moment2.default)(user.fechaPago).format("DD/MM/YY hh:mm a"), (0, _moment2.default)(user.fechaCorte).format("DD/MM/YY hh:mm a"), tipo, user.email);
        console.log("Pago recibido y aplicado para -" + user.nombre);
        res.status(200).end();
      });
    });
  }).catch(function (err) {
    console.log(err);
  });
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function bank(req, res) {
  var timestamp = (0, _moment2.default)({}).format('DDMMYYhhmmssSSS');
  var randomNumber = randomstring.generate({
    length: 5,
    charset: 'numeric'
  });
  var order_id = "PLA-" + timestamp + "-" + randomNumber;
  var bankChargeRequest = {
    'method': 'bank_account',
    'amount': parseInt(req.body.amount),
    'description': 'Pago de PortaliaPlus paquete ' + req.body.plan + ' por un plazo de ' + req.body.period + ' meses realizado el ' + (0, _moment2.default)({}).format('DD/MM/YY'),
    'order_id': order_id,
    'due_date': (0, _moment2.default)({}).add(1, 'w'),
    'customer': {
      'name': req.body.nombre,
      'email': req.body.email
    }
  };
  openpay.charges.create(bankChargeRequest, function (error, charge) {
    if (error) {
      res.status(error.http_code).json(error).end();
    } else {
      _user2.default.findById(req.user._id).exec().then(function (user) {
        charge.pdf = _environment2.default.auth.openpay.URL + '/spei-pdf/' + _environment2.default.auth.openpay.ID + '/' + charge.id;
        user.planSolicitadoPricing.discount = req.body.discount;
        user.planSolicitado = {
          plan: req.body.plan,
          period: req.body.period,
          openPayResponse: charge
        };
        user.save().then(function () {
          res.status(200).json(charge);
        });
      }).catch(function () {
        res.status(422).json(charge);
      });
    }
  });
}

function card(req, res) {
  var timestamp = (0, _moment2.default)({}).format('DDMMYYhhmmssSSS');
  var randomNumber = randomstring.generate({
    length: 5,
    charset: 'numeric'
  });
  var order_id = "PLA-" + timestamp + "-" + randomNumber;
  req.body.paymentData.order_id = order_id;
  return openpay.charges.create(req.body.paymentData, function (error, charge) {
    if (error) {
      res.status(error.http_code).json(error).end();
    } else if (charge) {
      _user2.default.findById(req.user._id).exec().then(function (user) {
        user.planSolicitadoPricing.discount = req.body.discount;
        user.planSolicitado = {
          plan: req.body.plan,
          period: req.body.period,
          openPayResponse: charge
        };
        if (user.datosDeFacturacion.requested == true) {
          console.log('lol matame');
          console.log(charge);
          var dataFactura = {};
          dataFactura.user = user;
          dataFactura.body = user.datosDeFacturacion;
          dataFactura.body.precioMes = user.planSolicitadoPricing.frequentCost;
          if (user.planSolicitadoPricing.periodo == 'year') {
            dataFactura.body.meses = 12;
          } else {
            dataFactura.body.meses = 6;
          }
          dataFactura.body.planSolicitado = user.planSolicitadoPricing.plan;
          dataFactura.body.idOrden = charge.authorization;
          dataFactura.body.method = charge.method;
          if (charge.method == 'card') {
            dataFactura.body.cardType = charge.card.type;
          }
          billing.killme(dataFactura);
        }
        user.save().then(function () {
          res.status(200).json(charge);
        });
      }).catch(function () {
        res.status(422).json(charge);
      });
    }
  });
}

function subscription(req, res) {
  switch (req.body.type) {
    case 1:
      //Semestral
      switch (req.body.payments) {
        case 6:
          //Mensual
          var plan = _environment2.default.plans.Semestral.Mensual.ID;
          var planS = _environment2.default.plans.Semestral.Mensual.plan;
          break;
        case 2:
          //Trimestral
          var plan = _environment2.default.plans.Semestral.Trimestral.ID;
          var planS = _environment2.default.plans.Semestral.Trimestral.plan;
          break;
        default:
          var plan = false;
      }
      break;
    case 2:
      //Anual
      switch (req.body.payments) {
        case 12:
          //Mensual
          var plan = _environment2.default.plans.Anual.Mensual.ID;
          var planS = _environment2.default.plans.Anual.Mensual.plan;
          break;
        case 4:
          //Trimestral
          var plan = _environment2.default.plans.Anual.Trimestral.ID;
          var planS = _environment2.default.plans.Anual.Trimestral.plan;
          break;
        case 2:
          //Semestral
          var plan = _environment2.default.plans.Anual.Semestral.ID;
          var planS = _environment2.default.plans.Anual.Semestral.plan;
          break;
        default:
          var plan = false;
      }
      break;
    default:
      var plan = false;
  }

  if (!plan) {
    res.status(422).end();
    return;
  }

  if (req.user.openpayID == undefined) {
    addCustomer(req, function (error) {
      if (error) {
        res.status(error.http_code).json(error);
      } else {
        createSubscription(planS, plan, req, function (error) {
          if (error) {
            res.status(error.http_code).json(error).end();
          } else {
            res.status(200).end();
          }
        });
      }
    });
  } else {
    createSubscription(planS, plan, req, function (error) {
      if (error) {
        res.status(error.http_code).json(error).end();
      } else {
        res.status(200).end();
      }
    });
  }
};

function webhook(req, res) {
  if (req.body.type === 'verification') {
    console.log(req.body);
    res.status(200).end();
    return;
  }
  if (req.body.type === 'charge.succeeded') {
    // console.log('Line 375:', req.body);
    verifyCharge(req.body.transaction.id, function (x) {
      if (x) {
        if (x.subscription_id) {
          updateUserPlanSub(res, x);
        } else {
          updateUserPlan(res, x);
        }
      } else {
        res.status(200).end();
      }
    });
  } else if (req.body.type === 'charge.failed') {
    console.log(req.body);
    console.log("Fallido");
    //TODO alguien intento pagar y fallo avisar con un correo a soporte para darle seguimeitno
    res.status(200).end();
  } else {
    console.log(req.body);
    console.log("Otro");
    res.status(200).end();
  }
}
//# sourceMappingURL=openpay.controller.js.map
