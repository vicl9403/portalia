/**
 * Candidate model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _candidate = require('./candidate.model');

var _candidate2 = _interopRequireDefault(_candidate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CandidateEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
CandidateEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _candidate2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    CandidateEvents.emit(event + ':' + doc._id, doc);
    CandidateEvents.emit(event, doc);
  };
}

exports.default = CandidateEvents;
//# sourceMappingURL=candidate.events.js.map
