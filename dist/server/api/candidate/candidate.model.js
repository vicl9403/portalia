'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CandidateSchema = new _mongoose2.default.Schema({
  email: String,
  phone: Number
});

exports.default = _mongoose2.default.model('Candidate', CandidateSchema);
//# sourceMappingURL=candidate.model.js.map
