'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSearchKeys = getSearchKeys;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getSearchKeys(req, res) {
  return res.status(200).send({
    'id': _environment2.default.algoliaconfig.idApplication,
    'key': _environment2.default.algoliaconfig.searchOnlyKey
  });
}
//# sourceMappingURL=algolia.controller.js.map
