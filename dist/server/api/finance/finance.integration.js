'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newFinance;

describe('Finance API:', function () {

  describe('GET /api/finances', function () {
    var finances;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/finances').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        finances = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      finances.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/finances', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/finances').send({
        name: 'New Finance',
        info: 'This is the brand new finance!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newFinance = res.body;
        done();
      });
    });

    it('should respond with the newly created finance', function () {
      newFinance.name.should.equal('New Finance');
      newFinance.info.should.equal('This is the brand new finance!!!');
    });
  });

  describe('GET /api/finances/:id', function () {
    var finance;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/finances/' + newFinance._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        finance = res.body;
        done();
      });
    });

    afterEach(function () {
      finance = {};
    });

    it('should respond with the requested finance', function () {
      finance.name.should.equal('New Finance');
      finance.info.should.equal('This is the brand new finance!!!');
    });
  });

  describe('PUT /api/finances/:id', function () {
    var updatedFinance;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/finances/' + newFinance._id).send({
        name: 'Updated Finance',
        info: 'This is the updated finance!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedFinance = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedFinance = {};
    });

    it('should respond with the updated finance', function () {
      updatedFinance.name.should.equal('Updated Finance');
      updatedFinance.info.should.equal('This is the updated finance!!!');
    });
  });

  describe('DELETE /api/finances/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/finances/' + newFinance._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when finance does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/finances/' + newFinance._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=finance.integration.js.map
