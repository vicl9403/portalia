'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FinanceSchema = new _mongoose2.default.Schema({
  moneda: {
    usdAsk: Number,
    usdBid: Number,
    eurAsk: Number,
    eurBid: Number,
    gbpAsk: Number,
    gbpBid: Number,
    cnyAsk: Number,
    cnyBid: Number
  },
  metal: {
    aceroAsk: Number,
    aceroBid: Number,
    cobreAsk: Number,
    cobreBid: Number,
    plataAsk: Number,
    plataBid: Number,
    oilAsk: Number,
    oilBid: Number
  },
  elote: {
    cornAsk: Number,
    cornBid: Number,
    weatAsk: Number,
    weatBid: Number,
    soyAsk: Number,
    soyBid: Number
  }
});

exports.default = _mongoose2.default.model('Finance', FinanceSchema);
//# sourceMappingURL=finance.model.js.map
