'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var financeCtrlStub = {
  index: 'financeCtrl.index',
  show: 'financeCtrl.show',
  create: 'financeCtrl.create',
  update: 'financeCtrl.update',
  destroy: 'financeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var financeIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './finance.controller': financeCtrlStub
});

describe('Finance API Router:', function () {

  it('should return an express router instance', function () {
    financeIndex.should.equal(routerStub);
  });

  describe('GET /api/finances', function () {

    it('should route to finance.controller.index', function () {
      routerStub.get.withArgs('/', 'financeCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/finances/:id', function () {

    it('should route to finance.controller.show', function () {
      routerStub.get.withArgs('/:id', 'financeCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/finances', function () {

    it('should route to finance.controller.create', function () {
      routerStub.post.withArgs('/', 'financeCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/finances/:id', function () {

    it('should route to finance.controller.update', function () {
      routerStub.put.withArgs('/:id', 'financeCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/finances/:id', function () {

    it('should route to finance.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'financeCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/finances/:id', function () {

    it('should route to finance.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'financeCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
