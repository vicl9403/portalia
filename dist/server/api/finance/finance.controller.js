/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/finances              ->  index
 * POST    /api/finances              ->  create
 * GET     /api/finances/:id          ->  show
 * PUT     /api/finances/:id          ->  update
 * DELETE  /api/finances/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _finance = require('./finance.model');

var _finance2 = _interopRequireDefault(_finance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var schedule = require('node-schedule');
var request = require('request');
var bodyMonedas;
var bodyMetales;
var bodyElotes;

var j = schedule.scheduleJob('0 */4 * * *', function () {
  console.log('The answer to life, the universe, and everything!'); //K
  request('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDMXN%22,%22EURMXN%22,%22GBPMXN%22,%22CNYMXN%22)&env=store://datatables.org/alltableswithkeys&format=json', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      // console.log(body);
      bodyMonedas = body;
      request('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22X%22,%22CPER%22,%22SLV%22,%22OIL%22)&env=store://datatables.org/alltableswithkeys&format=json', function (error, response, body) {
        if (!error && response.statusCode == 200) {
          // console.log(body);
          bodyMetales = body;
          // updateDBMonedas(body)
          request('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22CORN%22,%22WEAT%22,%22SOYB%22)&env=store://datatables.org/alltableswithkeys&format=json', function (error, response, body) {
            if (!error && response.statusCode == 200) {
              // console.log(body);
              bodyElotes = body;
              updateDBMonedas(bodyMonedas, bodyMetales, bodyElotes);
            }
          });
        }
      });
    }
  });
});

function updateDBMonedas(datosMoneda, datosMetal, datosElote) {
  var req = {};
  req.body = {};
  req.body.moneda = {};
  req.body.metal = {};
  req.body.elote = {};
  var resultsMonedaDos = JSON.parse(datosMoneda);
  var resultsMoneda = resultsMonedaDos.query.results.rate;
  req.body.moneda.usdAsk = resultsMoneda[0].Ask;
  req.body.moneda.usdBid = resultsMoneda[0].Bid;
  req.body.moneda.eurAsk = resultsMoneda[1].Ask;
  req.body.moneda.eurBid = resultsMoneda[1].Bid;
  req.body.moneda.gbpAsk = resultsMoneda[2].Ask;
  req.body.moneda.gbpBid = resultsMoneda[2].Bid;
  req.body.moneda.cnyAsk = resultsMoneda[3].Ask;
  req.body.moneda.cnyBid = resultsMoneda[3].Bid;
  var resultsMetalDos = JSON.parse(datosMetal);
  var resultsMetal = resultsMetalDos.query.results.quote;
  req.body.metal.aceroAsk = resultsMetal[0].Ask;
  req.body.metal.aceroBid = resultsMetal[0].Bid;
  req.body.metal.cobreAsk = resultsMetal[1].Ask;
  req.body.metal.cobreBid = resultsMetal[1].Bid;
  req.body.metal.plataAsk = resultsMetal[2].Ask;
  req.body.metal.plataBid = resultsMetal[2].Bid;
  req.body.metal.oilAsk = resultsMetal[3].Ask;
  req.body.metal.oilBid = resultsMetal[3].Bid;
  var resultsEloteDos = JSON.parse(datosElote);
  var resultsElote = resultsEloteDos.query.results.quote;
  req.body.elote.cornAsk = resultsElote[0].Ask;
  req.body.elote.cornBid = resultsElote[0].Bid;
  req.body.elote.weatAsk = resultsElote[1].Ask;
  req.body.elote.weatBid = resultsElote[1].Bid;
  req.body.elote.soyAsk = resultsElote[2].Ask;
  req.body.elote.soyBid = resultsElote[2].Bid;
  return _finance2.default.find({}).remove().then(function () {
    _finance2.default.create(req.body);
  });
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Finances
function index(req, res) {
  return _finance2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Finance from the DB
function show(req, res) {
  return _finance2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Finance in the DB
function create(req, res) {
  return _finance2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Finance in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _finance2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Finance from the DB
function destroy(req, res) {
  return _finance2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=finance.controller.js.map
