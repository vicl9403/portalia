/**
 * Finance model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _finance = require('./finance.model');

var _finance2 = _interopRequireDefault(_finance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FinanceEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
FinanceEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _finance2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    FinanceEvents.emit(event + ':' + doc._id, doc);
    FinanceEvents.emit(event, doc);
  };
}

exports.default = FinanceEvents;
//# sourceMappingURL=finance.events.js.map
