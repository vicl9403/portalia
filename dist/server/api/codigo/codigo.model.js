'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CodigoSchema = new _mongoose2.default.Schema({
  codigo: String,
  fechaInicial: Date,
  fechaFinal: Date,
  numeroOfrecido: Number,
  unidadOfrecida: String,
  paquete: String,
  usos: Number,
  usosRestantes: Number,
  email: String,
  redeemers: []
});

exports.default = _mongoose2.default.model('Codigo', CodigoSchema);
//# sourceMappingURL=codigo.model.js.map
