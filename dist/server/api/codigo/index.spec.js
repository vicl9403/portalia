'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var codigoCtrlStub = {
  index: 'codigoCtrl.index',
  show: 'codigoCtrl.show',
  create: 'codigoCtrl.create',
  update: 'codigoCtrl.update',
  destroy: 'codigoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var codigoIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './codigo.controller': codigoCtrlStub
});

describe('Codigo API Router:', function () {

  it('should return an express router instance', function () {
    codigoIndex.should.equal(routerStub);
  });

  describe('GET /api/codigos', function () {

    it('should route to codigo.controller.index', function () {
      routerStub.get.withArgs('/', 'codigoCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/codigos/:id', function () {

    it('should route to codigo.controller.show', function () {
      routerStub.get.withArgs('/:id', 'codigoCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/codigos', function () {

    it('should route to codigo.controller.create', function () {
      routerStub.post.withArgs('/', 'codigoCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/codigos/:id', function () {

    it('should route to codigo.controller.update', function () {
      routerStub.put.withArgs('/:id', 'codigoCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/codigos/:id', function () {

    it('should route to codigo.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'codigoCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/codigos/:id', function () {

    it('should route to codigo.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'codigoCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
