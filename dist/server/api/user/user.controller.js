'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendQuotesMail = sendQuotesMail;
exports.generateWPUser = generateWPUser;
exports.index = index;
exports.getUserListByIds = getUserListByIds;
exports.indexAdminMeses = indexAdminMeses;
exports.activarPromo = activarPromo;
exports.moverImpo = moverImpo;
exports.getVisitors = getVisitors;
exports.create = create;
exports.show = show;
exports.destroy = destroy;
exports.changePassword = changePassword;
exports.me = me;
exports.countSectors = countSectors;
exports.authCallback = authCallback;
exports.update = update;
exports.reset = reset;
exports.activar = activar;
exports.busquedaCategoria = busquedaCategoria;
exports.countSubs = countSubs;
exports.getSectors = getSectors;
exports.busquedaAnuncios = busquedaAnuncios;
exports.hacksote = hacksote;
exports.usuariosDestacados = usuariosDestacados;
exports.corridaFoto = corridaFoto;
exports.corridaFotoSecundaria = corridaFotoSecundaria;
exports.busquedaAnunciosPremium = busquedaAnunciosPremium;
exports.busquedaGeografica = busquedaGeografica;
exports.busquedaTags = busquedaTags;
exports.searchToUpdate = searchToUpdate;
exports.busquedaNombre = busquedaNombre;
exports.busquedaTagsCategory = busquedaTagsCategory;
exports.check = check;
exports.borrarfoto = borrarfoto;
exports.borrarVideo = borrarVideo;
exports.calcularCodigos = calcularCodigos;
exports.moveToMateriales = moveToMateriales;
exports.moveToSuministros = moveToSuministros;
exports.busquedaAvanzada = busquedaAvanzada;
exports.pruebaChat = pruebaChat;
exports.newSearchSystem = newSearchSystem;
exports.queryMailing = queryMailing;
exports.testingCount = testingCount;
exports.csvGenerator = csvGenerator;
exports.aggregateCategories = aggregateCategories;
exports.aggregatePreCategories = aggregatePreCategories;

var _user = require('./user.model');

var _user2 = _interopRequireDefault(_user);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _sector = require('../sector/sector.model');

var _sector2 = _interopRequireDefault(_sector);

var _quotation = require('../quotation/quotation.model');

var _quotation2 = _interopRequireDefault(_quotation);

var _busqueda = require('../busqueda/busqueda.model');

var _busqueda2 = _interopRequireDefault(_busqueda);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _passwordRecovery = require('../passwordRecovery/passwordRecovery.model');

var _passwordRecovery2 = _interopRequireDefault(_passwordRecovery);

var _mailConfirmation = require('../mailConfirmation/mailConfirmation.model');

var _mailConfirmation2 = _interopRequireDefault(_mailConfirmation);

var _algolia = require('./algolia');

var algoliaSearch = _interopRequireWildcard(_algolia);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import estado from '../estado/estado.model';
var json2csv = require('json2csv');
var client = new Mailin("https://api.sendinblue.com/v2.0", "wFKdNjXJx3ZYUL8M");
var fs = require('fs');
var mv = require('mv');
var mkdirp = require('mkdirp');
var Finder = require('fs-finder');
var request = require('request');
var schedule = require('node-schedule');
var util = require('util');
var http = require('http');
var querystring = require('querystring');

var _jade = require('jade');
var P = require('bluebird');
var pagination = require('mongoose-pagination');
var asyncLoop = require('node-async-loop');

var transporter = _nodemailer2.default.createTransport({
  host: 'server.4mean.mx',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'hola@portaliaplus.com',
    pass: 'Qwerty123$%'
  },
  tls: {
    rejectUnauthorized: false
  }
});

var j = schedule.scheduleJob('0 0 * * *', function () {
  //Esta es una función Cron que se ejecuta diariamente a las 00:00am para verificar la fecha de corte de los usuarios select y/o premium
  console.log('Verificación de paquetes para los ususarios');
  console.log('Verificación de has photo');
  corridaFoto();
  corridaVisitas();
  var d = new Date(Date.now()).toISOString(); //Se genera la fecha actual
  var ds = d.toString();
  _user2.default.find({
    $and: [{
      "fechaCorte": {
        $lt: new Date(ds) //Este query trae a todos los usuarios cuya fecha de corte sea anterior a la actual (o sea, expirada)
      }
    }]
  }, '-salt -password').exec().then(function (users) {
    console.log(users);
    for (var i = 0; i < users.length; i++) {
      //En este for se le baja su paquete a gratis, se le borra le fecha de corte, y se le eliminan las subcategorías que tenía
      users[i].paquete = 'gratis';
      users[i].fechaCorte = undefined;
      users[i].subcategories = [];
      users[i].save();
      console.log(users);
    }
  });
});

// No ejecutar a menos que se quiera generar una
// indexación de una cantidad importante de usuarios
//algoliaSearch.createIndexFromAllUsers();

var setContactsPerDayToZero = schedule.scheduleJob('0 0 * * *', function () {
  _user2.default.update({}, { $set: { contactsPerDay: 0 } }, { "multi": true }, function (err) {
    console.log(err);
  });
});

var mailMensual = schedule.scheduleJob('0 11 1 * *', function () {
  console.log('Mailing mensual de balance');
  mailingBalnce();
});

var mailMensual = schedule.scheduleJob('0 0 2 * *', function () {
  console.log('Mailing mensual de cotizaciones'); //K
  cotizacionesBalance();
});

function sendQuotesMail(req, res) {
  cotizacionesBalance();
  res.status(200).send('ok');
}
function generateWPUser(req, res) {
  request.get('https://marketing-digital.portaliaplus.com/hacksote.php?username=' + req.body.nombre + '&password=12345&email=' + req.body.email + '&securityhash=da39a3ee5e6b4b0d3255bfef95601890afd80709').on('response', function (response) {
    var dataSend = {
      "id": 56,
      "to": req.body.email,
      "attr": {
        "PLAN": 'Select',
        "USERNAME": req.body.nombre,
        "PASSWORD": '12345'
      },
      "headers": {
        "Content-Type": "text/html;charset=iso-8859-1",
        "X-param1": "value1",
        "X-param2": "value2",
        "X-Mailin-custom": "bienvenido al Webmarketing",
        "X-Mailin-tag": "bienvenido al Webmarketing"
      }
    };
    client.send_transactional_template(dataSend).on('complete', function (lol) {
      var dataJSON = JSON.parse(lol);
      console.log("Response: " + req.body.email + ' ');
      console.log(lol);
      res.status(200).json('éxito');
    });
  });
}

var updateSubsectoresForAdmin = schedule.scheduleJob('0 0 * * *', function () {
  contarSubsectores();
  console.log('Updating registers by category');
});

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function (err) {
    res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      // Actualizar el índice de Algolia
      algoliaSearch.setUserData(entity._id);

      res.status(statusCode).json(entity);
    }
  };
}

/**
 * Get list of users
 * restriction: 'admin'
 */
function index(req, res) {
  return _user2.default.find({}, '-salt -password').exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function getUserListByIds(req, res) {

  var ids = req.body.data;
  var objects = [];
  for (var i = 0; i < ids.length; i++) {
    objects.push({
      "_id": ids[i]
    });
  }

  return _user2.default.find({
    $or: objects
  }, '-salt -passwormud').sort({ 'noPaquete': -1, 'hasPic': 1 }).exec().then(function (users) {
    return res.status(200).json(users);
  });
}

function indexAdminMeses(req, res) {
  var idsGrat = [];
  var monthsGrat = [];
  var idsSel = [];
  var monthsSel = [];
  var idsPlus = [];
  var monthsPlus = [];
  var toReturn = {
    Gratuito: {
      Enero: 0,
      Febrero: 0,
      Marzo: 0,
      Abril: 0,
      Mayo: 0,
      Junio: 0,
      Julio: 0,
      Agosto: 0,
      Septiembre: 0,
      Octubre: 0,
      Noviembre: 0,
      Diciembre: 0
    },
    SelectP: {
      Enero: 0,
      Febrero: 0,
      Marzo: 0,
      Abril: 0,
      Mayo: 0,
      Junio: 0,
      Julio: 0,
      Agosto: 0,
      Septiembre: 0,
      Octubre: 0,
      Noviembre: 0,
      Diciembre: 0
    },
    Plus: {
      Enero: 0,
      Febrero: 0,
      Marzo: 0,
      Abril: 0,
      Mayo: 0,
      Junio: 0,
      Julio: 0,
      Agosto: 0,
      Septiembre: 0,
      Octubre: 0,
      Noviembre: 0,
      Diciembre: 0
    }
  };

  return _user2.default.find({
    role: 'empresa'
  }, '-salt -password').exec().then(function (users) {
    var usersReturn = users;

    for (var x = 0; x < usersReturn.length; x++) {
      if (usersReturn[x].paquete == 'plus') {
        idsPlus.push(usersReturn[x]._id.getTimestamp());
      } else if (usersReturn[x].paquete == 'gratis') {
        idsGrat.push(usersReturn[x]._id.getTimestamp());
      } else if (usersReturn[x].paquete == 'select') {
        idsSel.push(usersReturn[x]._id.getTimestamp());
      }
    }

    for (var plus = 0; plus < idsPlus.length; plus++) {
      monthsPlus[plus] = idsPlus[plus].getMonth();
      if (monthsPlus[plus] == 0) {
        toReturn.Plus.Enero += 1;
      } else if (monthsPlus[plus] == 1) {
        toReturn.Plus.Febrero += 1;
      } else if (monthsPlus[plus] == 2) {
        toReturn.Plus.Marzo += 1;
      } else if (monthsPlus[plus] == 3) {
        toReturn.Plus.Abril += 1;
      } else if (monthsPlus[plus] == 4) {
        toReturn.Plus.Mayo += 1;
      } else if (monthsPlus[plus] == 5) {
        toReturn.Plus.Junio += 1;
      } else if (monthsPlus[plus] == 6) {
        toReturn.Plus.Julio += 1;
      } else if (monthsPlus[plus] == 7) {
        toReturn.Plus.Agosto += 1;
      } else if (monthsPlus[plus] == 8) {
        toReturn.Plus.Septiembre += 1;
      } else if (monthsPlus[plus] == 9) {
        toReturn.Plus.Octubre += 1;
      } else if (monthsPlus[plus] == 10) {
        toReturn.Plus.Noviembre += 1;
      } else if (monthsPlus[plus] == 11) {
        toReturn.Plus.Diciembre += 1;
      }
    }

    for (var gratis = 0; gratis < idsGrat.length; gratis++) {
      monthsGrat[gratis] = idsGrat[gratis].getMonth();
      if (monthsGrat[gratis] == 0) {
        toReturn.Gratuito.Enero += 1;
      } else if (monthsGrat[gratis] == 1) {
        toReturn.Gratuito.Febrero += 1;
      } else if (monthsGrat[gratis] == 2) {
        toReturn.Gratuito.Marzo += 1;
      } else if (monthsGrat[gratis] == 3) {
        toReturn.Gratuito.Abril += 1;
      } else if (monthsGrat[gratis] == 4) {
        toReturn.Gratuito.Mayo += 1;
      } else if (monthsGrat[gratis] == 5) {
        toReturn.Gratuito.Junio += 1;
      } else if (monthsGrat[gratis] == 6) {
        toReturn.Gratuito.Julio += 1;
      } else if (monthsGrat[gratis] == 7) {
        toReturn.Gratuito.Agosto += 1;
      } else if (monthsGrat[gratis] == 8) {
        toReturn.Gratuito.Septiembre += 1;
      } else if (monthsGrat[gratis] == 9) {
        toReturn.Gratuito.Octubre += 1;
      } else if (monthsGrat[gratis] == 10) {
        toReturn.Gratuito.Noviembre += 1;
      } else if (monthsGrat[gratis] == 11) {
        toReturn.Gratuito.Diciembre += 1;
      }
    }

    for (var select = 0; select < idsSel.length; select++) {
      monthsSel[select] = idsSel[select].getMonth();
      if (monthsSel[select] == 0) {
        toReturn.SelectP.Enero += 1;
      } else if (monthsSel[select] == 1) {
        toReturn.SelectP.Febrero += 1;
      } else if (monthsSel[select] == 2) {
        toReturn.SelectP.Marzo += 1;
      } else if (monthsSel[select] == 3) {
        toReturn.SelectP.Abril += 1;
      } else if (monthsSel[select] == 4) {
        toReturn.SelectP.Mayo += 1;
      } else if (monthsSel[select] == 5) {
        toReturn.SelectP.Junio += 1;
      } else if (monthsSel[select] == 6) {
        toReturn.SelectP.Julio += 1;
      } else if (monthsSel[select] == 7) {
        toReturn.SelectP.Agosto += 1;
      } else if (monthsSel[select] == 8) {
        toReturn.SelectP.Septiembre += 1;
      } else if (monthsSel[select] == 9) {
        toReturn.SelectP.Octubre += 1;
      } else if (monthsSel[select] == 10) {
        toReturn.SelectP.Noviembre += 1;
      } else if (monthsSel[select] == 11) {
        toReturn.SelectP.Diciembre += 1;
      }
    }

    res.status(200).json(toReturn);
  }).catch(handleError(res));
}

function activarPromo(req, res) {
  return _user2.default.find({
    $and: [{
      'paquete': "gratis"
    }, {
      'role': {
        $ne: 'visitante'
      }
    }]
  }, '-salt -password').exec().then(function (users) {
    console.log(users.length);
    for (var i = 0; i < users.length; i++) {
      console.log("----->" + i);
      users[i].paquete = "select";
      users[i].fechaPago = (0, _moment2.default)();
      users[i].fechaCorte = (0, _moment2.default)().add(1, 'M');
      users[i].save();
    }
    res.status(200).json(users);
  }).catch(handleError(res));
}

function moverImpo(req, res) {
  return _user2.default.find({
    $and: [{
      'categoria_name': "Importación y exportación"
    }, {
      'role': {
        $ne: 'visitante'
      }
    }]
  }, '-salt -password').exec().then(function (users) {
    console.log(users.length);
    for (var i = 0; i < users.length; i++) {
      console.log("----->" + i);
      users[i].sector_name = "Logistica y transportación";
      users[i].categoria = '3';
      users[i].sector = '583f708d28ea2432005c0cf9';
      users[i].save();
    }
    res.status(200).json(users);
  }).catch(handleError(res));
}

function getVisitors(req, res) {
  return _user2.default.find({
    $and: [{
      'role': "visitante"
    }]
  }, '-salt -password').exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

/**
 * Creates a new user
 */
function create(req, res, next) {
  var newUser = new _user2.default(req.body);
  newUser.provider = 'local';

  if ((0, _moment2.default)().isBefore('2016-09-10') && newUser.role == "empresa") {
    newUser.paquete = "select";
    newUser.fechaPago = (0, _moment2.default)();
    newUser.fechaCorte = (0, _moment2.default)().add(1, 'M');
  }

  if (newUser.role == "empresa" && newUser.codigo_invitacion != "") {
    console.log('Enviando el correo');
    console.log(newUser.codigo_invitacion);

    var template = './server/views/aviso_registro_codigo.jade';

    // get template from file system
    fs.readFile(template, 'utf8', function (err, file) {
      if (err) {
        //handle errors
        console.log(err);
        return res.send('ERROR!');
      } else {
        //compile jade template into function
        var compiledTmpl = _jade.compile(file, {
          filename: template
        });
        // set context to be used in template
        var context = {};
        // get html back as a string with the context applied;
        var html = compiledTmpl(context);

        _user2.default.findOne({
          '_id': newUser.codigo_invitacion
        }, '-salt -password').exec().then(function (user) {
          if (user.email) {
            return transporter.sendMail({
              from: 'hola@portaliaplus.com', // sender address
              to: user.email, // list of receivers
              subject: "Felicidades tu amigo aceptó unirse a Portaliaplus", // Subject line
              html: html // html body
            }, function (error, info) {
              if (error) {
                console.log(error);
              } else {
                console.log('Message sent: ' + info.response);
              }
            });
          }
        });
      }
    });
  }

  newUser.save().then(function (user) {

    // Agregar el nuevo usuario a la indexación de Algolia
    algoliaSearch.setUserData(user._id);

    var token = _jsonwebtoken2.default.sign({
      _id: user._id
    }, _environment2.default.secrets.session, {
      expiresIn: 60 * 60 * 5
    });
    res.json({
      token: token
    });
  });
}

/**
 * Get a single user
 */
function show(req, res, next) {
  var userId = req.params.id;

  return _user2.default.findById(userId, '-salt -password').exec().then(function (user) {
    if (!user) {
      return res.status(404).end();
    }
    res.json(user);
  }).catch(function (err) {
    return next(err);
  });
}

//BALANCE MENSUAL ----------------------------------------------------------------------------------------------------
function mailingBalnce() {
  console.log("....");
  _user2.default.find({ "visitasGene.4": { "$exists": true } }, '-salt -password').exec().then(function (users) {
    console.log(users);
    mailingHelper(users);
  });
}
var contadorMailing = 0;

function mailingHelper(usuarios) {
  // console.log("contadorMailing: ", contadorMailing);
  // console.log("usuario ", usuarios.length)
  _user2.default.find({
    _id: {
      $in: usuarios[contadorMailing].visitasGene
    }
  }, '-salt -password').exec().then(function (user) {
    console.log("Nuevo usuario");
    var visitantes = [];
    var numeroAnonimos = 0;
    for (var x = 0; x < user.length; x++) {
      visitantes.push(user[x].nombre);
    }
    for (var y = 0; y < usuarios[contadorMailing].visitasGene.length; y++) {
      if (usuarios[contadorMailing].visitasGene[y] == undefined) {
        numeroAnonimos++;
      }
    }
    visitantes.push(numeroAnonimos);

    var data = {
      "id": 52,
      "to": usuarios[contadorMailing].email,
      "from": ["cotizaciones@portaliaplus.mx", "Cotizaciones"],
      "attr": {
        "VISITAUNO": visitantes[0] || 'Anónimo',
        "VISITADOS": visitantes[1] || 'Anónimo',
        "VISITATRES": visitantes[2] || 'Anónimo',
        "VISITACUATRO": visitantes[3] || 'Anónimo',
        "VISITACINCO": visitantes[4] || 'Anónimo',
        "BUSQUEDAUNO": usuarios[contadorMailing].busquedas[0] || ' ',
        "BUSQUEDADOS": usuarios[contadorMailing].busquedas[1] || ' ',
        "BUSQUEDATRES": usuarios[contadorMailing].busquedas[2] || ' '
      },
      "headers": {
        "Content-Type": "text/html;charset=iso-8859-1",
        "X-param1": "value1",
        "X-param2": "value2",
        "X-Mailin-custom": "balance mensual",
        "X-Mailin-tag": "balance mensual"
      }
    };

    client.send_transactional_template(data).on('complete', function (data) {
      var dataJSON = JSON.parse(data);
      console.log(dataJSON);
      // usuarios[contadorMailing].visitasGene = [];
      usuarios[contadorMailing].busquedas = [];
      // usuarios[contadorMailing].numeroVisitasGene = 0;
      usuarios[contadorMailing].save();
      if (contadorMailing != usuarios.length - 1) {
        contadorMailing++;
        mailingHelper(usuarios);
      }
    });
  });
}

//BALANCE MENSUAL ----------------------------------------------------------------------------------------------------

//BALANCE DE OPORTUNIDADES--------------------------------------------------------------------------------------------
// cotizacionesBalance();
function cotizacionesBalance() {
  _quotation2.default.find({
    $and: [{
      'status': 'approved'
      // {
      //   'user':
      //   {
      //     $size: 0
      //   }
      // }
    }]
  }).exec().then(function (quotes) {

    console.log(quotes);
    // Arreglo que contendrá todos los usuarios que
    // estén definidos como prospectos de una cotización
    var users = {};

    // Iterar sobre todas las cotizaciones

    var _loop = function _loop(i) {

      // Obtener los prospectos
      var prospects = quotes[i].prospects;

      // Iterar sobre todos los prospectos
      for (var _c = 0; _c < prospects.length; _c++) {

        // Esperar a que la cotización esté aprobada para el usuario
        if (prospects[_c].status == 'approved') {

          // Verificar que esté definido el prospecto y si lo está inicializarlo
          if (users[prospects[_c].user] == undefined) {
            users[prospects[_c].user] = [];
          }

          // Marcar como enviado al prospecto en cuestión
          prospects[_c].wasSend = true;

          // Hacer el push con el objeto necesario
          users[prospects[_c].user].push({
            empresa: prospects[_c].name,
            producto: quotes[i].product,
            fecha: quotes[i].createdAt,
            email: prospects[_c].email
          });
        }
      }

      // Guardar los prospectos de la cotización a los que se les envió
      _quotation2.default.findById(quotes[i]._id).exec().then(function (entity) {
        entity.prospects = prospects;
        entity.save();
      });
    };

    for (var i = 0; i < quotes.length; i++) {
      _loop(i);
    }

    // Setear el lenguaje de las fechas a español
    _moment2.default.locale('es');

    // Iterar sobre todos los usuarios
    for (var c in users) {

      var oportunidades = '';

      // Iterar sobre todas las oportunidades del usuario
      for (var i = 0; i < users[c].length; i++) {
        // Generar la inserción de variable para tener todos los productos con salto de línea
        oportunidades += '-' + (i + 1) + ' ' + users[c][i].producto + ' ' + (0, _moment2.default)(users[c][i].fecha).format('D MMMM YYYY') + '<br><br>';
      }

      var data = {
        "id": 70,
        "to": users[c][0].email,
        // "to": 'lopez.victor94@gmail.com',
        "from": "cotizaciones@portaliaplus.mx",
        "attr": {
          "MES": (0, _moment2.default)().format('MMMM'),
          "EMPRESA": users[c][0].empresa,
          "OPORTUNIDAD": oportunidades,
          "NUMEROOPORTUNIDADES": users[c].length,
          "FECHA": (0, _moment2.default)(users[c][0].fecha).format('D MMMM YYYY')
        },
        "headers": {
          "Content-Type": "text/html;charset=iso-8859-1",
          "X-param1": "value1",
          "X-param2": "value2",
          "X-Mailin-custom": "Oportunidades de negocio",
          "X-Mailin-tag": "Oportunidades de negocio"
        }
      };

      client.send_transactional_template(data).on('complete', function (data) {
        var dataJSON = JSON.parse(data);
        // console.log( dataJSON );
      });
    };
  });
}

//BALANCE DE OPORTUNIDADES--------------------------------------------------------------------------------------------

/**
 * Deletes a user
 * restriction: 'admin'
 */
function destroy(req, res) {
  return _user2.default.findByIdAndRemove(req.params.id).exec().then(function () {
    res.status(204).end();
  }).catch(handleError(res));
}

/**
 * Change a users password
 */
function changePassword(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return _user2.default.findById(userId).exec().then(function (user) {
    if (user.authenticate(oldPass)) {
      user.password = newPass;
      return user.save().then(function () {
        res.status(204).end();
      }).catch(validationError(res));
    } else {
      return res.status(403).end();
    }
  });
}

/**
 * Get my info
 */
function me(req, res, next) {
  var userId = req.user._id;

  return _user2.default.findOne({
    _id: userId
  }, '-salt -password').exec().then(function (user) {
    // don't ever give out the password or salt
    if (!user) {
      return res.status(401).end();
    }
    res.json(user);
  }).catch(function (err) {
    return next(err);
  });
}

function countSectors(req, res, next) {
  var sector = req.params.sector;

  return _user2.default.count({
    $and: [{
      sector_name: sector
    }, {
      'role': 'empresa'
    }]
  }).exec().then(function (numSectors) {
    // don't ever give out the password or salt
    res.json(numSectors);
  }).catch(function (err) {
    return next(err);
  });
}

/**
 * Authentication callback
 */
function authCallback(req, res, next) {
  res.redirect('/');
}

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _user2.default.findByIdAndUpdate(req.params.id, req.body).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

function reset(req, res) {
  _passwordRecovery2.default.findOne({
    urlKey: req.params.key
  }, function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log(result);
      _user2.default.findOne({
        email: result.email
      }).then(function (user) {
        user.password = req.body.password;
        result.remove();
        return user.save().then(function () {
          res.status(204).end();
        }).catch(validationError(res));
      });
    }
  });
} // TODO: Hacer que expire! Maldita vicky

function activar(req, res) {
  _mailConfirmation2.default.findOne({
    urlKey: req.params.key
  }, function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log(result);
      _user2.default.findOne({
        email: result.email
      }).then(function (user) {
        console.log(req.body);
        user.active = req.body.active;
        result.remove();
        return user.save().then(function () {
          res.status(204).end();
        }).catch(validationError(res));
      });
    }
  });
} // TODO: Hacer que expire! Maldita vicky

function busquedaCategoria(req, res) {
  console.log(req);
  return _user2.default.find({
    $and: [{
      'sector': req.body.sector._id
    }, {
      'categoria': req.body.index
    }]
  }, '_id nombre telefono web email direccion').exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function countSubs(req, res) {
  var toReturn = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: [],
    9: [],
    10: []
  };

  return _user2.default.find({
    $and: [{
      'sector': req.body._id
    }]
  }).exec().then(function (users) {
    for (var x = 0; x < req.body.subsectores.length; x++) {
      for (var y = 0; y < users.length; y++) {
        if (users[y].categoria == x) {
          toReturn[x].push(users[y].nombre);
        }
      }
    }
    res.status(200).json(toReturn);
  }).catch(function (err) {
    console.log(err);
    res.status(500).end();
  });
}

function getSectors(req, res) {
  var toReturn = {
    industriasManufactureras: {
      usuario: [],
      ene: 0,
      feb: 0,
      mar: 0,
      abr: 0,
      may: 0,
      jun: 0,
      jul: 0,
      ago: 0,
      sep: 0,
      oct: 0,
      nov: 0,
      dic: 0
    },
    construccion: {
      usuario: [],
      ene: 0,
      feb: 0,
      mar: 0,
      abr: 0,
      may: 0,
      jun: 0,
      jul: 0,
      ago: 0,
      sep: 0,
      oct: 0,
      nov: 0,
      dic: 0
    },
    electricidadAgua: {
      usuario: [],
      ene: 0,
      feb: 0,
      mar: 0,
      abr: 0,
      may: 0,
      jun: 0,
      jul: 0,
      ago: 0,
      sep: 0,
      oct: 0,
      nov: 0,
      dic: 0
    },
    serviciosEspecializados: {
      usuario: [],
      ene: 0,
      feb: 0,
      mar: 0,
      abr: 0,
      may: 0,
      jun: 0,
      jul: 0,
      ago: 0,
      sep: 0,
      oct: 0,
      nov: 0,
      dic: 0
    },
    turismo: {
      usuario: [],
      ene: 0,
      feb: 0,
      mar: 0,
      abr: 0,
      may: 0,
      jun: 0,
      jul: 0,
      ago: 0,
      sep: 0,
      oct: 0,
      nov: 0,
      dic: 0
    },
    agroindustria: {
      usuario: [],
      ene: 0,
      feb: 0,
      mar: 0,
      abr: 0,
      may: 0,
      jun: 0,
      jul: 0,
      ago: 0,
      sep: 0,
      oct: 0,
      nov: 0,
      dic: 0
    }
  };
  return _user2.default.find({}, '-salt -password').exec().then(function (users) {
    for (var x = 0; x < users.length; x++) {
      if (users[x].sector_name == 'Industrias manufactureras') {
        toReturn.industriasManufactureras.usuario.push(users[x]._id.getTimestamp());
      } else if (users[x].sector_name == 'Construcción') {
        toReturn.construccion.usuario.push(users[x]._id.getTimestamp());
      } else if (users[x].sector_name == 'Electricidad y agua') {
        toReturn.electricidadAgua.usuario.push(users[x]._id.getTimestamp());
      } else if (users[x].sector_name == 'Servicios especializados') {
        toReturn.serviciosEspecializados.usuario.push(users[x]._id.getTimestamp());
      } else if (users[x].sector_name == 'Turismo') {
        toReturn.turismo.usuario.push(users[x]._id.getTimestamp());
      } else if (users[x].sector_name == 'Agroindustria') {
        toReturn.agroindustria.usuario.push(users[x]._id.getTimestamp());
      }
    }

    for (var ind = 0; ind < toReturn.industriasManufactureras.usuario.length; ind++) {
      toReturn.industriasManufactureras.usuario[ind] = toReturn.industriasManufactureras.usuario[ind].getMonth();
      if (toReturn.industriasManufactureras.usuario[ind] == 0) {
        toReturn.industriasManufactureras.ene += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 1) {
        toReturn.industriasManufactureras.feb += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 2) {
        toReturn.industriasManufactureras.mar += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 3) {
        toReturn.industriasManufactureras.abr += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 4) {
        toReturn.industriasManufactureras.may += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 5) {
        toReturn.industriasManufactureras.jun += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 6) {
        toReturn.industriasManufactureras.jul += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 7) {
        toReturn.industriasManufactureras.ago += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 8) {
        toReturn.industriasManufactureras.sep += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 9) {
        toReturn.industriasManufactureras.oct += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 10) {
        toReturn.industriasManufactureras.nov += 1;
      } else if (toReturn.industriasManufactureras.usuario[ind] == 11) {
        toReturn.industriasManufactureras.dic += 1;
      }
    }

    for (var ind = 0; ind < toReturn.construccion.usuario.length; ind++) {
      toReturn.construccion.usuario[ind] = toReturn.construccion.usuario[ind].getMonth();
      if (toReturn.construccion.usuario[ind] == 0) {
        toReturn.construccion.ene += 1;
      } else if (toReturn.construccion.usuario[ind] == 1) {
        toReturn.construccion.feb += 1;
      } else if (toReturn.construccion.usuario[ind] == 2) {
        toReturn.construccion.mar += 1;
      } else if (toReturn.construccion.usuario[ind] == 3) {
        toReturn.construccion.abr += 1;
      } else if (toReturn.construccion.usuario[ind] == 4) {
        toReturn.construccion.may += 1;
      } else if (toReturn.construccion.usuario[ind] == 5) {
        toReturn.construccion.jun += 1;
      } else if (toReturn.construccion.usuario[ind] == 6) {
        toReturn.construccion.jul += 1;
      } else if (toReturn.construccion.usuario[ind] == 7) {
        toReturn.construccion.ago += 1;
      } else if (toReturn.construccion.usuario[ind] == 8) {
        toReturn.construccion.sep += 1;
      } else if (toReturn.construccion.usuario[ind] == 9) {
        toReturn.construccion.oct += 1;
      } else if (toReturn.construccion.usuario[ind] == 10) {
        toReturn.construccion.nov += 1;
      } else if (toReturn.construccion.usuario[ind] == 11) {
        toReturn.construccion.dic += 1;
      }
    }

    for (var ind = 0; ind < toReturn.electricidadAgua.usuario.length; ind++) {
      toReturn.electricidadAgua.usuario[ind] = toReturn.electricidadAgua.usuario[ind].getMonth();
      if (toReturn.electricidadAgua.usuario[ind] == 0) {
        toReturn.electricidadAgua.ene += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 1) {
        toReturn.electricidadAgua.feb += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 2) {
        toReturn.electricidadAgua.mar += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 3) {
        toReturn.electricidadAgua.abr += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 4) {
        toReturn.electricidadAgua.may += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 5) {
        toReturn.electricidadAgua.jun += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 6) {
        toReturn.electricidadAgua.jul += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 7) {
        toReturn.electricidadAgua.ago += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 8) {
        toReturn.electricidadAgua.sep += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 9) {
        toReturn.electricidadAgua.oct += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 10) {
        toReturn.electricidadAgua.nov += 1;
      } else if (toReturn.electricidadAgua.usuario[ind] == 11) {
        toReturn.electricidadAgua.dic += 1;
      }
    }

    for (var ind = 0; ind < toReturn.serviciosEspecializados.usuario.length; ind++) {
      toReturn.serviciosEspecializados.usuario[ind] = toReturn.serviciosEspecializados.usuario[ind].getMonth();
      if (toReturn.serviciosEspecializados.usuario[ind] == 0) {
        toReturn.serviciosEspecializados.ene += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 1) {
        toReturn.serviciosEspecializados.feb += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 2) {
        toReturn.serviciosEspecializados.mar += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 3) {
        toReturn.serviciosEspecializados.abr += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 4) {
        toReturn.serviciosEspecializados.may += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 5) {
        toReturn.serviciosEspecializados.jun += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 6) {
        toReturn.serviciosEspecializados.jul += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 7) {
        toReturn.serviciosEspecializados.ago += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 8) {
        toReturn.serviciosEspecializados.sep += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 9) {
        toReturn.serviciosEspecializados.oct += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 10) {
        toReturn.serviciosEspecializados.nov += 1;
      } else if (toReturn.serviciosEspecializados.usuario[ind] == 11) {
        toReturn.serviciosEspecializados.dic += 1;
      }
    }

    for (var ind = 0; ind < toReturn.turismo.usuario.length; ind++) {
      toReturn.turismo.usuario[ind] = toReturn.turismo.usuario[ind].getMonth();
      if (toReturn.turismo.usuario[ind] == 0) {
        toReturn.turismo.ene += 1;
      } else if (toReturn.turismo.usuario[ind] == 1) {
        toReturn.turismo.feb += 1;
      } else if (toReturn.turismo.usuario[ind] == 2) {
        toReturn.turismo.mar += 1;
      } else if (toReturn.turismo.usuario[ind] == 3) {
        toReturn.turismo.abr += 1;
      } else if (toReturn.turismo.usuario[ind] == 4) {
        toReturn.turismo.may += 1;
      } else if (toReturn.turismo.usuario[ind] == 5) {
        toReturn.turismo.jun += 1;
      } else if (toReturn.turismo.usuario[ind] == 6) {
        toReturn.turismo.jul += 1;
      } else if (toReturn.turismo.usuario[ind] == 7) {
        toReturn.turismo.ago += 1;
      } else if (toReturn.turismo.usuario[ind] == 8) {
        toReturn.turismo.sep += 1;
      } else if (toReturn.turismo.usuario[ind] == 9) {
        toReturn.turismo.oct += 1;
      } else if (toReturn.turismo.usuario[ind] == 10) {
        toReturn.turismo.nov += 1;
      } else if (toReturn.turismo.usuario[ind] == 11) {
        toReturn.turismo.dic += 1;
      }
    }

    for (var ind = 0; ind < toReturn.agroindustria.usuario.length; ind++) {
      toReturn.agroindustria.usuario[ind] = toReturn.agroindustria.usuario[ind].getMonth();
      if (toReturn.agroindustria.usuario[ind] == 0) {
        toReturn.agroindustria.ene += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 1) {
        toReturn.agroindustria.feb += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 2) {
        toReturn.agroindustria.mar += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 3) {
        toReturn.agroindustria.abr += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 4) {
        toReturn.agroindustria.may += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 5) {
        toReturn.agroindustria.jun += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 6) {
        toReturn.agroindustria.jul += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 7) {
        toReturn.agroindustria.ago += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 8) {
        toReturn.agroindustria.sep += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 9) {
        toReturn.agroindustria.oct += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 10) {
        toReturn.agroindustria.nov += 1;
      } else if (toReturn.agroindustria.usuario[ind] == 11) {
        toReturn.agroindustria.dic += 1;
      }
    }

    res.status(200).json(toReturn);
  }).catch(function (err) {
    console.log(err);
    res.status(501).end();
  });
}

function busquedaAnuncios(req, res) {
  console.log(req.body);
  return _user2.default.find({
    $and: [{
      'sector': req.body.sector._id
    }, {
      'categoria': req.body.index
    }, {
      'paquete': {
        $ne: 'plus'
      }
    }]
  }, '_id nombre telefono web email direccion paquete emailSec sector_name categoria_name numeroContactos contactos').sort('-paquete -hasPic _id nombre').skip(req.body.skip).limit(req.body.limit).exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function hacksote(req, res) {
  return _user2.default.find({
    $and: [{
      'role': 'empresa'
    }]
  }, '_id').exec().then(function (users) {
    var appendprron;
    for (var x = 0; x < users.length; x++) {
      var append = '<url>' + '<loc>https://portaliaplus.com/perfil/' + users[x]._id + '</loc>' + '<changefreq>weekly</changefreq>' + '<priority>0.4</priority>' + '</url>';

      appendprron += append;
    }
    fs.appendFile('./client/assets/hacksote.txt', appendprron, function (err) {
      console.log(err);
    });
  }).catch(handleError(res));
}

function usuariosDestacados(req, res) {
  return _user2.default.find({
    role: 'empresa'
  }, '-salt -password').sort('-numeroVisitasGene').limit(12).exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function corridaFoto(req, res) {
  var numberTrue = 0;
  var numberFalse = 0;
  return _user2.default.find({
    hasPic: false
  }).then(function (users) {
    for (var i = 0; i < users.length; i++) {
      //Buscar la foto de perfil en la carpeta del usuario
      var route = './client/assets/uploads/' + users[i].sector_name + '/' + users[i].categoria_name + '/' + users[i]._id + '/' + 'foto-perfil.jpg';
      var flag = true;

      //Validacion 1* que se tenga la foto de perfil
      flag = fs.existsSync(route);
      // console.log("Tiene foto: " + flag);
      if (flag == true) {
        numberTrue++;
      } else {
        numberFalse++;
      }
      users[i].hasPic = flag;
      if (users[i].password == undefined) {
        console.log('password undefined');
        users[i].password = '12345';
      }
      users[i].save().catch(function () {});
    }
    console.log(numberTrue);
    console.log(numberFalse);
    transporter.sendMail({
      from: 'hola@portaliaplus.com', // sender address
      to: 'lguevara@it4pymes.mx', // list of receivers
      subject: "Corrida de fotos ejecutada", // Subject line
      text: 'true: ' + numberTrue + ', false: ' + numberFalse // html body
    }, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Message sent: ' + info.response);
      }
    });
  });
}

function corridaFotoSecundaria(req, res) {
  var numberTrue = 0;
  var numberFalse = 0;
  return _user2.default.findById('582a0462200d0c15086dced0').then(function (user) {
    //Buscar la foto de perfil en la carpeta del usuario
    var route = './client/assets/uploads/' + user.sector_name + '/' + user.categoria_name + '/' + user._id + '/' + 'foto-perfil.jpg';
    var flag = true;

    //Validacion 1* que se tenga la foto de perfil
    flag = fs.existsSync(route);
    // console.log("Tiene foto: " + flag);
    if (flag == true) {
      numberTrue++;
    } else {
      numberFalse++;
    }
    user.hasPic = flag;
    if (user.password == undefined) {
      console.log('password undefined');
      user.password = '12345';
    }
    user.save().catch(function (err) {
      console.log(err);
    });
    transporter.sendMail({
      from: 'hola@portaliaplus.com', // sender address
      to: 'ramiroauditore@hotmail.com', // list of receivers
      subject: "Corrida de fotos ejecutada", // Subject line
      text: 'true: ' + numberTrue + ', false: ' + numberFalse // html body
    }, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Message sent: ' + info.response);
      }
    });
  });
}

function corridaVisitas(req, res) {
  return _user2.default.find({}).then(function (users) {
    for (var i = 0; i < users.length; i++) {
      users[i].numeroVisitasGene = users[i].visitasGene.length;
      users[i].save().catch(function () {});
    }
  });
}

function busquedaAnunciosPremium(req, res) {
  return _user2.default.find({
    $and: [{
      'sector': req.body.sector._id
    }, {
      'categoria': req.body.index
    }, {
      'paquete': 'plus'
    }]
  }, '_id nombre telefono web email direccion emailSec sector_name categoria_name numeroContactos contactos').sort('-paquete _id nombre').skip(req.body.skip).limit(req.body.limit).exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function contarSubsectores() {
  var sectores;
  var x = 0;
  var y = 0;
  return _sector2.default.find({}).exec().then(function (sectors) {
    sectores = sectors;
    contar(sectores, x, y);
  });
}

function contar(sectores, x, y) {
  return _user2.default.find({
    $and: [{
      'sector_name': sectores[x].name
    }, {
      'categoria_name': sectores[x].subsectores[y].name
    }]
  }, '_id nombre telefono web email direccion emailSec sector_name categoria_name numeroContactos contactos').exec().then(function (users) {
    console.log(sectores[x].subsectores[y].name, ": ", users.length);
    if (y < sectores[x].subsectores.length - 1) {
      sectores[x].subsectores[y].registros = users.length;
      y++;
      contar(sectores, x, y);
    } else if (y == sectores[x].subsectores.length - 1 && x < sectores.length) {
      sectores[x].subsectores[y].registros = users.length;
      _sector2.default.findById(sectores[x]._id).exec().then(saveUpdates(sectores[x]));
      x++;
      y = 0;
      contar(sectores, x, y);
    }
  });
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {

      algoliaSearch.setUserData(updated._id);
      return updated;
    });
  };
}

function busquedaGeografica(req, res) {
  var objectbusqueda = [{
    'sucursales': {
      $exists: true,
      $ne: []
    }
  }];

  if (req.body.estado != undefined) {
    objectbusqueda.push({
      $or: [{
        "sucursales.estadoSucursal": req.body.estado_name
      }, {
        'estado': req.body.estado
      }]

    });
  }
  if (req.body.municipio != undefined) {
    objectbusqueda.push({
      "municipio": req.body.municipio
    });
    objectbusqueda.push({
      $or: [{
        "municipio": req.body.municipio
      }, {
        "sucursales.ciudad": req.body.municipio_name
      }]

    });
  }
  if (req.body.sector._id != undefined) {
    objectbusqueda.push({
      "sector": req.body.sector._id
    });
  }
  if (req.body.index != undefined || req.body.index == "") {
    if (req.body.index == "") {} else {
      objectbusqueda.push({
        "categoria": req.body.index
      });
    }
  }
  _user2.default.find({
    $and: objectbusqueda
  }, '_id telefono web email direccion sucursales nombre').exec().then(function (users) {

    var maxDistance = req.body.radio || 5; //Aquí recibiremos la máxima distancia (aka radio), si no está definida se pone a 2km
    maxDistance /= 111.12; //Convertir el radio a radianes, el query se hace en radianes
    var busqueda;
    //Recibir coordinadas [ <longitude> , <latitude> ]
    var coords = [];
    coords[0] = req.body.longitude;
    coords[1] = req.body.latitude;
    //console.log(users[0].sucursales);
    if (req.body.longitude == "" && req.body.latitude == "") {
      res.status(200).json(users);
    }

    //Encontrar localizaciones
    _user2.default.find({
      $and: [{
        '_id': {
          $in: users
        }
      }, {
        'sucursales.loc': {
          $near: coords,
          $maxDistance: maxDistance
        }
      }]
    }, 'sucursales nombre').exec().then(function (resultado) {
      res.status(200).json(resultado);
      return;
    }).catch(function (err) {
      console.log(err);
    });
    // .catch(handleError(res));

    // res.status(200).json(users);
  });
  //.catch(handleError(res));
}

function busquedaTags(req, res) {
  var regTerm = new RegExp(req.body.keyword, 'i');
  _user2.default.find({
    $and: [{
      $or: [{
        nombre: regTerm
      }, {
        tags: regTerm
      }, {
        products: regTerm
      }]
    }, {
      role: 'empresa'
    }]
  }, '_id nombre tags products categoria_name estado municipio telefono email emailSec numeroContactos contactos').limit(5).then(function (user) {
    res.json(user).end();
  });
}

function searchToUpdate(req, res) {

  var regName = new RegExp(req.body.nombre, 'i');
  var regEmail = new RegExp(req.body.email, 'i');
  var regTelefono = new RegExp(req.body.telefono, 'i');

  _user2.default.find({
    $and: [{
      $and: [{
        nombre: regName
      }, {
        email: regEmail
      }, {
        telefono: regTelefono
      }]
    }]
  }, '_id nombre email telefono destacado').limit(10).then(function (user) {
    res.json(user).end();
  });
}

function busquedaNombre(req, res) {
  var regTerm = new RegExp(req.body.keyword, 'i');
  _user2.default.find({
    $and: [{
      $or: [{
        nombre: regTerm
      }, {
        tags: regTerm
      }, {
        products: regTerm
      }]
    }, {
      role: 'empresa'
    }]
  }, '_id nombre tags products categoria_name telefono email emailSec numeroContactos contactos').limit(5).then(function (user) {
    res.json(user).end();
  });
}

function busquedaTagsCategory(req, res) {
  var regTerm = new RegExp(req.body.keyword, 'i');
  _user2.default.find({
    $and: [{
      $or: [{
        nombre: regTerm
      }, {
        tags: regTerm
      }, {
        products: regTerm
      }]
    }, {
      categoria_name: req.body.subSectorName
    }]
  }, '_id nombre telefono web email direccion paquete emailSec numeroContactos contactos').limit(20).then(function (user) {
    console.log("------------" + user.length);
    res.json(user).end();
  });
}

function check(req, res) {
  console.log(req.body);
  _user2.default.findOne({
    'email': req.body.email
  }, '-salt -password').exec().then(function (user) {
    if (user === null) {
      res.status(201).end();
    } else {
      res.status(200).end();
    }
  });
}

function borrarfoto(req, res) {
  if (req.user._id == req.params.id) {
    _user2.default.findById(req.params.id).exec().then(function (user) {
      console.log(user.productsImg[req.params.position]);
      var path = './client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos/' + user.productsImg[req.params.position];
      fs.unlink(path, function (err) {
        console.log(err);
        if (err) {
          res.status(500).end();
        } else {
          res.status(200).end();
        }
      });
    });
  } else {
    res.status(403).end();
    return;
  }
}

function borrarVideo(req, res) {
  if (req.user._id == req.params.id) {
    _user2.default.findById(req.params.id).exec().then(function (user) {
      console.log(user.videos[req.params.position]);
      var path = './client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos/' + user.videos[req.params.position];
      fs.unlink(path, function (err) {
        console.log(err);
        if (err) {
          res.status(500).end();
        } else {
          res.status(200).end();
        }
      });
    });
  } else {
    res.status(403).end();
    return;
  }
}

function calcularCodigos(req, res) {
  //Buscar todos los registrados con el codigo de la persona logueada
  _user2.default.find({
    codigo_invitacion: req.user._id
  }, '_id nombre telefono web email direccion paquete emailSec sector_name categoria_name products about').then(function (users) {

    var response = {};
    var temp = [];
    response.total = users.length;
    //For de todos los ususarios para verificar los otros datos de la validacion
    for (var i = 0; i < users.length; i++) {
      //Buscar la foto de perfil en la carpeta del usuario
      var route = './client/assets/uploads/' + users[i].sector_name + '/' + users[i].categoria_name + '/' + users[i]._id + '/' + 'foto-perfil.jpg';
      var flag = true;

      //Validacion 1* que se tenga la foto de perfil
      flag = fs.existsSync(route);
      console.log("Tiene foto: " + flag);

      //Validacion 2* que se tengan 3 productos
      if (users[i].products.length < 4) {
        flag = false;
      }
      console.log("Tiene menos de 4 products: " + flag);
      //Validacion 3* que se tenga algo escrito en about
      if (users[i].about == "") {
        flag = false;
      }
      console.log("Tiene about: " + flag + ":----:" + users[i].about);

      //si alguna validacion es erronea se agrega al listado a borrar de la lista
      if (!flag) {
        temp.push(i);
      }
    }
    // se borran los usuarios no validos
    for (var i = 0; i < temp.length; i++) {
      users.splice(temp[i], 1);
    }
    // se calcula la respuesta
    response.validos = users.length;
    if (response.validos > 0) {
      response.credito = response.validos * 500;
      //Maximo 2500 de credito
      if (response.credito > 2500) {
        response.validos = 2500;
      }
    }

    res.json(response).end();
  });
}

function moveToMateriales() {
  var aidis = ['5900bb1e16d0b818494d08fb', '5914cf06675e8c1716636d2b', '57b4ec370524e2d31136d671'];
  asyncLoop(aidis, function (id, next) {
    _user2.default.findById(id, '-salt -password').exec().then(function (user) {
      console.log(user.nombre);
      mv('./client/assets/uploads/Construcción/' + user.categoria_name + '/' + user._id, './client/assets/uploads/Construcción/Maquinaria y equipo/' + user._id, { mkdirp: true }, function (err) {
        user.categoria_name = 'Maquinaria y equipo';
        user.save();
        next();
        // console.log(err);
      });
    });
  }, function (err) {
    if (err) {
      console.error('Error: ' + err.message);
      return;
    }
    console.log('Finished!');
  });
}

function moveToSuministros() {
  var aidis = ['57e93969790ad7194e40a9b2', '579941b41002da2f5a3c202e', '579abccb1002da2f5a3c2038', '57cde7a85592891b6e46a5af', '5887e51000881d3630f788e2', '58ab3d47511c82ed16a13f25', '58cd67706a43843647e365a4', '57e58120790ad7194e40a990', '57e61b27790ad7194e40a99b', '57e858eb790ad7194e40a9ac', '58d2cdc7a94c310706d60c57'];
  asyncLoop(aidis, function (id, next) {
    _user2.default.findById(id, '-salt -password').exec().then(function (user) {
      mv('./client/assets/uploads/Construcción/' + user.categoria_name + '/' + user._id, './client/assets/uploads/Construcción/Suministros para la construcción/' + user._id, { mkdirp: true }, function (err) {
        user.categoria_name = 'Suministros para la construcción';
        user.save();
        next();
        // console.log(err);
      });
    });
  }, function (err) {
    if (err) {
      console.error('Error: ' + err.message);
      return;
    }
    console.log('Finished!');
  });
}

function busquedaAvanzada(req, res) {
  console.log(req.body.ubicacion);
  var and = [];

  if (req.body.keyword != undefined) {
    and.push({
      $or: [{
        tags: new RegExp(req.body.keyword, 'i')
      }, {
        about: new RegExp(req.body.keyword, 'i')
      }, {
        products: new RegExp(req.body.keyword, 'i')
      }, {
        nombre: new RegExp(req.body.keyword, 'i')
      }]
    }, {
      role: 'empresa'
    });
  }

  if (req.body.ubicacion != undefined) {
    and.push({
      $or: [{
        estado_name: new RegExp(req.body.ubicacion, 'i')
      }, {
        municipio_name: new RegExp(req.body.ubicacion, 'i')
      }]
    });
  }

  if (req.body.sector != undefined) {
    and.push({
      sector_name: new RegExp(req.body.sector, 'i')
    });
  }

  if (req.body.categoria != undefined) {
    and.push({
      categoria_name: new RegExp(req.body.categoria, 'i')
    });
  }

  console.log(and);

  var query = {
    $and: and
  };

  return _user2.default.find(query).exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function pruebaChat(req, res) {
  console.log('ejecutado');
  var template = './server/views/pruebaChat.jade';

  // get template from file system
  fs.readFile(template, 'utf8', function (err, file) {
    //compile jade template into function
    var compiledTmpl = _jade.compile(file, {
      filename: template
    });
    // get html back as a string with the context applied;
    var html = compiledTmpl();

    return transporter.sendMail({
      from: 'hola@portaliaplus.com', // sender address
      to: req.body.destinatario, // list of receivers
      subject: "Tienes un nuevo chat en Portalia Plus", // Subject line
      html: html // html body
    }, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Message sent: ' + info.response);
        res.status(200).json(info.response);
      }
    });
  });
}

function newSearchSystem(req, res) {
  var and = [];
  var categorias;
  var estados;
  var sectores;
  var totalcount;

  if (req.body.keyword != undefined && req.body.keyword != "") {
    if (req.body.tipoBusqueda == 'producto' || req.body.tipoBusqueda == undefined) {
      and.push({
        $text: {
          $search: req.body.keyword,
          $diacriticSensitive: false,
          $language: 'es'
        }
      });
      and.push({
        'role': 'empresa'
      });
    } else if (req.body.tipoBusqueda == 'nombre') {
      and.push({
        nombre: new RegExp(req.body.keyword, 'i')
      });
      and.push({
        'role': 'empresa'
      });
    }
  }

  if (req.body.requestedPage == undefined) {
    req.body.requestedPage = 1;
    console.log('holi');
  }

  if (req.body.itemsPerPage == undefined) {
    req.body.itemsPerPage = 10;
  }

  if (req.body.ubicacion != undefined && req.body.ubicacion != '') {
    and.push({
      $or: [{
        estado_name: new RegExp(req.body.ubicacion, 'i')
      }, {
        municipio_name: new RegExp(req.body.ubicacion, 'i')
      }]
    });
  }

  if (req.body.sector != undefined && req.body.sector != '' && (req.body.categoria == undefined || req.body.categoria == '')) {
    and.push({
      sector_name: new RegExp(req.body.sector, 'i')
    });
  }

  if (req.body.categoria != undefined && req.body.categoria != '') {
    and.push({
      $or: [{
        categoria_name: new RegExp(req.body.categoria, 'i')
      }, {
        subcategories: new RegExp(req.body.categoria, 'i')
      }]
    });
  }

  var query = {
    $and: and
  };

  console.log(query);

  if (req.body.alphabetical != undefined) {
    if (req.body.alphabetical == 'true') {
      var sorting = 'nombre';
    } else if (req.body.alphabetical == 'false') {
      var sorting = '-nombre';
    }
  } else {
    var sorting = '-noPaquete -hasPic _id nombre';
  }

  if (and.length == 0) {
    console.log('k pz');
    var query = {
      'role': 'empresa'
    };

    console.log(query);

    _user2.default.aggregate([{
      $match: {
        'role': 'empresa'
      }
    }, {
      $group: {
        "_id": "$estado_name",
        "count": {
          $sum: 1
        }
      }
    }], function (err, result) {
      if (err) {
        res.status(500).end();
      } else {
        estados = result;
      }
    });

    _user2.default.aggregate([{
      $match: {
        'role': 'empresa'
      }
    }, {
      $group: {
        "_id": "$sector_name",
        "count": {
          $sum: 1
        }
      }
    }], function (err, result) {
      if (err) {
        res.status(500).end();
      } else {
        sectores = result;
      }
    });

    _user2.default.count({
      'role': 'empresa'
    }, function (err, c) {
      totalcount = c;
      console.log('Count is ' + c);
    });
    // User.find().count()
  } else {
    and.push({
      'role': 'empresa'
    });

    _user2.default.aggregate([{
      $match: {
        $and: and
      }
    }, {
      $group: {
        "_id": "$estado_name",
        "count": {
          $sum: 1
        }
      }
    }], function (err, result) {
      if (err) {
        res.status(500).end();
      } else {
        estados = result;
      }
    });

    _user2.default.aggregate([{
      $match: {
        $and: and
      }
    }, {
      $group: {
        "_id": "$sector_name",
        "count": {
          $sum: 1
        }
      }
    }], function (err, result) {
      if (err) {
        res.status(500).end();
      } else {
        sectores = result;
      }
    });

    if (req.body.sector != undefined) {
      _user2.default.aggregate([{
        $match: {
          $and: and
        }
      }, {
        $group: {
          "_id": "$categoria_name",
          "count": {
            $sum: 1
          }
        }
      }], function (err, result) {
        if (err) {
          res.status(500).end();
        } else {
          categorias = result;
        }
      });
    }
    console.log(util.inspect(query, {
      showHidden: false,
      depth: null
    }));
    _user2.default.count(query, function (err, c) {
      totalcount = c;
      console.log('Count is ' + c);
    });
  }
  var howManySkip = req.body.itemsPerPage * (req.body.requestedPage - 1);
  _user2.default.find(query).then(function (users) {
    _user2.default.find(query).sort(sorting).skip(howManySkip).limit(req.body.itemsPerPage).exec().then(function (users) {
      var respuesta = {
        'totalResults': totalcount,
        'currentPage': req.body.requestedPage,
        'totalPages': Math.ceil(totalcount / req.body.itemsPerPage),
        'categorias': categorias,
        'sectores': sectores,
        'estados': estados,
        'documents': users
      };
      res.status(200).json(respuesta);
    });
  });
}

function queryMailing(req, res) {
  if (req.body.tipoQuery == 'and') {
    var and = [];
    if (req.body.nombre != undefined) {
      and.push({
        nombre: new RegExp(req.body.nombre, 'i')
      });
    }

    if (req.body.email != undefined) {
      and.push({
        email: new RegExp(req.body.email, 'i')
      });
    }

    if (req.body.keyword != undefined) {
      and.push({
        tags: new RegExp(req.body.keyword, 'i')
      });
    }

    if (req.body.otraSub != undefined) {
      and.push({
        otraSub: new RegExp(req.body.otraSub, 'i')
      });
    }

    if (req.body.estado != undefined) {
      and.push({
        estado_name: new RegExp(req.body.estado, 'i')
      });
    }

    if (req.body.municipio != undefined) {
      and.push({
        municipio_name: new RegExp(req.body.municipio, 'i')
      });
    }

    if (req.body.sector != undefined) {
      and.push({
        sector_name: new RegExp(req.body.sector, 'i')
      });
    }

    if (req.body.categoria != undefined) {
      and.push({
        categoria_name: new RegExp(req.body.categoria, 'i')
      });
    }

    if (req.body.role != undefined) {
      and.push({
        role: new RegExp(req.body.role, 'i')
      });
    }

    if (req.body.paquete != undefined) {
      and.push({
        paquete: new RegExp(req.body.paquete, 'i')
      });
    }

    if (req.body.telefono != undefined) {
      and.push({
        telefono: new RegExp(req.body.telefono, 'i')
      });
    }

    var query = {
      $and: and
    };

    console.log(util.inspect(query, {
      showHidden: false,
      depth: null
    }));
  } else if (req.body.tipoQuery == 'or') {
    var or = [];
    if (req.body.nombre != undefined) {
      or.push({
        nombre: new RegExp(req.body.nombre, 'i')
      });
    }

    if (req.body.email != undefined) {
      or.push({
        email: new RegExp(req.body.email, 'i')
      });
    }

    if (req.body.keyword != undefined) {
      or.push({
        tags: new RegExp(req.body.keyword, 'i')
      });
    }

    if (req.body.estado != undefined) {
      or.push({
        estado_name: new RegExp(req.body.estado, 'i')
      });
    }

    if (req.body.municipio != undefined) {
      or.push({
        municipio_name: new RegExp(req.body.municipio, 'i')
      });
    }

    if (req.body.sector != undefined) {
      or.push({
        sector_name: new RegExp(req.body.sector, 'i')
      });
    }

    if (req.body.categoria != undefined) {
      or.push({
        categoria_name: new RegExp(req.body.categoria, 'i')
      });
    }

    if (req.body.role != undefined) {
      or.push({
        role: new RegExp(req.body.role, 'i')
      });
    }

    if (req.body.paquete != undefined) {
      or.push({
        paquete: new RegExp(req.body.paquete, 'i')
      });
    }

    if (req.body.telefono != undefined) {
      or.push({
        telefono: new RegExp(req.body.telefono, 'i')
      });
    }

    var query = {
      $or: or
    };

    console.log(util.inspect(query, {
      showHidden: false,
      depth: null
    }));
  }

  return _user2.default.find(query).exec().then(function (users) {
    res.status(200).json(users);
  }).catch(handleError(res));
}

function testingCount() {
  var and = [];
  and.push({
    estado_name: new RegExp('Jalisco', 'i')
  });

  _user2.default.distinct("estado_name", {
    $and: and
  }).exec().then(function (results) {
    console.log(results);
  });
}

function csvGenerator(req, res) {
  var and = [];
  if (req.body.hasOwnProperty("sector_name")) {
    and.push({
      sector_name: req.body.sector_name
    });
  }

  if (req.body.hasOwnProperty("categoria")) {
    and.push({
      categoria_name: req.body.categoria
    });
  }

  if (req.body.hasOwnProperty("estado")) {
    and.push({
      estado_name: req.body.estado
    });
  }

  var query = {
    $and: and
  };

  console.log(util.inspect(query, {
    showHidden: false,
    depth: null
  }));

  _user2.default.find(query, 'nombre email telefono sector_name categoria_name estado_name').exec().then(function (users) {
    console.log(users);
    if (users.length != 0) {
      var fields = ['nombre', 'email', 'telefono', 'sector_name', 'categoria_name', 'estado_name'];
      var csv = json2csv({
        data: users,
        fields: fields
      });

      fs.writeFile('./client/assets/uploads/file.csv', csv, function (err) {
        if (err) throw err;
        console.log('file saved');
        res.status(200).json('file.csv');
      });
    } else {
      res.status(200).json('No hay resultados');
    }
  }).catch(handleError(res));
}

// csvGenerator()

function perrona() {
  var postData = querystring.stringify({
    "prerenderToken": "74YQIPspZWQJQlir2iYh",
    "url": "https://www.portaliaplus.com/perfil/58d3054e78a6dfef68630433"
  });

  var options = {
    hostname: 'api.prerender.io',
    method: 'POST',
    path: '/recache',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  var req = http.request(options, function (res) {
    console.log('STATUS: ' + res.statusCode);
    console.log('HEADERS: ' + JSON.stringify(res.headers));
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      console.log('BODY: ' + chunk);
    });
    res.on('end', function () {
      console.log('No more data in response.');
    });
  });

  req.on('error', function (e) {
    console.log('problem with request: ' + e.message);
  });

  // write data to request body
  req.write(postData);
  req.end();
}

// perrona();

function aggregateCategories(req, res) {
  return _user2.default.aggregate([{
    $match: {
      $and: [{
        'provider': 'local'
      }, {
        'role': 'empresa'
      }]
    }
  }, {
    $group: {
      "_id": "$categoria_name",
      "sector": {
        $first: '$sector_name'
      },
      "count": {
        $sum: 1
      }
    }
  }], function (err, result) {
    if (err) {
      res.status(500).end();
    } else {
      res.status(200).json(result);
    }
  });
}

function aggregatePreCategories(req, res) {
  return _user2.default.aggregate([{
    $match: {
      'provider': {
        $nin: ["local", "facebook", "admin", "tmk"]
      }
    }
  }, {
    $group: {
      "_id": "$categoria_name",
      "sector": {
        $first: '$sector_name'
      },
      "count": {
        $sum: 1
      }
    }
  }], function (err, result) {
    if (err) {
      res.status(500).end();
    } else {
      res.status(200).json(result);
    }
  });
}
//# sourceMappingURL=user.controller.js.map
