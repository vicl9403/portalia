'use strict';

var _express = require('express');

var _user = require('./user.controller');

var controller = _interopRequireWildcard(_user);

var _auth = require('../../auth/auth.service');

var auth = _interopRequireWildcard(_auth);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var router = new _express.Router();

router.get('/', controller.index);
router.get('/adminMes', auth.hasRole('admin'), controller.indexAdminMeses);
router.get('/getVisitors', auth.hasRole('admin'), controller.getVisitors);
// router.get('/activarPromo', controller.activarPromo);
//router.get('/moverImpo', controller.moverImpo);

// Forzar el envío del balance mensual de cotizaciones
router.post('/sendQuotesMail', controller.sendQuotesMail);

router.get('/corridaFoto', controller.corridaFoto);
router.get('/corridaFotoSecundaria', controller.corridaFotoSecundaria);
router.get('/usuariosDestacados', controller.usuariosDestacados);
router.get('/aggregateCategories', controller.aggregateCategories);
router.get('/aggregatePreCategories', controller.aggregatePreCategories);
router.get('/moveToMateriales', controller.moveToMateriales);
router.get('/moveToSuministros', controller.moveToSuministros);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', controller.show);
router.get('/countsectors/:sector', controller.countSectors);
router.post('/', controller.create);
router.post('/check', controller.check);
router.post('/reset/:key', controller.reset);
router.post('/activar/:key', controller.activar);
router.patch('/:id', controller.update);
router.post("/busquedaCategoria", controller.busquedaCategoria);
router.post("/countSubs", auth.hasRole('admin'), controller.countSubs);
router.post("/adminSectores", auth.hasRole('admin'), controller.getSectors);
router.post("/busquedaAnuncios", controller.busquedaAnuncios);
router.post("/busquedaAnunciosPremium", controller.busquedaAnunciosPremium);
router.post("/busquedaGeografica", controller.busquedaGeografica);
router.post("/busquedaTags", controller.busquedaTags);
router.post("/searchToUpdate", controller.searchToUpdate);
router.post("/busquedaTagsCategory", controller.busquedaTagsCategory);
router.post("/busquedaAvanzada", controller.busquedaAvanzada);
router.post('/newSearchSystem', controller.newSearchSystem);
router.post('/queryMailing', controller.queryMailing);
router.post('/testingCount', controller.testingCount);
router.delete("/borrarfoto/:id/:sector/:categoria/:name/:position", auth.isAuthenticated(), controller.borrarfoto);
router.delete("/borrarVideo/:id/:sector/:categoria/:name/:position", auth.isAuthenticated(), controller.borrarVideo);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.post('/calcularCodigos', auth.isAuthenticated(), controller.calcularCodigos);
router.post('/nuevoChat', controller.pruebaChat);
router.post('/generarCSV', controller.csvGenerator);
router.post('/generarWP', controller.generateWPUser);

router.post('/getListOfUsers', controller.getUserListByIds);

module.exports = router;
//# sourceMappingURL=index.js.map
