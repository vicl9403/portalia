'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createIndexFromAllUsers = createIndexFromAllUsers;
exports.setUserData = setUserData;

var _algoliasearch = require('algoliasearch');

var _algoliasearch2 = _interopRequireDefault(_algoliasearch);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

var _user = require('./user.model');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var client = (0, _algoliasearch2.default)(_environment2.default.algoliaconfig.idApplication, _environment2.default.algoliaconfig.adminKey);

var index = client.initIndex('users');

function createIndexFromAllUsers() {

  var usersToIndex = [];

  var users = _user2.default.find().exec().then(function (res) {
    var users = res;
    for (var i = 0; i < users.length; i++) {

      var plan_number = 1;

      if (users[i].paquete == 'select') plan_number = 2;

      if (users[i].paquete == 'plus') plan_number = 3;

      if (usersToIndex.length >= 500) {
        // index.saveObjects( usersToIndex, function (err, content) {
        //   console.log( content );
        //   console.log( err );
        //   console.log('content ready');
        // });
        usersToIndex = [];
      }

      var user = {
        objectID: users[i]._id,
        name: users[i].nombre,
        sector: users[i].sector_name,
        category: users[i].categoria_name,
        state: users[i].estado_name,
        municipality: users[i].municipio_name,
        cover: users[i].cover,
        stars: users[i].stars,
        plan: plan_number,
        about: users[i].about,
        tags: users[i].tags,
        products: users[i].products,
        subCategories: users[i].subcategories
      };

      if (users[i].subcategories != null) {
        index.saveObject(user, function (err, content) {
          if (err) {
            console.log(err);
          } else {
            console.log('success');
          }
        });
      }

      usersToIndex.push({
        objectID: users[i]._id,
        name: users[i].nombre,
        sector: users[i].sector_name,
        category: users[i].categoria_name,
        state: users[i].estado_name,
        municipality: users[i].municipio_name,
        cover: users[i].cover,
        stars: users[i].stars,
        plan: plan_number,
        about: users[i].about,
        tags: users[i].tags,
        products: users[i].products,
        subCategories: users[i].subcategories
      });
    }

    if (usersToIndex.length > 0) {
      // Solo se debe de ejecutar una vez
      // index.saveObjects(usersToIndex, function (err, content) {
      //   console.log(content);
      //   console.log(err);
      //   console.log('content ready');
      // });
    }
  });
}

function setUserData(id) {

  // Asignar un tiempo de espera para actualizar el índice con el
  // usuario ya modificado
  setTimeout(function () {
    _user2.default.findById(id).then(function (user) {
      var plan_number = 1;

      if (user.paquete == 'select') plan_number = 2;

      if (user.paquete == 'plus') plan_number = 3;

      var usr = {
        objectID: user._id,
        name: user.nombre,
        sector: user.sector_name,
        category: user.categoria_name,
        state: user.estado_name,
        municipality: user.municipio_name,
        cover: user.cover,
        stars: user.stars,
        plan: plan_number,
        about: user.about,
        tags: user.tags,
        products: user.products,
        subCategories: user.subcategories
      };

      index.saveObject(usr, function (err, content) {
        console.log(content);
      });
    });
  }, 1000);
}
//# sourceMappingURL=algolia.js.map
