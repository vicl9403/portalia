'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newEstado;

describe('Estado API:', function () {

  describe('GET /api/estados', function () {
    var estados;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/estados').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        estados = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      estados.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/estados', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/estados').send({
        name: 'New Estado',
        info: 'This is the brand new estado!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newEstado = res.body;
        done();
      });
    });

    it('should respond with the newly created estado', function () {
      newEstado.name.should.equal('New Estado');
      newEstado.info.should.equal('This is the brand new estado!!!');
    });
  });

  describe('GET /api/estados/:id', function () {
    var estado;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/estados/' + newEstado._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        estado = res.body;
        done();
      });
    });

    afterEach(function () {
      estado = {};
    });

    it('should respond with the requested estado', function () {
      estado.name.should.equal('New Estado');
      estado.info.should.equal('This is the brand new estado!!!');
    });
  });

  describe('PUT /api/estados/:id', function () {
    var updatedEstado;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/estados/' + newEstado._id).send({
        name: 'Updated Estado',
        info: 'This is the updated estado!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedEstado = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedEstado = {};
    });

    it('should respond with the updated estado', function () {
      updatedEstado.name.should.equal('Updated Estado');
      updatedEstado.info.should.equal('This is the updated estado!!!');
    });
  });

  describe('DELETE /api/estados/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/estados/' + newEstado._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when estado does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/estados/' + newEstado._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=estado.integration.js.map
