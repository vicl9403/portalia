'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var estadoCtrlStub = {
  index: 'estadoCtrl.index',
  show: 'estadoCtrl.show',
  create: 'estadoCtrl.create',
  update: 'estadoCtrl.update',
  destroy: 'estadoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var estadoIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './estado.controller': estadoCtrlStub
});

describe('Estado API Router:', function () {

  it('should return an express router instance', function () {
    estadoIndex.should.equal(routerStub);
  });

  describe('GET /api/estados', function () {

    it('should route to estado.controller.index', function () {
      routerStub.get.withArgs('/', 'estadoCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/estados/:id', function () {

    it('should route to estado.controller.show', function () {
      routerStub.get.withArgs('/:id', 'estadoCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/estados', function () {

    it('should route to estado.controller.create', function () {
      routerStub.post.withArgs('/', 'estadoCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/estados/:id', function () {

    it('should route to estado.controller.update', function () {
      routerStub.put.withArgs('/:id', 'estadoCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/estados/:id', function () {

    it('should route to estado.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'estadoCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/estados/:id', function () {

    it('should route to estado.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'estadoCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
