/**
 * Estado model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _estado = require('./estado.model');

var _estado2 = _interopRequireDefault(_estado);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EstadoEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
EstadoEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _estado2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    EstadoEvents.emit(event + ':' + doc._id, doc);
    EstadoEvents.emit(event, doc);
  };
}

exports.default = EstadoEvents;
//# sourceMappingURL=estado.events.js.map
