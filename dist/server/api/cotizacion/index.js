'use strict';

var express = require('express');
var controller = require('./cotizacion.controller');

var router = express.Router();

// Esta es la ruta /api/cotizaciones

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

//Mail de cotizaciones
router.post('/nuevaCotizacion', controller.nuevaCotizacion);

module.exports = router;
//# sourceMappingURL=index.js.map
