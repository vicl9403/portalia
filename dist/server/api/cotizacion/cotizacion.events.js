/**
 * Cotizacion model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _cotizacion = require('./cotizacion.model');

var _cotizacion2 = _interopRequireDefault(_cotizacion);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CotizacionEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
CotizacionEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _cotizacion2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    CotizacionEvents.emit(event + ':' + doc._id, doc);
    CotizacionEvents.emit(event, doc);
  };
}

exports.default = CotizacionEvents;
//# sourceMappingURL=cotizacion.events.js.map
