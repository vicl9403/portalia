/**
 * PasswordRecovery model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _passwordRecovery = require('./passwordRecovery.model');

var _passwordRecovery2 = _interopRequireDefault(_passwordRecovery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PasswordRecoveryEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
PasswordRecoveryEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _passwordRecovery2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    PasswordRecoveryEvents.emit(event + ':' + doc._id, doc);
    PasswordRecoveryEvents.emit(event, doc);
  };
}

exports.default = PasswordRecoveryEvents;
//# sourceMappingURL=passwordRecovery.events.js.map
