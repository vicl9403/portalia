'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PasswordRecoverySchema = new _mongoose2.default.Schema({
  urlKey: String,
  email: String
});

exports.default = _mongoose2.default.model('PasswordRecovery', PasswordRecoverySchema);
//# sourceMappingURL=passwordRecovery.model.js.map
