'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newPasswordRecovery;

describe('PasswordRecovery API:', function () {

  describe('GET /api/passwordRecoverys', function () {
    var passwordRecoverys;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/passwordRecoverys').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        passwordRecoverys = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      passwordRecoverys.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/passwordRecoverys', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/passwordRecoverys').send({
        name: 'New PasswordRecovery',
        info: 'This is the brand new passwordRecovery!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newPasswordRecovery = res.body;
        done();
      });
    });

    it('should respond with the newly created passwordRecovery', function () {
      newPasswordRecovery.name.should.equal('New PasswordRecovery');
      newPasswordRecovery.info.should.equal('This is the brand new passwordRecovery!!!');
    });
  });

  describe('GET /api/passwordRecoverys/:id', function () {
    var passwordRecovery;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/passwordRecoverys/' + newPasswordRecovery._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        passwordRecovery = res.body;
        done();
      });
    });

    afterEach(function () {
      passwordRecovery = {};
    });

    it('should respond with the requested passwordRecovery', function () {
      passwordRecovery.name.should.equal('New PasswordRecovery');
      passwordRecovery.info.should.equal('This is the brand new passwordRecovery!!!');
    });
  });

  describe('PUT /api/passwordRecoverys/:id', function () {
    var updatedPasswordRecovery;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/passwordRecoverys/' + newPasswordRecovery._id).send({
        name: 'Updated PasswordRecovery',
        info: 'This is the updated passwordRecovery!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedPasswordRecovery = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedPasswordRecovery = {};
    });

    it('should respond with the updated passwordRecovery', function () {
      updatedPasswordRecovery.name.should.equal('Updated PasswordRecovery');
      updatedPasswordRecovery.info.should.equal('This is the updated passwordRecovery!!!');
    });
  });

  describe('DELETE /api/passwordRecoverys/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/passwordRecoverys/' + newPasswordRecovery._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when passwordRecovery does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/passwordRecoverys/' + newPasswordRecovery._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=passwordRecovery.integration.js.map
