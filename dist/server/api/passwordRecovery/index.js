'use strict';

var express = require('express');
var controller = require('./passwordRecovery.controller');

var router = express.Router();

router.get('/', controller.index);
router.post('/:code', controller.preRegister);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
//# sourceMappingURL=index.js.map
