/**
 * Mailing model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _mailing = require('./mailing.model');

var _mailing2 = _interopRequireDefault(_mailing);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MailingEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
MailingEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _mailing2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    MailingEvents.emit(event + ':' + doc._id, doc);
    MailingEvents.emit(event, doc);
  };
}

exports.default = MailingEvents;
//# sourceMappingURL=mailing.events.js.map
