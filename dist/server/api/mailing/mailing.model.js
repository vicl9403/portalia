'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MailingSchema = new _mongoose2.default.Schema({
  bloque: String,
  email: [{
    nombre: String,
    mail: String,
    categoria_name: String,
    sector_name: String,
    title: String,
    body: String
  }]
});

exports.default = _mongoose2.default.model('Mailing', MailingSchema);
//# sourceMappingURL=mailing.model.js.map
