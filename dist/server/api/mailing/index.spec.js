'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var mailingCtrlStub = {
  index: 'mailingCtrl.index',
  show: 'mailingCtrl.show',
  create: 'mailingCtrl.create',
  update: 'mailingCtrl.update',
  destroy: 'mailingCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var mailingIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './mailing.controller': mailingCtrlStub
});

describe('Mailing API Router:', function () {

  it('should return an express router instance', function () {
    mailingIndex.should.equal(routerStub);
  });

  describe('GET /api/mailings', function () {

    it('should route to mailing.controller.index', function () {
      routerStub.get.withArgs('/', 'mailingCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/mailings/:id', function () {

    it('should route to mailing.controller.show', function () {
      routerStub.get.withArgs('/:id', 'mailingCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/mailings', function () {

    it('should route to mailing.controller.create', function () {
      routerStub.post.withArgs('/', 'mailingCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/mailings/:id', function () {

    it('should route to mailing.controller.update', function () {
      routerStub.put.withArgs('/:id', 'mailingCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/mailings/:id', function () {

    it('should route to mailing.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'mailingCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/mailings/:id', function () {

    it('should route to mailing.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'mailingCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
