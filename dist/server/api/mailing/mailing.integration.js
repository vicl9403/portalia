'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newMailing;

describe('Mailing API:', function () {

  describe('GET /api/mailings', function () {
    var mailings;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/mailings').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        mailings = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      mailings.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/mailings', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/mailings').send({
        name: 'New Mailing',
        info: 'This is the brand new mailing!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newMailing = res.body;
        done();
      });
    });

    it('should respond with the newly created mailing', function () {
      newMailing.name.should.equal('New Mailing');
      newMailing.info.should.equal('This is the brand new mailing!!!');
    });
  });

  describe('GET /api/mailings/:id', function () {
    var mailing;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/mailings/' + newMailing._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        mailing = res.body;
        done();
      });
    });

    afterEach(function () {
      mailing = {};
    });

    it('should respond with the requested mailing', function () {
      mailing.name.should.equal('New Mailing');
      mailing.info.should.equal('This is the brand new mailing!!!');
    });
  });

  describe('PUT /api/mailings/:id', function () {
    var updatedMailing;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/mailings/' + newMailing._id).send({
        name: 'Updated Mailing',
        info: 'This is the updated mailing!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedMailing = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedMailing = {};
    });

    it('should respond with the updated mailing', function () {
      updatedMailing.name.should.equal('Updated Mailing');
      updatedMailing.info.should.equal('This is the updated mailing!!!');
    });
  });

  describe('DELETE /api/mailings/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/mailings/' + newMailing._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when mailing does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/mailings/' + newMailing._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=mailing.integration.js.map
