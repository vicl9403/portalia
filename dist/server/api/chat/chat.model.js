'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var User = _mongoose2.default.model('User');

var ChatSchema = new _mongoose2.default.Schema({
  // personsId: [{
  //   id: String,
  //   name: String
  // }],
  // msj: [{
  //   idEnv: String,
  //   msj: String,
  //   timestamp: {
  //     type: Date,
  //     default: Date.now
  //   }
  // }]

  userOrigin: [{ type: Schema.ObjectId, ref: "User" }],
  userDest: [{ type: Schema.ObjectId, ref: "User" }],
  messages: [{
    from: String,
    to: String,
    content: String,
    hasReadOrigin: { type: Boolean, default: false },
    hasReadDest: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now }
  }],
  active: {
    type: Boolean,
    default: true
  }

});

exports.default = _mongoose2.default.model('Chat', ChatSchema);
//# sourceMappingURL=chat.model.js.map
