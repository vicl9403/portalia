/**
 * Chat model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _chat = require('./chat.model');

var _chat2 = _interopRequireDefault(_chat);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChatEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
ChatEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _chat2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    ChatEvents.emit(event + ':' + doc._id, doc);
    ChatEvents.emit(event, doc);
  };
}

exports.default = ChatEvents;
//# sourceMappingURL=chat.events.js.map
