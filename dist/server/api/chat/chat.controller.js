/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/chats              ->  index
 * POST    /api/chats              ->  create
 * GET     /api/chats/:id          ->  show
 * PUT     /api/chats/:id          ->  update
 * DELETE  /api/chats/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.getUserChats = getUserChats;
exports.chatExists = chatExists;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _chat = require('./chat.model');

var _chat2 = _interopRequireDefault(_chat);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Chats
function index(req, res) {
  return _chat2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Obtener los chats de un usuario
function getUserChats(req, res) {

  var user = req.params.id;

  _chat2.default.find({
    $or: [{
      'userOrigin': user
    }, {
      'userDest': user
    }]
  }).populate({
    path: 'userOrigin'
  }).populate({
    path: 'userDest'
  }).exec(function (err, chats) {
    res.status(200).send(chats);
  });
}

function chatExists(req, res) {
  var id1 = req.params.id1;
  var id2 = req.params.id2;

  return _chat2.default.find({
    $or: [{
      $and: [{
        'userOrigin': id1
      }, {
        'userDest': id2
      }]
    }, {
      $and: [{
        'userOrigin': id2
      }, {
        'userDest': id1
      }]
    }]
  }).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}
// Gets a single Chat from the DB
function show(req, res) {
  return _chat2.default.find({
    personsId: {
      $elemMatch: {
        id: req.params.id
      }
    }
  }).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Chat in the DB
function create(req, res) {
  return _chat2.default.create(req.body).then(respondWithResult(res, 201));
}

// // Updates an existing Chat in the DB
// export function update(req, res) {
//   if (req.body._id) {
//     delete req.body._id;
//   }
//   return Chat.findById(req.params.id).exec()
//     .then(handleEntityNotFound(res))
//     .then(saveUpdates(req.body))
//     .then(respondWithResult(res))
//     .catch(handleError(res));
// }

function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  console.log(req.body);
  return _chat2.default.findByIdAndUpdate(req.params.id, req.body, { new: true }).exec().then(function (entity) {
    res.status(200).json(entity);
  }).catch(function (error) {
    console.log(error);
    res.status(500).end();
  });
}

// Deletes a Chat from the DB
function destroy(req, res) {
  return _chat2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=chat.controller.js.map
