/**
 * QuotesConfiguration model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _quotes_configuration = require('./quotes_configuration.model');

var _quotes_configuration2 = _interopRequireDefault(_quotes_configuration);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var QuotesConfigurationEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
QuotesConfigurationEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _quotes_configuration2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    QuotesConfigurationEvents.emit(event + ':' + doc._id, doc);
    QuotesConfigurationEvents.emit(event, doc);
  };
}

exports.default = QuotesConfigurationEvents;
//# sourceMappingURL=quotes_configuration.events.js.map
