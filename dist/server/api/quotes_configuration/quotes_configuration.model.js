'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var QuotesConfigurationSchema = new _mongoose2.default.Schema({

  // Nombre del bloque a sensar
  name: String,

  // Campos para sensar el score de búsqueda de prospectos
  name_score: { type: Number, required: true },
  products_score: { type: Number, required: true },
  tags_score: { type: Number, required: true },
  description_score: { type: Number, required: true },

  // Fecha de creación
  created_at: { type: Date, default: new Date() },

  // Score a utilizar
  min_score: { type: Number, required: true }

});

exports.default = _mongoose2.default.model('QuotesConfiguration', QuotesConfigurationSchema);
//# sourceMappingURL=quotes_configuration.model.js.map
