'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var quotesConfigurationCtrlStub = {
  index: 'quotesConfigurationCtrl.index',
  show: 'quotesConfigurationCtrl.show',
  create: 'quotesConfigurationCtrl.create',
  update: 'quotesConfigurationCtrl.update',
  destroy: 'quotesConfigurationCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var quotesConfigurationIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './quotes_configuration.controller': quotesConfigurationCtrlStub
});

describe('QuotesConfiguration API Router:', function () {

  it('should return an express router instance', function () {
    quotesConfigurationIndex.should.equal(routerStub);
  });

  describe('GET /api/quotes_configurations', function () {

    it('should route to quotesConfiguration.controller.index', function () {
      routerStub.get.withArgs('/', 'quotesConfigurationCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/quotes_configurations/:id', function () {

    it('should route to quotesConfiguration.controller.show', function () {
      routerStub.get.withArgs('/:id', 'quotesConfigurationCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/quotes_configurations', function () {

    it('should route to quotesConfiguration.controller.create', function () {
      routerStub.post.withArgs('/', 'quotesConfigurationCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/quotes_configurations/:id', function () {

    it('should route to quotesConfiguration.controller.update', function () {
      routerStub.put.withArgs('/:id', 'quotesConfigurationCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/quotes_configurations/:id', function () {

    it('should route to quotesConfiguration.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'quotesConfigurationCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/quotes_configurations/:id', function () {

    it('should route to quotesConfiguration.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'quotesConfigurationCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
