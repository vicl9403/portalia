/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/quotes_configurations              ->  index
 * POST    /api/quotes_configurations              ->  create
 * GET     /api/quotes_configurations/:id          ->  show
 * PUT     /api/quotes_configurations/:id          ->  update
 * DELETE  /api/quotes_configurations/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.last = last;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _quotes_configuration = require('./quotes_configuration.model');

var _quotes_configuration2 = _interopRequireDefault(_quotes_configuration);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    if (entity) {
      var updated = _lodash2.default.merge(entity, updates);
      return updated.save().then(function (updated) {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of QuotesConfigurations
function index(req, res) {
  return _quotes_configuration2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets last element of QuotesConfigurations
function last(req, res) {
  return _quotes_configuration2.default.findOne().sort('-_id').exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single QuotesConfiguration from the DB
function show(req, res) {
  return _quotes_configuration2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new QuotesConfiguration in the DB
function create(req, res) {
  return _quotes_configuration2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing QuotesConfiguration in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _quotes_configuration2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a QuotesConfiguration from the DB
function destroy(req, res) {
  return _quotes_configuration2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=quotes_configuration.controller.js.map
