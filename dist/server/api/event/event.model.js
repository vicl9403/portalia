'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EventSchema = new _mongoose2.default.Schema({
  title: String,
  start: Date,
  description: String,
  uerl: String,
  lugar: String,
  sector: String,
  imagen: String,
  end: Date,
  destacado: {
    type: Boolean,
    default: false
  }
});

exports.default = _mongoose2.default.model('Event', EventSchema);
//# sourceMappingURL=event.model.js.map
