/**
 * ProductCategory model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _product_category = require('./product_category.model');

var _product_category2 = _interopRequireDefault(_product_category);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProductCategoryEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
ProductCategoryEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _product_category2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    ProductCategoryEvents.emit(event + ':' + doc._id, doc);
    ProductCategoryEvents.emit(event, doc);
  };
}

exports.default = ProductCategoryEvents;
//# sourceMappingURL=product_category.events.js.map
