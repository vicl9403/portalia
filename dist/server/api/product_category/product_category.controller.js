/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/product_categories              ->  index
 * POST    /api/product_categories              ->  create
 * GET     /api/product_categories/:id          ->  show
 * PUT     /api/product_categories/:id          ->  update
 * DELETE  /api/product_categories/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.updateSubcategories = updateSubcategories;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _product_category = require('./product_category.model');

var _product_category2 = _interopRequireDefault(_product_category);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    if (entity) {
      var updated = _lodash2.default.merge(entity, updates);
      return updated.save().then(function (updated) {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of ProductCategorys
function index(req, res) {
  return _product_category2.default.find().exec().then(respondWithResult(res));
}

// Gets a single ProductCategory from the DB
function show(req, res) {
  return _product_category2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res));
}

// Creates a new ProductCategory in the DB
function create(req, res) {
  return _product_category2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing ProductCategory in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  console.log(req.body);
  return _product_category2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res));
}

function updateSubcategories(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _product_category2.default.findById(req.params.id).exec().then(function (entity) {
    entity.subCategories = req.body;
    entity.save();
    return res.send(200, 'success');
  });
}

// Deletes a ProductCategory from the DB
function destroy(req, res) {
  return _product_category2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res));
}
//# sourceMappingURL=product_category.controller.js.map
