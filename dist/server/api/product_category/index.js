'use strict';

var express = require('express');
var controller = require('./product_category.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.patch('/subcategories/:id', controller.updateSubcategories);
router.delete('/:id', controller.destroy);

module.exports = router;
//# sourceMappingURL=index.js.map
