'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newProductCategory;

describe('ProductCategory API:', function () {

  describe('GET /api/product_categories', function () {
    var productCategorys;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/product_categories').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        productCategorys = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      productCategorys.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/product_categories', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/product_categories').send({
        name: 'New ProductCategory',
        info: 'This is the brand new productCategory!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newProductCategory = res.body;
        done();
      });
    });

    it('should respond with the newly created productCategory', function () {
      newProductCategory.name.should.equal('New ProductCategory');
      newProductCategory.info.should.equal('This is the brand new productCategory!!!');
    });
  });

  describe('GET /api/product_categories/:id', function () {
    var productCategory;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/product_categories/' + newProductCategory._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        productCategory = res.body;
        done();
      });
    });

    afterEach(function () {
      productCategory = {};
    });

    it('should respond with the requested productCategory', function () {
      productCategory.name.should.equal('New ProductCategory');
      productCategory.info.should.equal('This is the brand new productCategory!!!');
    });
  });

  describe('PUT /api/product_categories/:id', function () {
    var updatedProductCategory;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/product_categories/' + newProductCategory._id).send({
        name: 'Updated ProductCategory',
        info: 'This is the updated productCategory!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedProductCategory = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedProductCategory = {};
    });

    it('should respond with the updated productCategory', function () {
      updatedProductCategory.name.should.equal('Updated ProductCategory');
      updatedProductCategory.info.should.equal('This is the updated productCategory!!!');
    });
  });

  describe('DELETE /api/product_categories/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/product_categories/' + newProductCategory._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when productCategory does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/product_categories/' + newProductCategory._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=product_category.integration.js.map
