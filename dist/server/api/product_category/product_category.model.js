'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProductCategorySchema = new _mongoose2.default.Schema({

  name: String,

  subCategories: [{
    name: { type: String },
    active: { type: Boolean }
  }],

  search: [{
    name: { type: String },
    active: { type: Boolean }
  }],

  active: Boolean

});

exports.default = _mongoose2.default.model('ProductCategory', ProductCategorySchema);
//# sourceMappingURL=product_category.model.js.map
