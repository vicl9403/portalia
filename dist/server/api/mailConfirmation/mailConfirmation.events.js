/**
 * MailConfirmation model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _mailConfirmation = require('./mailConfirmation.model');

var _mailConfirmation2 = _interopRequireDefault(_mailConfirmation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MailConfirmationEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
MailConfirmationEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _mailConfirmation2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    MailConfirmationEvents.emit(event + ':' + doc._id, doc);
    MailConfirmationEvents.emit(event, doc);
  };
}

exports.default = MailConfirmationEvents;
//# sourceMappingURL=mailConfirmation.events.js.map
