'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newMailConfirmation;

describe('MailConfirmation API:', function () {

  describe('GET /api/mailConfirmations', function () {
    var mailConfirmations;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/mailConfirmations').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        mailConfirmations = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      mailConfirmations.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/mailConfirmations', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/mailConfirmations').send({
        name: 'New MailConfirmation',
        info: 'This is the brand new mailConfirmation!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newMailConfirmation = res.body;
        done();
      });
    });

    it('should respond with the newly created mailConfirmation', function () {
      newMailConfirmation.name.should.equal('New MailConfirmation');
      newMailConfirmation.info.should.equal('This is the brand new mailConfirmation!!!');
    });
  });

  describe('GET /api/mailConfirmations/:id', function () {
    var mailConfirmation;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/mailConfirmations/' + newMailConfirmation._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        mailConfirmation = res.body;
        done();
      });
    });

    afterEach(function () {
      mailConfirmation = {};
    });

    it('should respond with the requested mailConfirmation', function () {
      mailConfirmation.name.should.equal('New MailConfirmation');
      mailConfirmation.info.should.equal('This is the brand new mailConfirmation!!!');
    });
  });

  describe('PUT /api/mailConfirmations/:id', function () {
    var updatedMailConfirmation;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/mailConfirmations/' + newMailConfirmation._id).send({
        name: 'Updated MailConfirmation',
        info: 'This is the updated mailConfirmation!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedMailConfirmation = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedMailConfirmation = {};
    });

    it('should respond with the updated mailConfirmation', function () {
      updatedMailConfirmation.name.should.equal('Updated MailConfirmation');
      updatedMailConfirmation.info.should.equal('This is the updated mailConfirmation!!!');
    });
  });

  describe('DELETE /api/mailConfirmations/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/mailConfirmations/' + newMailConfirmation._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when mailConfirmation does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/mailConfirmations/' + newMailConfirmation._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=mailConfirmation.integration.js.map
