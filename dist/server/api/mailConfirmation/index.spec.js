'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var mailConfirmationCtrlStub = {
  index: 'mailConfirmationCtrl.index',
  show: 'mailConfirmationCtrl.show',
  create: 'mailConfirmationCtrl.create',
  update: 'mailConfirmationCtrl.update',
  destroy: 'mailConfirmationCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var mailConfirmationIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './mailConfirmation.controller': mailConfirmationCtrlStub
});

describe('MailConfirmation API Router:', function () {

  it('should return an express router instance', function () {
    mailConfirmationIndex.should.equal(routerStub);
  });

  describe('GET /api/mailConfirmations', function () {

    it('should route to mailConfirmation.controller.index', function () {
      routerStub.get.withArgs('/', 'mailConfirmationCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/mailConfirmations/:id', function () {

    it('should route to mailConfirmation.controller.show', function () {
      routerStub.get.withArgs('/:id', 'mailConfirmationCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/mailConfirmations', function () {

    it('should route to mailConfirmation.controller.create', function () {
      routerStub.post.withArgs('/', 'mailConfirmationCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/mailConfirmations/:id', function () {

    it('should route to mailConfirmation.controller.update', function () {
      routerStub.put.withArgs('/:id', 'mailConfirmationCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/mailConfirmations/:id', function () {

    it('should route to mailConfirmation.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'mailConfirmationCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/mailConfirmations/:id', function () {

    it('should route to mailConfirmation.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'mailConfirmationCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
