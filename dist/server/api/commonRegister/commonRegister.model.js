'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CommonRegisterSchema = new _mongoose2.default.Schema({
  name: { type: String, unique: true },
  count: Number
});

exports.default = _mongoose2.default.model('CommonRegister', CommonRegisterSchema);
//# sourceMappingURL=commonRegister.model.js.map
