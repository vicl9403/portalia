'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storeIndex = storeIndex;
exports.setUserData = setUserData;

var _algoliasearch = require('algoliasearch');

var _algoliasearch2 = _interopRequireDefault(_algoliasearch);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

var _product = require('./product.model');

var _product2 = _interopRequireDefault(_product);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var client = (0, _algoliasearch2.default)(_environment2.default.algoliaconfig.idApplication, _environment2.default.algoliaconfig.adminKey);

var index = client.initIndex('products');

function storeIndex(product) {

  product.objectID = product._id;

  index.saveObject(product, function (err, content) {
    console.log(content);
  });
}

function setUserData(id) {

  // Asignar un tiempo de espera para actualizar el índice con el
  // usuario ya modificado
  setTimeout(function () {
    User.findById(id).then(function (user) {
      var plan_number = 1;

      if (user.paquete == 'select') plan_number = 2;

      if (user.paquete == 'plus') plan_number = 3;

      var usr = {
        objectID: user._id,
        name: user.nombre,
        sector: user.sector_name,
        category: user.categoria_name,
        state: user.estado_name,
        municipality: user.municipio_name,
        cover: user.cover,
        stars: user.stars,
        plan: plan_number,
        about: user.about,
        tags: user.tags,
        products: user.products,
        subCategories: user.subcategories
      };

      index.saveObject(usr, function (err, content) {
        console.log(content);
      });
    });
  }, 1000);
}
//# sourceMappingURL=algolia.js.map
