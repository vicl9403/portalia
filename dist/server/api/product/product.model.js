'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var User = _mongoose2.default.model('User');

var ProductSchema = new _mongoose2.default.Schema({

    // El nombre del producto
    name: { type: String },

    // El usuario que crea el producto
    user: { type: Schema.ObjectId, ref: "User" },

    // La imágen principal del producto
    mainPicture: { type: String },

    //Las imágenes secundarias del producto
    pictures: { type: Array },

    // Categoria principal
    category: String,

    // Categoría secundaria
    subCategory: String,

    // La descripción es de tipo summernote
    description: { type: String, required: true },

    // Ofertas parciales
    offer: {
        image: {},
        description: {},
        until: { type: Date }
    },

    // La ficha técnica es un archivo
    datasheet: { type: String },

    currency: { type: String, default: 'MXN' },
    price: { type: Number },

    addedValue: { type: String },

    createdAt: { type: Date, default: new Date() },

    active: { type: Boolean, default: true }

});

exports.default = _mongoose2.default.model('Product', ProductSchema);
//# sourceMappingURL=product.model.js.map
