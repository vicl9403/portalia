/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products              ->  index
 * POST    /api/products              ->  create
 * GET     /api/products/:id          ->  show
 * PUT     /api/products/:id          ->  update
 * DELETE  /api/products/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updatePictures = updatePictures;
exports.index = index;
exports.userProducts = userProducts;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _product = require('./product.model');

var _product2 = _interopRequireDefault(_product);

var _algolia = require('./algolia');

var algoliaSearch = _interopRequireWildcard(_algolia);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {

      algoliaSearch.storeIndex(entity);
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    if (entity) {
      var updated = _lodash2.default.merge(entity, updates);
      console.log(updates.pictures);
      // Actializando el arreglo de las imágenes
      updated.pictures = updates.pictures;

      return updated.save().then(function (updated) {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function updatePictures(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  _product2.default.findById(req.params.id).exec().then(function (entity) {
    entity.pictures = req.body;
    entity.save();
    return res.send(200, entity);
  });
}

// Gets a list of Products
function index(req, res) {
  return _product2.default.find().exec().then(respondWithResult(res));
}

function userProducts(req, res) {
  return _product2.default.find({
    "user": req.params.id
  }).exec().then(respondWithResult(res));
}

// Gets a single Product from the DB
function show(req, res) {
  return _product2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res));
}

// Creates a new Product in the DB
function create(req, res) {
  return _product2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Product in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _product2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res));
}

// Deletes a Product from the DB
function destroy(req, res) {
  return _product2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res));
}
//# sourceMappingURL=product.controller.js.map
