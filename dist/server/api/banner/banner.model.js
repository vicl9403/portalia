'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var User = _mongoose2.default.model('User');

var BannerSchema = new _mongoose2.default.Schema({

  since: { type: Date, required: true },
  until: { type: Date, required: true },

  tags: { type: Array, required: true },

  img1: { type: String },
  img2: { type: String },

  user: { type: Schema.ObjectId, ref: "User" },

  status: { type: String },

  price: { type: Number },

  active: Boolean

});

exports.default = _mongoose2.default.model('Banner', BannerSchema);
//# sourceMappingURL=banner.model.js.map
