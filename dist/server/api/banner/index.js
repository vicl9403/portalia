'use strict';

var express = require('express');
var controller = require('./banner.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.get('/user/:id', controller.getUserBanners);
router.get('/findKeyword/:keyword', controller.findKeyword);
router.get('/status/:status', controller.getBannersByStatus);

module.exports = router;
//# sourceMappingURL=index.js.map
