/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/banners              ->  index
 * POST    /api/banners              ->  create
 * GET     /api/banners/:id          ->  show
 * PUT     /api/banners/:id          ->  update
 * DELETE  /api/banners/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.findKeyword = findKeyword;
exports.getUserBanners = getUserBanners;
exports.getBannersByStatus = getBannersByStatus;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _banner = require('./banner.model');

var _banner2 = _interopRequireDefault(_banner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function saveUpdates(updates) {
  return function (entity) {
    if (entity) {
      var updated = _lodash2.default.merge(entity, updates);
      return updated.save().then(function (updated) {
        return updated;
      });
    }
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Banners
function index(req, res) {
  return _banner2.default.find().exec().then(respondWithResult(res));
}

// Gets a single Banner from the DB
function show(req, res) {
  return _banner2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res));
}

// Creates a new Banner in the DB
function create(req, res) {
  return _banner2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing Banner in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _banner2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res));
}

// Deletes a Banner from the DB
function destroy(req, res) {
  return _banner2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res));
}

function findKeyword(req, res) {
  var keyword = req.params.keyword;

  _banner2.default.find({
    $and: [{ $text: { $search: keyword } }, { "status": { $ne: 'deleted' } }]
  }, { score: { $meta: "textScore" } }).sort({ score: { $meta: 'textScore' } }).exec(function (err, results) {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).send(results);
  });
}

function getUserBanners(req, res) {
  _banner2.default.find({
    $and: [{ "status": { $ne: 'deleted' } }, { user: req.params.id }]
  }).sort('-_id').exec(function (err, banners) {
    if (err) {
      res.status(500).send(err);
    }

    res.status(200).send(banners);
  });
}

function getBannersByStatus(req, res) {

  var page = req.query.page;
  var limitPerpage = 10;

  _banner2.default.find({
    "status": req.params.status
  }).populate('user', '-password -salt').limit(limitPerpage).skip(page * limitPerpage).exec(function (err, banners) {

    if (err) {
      res.status(402).send(err);
    }

    _banner2.default.count({
      "status": req.params.status
    }).exec(function (err, count) {

      if (err) {
        res.status(402).send(err);
      }

      var data = {
        count: count,
        items: banners
      };

      res.status(200).send(data);
    });
  });
}
//# sourceMappingURL=banner.controller.js.map
