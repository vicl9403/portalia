'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.create = create;
exports.imagenEvento = imagenEvento;
exports.products = products;
exports.videos = videos;
exports.storeCatalog = storeCatalog;
exports.storeProductDatasheet = storeProductDatasheet;
exports.info = info;
exports.uploadImage = uploadImage;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

var _plan = require('../plan/plan.model');

var _plan2 = _interopRequireDefault(_plan);

var _product = require('../product/product.model');

var _product2 = _interopRequireDefault(_product);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mkdirp = require('mkdirp');
var P = require('bluebird');
var util = require('util');
var fs = require('fs');

var globalPlan;
_plan2.default.findById("57acf9c0b849b848922c8ef9").exec().then(function (plan) {
  globalPlan = plan;
});

// Extensiones generales para la subida de cualquier archivo
var allowedExtensions = ['txt', 'csv', 'pdf', 'xls', 'xlsx', 'doc', 'docx', 'ppt', 'pptx', 'jpg', 'jpeg', 'png', 'gif', 'svg'];

function create(req, res, next) {

  console.log(req.params.sector);

  mkdirp('./client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id, function (err) {
    if (err) {
      if (err.code === 'EEXIST') {
        uploadFile();
      } else {
        res.sendStatus(500);
        return;
      } // something else went wrong
    } else {
      uploadFile();
    } // successfully created folder
  });

  var path = './client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id;
  var storage = _multer2.default.diskStorage({ //multers disk storage settings
    destination: function destination(req, file, cb) {
      cb(null, path);
    },
    filename: function filename(req, file, cb) {
      var datetimestamp = Date.now();
      //cb(null, "foto-perfil" + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
      cb(null, "foto-perfil.jpg");
    }
  });

  var upload = (0, _multer2.default)({
    limits: {
      fileSize: 1024000000,
      files: 1
    },
    storage: storage
  }).single('file');

  var uploadFile = function uploadFile() {

    upload(req, res, function (err) {
      if (err) {

        res.json({
          errorCode: 1,
          errDesc: err
        });
        return;
      }
      console.log('test');
      res.json({
        errorCode: 0,
        errDesc: null
      });
    });
  };
}

function imagenEvento(req, res, next) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }console.log(req.params);

  mkdirp('./client/assets/uploads/eventos', function (err) {
    if (err) {
      if (err.code === 'EEXIST') {
        uploadFile();
      } else {
        res.sendStatus(500);
        return;
      } // something else went wrong
    } else {
      uploadFile();
    } // successfully created folder
  });

  var path = './client/assets/uploads/eventos';
  var storage = _multer2.default.diskStorage({ //multers disk storage settings
    destination: function destination(req, file, cb) {
      cb(null, path);
    },
    filename: function filename(req, file, cb) {
      var datetimestamp = Date.now();
      //cb(null, "foto-perfil" + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
      cb(null, "evento" + text + ".jpg");
    }
  });

  var upload = (0, _multer2.default)({
    limits: {
      fileSize: 1024000000,
      files: 1
    },
    storage: storage
  }).single('file');

  var uploadFile = function uploadFile() {
    upload(req, res, function (err) {
      if (err) {
        res.json({
          errorCode: 1,
          errDesc: err
        });
        return;
      }
      res.json({
        errorCode: 0,
        errDesc: null,
        nombre: "evento" + text + ".jpg"
      });
    });
  };
}

function products(req, res) {

  console.log('uploading');
  _user2.default.findById(req.params.id).exec().then(function (user) {

    if (user.paquete == "gratis" && user.productsImg.length >= 5) {
      res.sendStatus(500);
      return;
    }
    if (user.paquete == "select" && user.productsImg.length >= 15) {
      res.sendStatus(500);
      return;
    }

    mkdirp('./client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos', function (err) {
      if (err) {
        if (err.code === 'EEXIST') {
          uploadFile();
        } else {
          res.sendStatus(500);
          return;
        } // something else went wrong
      } else {
        uploadFile();
      } // successfully created folder
    });

    var fileName = "";
    var path = './client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos';
    var storage = _multer2.default.diskStorage({ //multers disk storage settings
      destination: function destination(req, file, cb) {
        cb(null, path);
      },
      filename: function filename(req, file, cb) {
        var datetimestamp = Date.now();
        fileName = "image_" + req.params.name + "_" + datetimestamp + ".jpg";
        //cb(null, "foto-perfil" + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
        cb(null, fileName);
      }
    });

    var upload = (0, _multer2.default)({
      storage: storage
    }).single('file');

    var uploadFile = function uploadFile() {
      upload(req, res, function (err) {
        if (err) {
          res.json({
            errorCode: 1,
            errDesc: err
          });
          return;
        }

        console.log(user.productsImg.length);
        if (user.productsImg.length > 0) {
          console.log("1");
          user.productsImg.push(fileName);
        } else {
          console.log("2");
          user.productsImg = [fileName];
        }
        user.save().then(function () {
          res.json({
            imgName: fileName,
            errorCode: 0,
            errDesc: null
          });
        });
      });
    };
  });
}

function videos(req, res) {
  _user2.default.findById(req.params.id).exec().then(function (user) {

    //TODO Error custom para .... ahorita nos quieren chingar si llegana este error
    //globalPlan.beneficios[5].paquete[0].name 6 es el index de Videos en el array de beneficios y 0 es gratis o 1 plus o 2 select ya solo cambio name o limite;
    if (user.paquete == "gratis" && user.videos.length >= globalPlan.beneficios[6].paquete[0].limite) {
      res.sendStatus(500);
      return;
    }
    if (user.paquete == globalPlan.beneficios[6].paquete[1].name.toLowerCase() && user.videos.length >= parseInt(globalPlan.beneficios[6].paquete[1].limite)) {
      res.sendStatus(500);
      return;
    }
    if (user.paquete == globalPlan.beneficios[6].paquete[2].name.toLowerCase() && user.videos.length >= parseInt(globalPlan.beneficios[6].paquete[2].limite)) {
      res.sendStatus(500);
      return;
    }

    mkdirp('./client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos', function (err) {
      if (err) {
        if (err.code === 'EEXIST') {
          uploadFile();
        } else {
          res.sendStatus(500);
          return;
        } // something else went wrong
      } else {
        uploadFile();
      } // successfully created folder
    });

    var fileName = "";
    var path = './client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos';
    var storage = _multer2.default.diskStorage({ //multers disk storage settings
      destination: function destination(req, file, cb) {
        cb(null, path);
      },
      filename: function filename(req, file, cb) {
        var datetimestamp = Date.now();
        fileName = "video_" + req.params.name + "_" + datetimestamp + "." + file.originalname.split('.')[file.originalname.split('.').length - 1];;
        //cb(null, "foto-perfil" + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
        cb(null, fileName);
      }
    });

    var upload = (0, _multer2.default)({
      storage: storage
    }).single('file');

    var uploadFile = function uploadFile() {
      upload(req, res, function (err) {
        if (err) {
          res.json({
            errorCode: 1,
            errDesc: err
          });
          return;
        }

        console.log(user.videos.length);
        if (user.videos.length > 0) {
          console.log("1");
          user.videos.push(fileName);
        } else {
          console.log("2");
          user.videos = [fileName];
        }
        user.save().then(function () {
          res.json({
            vidName: fileName,
            errorCode: 0,
            errDesc: null
          });
        });
      });
    };
  });
}

function storeCatalog(req, res) {

  // var fileName = "";
  //
  // let newFileName = '';
  //
  // var path = './client/assets/uploads/catalogos/' + req.params.id ;
  //
  // let storageCatalog = multer.diskStorage({ //multers disk storage settings
  //   destination: function(req, file, cb) {
  //     cb(null, path)
  //   },
  //   filename: function(req, file, cb) {
  //     console.log('im here');
  //     let originalName = file.originalname.split('.');
  //     let extension = originalName[ originalName.length - 1 ] ;
  //     console.log(extension);
  //
  //     if ( allowedExtensions.indexOf( extension ) == -1 )
  //       return res.send(500);
  //
  //     newFileName = 'catalog_' + new Date() + "." + extension ;
  //
  //     cb(null, newFileName);
  //
  //     if( user.productsCatalog != undefined )
  //       user.productsCatalog.push( {
  //         url : `assets/uploads/catalogos/${ user._id }/${ newFileName }`,
  //         name : req.params.filename
  //       });
  //     else
  //       user.productsCatalog = [ newFileName ];
  //
  //
  //     user.save();
  //
  //     res.status(200).send( { filename : newFileName} );
  //   }
  // });
  //
  // User.findById(req.params.id).exec()
  //   .then( function ( user ) {
  //     // Ejemplo de validación
  //     // if (user.paquete == "gratis" && user.productsImg.length >= 5 ) {
  //     //   res.sendStatus(500);
  //     //   return;
  //     // }
  //
  //     if ( user.productsCatalog.length > 5 ) {
  //       return res.sendStatus(500);
  //     }
  //
  //     mkdirp('./client/assets/uploads/catalogos/' + req.params.id , function(err) {
  //       if (err) {
  //         if (err.code === 'EEXIST') {
  //           uploadCatalog();
  //         }
  //         else {
  //           res.sendStatus(500);
  //           return;
  //         } // something else went wrong
  //       }
  //       else
  //       {
  //         uploadCatalog();
  //       } // successfully created folder
  //     });
  //
  //
  //
  //     let upload = multer({
  //       storage: storageCatalog
  //     }).single('file');
  //
  //     let uploadCatalog = function() {
  //       upload(req, res, function(err) {
  //         if (err) {
  //           res.json({
  //             errorCode: 1,
  //             errDesc: err
  //           });
  //           return;
  //         }
  //
  //
  //       })
  //     };
  //
  //
  //   });
}

function storeProductDatasheet(req, res) {

  // Product.findById(req.params.id).exec()
  //   .then( function ( product ) {
  //     // Ejemplo de validación
  //     // if (user.paquete == "gratis" && user.productsImg.length >= 5 ) {
  //     //   res.sendStatus(500);
  //     //   return;
  //     // }
  //
  //     // let path = req.body.path;
  //     let path = './client/assets/uploads/productos/' + product._id + '/' + product.name + '/hoja-de-datos';
  //
  //     let newFileName = req.body.name;
  //
  //     mkdirp( path , function(err) {
  //       if (err)
  //       {
  //         if (err.code === 'EEXIST') {
  //           uploadFile();
  //         }
  //         else {
  //           res.sendStatus(500);
  //           return;
  //         } // something else went wrong
  //       }
  //       else
  //       {
  //         uploadFile();
  //       } // successfully created folder
  //     });
  //
  //     var fileName = "";
  //
  //
  //     var storage = multer.diskStorage({ //multers disk storage settings
  //       destination: function(req, file, cb) {
  //         cb(null, path)
  //       },
  //       filename: function(req, file, cb) {
  //
  //         let originalName = file.originalname.split('.');
  //         let extension = originalName[ originalName.length - 1 ] ;
  //         console.log(extension);
  //
  //         if ( allowedExtensions.indexOf( extension ) == -1 )
  //           return res.send(500);
  //
  //         newFileName = newFileName + "." + extension ;
  //
  //         cb(null, newFileName);
  //
  //         if( user.productsCatalog != undefined )
  //           user.productsCatalog.push( {
  //             url : `assets/uploads/catalogos/${ user._id }/${ newFileName }`,
  //             name : req.params.filename
  //           });
  //         else
  //           user.productsCatalog = [ newFileName ];
  //
  //         user.save();
  //
  //         res.status(200).send( { filename : newFileName} );
  //       }
  //     });
  //
  //     var upload = multer({
  //       storage: storage
  //     }).single('file');
  //
  //     var uploadFile = function() {
  //       upload(req, res, function(err) {
  //         if (err) {
  //           res.json({
  //             errorCode: 1,
  //             errDesc: err
  //           });
  //           return;
  //         }
  //
  //       })
  //     };
  //
  //
  //   });

}

function uploadSingleFile(path, fileName) {}

function info(req, res) {
  _user2.default.findById(req.params.id).exec().then(function (user) {

    mkdirp('./client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos', function (err) {
      if (err) {
        if (err.code === 'EEXIST') {
          uploadFile();
        } else {
          res.sendStatus(500);
          return;
        } // something else went wrong
      } else {
        uploadFile();
      } // successfully created folder
    });

    var fileName = "";
    var path = './client/assets/uploads/' + req.params.sector + '/' + req.params.categoria + '/' + req.params.id + '/productos';
    var storage = _multer2.default.diskStorage({ //multers disk storage settings
      destination: function destination(req, file, cb) {
        cb(null, path);
      },
      filename: function filename(req, file, cb) {
        var datetimestamp = Date.now();
        fileName = "Archivo." + file.originalname.split('.')[file.originalname.split('.').length - 1];
        //cb(null, "foto-perfil" + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
        cb(null, fileName);
      }
    });

    var upload = (0, _multer2.default)({
      storage: storage
    }).single('file');

    var uploadFile = function uploadFile() {
      upload(req, res, function (err) {
        if (err) {
          res.json({
            errorCode: 1,
            errDesc: err
          });
          return;
        }
        user.archivo = [fileName];

        user.save().then(function () {
          res.json({
            archivo: fileName,
            errorCode: 0,
            errDesc: null
          });
        });
      });
    };
  });
}

function uploadImage(req, res) {
  var fs = require('fs');

  if (!req.files) return res.status(400).send('No hay archivos.');

  var allowedExtensions = ['.jpg', '.jpeg', '.png', '.gif'];

  // Preparar arreglo con todos los archivos
  var files = [];
  var fileKeys = Object.keys(req.files);

  // Obtener todos los archivos del request y ponerlos en un array
  fileKeys.forEach(function (key) {
    files.push(req.files[key]);
  });

  // Obtener el path del guardado del request
  var completePath = './client/assets/uploads/' + req.body.path;

  // Toto este bloque de aquí es para crear los directorios en caso de que no se hayan creado
  // anteriormente, recorre todo el path y va creando las carpetas en caso que no existan
  var path = req.body.path;
  var pathSplitted = path.split('/');
  var partial = './client/assets/uploads';

  // Este es el ciclo que hace lo descrito en los comentarios anteriores
  for (var i = 0; i < pathSplitted.length; i++) {

    partial += '/' + pathSplitted[i];

    if (!fs.existsSync(partial)) {
      fs.mkdirSync(partial);
      console.log('test');
    }
  }

  var name = req.body.name;

  // Iterar sobre todos los archivos
  files.forEach(function (file) {

    var fileExtension = file.name.split('.').pop();

    if (allowedExtensions.indexOf(fileExtension)) {

      var uniqueName = '';

      if (name == undefined) {
        var aux = new Date().getTime().toString() + '-' + file.name;
        uniqueName = completePath + aux;
        path = path + aux;
      } else {
        uniqueName = completePath + name + '.' + fileExtension;
        path += name + '.' + fileExtension;
      }

      file.mv(uniqueName, function (err) {
        if (err) {
          console.log(err);
          return res.status(500);
        } else {
          return res.status(200).send(uniqueName.replace('./client', ''));
        }
      });
    }
  });

  return res.status(200);
}
//# sourceMappingURL=upload.controller.js.map
