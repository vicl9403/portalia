'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CatalogoCamposFormularioSchema = new _mongoose2.default.Schema({
  nombre: String,
  tipo: String,
  formularioID: Number,
  tooltip: String,
  proyecto: Number,
  orden: Number,
  extra: {}
});

exports.default = _mongoose2.default.model('CatalogoCamposFormulario', CatalogoCamposFormularioSchema);
//# sourceMappingURL=catalogo_campos_formulario.model.js.map
