'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var catalogoCamposFormularioCtrlStub = {
  index: 'catalogoCamposFormularioCtrl.index',
  show: 'catalogoCamposFormularioCtrl.show',
  create: 'catalogoCamposFormularioCtrl.create',
  update: 'catalogoCamposFormularioCtrl.update',
  destroy: 'catalogoCamposFormularioCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var catalogoCamposFormularioIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './catalogo_campos_formulario.controller': catalogoCamposFormularioCtrlStub
});

describe('CatalogoCamposFormulario API Router:', function () {

  it('should return an express router instance', function () {
    catalogoCamposFormularioIndex.should.equal(routerStub);
  });

  describe('GET /api/catalogo_campos_formularios', function () {

    it('should route to catalogoCamposFormulario.controller.index', function () {
      routerStub.get.withArgs('/', 'catalogoCamposFormularioCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/catalogo_campos_formularios/:id', function () {

    it('should route to catalogoCamposFormulario.controller.show', function () {
      routerStub.get.withArgs('/:id', 'catalogoCamposFormularioCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/catalogo_campos_formularios', function () {

    it('should route to catalogoCamposFormulario.controller.create', function () {
      routerStub.post.withArgs('/', 'catalogoCamposFormularioCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/catalogo_campos_formularios/:id', function () {

    it('should route to catalogoCamposFormulario.controller.update', function () {
      routerStub.put.withArgs('/:id', 'catalogoCamposFormularioCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/catalogo_campos_formularios/:id', function () {

    it('should route to catalogoCamposFormulario.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'catalogoCamposFormularioCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/catalogo_campos_formularios/:id', function () {

    it('should route to catalogoCamposFormulario.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'catalogoCamposFormularioCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
