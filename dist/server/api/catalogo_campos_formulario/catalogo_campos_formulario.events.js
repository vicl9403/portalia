/**
 * CatalogoCamposFormulario model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _catalogo_campos_formulario = require('./catalogo_campos_formulario.model');

var _catalogo_campos_formulario2 = _interopRequireDefault(_catalogo_campos_formulario);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CatalogoCamposFormularioEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
CatalogoCamposFormularioEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _catalogo_campos_formulario2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    CatalogoCamposFormularioEvents.emit(event + ':' + doc._id, doc);
    CatalogoCamposFormularioEvents.emit(event, doc);
  };
}

exports.default = CatalogoCamposFormularioEvents;
//# sourceMappingURL=catalogo_campos_formulario.events.js.map
