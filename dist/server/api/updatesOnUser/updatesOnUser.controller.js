/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/updatesOnUsers              ->  index
 * POST    /api/updatesOnUsers              ->  create
 * GET     /api/updatesOnUsers/:id          ->  show
 * PUT     /api/updatesOnUsers/:id          ->  update
 * DELETE  /api/updatesOnUsers/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.getByDate = getByDate;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _updatesOnUser = require('./updatesOnUser.model');

var _updatesOnUser2 = _interopRequireDefault(_updatesOnUser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of UpdatesOnUsers
function index(req, res) {
  return _updatesOnUser2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single UpdatesOnUser from the DB
function show(req, res) {
  return _updatesOnUser2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Gets
function getByDate(req, res) {

  return _updatesOnUser2.default.find({
    $and: [{
      'date': {
        $gte: req.body.start,
        $lt: req.body.end
      }
    }]
  }).exec().then(function (logs) {
    res.status(200).json(logs);
  });
}

// Creates a new UpdatesOnUser in the DB
function create(req, res) {
  return _updatesOnUser2.default.create(req.body).then(respondWithResult(res, 201));
}

// Updates an existing UpdatesOnUser in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _updatesOnUser2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a UpdatesOnUser from the DB
function destroy(req, res) {
  return _updatesOnUser2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=updatesOnUser.controller.js.map
