'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var updatesOnUserCtrlStub = {
  index: 'updatesOnUserCtrl.index',
  show: 'updatesOnUserCtrl.show',
  create: 'updatesOnUserCtrl.create',
  update: 'updatesOnUserCtrl.update',
  destroy: 'updatesOnUserCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var updatesOnUserIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './updatesOnUser.controller': updatesOnUserCtrlStub
});

describe('UpdatesOnUser API Router:', function () {

  it('should return an express router instance', function () {
    updatesOnUserIndex.should.equal(routerStub);
  });

  describe('GET /api/updatesOnUsers', function () {

    it('should route to updatesOnUser.controller.index', function () {
      routerStub.get.withArgs('/', 'updatesOnUserCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/updatesOnUsers/:id', function () {

    it('should route to updatesOnUser.controller.show', function () {
      routerStub.get.withArgs('/:id', 'updatesOnUserCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/updatesOnUsers', function () {

    it('should route to updatesOnUser.controller.create', function () {
      routerStub.post.withArgs('/', 'updatesOnUserCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/updatesOnUsers/:id', function () {

    it('should route to updatesOnUser.controller.update', function () {
      routerStub.put.withArgs('/:id', 'updatesOnUserCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/updatesOnUsers/:id', function () {

    it('should route to updatesOnUser.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'updatesOnUserCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/updatesOnUsers/:id', function () {

    it('should route to updatesOnUser.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'updatesOnUserCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
