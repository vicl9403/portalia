/**
 * UpdatesOnUser model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _updatesOnUser = require('./updatesOnUser.model');

var _updatesOnUser2 = _interopRequireDefault(_updatesOnUser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UpdatesOnUserEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
UpdatesOnUserEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _updatesOnUser2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    UpdatesOnUserEvents.emit(event + ':' + doc._id, doc);
    UpdatesOnUserEvents.emit(event, doc);
  };
}

exports.default = UpdatesOnUserEvents;
//# sourceMappingURL=updatesOnUser.events.js.map
