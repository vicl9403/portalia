'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _user = require('../user/user.model');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// var mongoose = require("mongoose");
var Schema = _mongoose2.default.Schema;

var UpdatesOnUserSchema = new _mongoose2.default.Schema({
	user: { type: Array },
	userUpdated: { type: Array },
	// user: [ { type: Schema.Types.ObjectId, ref: 'User'}],
	// userUpdated: [ { type: Schema.Types.ObjectId, ref: 'User'}],
	type: String,
	date: { type: Date, default: Date.now }
});

exports.default = _mongoose2.default.model('UpdatesOnUser', UpdatesOnUserSchema);
//# sourceMappingURL=updatesOnUser.model.js.map
