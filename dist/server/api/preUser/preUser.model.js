'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PreUserSchema = new _mongoose2.default.Schema({
  active: {
    type: Boolean,
    default: false
  },
  hasPic: {
    type: Boolean,
    default: false
  },
  numeroVisitasGene: Number,
  email: {
    type: String,
    lowercase: true,
    required: function required() {
      if (authTypes.indexOf(this.provider) === -1) {
        return true;
      } else {
        return false;
      }
    }
  },
  emailSec: {
    type: String,
    lowercase: true
  },
  videos: [],
  visitasTel: [],
  visitasGene: [],
  archivo: String,
  role: {
    type: String,
    default: 'user'
  },
  paquete: {
    type: String,
    default: 'gratis'
  },
  password: {
    type: String,
    required: function required() {
      if (authTypes.indexOf(this.provider) === -1) {
        return true;
      } else {
        return false;
      }
    }
  },
  facebook: {},
  tags: {
    type: Array
  },
  products: {
    type: Array
  },
  productsImg: [],
  telefono: String,
  provider: String,
  salt: String,
  nombre: String,
  razon: String,
  rfc: String,
  web: String,
  direccion: String,
  sector: String,
  categoria: String,
  sector_name: String,
  categoria_name: String,
  subCategoria: String,
  otraSub: String,
  estado: String,
  municipio: String,
  codigo_invitacion: String,
  fechaPago: Date,
  fechaCorte: Date,
  about: {
    type: String,
    default: ''
  },
  ventajas: {
    type: String,
    default: ''
  },
  redes: {
    facebook: {
      type: String,
      default: 'http://www.facebook.com/'
    },
    twitter: {
      type: String,
      default: 'http://www.twitter.com/'
    },
    youtube: {
      type: String,
      default: 'http://www.youtube.com/'
    },
    linkedin: {
      type: String,
      default: 'http://www.linkedin.com/'
    },
    googleplus: {
      type: String,
      default: 'http://www.plus.google.com/'
    }
  },

  sucursales: [{
    estadoSucursal: {
      type: String,
      default: 'Placeholder'
    },
    ciudad: {
      type: String,
      default: 'Placeholder'
    },
    colonia: {
      type: String,
      default: 'Placeholder'
    },
    calle: {
      type: String,
      default: 'Placeholder'
    },
    numero: {
      type: String,
      default: 'Placeholder'
    },
    cp: {
      type: String,
      default: 'Placeholder'
    },
    nombreSucu: {
      type: String,
      default: 'Nombre de la Sucursal'
    },
    loc: {
      type: [Number],
      index: '2d'
    },
    latitud: {
      type: String,
      default: 'Placeholder'
    },
    longitud: {
      type: String,
      default: 'Placeholder'
    }

  }],

  favoritos: [],
  articulosGuardados: [],
  suscripcion: {
    activa: {
      type: Boolean,
      default: false
    },
    selecciondas: []
  },
  planSolicitado: {
    plan: String,
    period: Number,
    openPayResponse: {}
  },
  planSolicitadoPricing: {
    plan: String,
    discount: Number,
    period: Number,
    price: Number
  },
  datosDeFacturacion: {
    requested: {
      type: Boolean,
      default: false
    },
    razon: String,
    rfc: String,
    calle: String,
    numeroCalle: String,
    colonia: String,
    codigoPostal: String,
    ciudad: String,
    estado: String,
    correoFactu: String
  }
});

exports.default = _mongoose2.default.model('PreUser', PreUserSchema);
//# sourceMappingURL=preUser.model.js.map
