'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var preUserCtrlStub = {
  index: 'preUserCtrl.index',
  show: 'preUserCtrl.show',
  create: 'preUserCtrl.create',
  update: 'preUserCtrl.update',
  destroy: 'preUserCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var preUserIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './preUser.controller': preUserCtrlStub
});

describe('PreUser API Router:', function () {

  it('should return an express router instance', function () {
    preUserIndex.should.equal(routerStub);
  });

  describe('GET /api/preUsers', function () {

    it('should route to preUser.controller.index', function () {
      routerStub.get.withArgs('/', 'preUserCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/preUsers/:id', function () {

    it('should route to preUser.controller.show', function () {
      routerStub.get.withArgs('/:id', 'preUserCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/preUsers', function () {

    it('should route to preUser.controller.create', function () {
      routerStub.post.withArgs('/', 'preUserCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/preUsers/:id', function () {

    it('should route to preUser.controller.update', function () {
      routerStub.put.withArgs('/:id', 'preUserCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/preUsers/:id', function () {

    it('should route to preUser.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'preUserCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/preUsers/:id', function () {

    it('should route to preUser.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'preUserCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
