'use strict';

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('../..');


var newPreUser;

describe('PreUser API:', function () {

  describe('GET /api/preUsers', function () {
    var preUsers;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/preUsers').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        preUsers = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      preUsers.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/preUsers', function () {
    beforeEach(function (done) {
      (0, _supertest2.default)(app).post('/api/preUsers').send({
        name: 'New PreUser',
        info: 'This is the brand new preUser!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newPreUser = res.body;
        done();
      });
    });

    it('should respond with the newly created preUser', function () {
      newPreUser.name.should.equal('New PreUser');
      newPreUser.info.should.equal('This is the brand new preUser!!!');
    });
  });

  describe('GET /api/preUsers/:id', function () {
    var preUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).get('/api/preUsers/' + newPreUser._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        preUser = res.body;
        done();
      });
    });

    afterEach(function () {
      preUser = {};
    });

    it('should respond with the requested preUser', function () {
      preUser.name.should.equal('New PreUser');
      preUser.info.should.equal('This is the brand new preUser!!!');
    });
  });

  describe('PUT /api/preUsers/:id', function () {
    var updatedPreUser;

    beforeEach(function (done) {
      (0, _supertest2.default)(app).put('/api/preUsers/' + newPreUser._id).send({
        name: 'Updated PreUser',
        info: 'This is the updated preUser!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedPreUser = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedPreUser = {};
    });

    it('should respond with the updated preUser', function () {
      updatedPreUser.name.should.equal('Updated PreUser');
      updatedPreUser.info.should.equal('This is the updated preUser!!!');
    });
  });

  describe('DELETE /api/preUsers/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2.default)(app).delete('/api/preUsers/' + newPreUser._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when preUser does not exist', function (done) {
      (0, _supertest2.default)(app).delete('/api/preUsers/' + newPreUser._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=preUser.integration.js.map
