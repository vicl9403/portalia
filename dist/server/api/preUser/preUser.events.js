/**
 * PreUser model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _preUser = require('./preUser.model');

var _preUser2 = _interopRequireDefault(_preUser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PreUserEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
PreUserEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _preUser2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    PreUserEvents.emit(event + ':' + doc._id, doc);
    PreUserEvents.emit(event, doc);
  };
}

exports.default = PreUserEvents;
//# sourceMappingURL=preUser.events.js.map
