'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var catalogoFormularioCtrlStub = {
  index: 'catalogoFormularioCtrl.index',
  show: 'catalogoFormularioCtrl.show',
  create: 'catalogoFormularioCtrl.create',
  update: 'catalogoFormularioCtrl.update',
  destroy: 'catalogoFormularioCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var catalogoFormularioIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './catalogo_formulario.controller': catalogoFormularioCtrlStub
});

describe('CatalogoFormulario API Router:', function () {

  it('should return an express router instance', function () {
    catalogoFormularioIndex.should.equal(routerStub);
  });

  describe('GET /api/catalogo_formularios', function () {

    it('should route to catalogoFormulario.controller.index', function () {
      routerStub.get.withArgs('/', 'catalogoFormularioCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/catalogo_formularios/:id', function () {

    it('should route to catalogoFormulario.controller.show', function () {
      routerStub.get.withArgs('/:id', 'catalogoFormularioCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/catalogo_formularios', function () {

    it('should route to catalogoFormulario.controller.create', function () {
      routerStub.post.withArgs('/', 'catalogoFormularioCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/catalogo_formularios/:id', function () {

    it('should route to catalogoFormulario.controller.update', function () {
      routerStub.put.withArgs('/:id', 'catalogoFormularioCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/catalogo_formularios/:id', function () {

    it('should route to catalogoFormulario.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'catalogoFormularioCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/catalogo_formularios/:id', function () {

    it('should route to catalogoFormulario.controller.destroy', function () {
      routerStub.delete.withArgs('/:id', 'catalogoFormularioCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
