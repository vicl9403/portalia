'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CatalogoFormularioSchema = new _mongoose2.default.Schema({
    formularioID: Number,
    data: {}
});

exports.default = _mongoose2.default.model('CatalogoFormulario', CatalogoFormularioSchema);
//# sourceMappingURL=catalogo_formulario.model.js.map
