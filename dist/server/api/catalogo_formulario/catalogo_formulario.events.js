/**
 * CatalogoFormulario model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _catalogo_formulario = require('./catalogo_formulario.model');

var _catalogo_formulario2 = _interopRequireDefault(_catalogo_formulario);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CatalogoFormularioEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
CatalogoFormularioEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _catalogo_formulario2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    CatalogoFormularioEvents.emit(event + ':' + doc._id, doc);
    CatalogoFormularioEvents.emit(event, doc);
  };
}

exports.default = CatalogoFormularioEvents;
//# sourceMappingURL=catalogo_formulario.events.js.map
