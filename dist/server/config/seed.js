/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var _user = require('../api/user/user.model');

var _user2 = _interopRequireDefault(_user);

var _sector = require('../api/sector/sector.model');

var _sector2 = _interopRequireDefault(_sector);

var _chat = require('../api/chat/chat.model');

var _chat2 = _interopRequireDefault(_chat);

var _estado = require('../api/estado/estado.model');

var _estado2 = _interopRequireDefault(_estado);

var _plan = require('../api/plan/plan.model');

var _plan2 = _interopRequireDefault(_plan);

var _updatesOnUser = require('../api/updatesOnUser/updatesOnUser.model');

var _updatesOnUser2 = _interopRequireDefault(_updatesOnUser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=seed.js.map
