'use strict';

// Test specific configuration
// ===========================

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://AdminPruebas:portaliapruebas@184.171.253.98:27017/PortaliaPruebas'
  },
  port: 9001,
  sequelize: {
    uri: 'sqlite://',
    options: {
      logging: false,
      storage: 'test.sqlite',
      define: {
        timestamps: false
      }
    }
  },
  plans: {
    Semestral: {
      Mensual: {
        ID: 'pvbbr2dmj2moiamviqdc',
        plan: 'select'
      },
      Trimestral: {
        ID: 'plefknak4okyefqyrxjj',
        plan: 'select'
      }
    },
    Anual: {
      Mensual: {
        ID: 'p9ihit3hf5fbx35wg118',
        plan: 'select'
      },
      Trimestral: {
        ID: 'pzobdltv9frgknwebi1s',
        plan: 'select'
      },
      Semestral: {
        ID: 'pkibi8w7f3iktgsomrzl',
        plan: 'select'
      }
    }
  },
  auth: {
    openpay: _defineProperty({
      ID: process.env.OPENPAY_ID || 'my1qymxtirofw0vduzb4',
      KEY: process.env.OPENPAY_KEY || 'sk_167b1a05bea74b30972782c02adbe275',
      URL: process.env.OPENPAY_URL || 'https://sandbox-dashboard.openpay.mx'
    }, 'URL', process.env.PRODUCTION || false)
  }
};
//# sourceMappingURL=test.js.map
