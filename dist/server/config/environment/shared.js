'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  mode: 'portalia',
  //Production
  // OPENPAY_ID: 'mkx9erv2qm0efa14gvvz',
  // OPENPAY_PUBLICKEY: 'pk_b8c80547e64b415c80287b5ff935d19f',
  // Debug
  OPENPAY_ID: 'my1qymxtirofw0vduzb4',
  OPENPAY_PUBLICKEY: 'pk_b791f0a43df74360b316021350961f63'
};
//# sourceMappingURL=shared.js.map
