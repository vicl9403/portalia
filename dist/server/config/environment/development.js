'use strict';

// Development specific configuration
// ==================================

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/portalia-dev'
  },
  port: 9000,

  // Seed database on startup
  seedDB: false,

  algoliaconfig: {
    idApplication: 'TLTIO1USTU',
    adminKey: '383b53cb6c274ecc37cb61eab6ce1a04',
    searchOnlyKey: 'ba114bfd085d193af38c6017937cfb37'
  },

  plans: {
    Semestral: {
      Mensual: {
        ID: 'pvbbr2dmj2moiamviqdc',
        plan: 'select'
      },
      Trimestral: {
        ID: 'plefknak4okyefqyrxjj',
        plan: 'select'
      }
    },
    Anual: {
      Mensual: {
        ID: 'p9ihit3hf5fbx35wg118',
        plan: 'select'
      },
      Trimestral: {
        ID: 'pzobdltv9frgknwebi1s',
        plan: 'select'
      },
      Semestral: {
        ID: 'pkibi8w7f3iktgsomrzl',
        plan: 'select'
      }
    }
  },

  auth: {
    paypal: {
      ID: process.env.PAYPAL_ID || '',
      KEY: process.env.PAYPAL_KEY || '',
      URL: process.env.PAYPAL_URL || ''
    },
    openpay: _defineProperty({
      ID: process.env.OPENPAY_ID || 'my1qymxtirofw0vduzb4',
      KEY: process.env.OPENPAY_KEY || 'sk_167b1a05bea74b30972782c02adbe275',
      URL: process.env.OPENPAY_URL || 'https://sandbox-dashboard.openpay.mx'
    }, 'URL', process.env.PRODUCTION || false)
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || '542917605891843',
    clientSecret: process.env.FACEBOOK_SECRET || '40ed0b2242aa142edaf8d1c157758ef5',
    callbackURL: 'http://portalia.it4pymes.mx:9000/auth/facebook/callback'
  }
};
//# sourceMappingURL=development.js.map
